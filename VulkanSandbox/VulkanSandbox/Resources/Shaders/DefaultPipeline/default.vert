#version 450

#pragma shader_stage(vertex)

#extension GL_ARB_separate_shader_objects : enable

#include "Engine/GlobalUniforms.glsl"

layout(set = 2, binding = 0) uniform Object
{
	mat4 model;
} object;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inUV;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec2 fragUV;

void main()
{
	gl_Position = global.projection * global.view * object.model * vec4(inPosition, 1.0);
	fragColor = inColor;
	fragUV = inUV;
}