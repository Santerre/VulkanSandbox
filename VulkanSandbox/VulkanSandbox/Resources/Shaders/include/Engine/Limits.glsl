#ifndef LIMITS_GLSL
#define LIMITS_GLSL

#ifdef KU_MAX_TASK_OUTPUT
const uint KU_Limits_MaxTaskOutput = KU_MAX_TASK_OUTPUT;
#else
const uint KU_Limits_MaxTaskOutput = 65000;
#endif

#ifdef KU_MAX_TASK_GROUP_SIZE
const uint KU_Limits_MaxTaskGroupSize = KU_MAX_TASK_GROUP_SIZE;
#else
const uint KU_Limits_MaxTaskGroupSize = 32;
#endif

#ifdef KU_MAX_MESH_TESSELLATION
const uint KU_Limits_MaxMeshTessellation = KU_MAX_MESH_TESSELLATION;
#else
// The hightest value smaller than the prefered max vertices count
const uint KU_Limits_MaxMeshTessellation = uint(sqrt(64)) - 1; // = 7
#endif

#ifdef KU_MAX_MESH_GROUPSIZE
const uint KU_Limits_MaxMeshGroupSize = KU_MAX_MESH_GROUPSIZE;
#else
const uint KU_Limits_MaxMeshGroupSize = 32;
#endif

#endif //LIMITS_GLSL