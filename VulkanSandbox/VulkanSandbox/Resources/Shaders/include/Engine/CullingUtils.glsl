#ifndef CULLING_UTILS_GLSL
#define CULLING_UTILS_GLSL

vec4 getBoxCorner(vec3 bboxMin, vec3 bboxMax, int n)
{
  switch(n){
  case 0:
    return vec4(bboxMin.x,bboxMin.y,bboxMin.z,1);
  case 1:
    return vec4(bboxMax.x,bboxMin.y,bboxMin.z,1);
  case 2:
    return vec4(bboxMin.x,bboxMax.y,bboxMin.z,1);
  case 3:
    return vec4(bboxMax.x,bboxMax.y,bboxMin.z,1);
  case 4:
    return vec4(bboxMin.x,bboxMin.y,bboxMax.z,1);
  case 5:
    return vec4(bboxMax.x,bboxMin.y,bboxMax.z,1);
  case 6:
    return vec4(bboxMin.x,bboxMax.y,bboxMax.z,1);
  case 7:
    return vec4(bboxMax.x,bboxMax.y,bboxMax.z,1);
  }
}

bool isCulled(vec4 pos)
{
  if (pos.x < -pos.w) return true;
  if (pos.x >  pos.w) return true;
  if (pos.y < -pos.w) return true;
  if (pos.y >  pos.w) return true;
  if (pos.z < -pos.w) return true;
  if (pos.z >  pos.w) return true;
  if (pos.w <= 0)	  return true; 
  return false;
}

uint getCullBits(vec4 hPos)
{
  uint cullBits = 0;
  cullBits |= hPos.x < -hPos.w ?  1 : 0;
  cullBits |= hPos.x >  hPos.w ?  2 : 0;
  cullBits |= hPos.y < -hPos.w ?  4 : 0;
  cullBits |= hPos.y >  hPos.w ?  8 : 0;
  cullBits |= hPos.z <  0      ? 16 : 0;
  cullBits |= hPos.z >  hPos.w ? 32 : 0;
  cullBits |= hPos.w <= 0      ? 64 : 0; 
  return cullBits;
}

//bool isAABBCulled(vec3 min, vec3 max, mat4 viewProj)
//{
//   for (int n = 0; n < 8; n++)
//   {
//       vec4 corner = viewProj * getBoxCorner(vec3 bboxMin, vec3 bboxMax, int n);
//       if (!isCulled(corner))
//          return false;
//   }
//   return true;
//}

#endif //CULLING_UTILS_GLSL