#ifndef DEBUG_UTILS_GLSL
#define DEBUG_UTILS_GLSL

float createLag(int count)
{
	float res = 0.0001;
	for (int i = 0; i < count; i++)
		res *= 0.0001;
	return res;
}

#endif //DEBUG_UTILS_GLSL