#ifndef ENGINE_DEFINITIONS_GLSL
#define ENGINE_DEFINITIONS_GLSL

layout(set = 0, binding = 0) uniform GlobalUniform
{
	mat4 view;
	mat4 projection;

	mat4 cullView;
} global;

layout(set = 0, binding = 1) uniform GlobalCamUniform
{
	vec2 camExtents;
	vec3 eye;
} camGlobal;

struct AmbientLight
{
	vec3	color;
	float	intensity;
}; 

struct DirectionalLight
{
	vec3	color;
	float	intensity;
	vec3	direction;
};

layout(set = 0, binding = 2) uniform GlobalLightUniform
{
	AmbientLight ambient;

	DirectionalLight directional;
} light;

#endif //ENGINE_DEFINITIONS_GLSL