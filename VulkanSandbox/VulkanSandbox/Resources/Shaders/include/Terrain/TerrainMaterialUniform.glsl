#ifndef TERRAIN_MATERIAL_UNIFORM_GLSL
#define TERRAIN_MATERIAL_UNIFORM_GLSL

layout(set = 1, binding = 4) uniform Mat
{
	float normUpValue;
	float slopeUpCos;
	float slopeDownCos;
	
	float upDownBlend;
	
	float cliffUpTexBlend;
	float cliffDownTexBlend;

	float tessTargetScreenSize;

	float fogDistance;

	vec2 tiling;
	vec2 heightMapTiling;

	vec3 fogColor;

    float forceTessLevel;
    float forceTaskTessLevel;
    float forceMeshTessLevel;
} mat;

#endif //TERRAIN_MATERIAL_UNIFORM_GLSL