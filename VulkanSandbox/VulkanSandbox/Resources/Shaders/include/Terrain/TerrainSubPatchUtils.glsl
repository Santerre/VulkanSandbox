#ifndef TERRAIN_SUBPATCH_UTILS_GLSL
#define TERRAIN_SUBPATCH_UTILS_GLSL

#ifdef KU_SUBPATCH_TESSELLATION
const uint g_subPatchTessellationFactor = KU_SUBPATCH_TESSELLATION;
#else
const uint g_subPatchTessellationFactor = 5;
#endif
const uint g_subPatchTotal = g_subPatchTessellationFactor * g_subPatchTessellationFactor;

#endif //TERRAIN_MATERIAL_UNIFORM_GLSL