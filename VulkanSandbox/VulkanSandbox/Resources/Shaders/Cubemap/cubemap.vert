#version 450

#pragma shader_stage(vertex)

#extension GL_ARB_separate_shader_objects : enable

#include "Engine/GlobalUniforms.glsl"

layout(location = 0) in vec3 inPosition;

layout(location = 0) out vec3 fragUV;

void main()
{
	vec3 position = mat3(global.view) * inPosition;
	gl_Position = (global.projection * vec4(position, 0.0)).xyzz;

	fragUV = inPosition;
}