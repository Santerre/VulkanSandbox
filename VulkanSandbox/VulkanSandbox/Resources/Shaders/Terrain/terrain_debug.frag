#version 450

#pragma shader_stage(fragment)

#extension GL_ARB_separate_shader_objects : enable

#include "Engine/GlobalUniforms.glsl"
#include "Terrain/TerrainMaterialUniform.glsl"

layout(location = 0) in vec4 color;
layout(location = 1) in vec2 fragUV;
layout(location = 2) in float fragHeight;
layout(location = 3) in float fragWorldHeight;
layout(location = 4) in vec4 fragViewPos;

layout(location = 0) out vec4 outColor;

layout(set = 1, binding = 0) uniform sampler2D heightMapSampler[2];
layout(set = 1, binding = 1) uniform sampler2D grassSampler[2];
layout(set = 1, binding = 2) uniform sampler2D snowSampler[2];
layout(set = 1, binding = 3) uniform sampler2D cliffSampler[2];

void main()
{
	outColor = vec4(1, 1, 1, 1);
	gl_FragDepth = gl_FragCoord.z - gl_FragCoord.z * 0.00001;
}