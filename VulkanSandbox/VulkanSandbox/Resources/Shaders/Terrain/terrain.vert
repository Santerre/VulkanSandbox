#version 450
#extension GL_ARB_separate_shader_objects : enable

#pragma shader_stage(vertex)

#include "Engine/GlobalUniforms.glsl"
#include "Terrain/TerrainMaterialUniform.glsl"

layout(set = 2, binding = 0) uniform Object
{
	vec3 extents;
	vec3 location;
} object;

layout(location = 0) in vec4 patchCoords;

layout(location = 0) out vec2 tcsUVCoords;
layout(location = 1) out vec2 tcsPatchSize;

void main()
{
	tcsUVCoords = patchCoords.xy * object.extents.xz - object.extents.xz * 0.5;
	tcsPatchSize = object.extents.xz * patchCoords.z;
	gl_Position = vec4(tcsUVCoords.x, 0.0, tcsUVCoords.y, 1.0);
}