#version 450

#pragma shader_stage(fragment)

#extension GL_ARB_separate_shader_objects : enable

#include "Engine/GlobalUniforms.glsl"
#include "Terrain/TerrainMaterialUniform.glsl"

layout(location = 0) in vec4 color;
layout(location = 1) in vec2 fragUV;
layout(location = 2) in float fragHeight;
layout(location = 3) in float fragWorldHeight;
layout(location = 4) in vec4 fragViewPos;

layout(location = 0) out vec4 outColor;

layout(set = 1, binding = 0) uniform sampler2D heightMapSampler[2];
layout(set = 1, binding = 1) uniform sampler2D grassSampler[2];
layout(set = 1, binding = 2) uniform sampler2D snowSampler[2];
layout(set = 1, binding = 3) uniform sampler2D cliffSampler[2];

vec3 MapNormal(vec3 normal, vec3 faceNormal, vec3 uvRight, vec3 uvTop)
{
	mat3 lookAt = mat3
	(
		vec3(uvRight.x, uvTop.x, faceNormal.x),
		vec3(uvRight.y, uvTop.y, faceNormal.y),
		vec3(uvRight.z, uvTop.z, faceNormal.z)
	);

	vec3 res;
	res = normal * 2.f - 1.f;
	res = transpose(lookAt) * res;
	res.xz *= -1;
	return res;
}

float ComputeAlpha(vec3 normal, float cos, float blend)
{
	return clamp((1 - dot(normal, vec3(0, 1, 0)) - cos + blend) / (blend * 2.0), 0, 1);
}

void main()
{
	vec3 heightNormal = MapNormal(texture(heightMapSampler[1], fragUV * mat.heightMapTiling).xyz, vec3(0, 1.f / max(fragWorldHeight, 0.0001f), 0), vec3(1, 0, 0), vec3(0, 0, 1));
	heightNormal = normalize(heightNormal);

    vec4 albedo = vec4(0.2, 0.8, 0.2, 1);
    vec3 fragNormal = heightNormal;
#if SAMPLE_TEXTURE
	// Albedo
	vec2 tiledCoords =  fragUV * mat.tiling;
	vec4 upColor = texture(snowSampler[0], tiledCoords);
	vec4 downColor = texture(grassSampler[0], tiledCoords);
	vec4 cliffColor = texture(cliffSampler[0], tiledCoords);

	vec4 upNormal = texture(snowSampler[1], tiledCoords);
	vec4 downNormal = texture(grassSampler[1], tiledCoords);
	vec4 cliffNormal = texture(cliffSampler[1], tiledCoords);
	
	float upAlpha = ComputeAlpha(heightNormal, mat.slopeUpCos, mat.cliffUpTexBlend);
	float downAlpha = ComputeAlpha(heightNormal, mat.slopeDownCos, mat.cliffDownTexBlend);
	float upDownAlpha = clamp((fragHeight - mat.normUpValue + 2 * mat.upDownBlend) / (mat.upDownBlend * 2.0), 0, 1);
	albedo = mix(mix(downColor, cliffColor, downAlpha), mix(upColor, cliffColor, upAlpha), upDownAlpha);

	// Normal
	vec3 texNormal = mix(mix(downNormal, cliffNormal, downAlpha), mix(upNormal, cliffNormal, upAlpha), upDownAlpha).xyz;
	vec3 uvTop = cross(heightNormal, vec3(1, 0, 0));
	fragNormal = MapNormal(texNormal, heightNormal, cross(heightNormal, uvTop), uvTop);
#endif

	// Lights
	vec3 ambientLightColor = light.ambient.color * light.ambient.intensity;
	vec3 directionalLightColor = light.directional.color * light.directional.intensity * max(dot(-light.directional.direction, fragNormal), 0);

	// Final
	outColor = albedo * vec4(directionalLightColor + ambientLightColor, 1);

	// Fog
	float fogDist = 1000;
	outColor = mix(outColor, vec4(mat.fogColor, 1), clamp(-fragViewPos.z / mat.fogDistance, 0, 1));

    //outColor = color;
}