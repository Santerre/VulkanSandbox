#version 460

#pragma shader_stage(tesscontrol)

#extension GL_ARB_separate_shader_objects : enable

#extension GL_KHR_shader_subgroup_arithmetic : enable
#extension GL_KHR_shader_subgroup_vote : enable

#include "Engine/GlobalUniforms.glsl"
#include "Engine/CullingUtils.glsl"
#include "Terrain/TerrainMaterialUniform.glsl"

layout(vertices	= 4) out;

layout(set = 1, binding = 0) uniform sampler2D heighMapSampler[2];

layout(set = 2, binding = 0) uniform Object
{
	vec3 extents;
	vec3 location;
} object;

layout(location = 0) in vec2 tcsUVCoords[];
layout(location = 1) in vec2 tcsPatchSize[];

layout(location = 0) out vec2 tesUV[];

const vec2 g_offsets[4] = vec2[4]
(
	vec2(0.0, 0.0),
	vec2(0.0, 1.0),
	vec2(1.0, 0.0),
	vec2(1.0, 1.0)
);

const uvec2 edgeID[4] = uvec2[4]
(
	uvec2(2, 0),
	uvec2(0, 1),
	uvec2(1, 3),
	uvec2(3, 2)
);

// trix to have shared values
out layout(location = 12) vec4 viewSpaceLocation[4];

float ComputeEdgeTessLevel(float factor, vec4 viewSpaceLocationA, vec4 viewSpaceLocationB)
{
	vec3 vVec = (viewSpaceLocationA - viewSpaceLocationB).xyz;
	vec3 O = ((viewSpaceLocationA + viewSpaceLocationB) * 0.5).xyz;
	float r = length(vVec);
	float d = length(O);
	float cotfov = global.projection[0][0];
	float pr = cotfov * r / max(sqrt(d * d - r * r), 0.0001f);
	float size = pr * camGlobal.camExtents.x;
	
	return max(size / factor, 1);
}

int ceilPower2(float v)
{
return int(pow(ceil(log2(v))+1, 2));
}

void main()
{
	vec3 resizedOffsets = vec3(g_offsets[gl_InvocationID] * tcsPatchSize[0].xy, 0.0);
	vec4 flatWorldPos = vec4(gl_in[0].gl_Position.xyz + resizedOffsets.xzy, 1.0);
	gl_out[gl_InvocationID].gl_Position = flatWorldPos;

	tesUV[gl_InvocationID] = flatWorldPos.xz;

    int maxTess = 64;
#if NO_TESSELLATION
    maxTess = 1;
#endif

#if DO_CULLING
	vec4 downCullViewProj = global.projection * global.cullView * flatWorldPos;
	vec4 upCullViewProj = global.projection * global.cullView * (flatWorldPos + vec4(0.0, object.extents.y, 0.0, 0.0));
	
	uint culledBits = getCullBits(downCullViewProj);
	culledBits &= getCullBits(upCullViewProj);
	if (subgroupAnd(culledBits) != 0)
	{
		maxTess = 1;
	}
#endif

	viewSpaceLocation[gl_InvocationID] = global.cullView * (flatWorldPos + vec4(0.0, texture(heighMapSampler[0], tesUV[gl_InvocationID] * mat.heightMapTiling).x * object.extents.y, 0.0, 0.0));

	barrier();

	gl_TessLevelOuter[gl_InvocationID] = min(maxTess, ComputeEdgeTessLevel(mat.tessTargetScreenSize, viewSpaceLocation[edgeID[gl_InvocationID].x], viewSpaceLocation[edgeID[gl_InvocationID].y]));

    if (mat.forceTessLevel >= 0)
        gl_TessLevelOuter[gl_InvocationID] = mat.forceTessLevel;

	for (int i = 0; i < 2; i++)
        gl_TessLevelInner[i] = min(maxTess, 0.25 * (gl_TessLevelOuter[0] + gl_TessLevelOuter[1] + gl_TessLevelOuter[2] + gl_TessLevelOuter[3]));
		//gl_TessLevelInner[i] = min(maxTess, 0.25 * (subgroupAdd(gl_TessLevelOuter[gl_InvocationID])));
        //gl_TessLevelInner[i] = min(maxTess, subgroupMax(gl_TessLevelOuter[gl_InvocationID]));
}