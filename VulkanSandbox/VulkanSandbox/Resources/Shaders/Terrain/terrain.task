#version 460

#extension GL_NV_mesh_shader : enable
#extension GL_ARB_separate_shader_objects : enable

#extension GL_KHR_shader_subgroup_arithmetic : enable
#extension GL_KHR_shader_subgroup_vote : enable

#pragma shader_stage(task)

#include "Engine/GlobalUniforms.glsl"
#include "Engine/CullingUtils.glsl"
#include "Engine/Limits.glsl"
#include "Terrain/TerrainMaterialUniform.glsl"
#include "Terrain/TerrainSubPatchUtils.glsl"

layout(set = 1, binding = 0) uniform sampler2D heightMapSampler[2];

const uint g_groupSize = 32;
const uint g_subPatchPerThread = uint(ceil(g_subPatchTotal / float(g_groupSize)));

layout(local_size_x=g_groupSize) in;

layout(set = 2, binding = 0) uniform Object
{
	vec3 extents;
	vec3 location;
} object;

layout(set = 3, binding = 0) buffer patchData
{
	vec4 patchCoords[];
} g_patchData;

struct PerSubPatch
{
	vec2 subPatchLocation;
	uint invocationCount;
	
	vec2 invocationTessellationInner;
	vec2 tessellationFactorInner;

	vec4 invocationTessellationOuter;
	vec4 tessellationFactorOuter;
};

taskNV out OUT
{
	vec2 subPatchSize;
	PerSubPatch perSubPatch[g_subPatchTotal];
} g_o;

const uvec2 edgeID[4] = uvec2[4]
(
	uvec2(2, 0),
	uvec2(0, 1),
	uvec2(1, 3),
	uvec2(3, 2)
);

const vec2 g_offsets[4] = vec2[4]
(
	vec2(0.0, 0.0),
    vec2(0.0, 1.0),
	vec2(1.0, 0.0),
	vec2(1.0, 1.0)
);

const vec4 color[4] = vec4[4]
(
	vec4(1.0,0.0,0.0, 1.0),
	vec4(0, 1.0, 0.0, 1.0),
	vec4(0.0, 0.0, 1.0, 1.0),
	vec4(1.0, 1.0, 0.0, 1.0)
);

float ComputeEdgeTessLevel(float factor, vec4 viewSpaceLocationA, vec4 viewSpaceLocationB)
{
	vec3 vVec = (viewSpaceLocationA - viewSpaceLocationB).xyz;
	vec3 O = ((viewSpaceLocationA + viewSpaceLocationB) * 0.5).xyz;
	float r = length(vVec);
	float d = length(O);
	float cotfov = global.projection[0][0];
	float pr = cotfov * r / max(sqrt(d * d - r * r), 0.0001f);
	float size = pr * camGlobal.camExtents.x;
	
	return max(size / factor, 1);
}

uvec2 ComputeCoords(uint x, uint sizeX)
{
	uvec2 res;
	res.y = x / sizeX;
	res.x = x - sizeX * res.y;
	return res;
}

struct VertexData
{
	vec2 world2D;
	vec2 padding;
	vec4 viewSpace;
};

const uint g_subPatchVerticesSide = g_subPatchTessellationFactor + 1;
const uint g_subPatchVerticesCount = (g_subPatchTessellationFactor + 1) * (g_subPatchTessellationFactor + 1);
shared VertexData s_vertices[g_subPatchVerticesCount];

const uint g_vertexIDOffsets[4] = uint[4]
(
	0,
	g_subPatchVerticesSide,
    1,
	g_subPatchVerticesSide + 1
);

uint getVertexID(uvec2 subPatchCoords, uint indexID)
{
	return subPatchCoords.x + subPatchCoords.y * g_subPatchVerticesSide + g_vertexIDOffsets[indexID];
}

void main()
{
	uint id = gl_WorkGroupID.x;
	uint threadTaskCount = 0;
	vec4 patchCoords = g_patchData.patchCoords[id];
	vec2 patchSize = object.extents.xz * patchCoords.z;
	vec2 subPatchSize = patchSize / vec2(g_subPatchTessellationFactor);

	vec2 subPatchInvocationCounts[g_subPatchPerThread];

	vec2 patchLocation = patchCoords.xy * object.extents.xz - object.extents.xz * 0.5;
	for (uint i = 0; i < g_subPatchVerticesCount; i += g_groupSize)
	{
		uint vertexID = min(i + gl_SubgroupInvocationID, g_subPatchVerticesCount - 1);

		vec2 vertexPos2D = patchLocation + ComputeCoords(vertexID, g_subPatchVerticesSide) * subPatchSize;
		s_vertices[vertexID].world2D = vertexPos2D;
		
		float height = texture(heightMapSampler[0], vertexPos2D * mat.heightMapTiling).x;
		float worldHeight = height * object.extents.y;

		vec3 vertexWorldPos = vec3(vertexPos2D.x, worldHeight, vertexPos2D.y);

		s_vertices[vertexID].viewSpace = global.cullView * vec4(vertexWorldPos, 1.0);
	}

	barrier();
	memoryBarrierShared();

	for (uint i = 0; i < g_subPatchTotal; i += g_groupSize)
	{
		uint subPatchID = i + gl_LocalInvocationIndex;
        if (subPatchID >= g_subPatchTotal)
            continue;
		uvec2 subPatchCoords = ComputeCoords(subPatchID, g_subPatchTessellationFactor);
		
		uint culledBits = ~0;
		VertexData cornerVertexData[4];
		for (uint j = 0; j < 4; j++)
		{
			uint vertexID = getVertexID(subPatchCoords, j);
			cornerVertexData[j] = s_vertices[vertexID];

		#if USE_CULLING
			vec2 world2D = cornerVertexData[j].world2D;
			vec4 downCullViewProj = global.projection * global.cullView * vec4(world2D.x, 0, world2D.y, 1.0);
			vec4 upCullViewProj = global.projection * global.cullView * vec4(world2D.x, object.extents.y, world2D.y, 1.0);
	
			culledBits &= getCullBits(downCullViewProj);
			culledBits &= getCullBits(upCullViewProj);
		#endif
		}

	#if USE_CULLING
		if (culledBits != 0)
		{
			g_o.perSubPatch[subPatchID].invocationCount = 0;
			continue;
		}
	#endif

		vec4 subPatchTessOuter;
		for (int j = 0; j < 4; j++)
		{
			vec4 viewSpaceA = cornerVertexData[edgeID[j].x].viewSpace;
			vec4 viewSpaceB = cornerVertexData[edgeID[j].y].viewSpace;
			subPatchTessOuter[j] = ComputeEdgeTessLevel(mat.tessTargetScreenSize, viewSpaceA, viewSpaceB);
		}

		if (mat.forceTessLevel >= 1)
			subPatchTessOuter = vec4(mat.forceTessLevel / g_subPatchTessellationFactor);

		uint maxTaskTess = uint(sqrt(KU_Limits_MaxTaskOutput / g_subPatchTotal));
		if (mat.forceTaskTessLevel >= 1)
			maxTaskTess = uint(mat.forceTaskTessLevel);

		uint maxMeshTess = KU_Limits_MaxMeshTessellation - 1;
		if (mat.forceMeshTessLevel >= 1)
			maxMeshTess = uint(mat.forceMeshTessLevel);

		vec4 taskTessOuter = clamp(subPatchTessOuter / maxMeshTess, 1, maxTaskTess);
		if (mat.forceTaskTessLevel >= 1)
			taskTessOuter = vec4(mat.forceTaskTessLevel);

		vec4 meshTessOuter = clamp(subPatchTessOuter / taskTessOuter, 1, maxMeshTess);
#if USE_MESH_TRANSITION == 0
		meshTessOuter = uvec4(meshTessOuter);
#endif
		if (mat.forceMeshTessLevel >= 1)
			meshTessOuter = vec4(mat.forceMeshTessLevel);

		vec2 taskTessInner;
		taskTessInner.x = max(taskTessOuter[1], taskTessOuter[3]);
		taskTessInner.y = max(taskTessOuter[0], taskTessOuter[2]);

		vec2 meshTessInner;
		meshTessInner.x = max(meshTessOuter[1], meshTessOuter[3]);
		meshTessInner.y = max(meshTessOuter[0], meshTessOuter[2]);

		uint invocationCount = int(ceil(taskTessInner.x) * ceil(taskTessInner.y));
		threadTaskCount += invocationCount;

		vec2 subPatchLocation = patchLocation + subPatchSize * subPatchCoords;
		g_o.perSubPatch[subPatchID].tessellationFactorInner = taskTessInner;
		g_o.perSubPatch[subPatchID].tessellationFactorOuter = taskTessOuter;
		g_o.perSubPatch[subPatchID].invocationCount = invocationCount;
        g_o.perSubPatch[subPatchID].invocationTessellationInner = meshTessInner;
        g_o.perSubPatch[subPatchID].invocationTessellationOuter = meshTessOuter;
		g_o.perSubPatch[subPatchID].subPatchLocation = subPatchLocation;
	}

	g_o.subPatchSize = subPatchSize;
	gl_TaskCountNV = subgroupAdd(threadTaskCount);
}