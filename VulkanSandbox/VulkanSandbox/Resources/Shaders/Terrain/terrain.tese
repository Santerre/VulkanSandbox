#version 450
#extension GL_ARB_separate_shader_objects : enable

#pragma shader_stage(tesseval)

#include "Engine/GlobalUniforms.glsl"
#include "Terrain/TerrainMaterialUniform.glsl"

layout(quads, fractional_even_spacing, cw) in;

layout(set = 1, binding = 0) uniform sampler2D heightMapSampler[2];

layout(set = 2, binding = 0) uniform Object
{
	vec3 extents;
	vec3 location;
} object;

const float normalPrecision = 0.001f; 

layout(location = 0) in vec2 tesUV[];

layout(location = 0) out vec4 color;
layout(location = 1) out vec2 fragUV;
layout(location = 2) out float fragHeight;
layout(location = 3) out float fragWorldHeight;
layout(location = 4) out vec4 fragViewPos;

#define quadInterp(array) mix(mix(array[0], array[1], gl_TessCoord.x), mix(array[2], array[3], gl_TessCoord.x), gl_TessCoord.y)
#define quadInterpVal(array, value) mix(mix(array[0].value, array[1].value, gl_TessCoord.x), mix(array[2].value, array[3].value, gl_TessCoord.x), gl_TessCoord.y)

vec3 ComputeNormal(vec2 P, sampler2D heightMap)
{
	vec3 off = vec3(normalPrecision, normalPrecision, 0.0);
	float hL = texture(heightMap, P.xy - off.xz).r;
	float hR = texture(heightMap, P.xy + off.xz).r;
	float hD = texture(heightMap, P.xy - off.zy).r;
	float hU = texture(heightMap, P.xy + off.zy).r;
	
	vec3 N;
	// deduce terrain normal
	N.x = hL - hR;
	N.y = -2.0 * normalPrecision;
	N.z = hD - hU;
	return normalize(N);
}

void main()
{
	fragUV = quadInterp(tesUV);

	fragHeight = texture(heightMapSampler[0], fragUV * mat.heightMapTiling).x;
	fragWorldHeight = fragHeight * object.extents.y;
	vec3 worldPos = quadInterpVal(gl_in, gl_Position).xyz + vec3(0, fragWorldHeight, 0);
	fragViewPos = global.view * vec4(worldPos, 1.0);
	gl_Position = global.projection * fragViewPos;
	color = vec4(fragUV, 0, 1);
}
