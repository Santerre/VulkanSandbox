﻿#include "Applications/HelloTriangleApplication.h"

#include <algorithm>
#include <thread>
#include <array>

#include "glm/gtc/matrix_transform.hpp"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_vulkan.h"

#include "Core/FrameRenderContext.h"

#include "RHI/Vulkan/VKRHI_TypeConverters.h"

#include "RHI/Vulkan/PipelineTemplate/BasePipelineTemplate.h"

#include "Debugging/Assert.h"
#include "Debugging/Log.h"
#include "Debugging/Profiler.h"

#include "Resource/Shader.h"
#include "Resource/DynamicShader.h"
#include "Resource/Mesh.h"
#include "Resource/Texture.h"
#include "Resource/CubemapImage.h"
#include "Resource/ProfileData.h"

#include "Scene/TestScene.h"
#include "Scene/TestInputScene.h"

#include "Scene/SceneComponent/StaticMesh.h"

#include "External/Glm.h"


bool HelloTriangleApplication::Init()
{
    Profiler::Get().Init();

	_hierarchicalMemory.Init();
    _resourceLibrary.Init();
	_resourceMgr.Init(this, &_resourceLibrary);
    _resourceMgr.OnResourceRegistered.AddListener([this](std::shared_ptr<IResourceWrapper> wrapper)
    {
        _addedResources.push_back(wrapper);
    });

	KU_VERIFY(InitShaderCompilation());

	KU_ASSERT(!_window);
	_window.reset(new Window(GetApplicationName()));
	_window->Open();
	_window->SetWindowUserPointer(reinterpret_cast<void*>(&_rawInputContext));

	if (!InitVulkan())
		return false;

	if (!InitInput())
		return false;

	SetupDebugCallback();

	KU_VERIFY(CreateScene());

	if (!CreateSurface())
	{
		ELOG(LogVulkan, "Couldn't create the target surface");
		return false;
	}
	KU_VERIFY(InitImGUI());

	if (!PickPhysicalDevice())
	{
		ELOG(LogVulkan, "Couldn't find a valid physical device");
		return false;
	}

	KU_VERIFY(CreateLogicalDevice());
	_graphMemAllocator.FromDevice(_logicalDevice);
	_backbufferDrawableSemaphore.Initialize(_logicalDevice);
	KU_VERIFY(CreateCommandPool());

	// Default resources
	KU_VERIFY(CreateDescriptorSetLayouts());
	KU_VERIFY(CreateTextureSamplers());

	CreateDynamicRenderingObjects();

	_time.Init();
	
#ifdef INCLUDE_EDITOR
	_resourceEditor.Init(&_resourceMgr);

	ProfilerUI::InitDesc profilerDesc{};
	profilerDesc.resourceManager = &_resourceMgr;
	profilerDesc.onRequestEndProfiling = [this](){ _bProfilerUIRequestEndProfiling = true; };
	profilerDesc.onRequestStartProfiling = [this]() { _bProfilerUIRequestStartProfiling = true; };
	_profilerUI.FromDesc(profilerDesc);
#endif // INCLUDE_EDITOR

	return true;
}

void HelloTriangleApplication::CreateDynamicRenderingObjects()
{
	KU_VERIFY(CreateSwapChain());
	KU_VERIFY(CreateDescriptorPool());
	KU_VERIFY(CreateDepthResources());
	KU_VERIFY(CreateRenderPass());
	KU_VERIFY(RegisterSceneForRendering());
	KU_VERIFY(CreateGraphicsPipeline());
	KU_VERIFY(CreateFrames());
	KU_VERIFY(InitImGUIDynamic());
}

void HelloTriangleApplication::DestroyDynamicRenderingObjects()
{
	for (auto&& frame : _frames)
	{
		frame.WaitDraw();
		frame.Shutdown();
	}
	_frames.clear();

	ShutdownImGUIDynamic();

	DestroyPipelines();

	_availablePipelineLayouts.ShutdownLayouts(_logicalDevice);

	_opaqueRenderPass.Shutdown();
	_editorRenderPass.Shutdown();

	if (_descriptorPool)
		vkDestroyDescriptorPool(_logicalDevice.GetVkDevice(), _descriptorPool, nullptr);

	_depthBufferMem.Shutdown();
	_depthBuffer.Shutdown();

	_swapChain.Shutdown();

	ClearMeshData();
}

void HelloTriangleApplication::RefreshSceneRendering()
{
	FlushRendering();
	DestroyDynamicRenderingObjects();

	CreateDynamicRenderingObjects();
}

void HelloTriangleApplication::RequestExtensions(std::vector<const char*>& extensions) const
{
	if (_enableDebugLayer)
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
}

void HelloTriangleApplication::RequestLayers(std::vector<const char*>& layers) const
{
	if (_enableDebugLayer)
		layers.push_back("VK_LAYER_LUNARG_standard_validation");
}

bool HelloTriangleApplication::FindSupportedFormats(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features, VkFormat& outFormat) const
{
	for (auto&& format : candidates)
	{
		VkFormatProperties props;
		vkGetPhysicalDeviceFormatProperties(_mainPhysicalDevice->GetVkPhysicalDevice(), format, &props);
		if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features
			|| tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
		{
			outFormat = format;
			return true;
		}
	}

	outFormat = VK_FORMAT_UNDEFINED;
	return false;
}
bool HelloTriangleApplication::FindSupportedDepthFormats(VkFormat& outFormat) const
{
	return FindSupportedFormats(
			{ VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT }
		,	VK_IMAGE_TILING_OPTIMAL
		,	VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
		,	outFormat
	);
}

bool HelloTriangleApplication::HasStencilComponent(VkFormat format) const
{
	return format == VK_FORMAT_D16_UNORM_S8_UINT
		|| format == VK_FORMAT_D24_UNORM_S8_UINT
		|| format == VK_FORMAT_D32_SFLOAT_S8_UINT;
}


std::vector<const char*> HelloTriangleApplication::DetermineLayers()
{
	VLOG(LogVulkan, "-- Vulkan available layers -- ");
	uint32_t layerCount = 0;
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
	std::vector<VkLayerProperties> layers(layerCount);
	vkEnumerateInstanceLayerProperties(&layerCount, layers.data());

	RequestLayers(_requiredLayers);

	for (auto layer : layers)
		VLOG(LogVulkan, "%s", layer.layerName);

	std::vector<const char*> filteredLayers;
	for (auto&& lookedLayer : _requiredLayers)
	{
		bool isFound = layers.end() == std::find_if(layers.begin(), layers.end(), [&lookedLayer](VkLayerProperties const& property) -> bool
		{
			return property.layerName == lookedLayer;
		});

		if (!isFound)
			LOG(LogVulkan, "Required layer %s not found", lookedLayer);
		else
			filteredLayers.push_back(lookedLayer);
	}

	return filteredLayers;
}

bool HelloTriangleApplication::InitVulkan()
{
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = GetApplicationName();
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "No Engine";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_1;

#ifdef _DEBUG
	_enableDebugLayer = true;
#endif

	uint32_t extentionsCount;
	const char** ext = glfwGetRequiredInstanceExtensions(&extentionsCount);
	std::vector<const char*> extensions(ext, ext + extentionsCount);
	RequestExtensions(extensions);

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;
	createInfo.ppEnabledExtensionNames = extensions.data();
	createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());

	_instanceLayers = DetermineLayers();
    createInfo.enabledLayerCount = static_cast<uint32_t>(_instanceLayers.size());
	createInfo.ppEnabledLayerNames = _instanceLayers.data();

	VkResult result = vkCreateInstance(&createInfo, nullptr, &_vkInstance);
    auto res = (PFN_vkGetPhysicalDeviceProperties2)vkGetInstanceProcAddr(_vkInstance, "vkGetPhysicalDeviceProperties2");

	if (result != VK_SUCCESS)
	{
		ELOG(LogVulkan, "Error while creating the instance.");
		return false;
	}

	LOG(LogVulkan, "Vulkan Instance Initialized");

	return true;
}

std::vector<const char*> const& HelloTriangleApplication::GetRequiredValidationLayers() const
{
	return _requiredLayers;
}

Handle<SPVShader> HelloTriangleApplication::LoadShader( std::string const& shaderPath )
{
    Path path = shaderPath;
    ShaderType type = ShaderParserUtils::PathToShaderType(shaderPath);
    if (type == ShaderType::None)
    {
        WLOG(LogResource, "Couldn't get the type of the shader %s", shaderPath.c_str());
	    return _resourceMgr.Get<SPVShader>(shaderPath);
    }

    Path shaderDescPath = path.ChangeExtension(DynamicShader::GetShaderDefaultExtension(type));
    Handle<DynamicShader> dynamicShader = _resourceMgr.Get<DynamicShader>(shaderDescPath.GetString());
    if (dynamicShader)
    {
        Handle<SPVShader> res = dynamicShader->RequestSPVShader(_shaderCompilationContext, _shaderCompiler);
        if (!res)
        {
            ELOG(LogShaderCompilation, "Couldn't recompile the shader %s", shaderPath.c_str());
            res = _resourceMgr.Get<SPVShader>(shaderPath);
        }
        return res;
    }

    return _resourceMgr.Get<SPVShader>(shaderPath);
}

std::vector<VkPipelineShaderStageCreateInfo> HelloTriangleApplication::LoadShaders(std::vector<std::string> const& shaderPaths)
{
	std::vector<VkPipelineShaderStageCreateInfo> shaders;
	std::transform(shaderPaths.begin(), shaderPaths.end(), std::back_inserter(shaders), [this](std::string const& str) -> auto
	{
        Handle<SPVShader> shader = LoadShader(str);
        KU_ASSERT(shader);
		return shader->GetRHIShader()->GetShaderCreateInfo();
	});

	return shaders;
}

Input::GLFWRawInputContext* RetrieveFunc(GLFWwindow* window)
{
	return reinterpret_cast<Input::GLFWRawInputContext*>(Window::GetWindowUserPointer(window));
}

bool HelloTriangleApplication::InitInput()
{
	using namespace Input;

	_rawInputContext.FromGLFWWindow<&RetrieveFunc>(_window->GetGLFWWindow());
	_inputMgr.FromRawInputContext(&_rawInputContext);

	{
		InputContext context("EditorContext", -14);
		context.SetActive(_isInEditMode);
		context.SetBlockInputs(true);
		_inputMgr.CreateContext(std::move(context));
	}
	{
		InputContext context("EditorOverlay", -15);
		{
			auto& action = context.AddAction(
				"OpenEditor",
				{
						{ RawInput::K_F2 }
				});

			action.onAction.AddListener([this]()
			{
				ToggleEditor();
			});
		}
        {
			auto& action = context.AddAction(
				"ReloadSceneDisplay",
				{
						{ RawInput::K_F5 }
				});

			action.onAction.AddListener([this]()
			{
				_requestFrameRefresh = true;
			});
		}
		_inputMgr.CreateContext(std::move(context));
	}
	_rawInputContext.SetCursorEnabled(_isInEditMode);

	return true;
}

void HelloTriangleApplication::ToggleEditor()
{
#ifdef INCLUDE_EDITOR
    _isInEditMode = !_isInEditMode;
	_inputMgr.GetContext("EditorContext").SetActive(_isInEditMode);
	_rawInputContext.SetCursorEnabled(_isInEditMode);

    if (_isInEditMode)
    {
        _rawInputContext.keyDelegate.AddListener(&ImGui_ImplGlfw_KeyCallback);
        _rawInputContext.charDelegate.AddListener(&ImGui_ImplGlfw_CharCallback);
        _rawInputContext.scrollDelegate.AddListener(&ImGui_ImplGlfw_ScrollCallback);
        _rawInputContext.mouseButtonDelegate.AddListener(&ImGui_ImplGlfw_MouseButtonCallback);
    }
    else
    {
        _rawInputContext.keyDelegate.RemoveListener(&ImGui_ImplGlfw_KeyCallback);
        _rawInputContext.charDelegate.RemoveListener(&ImGui_ImplGlfw_CharCallback);
        _rawInputContext.scrollDelegate.RemoveListener(&ImGui_ImplGlfw_ScrollCallback);
        _rawInputContext.mouseButtonDelegate.RemoveListener(&ImGui_ImplGlfw_MouseButtonCallback);
    }
#endif // INCLUDE_EDITOR
}

bool HelloTriangleApplication::CreateScene()
{
	//_scene = std::make_unique<TestInputScene>();
	_scene = std::make_unique<TestScene>();

	SceneContext context = {};
	context.renderSystem = &_renderSystem;
	context.resourceMgr = &_resourceMgr;
	context.window = _window.get();
	context.inputManager = &_inputMgr;

	SceneDesc desc = {};
	desc.context = context;

	_scene->FromDesc(desc);

	return true;
}

bool HelloTriangleApplication::CreateSurface()
{
	return glfwCreateWindowSurface(_vkInstance, _window->GetGLFWWindow(), nullptr, &_surface) == VK_SUCCESS;
}

bool HelloTriangleApplication::PickPhysicalDevice()
{
	uint32_t physicalDevicesCount;
	vkEnumeratePhysicalDevices(_vkInstance, &physicalDevicesCount, nullptr);

	std::vector<VkPhysicalDevice> physicalDevices(physicalDevicesCount);
	vkEnumeratePhysicalDevices(_vkInstance, &physicalDevicesCount, physicalDevices.data());

	_physicalDevices.resize(physicalDevicesCount);

	VKRHI_PhysicalDevice* bestDevice = nullptr;
	int maxScore = 0;
	for (size_t i = 0; i < physicalDevicesCount; i++)
	{
		_physicalDevices[i].FromVkDevice(_vkInstance,physicalDevices[i], _surface);

		if (!_physicalDevices[i].IsSuitable())
			continue;

		if (!_physicalDevices[i].SupportsExtensions(_requiredDeviceExtensions))
			continue;

		int score = _physicalDevices[i].Score(_optionalDeviceExtensions);
		if (score > maxScore)
		{
			maxScore = score;
			bestDevice = &_physicalDevices[i];
		}
	}

	if (maxScore == 0)
		return false;

	_mainPhysicalDevice = bestDevice;
	return true;
}

bool HelloTriangleApplication::SetDeviceCompilationMacros(VKRHI_Device const& device)
{
	auto&& meshShaderProperties = device.GetPhysicalDevice().GetPhysicalDeviceProperties().meshShaderProperties;
	_baseShaderCompilationContext.SetGlobalMacro("KU_MAX_TASK_OUTPUT", meshShaderProperties.maxTaskOutputCount);
	_baseShaderCompilationContext.SetGlobalMacro("KU_MAX_TASK_GROUPSIZE", meshShaderProperties.maxTaskWorkGroupSize[0]);
	uint32_t maxMeshTessellation = static_cast<uint32_t>(std::sqrt(meshShaderProperties.maxMeshOutputVertices)) - 1;
	_baseShaderCompilationContext.SetGlobalMacro("KU_MAX_MESH_TESSELLATION", maxMeshTessellation);
	_baseShaderCompilationContext.SetGlobalMacro("KU_MAX_MESH_GROUPSIZE", meshShaderProperties.maxMeshWorkGroupSize[0]);

	uint32_t maxsubPatchTessellation = static_cast<uint32_t>(std::sqrt(meshShaderProperties.maxTaskOutputCount));
	_baseShaderCompilationContext.SetGlobalMacro("KU_SUBPATCH_TESSELLATION", maxsubPatchTessellation);

	_shaderCompilationContext = _baseShaderCompilationContext;
	_shaderCompilationContext.SetGlobalMacro("KU_MAX_MESH_TESSELLATION", 7);
	_shaderCompilationContext.SetGlobalMacro("KU_SUBPATCH_TESSELLATION", 5);
	return true;
}

bool HelloTriangleApplication::CreateLogicalDevice()
{
	if (!_mainPhysicalDevice)
	{
		ELOG(LogVulkan, "Physical device not loaded, aborting logical device creation.");
		return false;
	}

    std::vector<const char*> extensions(_requiredDeviceExtensions);
    for (auto&& ext : _optionalDeviceExtensions)
    {
        if (_mainPhysicalDevice->SupportsExtensions({ ext }))
            extensions.push_back(ext);
    }

    if (!_logicalDevice.FromPhysicalDevice(*_mainPhysicalDevice, _instanceLayers, extensions))
	{
		ELOG(LogVulkan, "An error happened during the logical device creation.");
		return false;
	}

    _useMeshShading = _logicalDevice.IsMeshShadingEnabled();
    //_useMeshShading = false;
    _forceWireframe = false;
    _terrainDebugDisplay = false;
    _forceSolidClear = false;
    _solidClearColor = glm::vec3(0.4, 0.59, 0.90);

    VKRHI_TransferQueueDescriptor desc = {};
    desc.graphMemAllocator = &_graphMemAllocator;
    // Should be the transfer queue
    desc.queue = _logicalDevice.GetGraphicQueue();
    desc.queueFamilyIndex = _logicalDevice.GetPhysicalDevice().GetQueueFamilyIndices().graphicsFamily.value();
    _transferQueue.FromDesc(_logicalDevice, desc);

	SetDeviceCompilationMacros( _logicalDevice );

	return true;
}

bool HelloTriangleApplication::CreateSwapChain()
{
	VKRHI_SwapChain::InitDesc swapChainDesc = {};
	swapChainDesc.desiredImageCount = static_cast<uint32_t>(_maxBackBufferCount);
	swapChainDesc.surface = _surface;
	swapChainDesc.windowExtents = _window->GetExtents();
	if (!_swapChain.FromDesc(_logicalDevice, swapChainDesc))
	{
		ELOG(LogVulkan, "An error happened during the swapChain creation.");
		return false;
	}

	return true;
}


bool HelloTriangleApplication::CreateDescriptorPool()
{
	uint32_t descCount = static_cast<uint32_t>(_swapChain.GetImageViews().size());

	constexpr std::array<VkDescriptorType, 2> types = 
	{ 
		VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 
		VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER 
	};
	
	std::array<VkDescriptorPoolSize, types.size()> descPoolSizes = {};
	for (size_t i = 0u; i < types.size(); i++)
	{
		descPoolSizes[i].type = types[i];
		descPoolSizes[i].descriptorCount = descCount * _descSetLayouts.CountLayoutsOfType(types[i]);
	}

	VkDescriptorPoolCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	info.poolSizeCount = static_cast<uint32_t>(descPoolSizes.size());
	info.pPoolSizes = descPoolSizes.data();
	info.maxSets = static_cast<uint32_t>(descCount * _descSetLayouts.layouts.size());

	if (vkCreateDescriptorPool(_logicalDevice.GetVkDevice(), &info, nullptr, &_descriptorPool) != VK_SUCCESS)
	{
		ELOG(LogVulkan, "An error happened during the descriptor pool creation.");
		return false;
	}
	return true;
}

bool HelloTriangleApplication::CreateDepthResources()
{
	VkFormat format = {};
	KU_VERIFY(FindSupportedDepthFormats(format));

	ImageMemoryDesc desc = {};
	desc.allocator = &_graphMemAllocator;
	desc.extent = _swapChain.GetSurfaceExtents();
	desc.format = format;
	desc.tiling = VK_IMAGE_TILING_OPTIMAL;
	desc.memProperties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
	desc.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
	KU_VERIFY(_depthBufferMem.FromDesc(_logicalDevice, desc));

	ImageDesc imgDesc = {};
	imgDesc.format = format;
	imgDesc.aspectFlags = VK_IMAGE_ASPECT_DEPTH_BIT;
	KU_VERIFY(_depthBuffer.FromImageMemory(_logicalDevice, _depthBufferMem, imgDesc));

    VKRHI_CommandBuffer& cmdBuffer = _transferQueue.BeginCommandBuffer();
	_transferQueue.CmdTransitionImageLayout(cmdBuffer, _depthBufferMem, format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
    _transferQueue.EndCommandBuffer(cmdBuffer);

	return true;
}

bool HelloTriangleApplication::CreateTextureSamplers()
{
	VkSamplerCreateInfo baseInfo = {};
	baseInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;

	baseInfo.magFilter = VK_FILTER_LINEAR;
	baseInfo.minFilter = VK_FILTER_LINEAR;

	baseInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	baseInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	baseInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

	baseInfo.compareEnable = VK_FALSE;
	baseInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	baseInfo.anisotropyEnable = true;
	baseInfo.maxAnisotropy = 16;
	baseInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	baseInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	baseInfo.mipLodBias = 0.0f;
	baseInfo.minLod = 0.0f;
	baseInfo.maxLod = 200.f;

	{
		VkSamplerCreateInfo defaultInfo = baseInfo;

		if (vkCreateSampler(_logicalDevice.GetVkDevice(), &defaultInfo, nullptr, &_defaultSampler) != VK_SUCCESS)
		{
			ELOG(LogVulkan, "An error happened during the default sampler creation.");
			return false;
		}
	}

	{
		VkSamplerCreateInfo info = baseInfo;

		info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

		if (vkCreateSampler(_logicalDevice.GetVkDevice(), &info, nullptr, &_terrainHeightmapSampler) != VK_SUCCESS)
		{
			ELOG(LogVulkan, "An error happened during the cubemap sampler creation.");
			return false;
		}
	}
	
	{
		VkSamplerCreateInfo cubemapInfo = baseInfo;
		cubemapInfo.anisotropyEnable = false;
		cubemapInfo.maxAnisotropy = 1;

		cubemapInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		cubemapInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		cubemapInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;

		if (vkCreateSampler(_logicalDevice.GetVkDevice(), &cubemapInfo, nullptr, &_cubemapSampler) != VK_SUCCESS)
		{
			ELOG(LogVulkan, "An error happened during the cubemap sampler creation.");
			return false;
		}
	}

	return true;
}

void HelloTriangleApplication::DestroyTextureSamplers()
{
	if (_defaultSampler)
		vkDestroySampler(_logicalDevice.GetVkDevice(), _defaultSampler, nullptr);

	if (_terrainHeightmapSampler)
		vkDestroySampler(_logicalDevice.GetVkDevice(), _terrainHeightmapSampler, nullptr);

	if (_cubemapSampler)
		vkDestroySampler(_logicalDevice.GetVkDevice(), _cubemapSampler, nullptr);

}

bool HelloTriangleApplication::CreateRenderPass()
{
	// Opaque
	{
		VKRHI_RenderPassDesc desc = {};
		desc.renderAreaExtents = _swapChain.GetSurfaceExtents();

		VKRHI_PassAttachmentDesc colorDesc(0, _swapChain.GetSurfaceFormat().format);
		colorDesc.attachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorDesc.attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorDesc.attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorDesc.attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		colorDesc.attachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		colorDesc.clearColor = { 0.1f, 0.1f, 0.1f, 1.0f };

		desc.colorAttachments = { colorDesc };

		VKRHI_PassAttachmentDesc depthDesc(1, _depthBuffer.GetFormat());
		depthDesc.attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthDesc.attachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		depthDesc.attachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		depthDesc.clearColor = { 1.f, 0.f, 0.f, 0.f };

		desc.depthStencilAttachment = { depthDesc };

        VKRHI_SubpassDescription terrainSubpassDesc;
        terrainSubpassDesc.colorAttachments = desc.colorAttachments;
        terrainSubpassDesc.depthStencilAttachment = desc.depthStencilAttachment;
        terrainSubpassDesc.contents = VkSubpassContents::VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS;
        desc.subpasses.push_back(terrainSubpassDesc);
        
		if (!_opaqueRenderPass.FromDesc(_logicalDevice, std::move(desc)))
		{
			ELOG(LogVulkan, "Couldn't create the object render pass");
			return false;
		}
	}

#ifdef INCLUDE_EDITOR
	// Editor
	{
		VKRHI_RenderPassDesc desc = {};
		desc.renderAreaExtents = _swapChain.GetSurfaceExtents();

		VKRHI_PassAttachmentDesc colorDesc(0, _swapChain.GetSurfaceFormat().format);
		colorDesc.attachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorDesc.attachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		colorDesc.attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorDesc.attachment.initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		colorDesc.attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		colorDesc.attachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		desc.colorAttachments = { colorDesc };

		if (!_editorRenderPass.FromDesc(_logicalDevice, std::move(desc)))
		{
			ELOG(LogVulkan, "Couldn't create the editor render pass");
			return false;
		}
	}
#endif // INCLUDE_EDITOR

	return true;
}

bool HelloTriangleApplication::RegisterSceneForRendering()
{
	SC::StaticMesh const* staticMesh = _scene->GetStaticMesh();
    if (staticMesh)
	{
	    if (!RegisterStaticMeshForRendering(staticMesh))
		{
			ELOG(LogVulkan, "Couldn't register the static mesh for rendering.");
			return false;
		}
	}

	SC::Terrain const* terrain = _scene->GetTerrain();
	if (terrain)
	{
		if (!RegisterTerrainForRendering(terrain))
		{
			ELOG(LogVulkan, "Couldn't register the terrain for rendering.");
			return false;
		}
	}
    if (_scene->GetCubemap() && !_forceSolidClear)
	{
	    if (!RegisterCubemapForRendering(_scene->GetCubemap()))
		{
			ELOG(LogVulkan, "Couldn't register the Cubemap for rendering.");
			return false;
		}
	}

	return true;
}

bool HelloTriangleApplication::CreateGraphicsPipeline()
{
	// Objects
	{
		std::vector<VkPipelineShaderStageCreateInfo> baseShaderModules = LoadShaders({
			"D:/Shaders/DefaultPipeline/default.vertspv",
			"D:/Shaders/DefaultPipeline/default.fragspv" });

		// Input assembly
		VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

        VkVertexInputBindingDescription inputDesc = VKRHI_MeshRenderObject::GetInputBindingDescription();
		auto vertsInputDescs = VKRHI_MeshRenderObject::GetInputAttributeDescriptions();
		vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertsInputDescs.size());
		vertexInputInfo.pVertexAttributeDescriptions = vertsInputDescs.data();
		vertexInputInfo.vertexBindingDescriptionCount = 1;
		vertexInputInfo.pVertexBindingDescriptions = &inputDesc;

		//Pipeline layout
		std::array<VkDescriptorSetLayout, 3> sets =
		{
			_descSetLayouts.global.GetRHILayout(),
			_descSetLayouts.staticMesh.defaultMat.GetRHILayout(),
			_descSetLayouts.staticMesh.object.GetRHILayout()
		};

		VkPipelineLayoutCreateInfo layoutCreateInfo = {};
		layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		layoutCreateInfo.setLayoutCount = static_cast<uint32_t>(sets.size());
		layoutCreateInfo.pSetLayouts = sets.data();
		layoutCreateInfo.pushConstantRangeCount = 0;
		layoutCreateInfo.pPushConstantRanges = nullptr;

		if (vkCreatePipelineLayout(_logicalDevice.GetVkDevice(), &layoutCreateInfo, nullptr, &_availablePipelineLayouts.staticMeshLayout) != VK_SUCCESS)
		{
			ELOG(LogVulkan, "Couldn't create the pipeline layout.");
			return false;
		}

		VKRHI_BasePipelineCreateDesc baseTemplateDesc = {};
		baseTemplateDesc.surfaceExtents = _swapChain.GetSurfaceExtents();
		baseTemplateDesc.shaderModules = &baseShaderModules;
		baseTemplateDesc.renderPass = &_opaqueRenderPass;
		baseTemplateDesc.pipelineLayout = _availablePipelineLayouts.staticMeshLayout;

		VKRHI_PipelineDescriptor pipelineDesc;
		VKRHI_BasePipelineTemplate::CreateDefaultDescriptor(baseTemplateDesc, pipelineDesc);
		pipelineDesc.vertexInputInfo = vertexInputInfo;
        if (_forceWireframe)
            pipelineDesc.rasterizationCreateInfo.polygonMode = VK_POLYGON_MODE_LINE;
        //pipelineDesc.rasterizationCreateInfo.lineWidth = 3;
        //
        //std::array<VkDynamicState, 1> dynamicStates = {
		//    VK_DYNAMIC_STATE_LINE_WIDTH
	    //};
        //VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo = {};
	    //dynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	    //dynamicStateCreateInfo.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
	    //dynamicStateCreateInfo.pDynamicStates = dynamicStates.data();
      
        //pipelineDesc.pipelineInfo.pDynamicState = &dynamicStateCreateInfo;

		if (vkCreateGraphicsPipelines(_logicalDevice.GetVkDevice(), VK_NULL_HANDLE, 1, &pipelineDesc.pipelineInfo, nullptr, &_opaquePipeline) != VK_SUCCESS)
		{
			ELOG(LogVulkan, "Couldn't create the static mesh pipeline.");
			return false;
		}
	}

	// Terrain
	if (_terrainRO)
	{
        
        std::string terrainFragShaderName = "D:/Shaders/Terrain/terrain.fragspv";
        if (_terrainDebugDisplay)
            terrainFragShaderName = "D:/Shaders/Terrain/terrain_debug.fragspv";

        // Mesh Shader Terrain
        if (_useMeshShading)
        {
            std::vector<VkPipelineShaderStageCreateInfo> baseShaderModules = LoadShaders({
                "D:/Shaders/Terrain/terrain.taskspv",
                "D:/Shaders/Terrain/terrain.meshspv",
                terrainFragShaderName});

            //Pipeline layout
            std::array<VkDescriptorSetLayout, 4> sets =
            {
                _descSetLayouts.global.GetRHILayout(),
                _descSetLayouts.terrain.mat.GetRHILayout(),
                _descSetLayouts.terrain.object.GetRHILayout(),
                _descSetLayouts.terrain.meshShaderObject.GetRHILayout()
            };

            std::array<VkPushConstantRange, 0> ranges = {};
            //ranges[0].offset = 0;
            //ranges[0].size = 4;
            //ranges[0].stageFlags = VK_SHADER_STAGE_TASK_BIT_NV;

            VkPipelineLayoutCreateInfo layoutCreateInfo = {};
            layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
            layoutCreateInfo.setLayoutCount = static_cast<uint32_t>(sets.size());
            layoutCreateInfo.pSetLayouts = sets.data();
            layoutCreateInfo.pushConstantRangeCount = static_cast<uint32_t>(ranges.size());
            layoutCreateInfo.pPushConstantRanges = ranges.data();

            if (vkCreatePipelineLayout(_logicalDevice.GetVkDevice(), &layoutCreateInfo, nullptr, &_availablePipelineLayouts.meshShaderTerrainLayout) != VK_SUCCESS)
            {
                ELOG(LogVulkan, "Couldn't create the pipeline layout.");
                return false;
            }

            VKRHI_BasePipelineCreateDesc baseTemplateDesc = {};
            baseTemplateDesc.surfaceExtents = _swapChain.GetSurfaceExtents();
            baseTemplateDesc.shaderModules = &baseShaderModules;
            baseTemplateDesc.renderPass = &_opaqueRenderPass;
            baseTemplateDesc.pipelineLayout = _availablePipelineLayouts.meshShaderTerrainLayout;

            VKRHI_PipelineDescriptor pipelineDesc;
            VKRHI_BasePipelineTemplate::CreateDefaultDescriptor(baseTemplateDesc, pipelineDesc);
            pipelineDesc.rasterizationCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
            if (_forceWireframe)
                pipelineDesc.rasterizationCreateInfo.polygonMode = VK_POLYGON_MODE_LINE;

            if (vkCreateGraphicsPipelines(_logicalDevice.GetVkDevice(), VK_NULL_HANDLE, 1, &pipelineDesc.pipelineInfo, nullptr, &_meshShaderTerrainPipeline) != VK_SUCCESS)
            {
                ELOG(LogVulkan, "Couldn't create the tesrrain mesh shading pipeline.");
                return false;
            }

            std::vector<VkPipelineShaderStageCreateInfo> debugShaderModules = LoadShaders({
                "D:/Shaders/Terrain/terrain.taskspv",
                "D:/Shaders/Terrain/terrain.meshspv",
                "D:/Shaders/Terrain/terrain_debug.fragspv" });
            baseTemplateDesc.shaderModules = &debugShaderModules;
            VKRHI_BasePipelineTemplate::CreateDefaultDescriptor(baseTemplateDesc, pipelineDesc);
            pipelineDesc.rasterizationCreateInfo.lineWidth = 2;
            pipelineDesc.rasterizationCreateInfo.polygonMode = VK_POLYGON_MODE_LINE;

            std::array<VkDynamicState, 1> dynamicStates = {
		        VK_DYNAMIC_STATE_LINE_WIDTH
	        };
            VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo = {};
	        dynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	        dynamicStateCreateInfo.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
	        dynamicStateCreateInfo.pDynamicStates = dynamicStates.data();
      
            pipelineDesc.pipelineInfo.pDynamicState = &dynamicStateCreateInfo;

            if (vkCreateGraphicsPipelines(_logicalDevice.GetVkDevice(), VK_NULL_HANDLE, 1, &pipelineDesc.pipelineInfo, nullptr, &_terrainPipeline) != VK_SUCCESS)
            {
                ELOG(LogVulkan, "Couldn't create the tesrrain mesh shading pipeline.");
                return false;
            }
	    }
        else
        {
		    std::vector<VkPipelineShaderStageCreateInfo> baseShaderModules = LoadShaders({
			    "D:/Shaders/Terrain/terrain.vertspv",
			    "D:/Shaders/Terrain/terrain.tescspv",
			    "D:/Shaders/Terrain/terrain.tesespv",
			    terrainFragShaderName });

            VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
            // Input assembly
            vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

            std::vector<VkVertexInputAttributeDescription> vertsInputDescs = {};

            VkVertexInputAttributeDescription& coordDesc = vertsInputDescs.emplace_back();
            coordDesc.binding = 0;
            coordDesc.format = VK_FORMAT_R32G32B32A32_SFLOAT;
            coordDesc.location = 0;
            coordDesc.offset = offsetof(VKRHI_TerrainRenderVertData, objectSpaceCoords);
        
            VkVertexInputBindingDescription inputBindingDesc = {};
            inputBindingDesc.binding = 0;
            inputBindingDesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
            inputBindingDesc.stride = sizeof(VKRHI_TerrainRenderVertData);

            vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertsInputDescs.size());
            vertexInputInfo.pVertexAttributeDescriptions = vertsInputDescs.data();
            vertexInputInfo.vertexBindingDescriptionCount = 1;
            vertexInputInfo.pVertexBindingDescriptions = &inputBindingDesc;
		
		    //Pipeline layout
		    std::array<VkDescriptorSetLayout, 3> sets =
		    {
			    _descSetLayouts.global.GetRHILayout(),
			    _descSetLayouts.terrain.mat.GetRHILayout(),
			    _descSetLayouts.terrain.object.GetRHILayout()
		    };

		    VkPipelineLayoutCreateInfo layoutCreateInfo = {};
		    layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		    layoutCreateInfo.setLayoutCount = static_cast<uint32_t>(sets.size());
		    layoutCreateInfo.pSetLayouts = sets.data();
		    layoutCreateInfo.pushConstantRangeCount = 0;
		    layoutCreateInfo.pPushConstantRanges = nullptr;

		    if (vkCreatePipelineLayout(_logicalDevice.GetVkDevice(), &layoutCreateInfo, nullptr, &_availablePipelineLayouts.terrainLayout) != VK_SUCCESS)
		    {
			    ELOG(LogVulkan, "Couldn't create the pipeline layout.");
			    return false;
		    }

		    VKRHI_BasePipelineCreateDesc baseTemplateDesc = {};
		    baseTemplateDesc.surfaceExtents = _swapChain.GetSurfaceExtents();
		    baseTemplateDesc.shaderModules = &baseShaderModules;
		    baseTemplateDesc.renderPass = &_opaqueRenderPass;
		    baseTemplateDesc.pipelineLayout = _availablePipelineLayouts.terrainLayout;

		    VKRHI_PipelineDescriptor pipelineDesc;
		    VKRHI_BasePipelineTemplate::CreateDefaultDescriptor(baseTemplateDesc, pipelineDesc);
		    pipelineDesc.vertexInputInfo = vertexInputInfo;

		    VkPipelineTessellationStateCreateInfo tesselationCreateInfo = {};
            tesselationCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
		    tesselationCreateInfo.patchControlPoints = 1;

		    pipelineDesc.pipelineInfo.pTessellationState = &tesselationCreateInfo;
		    pipelineDesc.vertexInputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
            if (_forceWireframe)
		        pipelineDesc.rasterizationCreateInfo.polygonMode = VK_POLYGON_MODE_LINE;

		    if (vkCreateGraphicsPipelines(_logicalDevice.GetVkDevice(), VK_NULL_HANDLE, 1, &pipelineDesc.pipelineInfo, nullptr, &_terrainPipeline) != VK_SUCCESS)
		    {
			    ELOG(LogVulkan, "Couldn't create the terrain pipeline.");
			    return false;
		    }
        }
    }

	// Cubemap
    if (_cubemapRO)
	{
		std::vector<VkPipelineShaderStageCreateInfo> cubemapShaderModules = LoadShaders({
			"D:/Shaders/Cubemap/cubemap.vertspv",
			"D:/Shaders/Cubemap/cubemap.fragspv" });
		
		// Input assembly
		VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

        VkVertexInputBindingDescription inputDesc = VKRHI_MeshRenderObject::GetInputBindingDescription();
		auto vertsInputDescs = VKRHI_MeshRenderObject::GetInputAttributeDescriptions(VKRHI_MeshRenderObject::AttributeBit::Position);
		vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertsInputDescs.size());
		vertexInputInfo.pVertexAttributeDescriptions = vertsInputDescs.data();
		vertexInputInfo.vertexBindingDescriptionCount = 1;
		vertexInputInfo.pVertexBindingDescriptions = &inputDesc;

		//Pipeline layout
		std::array<VkDescriptorSetLayout, 2> sets =
		{
			_descSetLayouts.global.GetRHILayout(),
			_descSetLayouts.cubemap.mat.GetRHILayout(),
		};

		VkPipelineLayoutCreateInfo layoutCreateInfo = {};
		layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		layoutCreateInfo.setLayoutCount = static_cast<uint32_t>(sets.size());
		layoutCreateInfo.pSetLayouts = sets.data();
		layoutCreateInfo.pushConstantRangeCount = 0;
		layoutCreateInfo.pPushConstantRanges = nullptr;

		if (vkCreatePipelineLayout(_logicalDevice.GetVkDevice(), &layoutCreateInfo, nullptr, &_availablePipelineLayouts.cubemapLayout) != VK_SUCCESS)
		{
			ELOG(LogVulkan, "Couldn't create the pipeline layout.");
			return false;
		}

		VKRHI_BasePipelineCreateDesc baseTemplateDesc = {};
		baseTemplateDesc.surfaceExtents = _swapChain.GetSurfaceExtents();
		baseTemplateDesc.shaderModules = &cubemapShaderModules;
		baseTemplateDesc.renderPass = &_opaqueRenderPass;
		baseTemplateDesc.pipelineLayout = _availablePipelineLayouts.cubemapLayout;

		VKRHI_PipelineDescriptor pipelineDesc;
		VKRHI_BasePipelineTemplate::CreateDefaultDescriptor(baseTemplateDesc, pipelineDesc);

		pipelineDesc.rasterizationCreateInfo.cullMode = VK_CULL_MODE_FRONT_BIT;
		pipelineDesc.depthStencilInfo.depthWriteEnable = false;
		pipelineDesc.depthStencilInfo.depthTestEnable = false;

		pipelineDesc.vertexInputInfo = vertexInputInfo;

		pipelineDesc.pipelineInfo.stageCount = static_cast<uint32_t>(cubemapShaderModules.size());
		pipelineDesc.pipelineInfo.pStages = cubemapShaderModules.data();

		pipelineDesc.pipelineInfo.basePipelineHandle = _opaquePipeline;
		pipelineDesc.pipelineInfo.basePipelineIndex = -1;

		if (vkCreateGraphicsPipelines(_logicalDevice.GetVkDevice(), VK_NULL_HANDLE, 1, &pipelineDesc.pipelineInfo, nullptr, &_cubemapPipeline) != VK_SUCCESS)
		{
			ELOG(LogVulkan, "Couldn't create the cubemap pipeline.");
			return false;
		}
	}

	return true;
}

void HelloTriangleApplication::FillFrameDescriptor(VKRHI_FrameDescriptor& descriptor)
{
    descriptor.commandPool = _commandPool;
    descriptor.descriptorPool = _descriptorPool;
    descriptor.opaqueRenderPass = &_opaqueRenderPass;
    descriptor.editorRenderPass = &_editorRenderPass;
    descriptor.swapChain = &_swapChain;
    descriptor.depthBuffer = &_depthBuffer;
    descriptor.availablePipelineLayouts = _availablePipelineLayouts;
    descriptor.descSetLayouts = &_descSetLayouts;
    descriptor.opaquePipeline = _opaquePipeline;
    descriptor.terrainPipeline = _terrainPipeline;
    descriptor.meshShaderTerrainPipeline = _meshShaderTerrainPipeline;
    descriptor.cubemapPipeline = _cubemapPipeline;
    descriptor.graphMemAllocator = &_graphMemAllocator;

    descriptor.scene = _scene.get();
    descriptor.vertexBuffers = &_vertexBuffers;
    descriptor.indexBuffers = &_indexBuffers;
    descriptor.meshesRO = &_meshesRO;
    descriptor.terrainRO = _terrainRO ? &_terrainRO.value() : nullptr;
    descriptor.cubemapRO = _cubemapRO ? &_cubemapRO.value() : nullptr;
    descriptor.images = &_images;
    descriptor.imageMemBuffers = &_imageMemBuffers;

    descriptor.renderingOptions.useMeshShading = _useMeshShading;
	descriptor.renderingOptions.subPatchTessellation = static_cast<uint32_t>(_shaderCompilationContext.GetIntGlobalMacros().at("KU_SUBPATCH_TESSELLATION"));
}

bool HelloTriangleApplication::CreateFrames()
{
	auto imageViews = _swapChain.GetImageViews();
	size_t imageCount = imageViews.size();
	_frames.resize(imageCount);

    VKRHI_FrameDescriptor descriptor{};
    FillFrameDescriptor(descriptor);
	
	for (size_t i = 0; i < imageCount; i++)
	{
		descriptor.imageIndex = static_cast<uint32_t>(i);

		_frames[i].FromVkFrameDescriptor(_logicalDevice, descriptor);
	}
	return true;
}

bool HelloTriangleApplication::CreateCommandPool()
{
	QueueFamilyIndices indices = _mainPhysicalDevice->GetQueueFamilyIndices();
	VkCommandPoolCreateInfo poolCreateInfo = {};
	poolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolCreateInfo.queueFamilyIndex = indices.graphicsFamily.value();
	poolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	if (vkCreateCommandPool(_logicalDevice.GetVkDevice(), &poolCreateInfo, nullptr, &_commandPool) != VK_SUCCESS)
	{
		ELOG(LogVulkan, "Couldn't create the command pool.");
		return false;
	}

	return true;
}

bool HelloTriangleApplication::InitShaderCompilation()
{
    ShaderCompiler::InitDesc desc{};
    desc.application = this;
    desc.resourceMgr = &_resourceMgr;
    desc.defaultCompileOptions.includePath = "D:/Shaders/include";
    desc.defaultCompileOptions.macroList = {};

    _shaderCompiler.FromDesc(desc);

	return true;
}

void HelloTriangleApplication::DestroyPipelines()
{
	if (_opaquePipeline)
    {
		vkDestroyPipeline(_logicalDevice.GetVkDevice(), _opaquePipeline, nullptr);
        _opaquePipeline = VK_NULL_HANDLE;
    }

	if (_terrainPipeline)
    {
		vkDestroyPipeline(_logicalDevice.GetVkDevice(), _terrainPipeline, nullptr);
        _terrainPipeline = VK_NULL_HANDLE;
    }
    
	if (_meshShaderTerrainPipeline)
    {
		vkDestroyPipeline(_logicalDevice.GetVkDevice(), _meshShaderTerrainPipeline, nullptr);
        _meshShaderTerrainPipeline = VK_NULL_HANDLE;
    }

	if (_cubemapPipeline)
    {
		vkDestroyPipeline(_logicalDevice.GetVkDevice(), _cubemapPipeline, nullptr);
        _cubemapPipeline = VK_NULL_HANDLE;
    }
}

void HelloTriangleApplication::SetupDebugCallback()
{
	if (!_enableDebugLayer)
		return;

	VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	createInfo.pfnUserCallback = DebugCallback;
	createInfo.pUserData = nullptr;

	if (CreateDebugUtilsMessengerEXT(_vkInstance, &createInfo, nullptr, &_debugCallback) != VK_SUCCESS)
		ELOG(LogVulkan, "Couldn't create the debug messenger");
}

void HelloTriangleApplication::Update()
{
	if (ShouldShutdown())
		return;

	KU_ASSERT(_window);
	if (!_window->ShouldClose())
	{
		Profiler& profiler = Profiler::Get();
		Profiler::FrameHandle profilerFrameHandle;
		if (_bProfilerUIRequestEndProfiling)
		{
			_bProfilerRecording = false;
			_bProfilerUIRequestEndProfiling = false;
			_profilerUI.AddDisplayedProfileData(_recordingProfileData);
			_recordingProfileData = {};
		}

		if (_bProfilerUIRequestStartProfiling)
		{
			_bProfilerUIRequestStartProfiling = false;
			_bProfilerRecording = true;
			_recordingProfileData = _resourceMgr.CreateResource<ProfileData>();
		}

		bool bProfilerRecordFrame = false;
		if (_bProfilerRecording)
		{
			bProfilerRecordFrame = true;
			profilerFrameHandle = profiler.NewFrame();
			profiler.StartRecord(profilerFrameHandle);
		}

		{
			KU_CPU_SCOPED_QUERY(WaitRender);
			size_t finalMaxFrameInFlight = GetMaxAquiredFrames();
			if (_maxFramesInFlight > 0)
				finalMaxFrameInFlight = static_cast<size_t>(_maxFramesInFlight);

			while (_inFlightFrames.size() >= finalMaxFrameInFlight)
			{
				// TODO Present only last frame
				GetFrame(_inFlightFrames.front()).Present();
				_inFlightFrames.pop();
			}
		}

		{
			KU_CPU_SCOPED_QUERY(WaitMaxFrameRate);
			if (_targetFrameRate != 0)
				_time.WaitFrameTime(std::chrono::microseconds((int)round(1000000.0 / _targetFrameRate)));
		}

		{
			KU_CPU_SCOPED_QUERY(PollEvent);
			_inputMgr.PollEvents();
		}

		if (_requestFrameRefresh)
		{
			KU_CPU_SCOPED_QUERY(RenderingRefresh);
			RefreshSceneRendering();
			_requestFrameRefresh = false;
		}

#ifdef INCLUDE_EDITOR
		{
			KU_CPU_SCOPED_QUERY(EditorNewFrame);

			ImGui_ImplVulkan_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();
		}
#endif // INCLUDE_EDITOR

		{
			KU_CPU_SCOPED_QUERY(FrameConstruction);
            {
			
				KU_CPU_SCOPED_QUERY(Update);
				_time.Update();

		        float deltaTime = _time.GetDeltaTime();
        #ifdef INCLUDE_EDITOR
		        EditorUpdate(deltaTime);
        #endif // INCLUDE_EDITOR

				// TODO - temporary - move it somewhere else
				RenderSystem::RenderingCapabilities capabilities;
				if (_useMeshShading)
					capabilities.maxTessellation = static_cast<uint32_t>(glm::sqrt(_shaderCompilationContext.GetIntGlobalMacros().at("KU_MAX_TASK_OUTPUT"))) * _shaderCompilationContext.GetIntGlobalMacros().at("KU_MAX_MESH_TESSELLATION");
				else
					capabilities.maxTessellation = 64;
				_renderSystem.UpdateRenderingCapabilities(capabilities);

		        SimulationUpdate(deltaTime);
            }

            {
				uint32_t currentImageID;
				{
					KU_CPU_SCOPED_QUERY(AquireNextImage);
					KU_VERIFY(_swapChain.AquireNextImage(_backbufferDrawableSemaphore, currentImageID));
				}
				{
					KU_CPU_SCOPED_QUERY(WaitFrameRender);
					_frames[currentImageID].WaitDraw();
				}

				{
					KU_CPU_SCOPED_QUERY(RenderRecord);
#ifdef INCLUDE_EDITOR
					ImGui::Render();
#endif // INCLUDE_EDITOR

					DrawFrame(currentImageID, _backbufferDrawableSemaphore);
					_inFlightFrames.push(currentImageID);

					if (_bImmediatlyWaitRender)
					{
						KU_CPU_SCOPED_QUERY(ImmediateWaitRender);
						_frames[currentImageID].WaitDraw();
					}
				}
			}
		}
		if (bProfilerRecordFrame)
		{
			Profiler::Get().EndRecord();
			Profiler::Get().ResolveGPURanges(profilerFrameHandle, _logicalDevice);
			
			_recordingProfileData->AddDump(Profiler::Get().PopFrame());
		}
	}
	else
	{
		SetShouldShutdown(true);
		return;
	}
}

void HelloTriangleApplication::EditorUpdate(float deltaTime)
{
#ifdef INCLUDE_EDITOR
	//if (_isInEditMode)
	//	ImGui::ShowDemoWindow();

	if (ImGui::BeginMainMenuBar())
	{
		ImGui::Checkbox("Show InGame", &_showGuiOutOfEditor);

		if (ImGui::Button("Resource Editor"))
            _showResEditor = true;

		ImGui::SameLine();
		if (ImGui::Button("Profiler"))
			_profilerUI.SetOpen(true);

		ImGui::EndMainMenuBar();
	}

	ImGui::Begin("Scene Properties Editor");
	

	auto frameTimeHistory = _time.GetTimeHistory();
	static constexpr size_t _statRange = 100;
	_timeStats.Update(frameTimeHistory.end() - _statRange, frameTimeHistory.end());
	ImGui::Text("%0.2f ms (%0.2f fps)              median : %0.2f [%0.2f, %0.2f]", _timeStats.GetMean() * 1000.f, 1.f / _timeStats.GetMean(), _timeStats.GetMedian() * 1000.f, _timeStats.GetMin() * 1000.f, _timeStats.GetMax() * 1000.f);
	float totalMin = *std::min_element(frameTimeHistory.begin(), frameTimeHistory.end());
	float plotMax = ceilf((1.f / totalMin) / 60.f) * 60.f;
	ImGui::PlotHistogram("FPS", [](void* data, int idx){ return 1.f / ((float*)data)[idx]; }, frameTimeHistory.data(), static_cast<int>(frameTimeHistory.size()), 0, nullptr, 0.0f, plotMax, ImVec2(0, 80));

	if (ImGui::CollapsingHeader("Scene"))
	{
		_scene->UpdatePropsEditor(deltaTime);
	}

    if (ImGui::CollapsingHeader("Rendering"))
    {
        ImGui::Checkbox("Force wireframe", &_forceWireframe);
        ImGui::Checkbox("Terrain debug display", &_terrainDebugDisplay);
        ImGui::Checkbox("Force solid clear", &_forceSolidClear);

        if (_forceSolidClear)
            ImGui::ColorEdit3("Clear color", glm::value_ptr(_solidClearColor));
	
        if (_logicalDevice.IsMeshShadingEnabled())
        {
            ImGui::Checkbox("Use Mesh Shading", &_useMeshShading);
        }
        else
        {
            ImGui::Text("Mesh Shading Unavailable");
        } 

		ImGui::SliderInt("Target Frame Rate", &_targetFrameRate, 0, 120);
		ImGui::InputInt("Max Backbuffer Count", &_maxBackBufferCount);
		glm::ivec2 surfaceMinMax = glm::ivec2(_swapChain.GetSurfaceImageCountMinMax());
		_maxBackBufferCount = std::max(std::min(_maxBackBufferCount, surfaceMinMax.y), surfaceMinMax.x);

		ImGui::Checkbox("Immediatly Wait Render", &_bImmediatlyWaitRender);
		bool bUseMaxFramesInFlight = _maxFramesInFlight != -1;
		if (ImGui::Checkbox("Custom max frame in flight", &bUseMaxFramesInFlight))
			_maxFramesInFlight = bUseMaxFramesInFlight ? 1 : -1;
		if (bUseMaxFramesInFlight)
		{
			ImGui::InputInt("Max frame in flight", &_maxFramesInFlight);
			_maxFramesInFlight = std::max(std::min(_maxFramesInFlight, static_cast<int>(GetMaxAquiredFrames())), 1);
		}

		if (ImGui::TreeNode("Global Shader Macros"))
		{
			auto&& baseMacros = _baseShaderCompilationContext.GetIntGlobalMacros();
			auto&& macros = _shaderCompilationContext.GetIntGlobalMacros();
			for (auto&& [name, value] : macros)
			{
				int newValue = value;
				if (ImGui::InputInt(name.c_str(), &newValue, 1, 100))
				{
					_shaderCompilationContext.SetGlobalMacro(name, std::clamp(newValue, 0, baseMacros.at(name)));
				}
			}
			ImGui::TreePop();
		}
    }
    // Resource editor
    if (_showResEditor)
    {
        if (_addedResources.size())
        {
            _resourceEditor.LoadResources(_addedResources);
            _addedResources.clear();
        }
        _showResEditor = _resourceEditor.Draw();
    }

	// profiler
	_profilerUI.Update(_recordingProfileData);
	_profilerUI.Draw();

	ImGui::End();
#endif // INCLUDE_EDITOR
}

void HelloTriangleApplication::SimulationUpdate(float deltaTime)
{
    KU_CPU_SCOPED_QUERY(SceneUpdate);
    _scene->Update(deltaTime);
}

void HelloTriangleApplication::ResolveFrame(size_t frameID)
{

}

void HelloTriangleApplication::DrawFrame(size_t frameID, VKRHI_Semaphore const& backbufferDrawableSemaphore)
{
	FrameRenderContext context = {};
    FillFrameDescriptor(context.frameDescriptor);
#ifdef INCLUDE_EDITOR
	context.shouldDrawEditor = _showGuiOutOfEditor || _isInEditMode;
#endif // INCLUDE_EDITOR
    context.transferQueue = &_transferQueue;
	context.backbufferDrawableSemaphore = &backbufferDrawableSemaphore;

    VKRHI_Frame& frame = GetFrame( frameID );
    frame.Draw(context);
}

void HelloTriangleApplication::FlushRendering()
{
	// Render only last frame
	while (_inFlightFrames.size())
	{
		GetFrame(_inFlightFrames.front()).Present();
		_inFlightFrames.pop();
	}
	WaitRenderDevice();
}

void HelloTriangleApplication::WaitRenderDevice()
{
	_logicalDevice.Wait();
}

void HelloTriangleApplication::Shutdown()
{
	_time.Shutdown();

	_profilerUI.Shutdown();

#ifdef INCLUDE_EDITOR
    _resourceEditor.Shutdown();
#endif // INCLUDE_EDITOR

	_scene->Shutdown();
	_resourceMgr.ReleaseAllResources();

	DestroyDynamicRenderingObjects();

	_descSetLayouts.Shutdown();
	DestroyTextureSamplers();

	if (_commandPool)
		vkDestroyCommandPool(_logicalDevice.GetVkDevice(), _commandPool, nullptr);

	_transferQueue.Shutdown();

	_backbufferDrawableSemaphore.Shutdown();
	_graphMemAllocator.Shutdown();
	_logicalDevice.Shutdown();

	if (_surface)
		vkDestroySurfaceKHR(_vkInstance, _surface, nullptr);

	if (_enableDebugLayer)
		DestroyDebugUtilsMessengerEXT(_vkInstance, _debugCallback, nullptr);

	_inputMgr.Shutdown();
	_rawInputContext.Shutdown();

	ShutdownImGUI();

	if (_window)
		_window->Close();

	if (_vkInstance)
		vkDestroyInstance(_vkInstance, nullptr);

    _shaderCompiler.Shutdown();

	_resourceMgr.Shutdown();
    _resourceLibrary.Shutdown();
	_hierarchicalMemory.Shutdown();

    Profiler::Get().Shutdown();

	LOG(LogVulkan, "Vulkan Instance Shutdown");
}

bool HelloTriangleApplication::ShouldShutdown() const
{
	return _isShutdownPending;
}

void HelloTriangleApplication::SetShouldShutdown(bool value)
{
	_isShutdownPending = value;
}

void HelloTriangleApplication::DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT callback, const VkAllocationCallbacks* pAllocator) 
{
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");

	if (func != nullptr) {
		func(instance, callback, pAllocator);
	}

}

VkResult HelloTriangleApplication::CreateDebugUtilsMessengerEXT(
	VkInstance instance, 
	const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, 
	const VkAllocationCallbacks* pAllocator, 
	VkDebugUtilsMessengerEXT* pCallback)
{
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
	if (func != nullptr) {
		return func(instance, pCreateInfo, pAllocator, pCallback);
	}
	else {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}

VKAPI_ATTR VkBool32 VKAPI_CALL HelloTriangleApplication::DebugCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType,
	const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void* pUserData)
{
	const char* message = pCallbackData->pMessage;

	switch (messageSeverity)
	{
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
		VLOG(LogVulkan, message);
		break;
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
		LOG(LogVulkan, message);
		break;
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
		WLOG(LogVulkan, message);
		break;
	case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		ELOG(LogVulkan, message);
		break;
	default:
		break;
	}

	return VK_FALSE;
}

bool HelloTriangleApplication::RegisterCubemapForRendering(SC::Cubemap const* cubemap)
{
	KU_ASSERT(cubemap);
	Handle<CubemapImage> cubemapImage = cubemap->GetImage();
	if (!cubemapImage || !cubemapImage->IsComplete())
	{
		KU_ASSERT(0);
		return false;
	}

	VKRHI_CubemapRenderObjectDesc cubemapRODesc = {};

	if (!RegisterMesh(cubemap->GetMesh(), cubemapRODesc.meshRODesc))
	{
		ELOG(LogVulkan, "Couldn't register the Cubemap mesh for rendering.");
		return false;
	}

	if (cubemap->GetImage())
	{
		if (!RegisterCubemapImage(cubemap->GetImage(), cubemapRODesc.imageBufferKey))
		{
			ELOG(LogVulkan, "Couldn't register the Cubemap image for rendering.");
			return false;
		}
	}

    _cubemapRO.emplace();
	_cubemapRO->FromDesc(cubemapRODesc);

	return true;
}

bool HelloTriangleApplication::RegisterTerrainForRendering(SC::Terrain const* terrain)
{
    KU_ASSERT(terrain);
    VKRHI_TerrainRenderObjectDesc terrainRODesc = {};

    // Terrain patch data
    {
        /*Handle<Mesh> mesh = _resourceMgr.Get<Mesh>("M:/" HM_RES_MESH_PATH "/Test/16x16TesGrid.raw");
        KU_ASSERT(mesh);
        RegisterMesh(mesh, terrainRODesc.meshRODesc);*/
    
        /*int precision = 1024;
        int patchCount = precision * precision;
        std::vector<VKRHI_TerrainRenderVertData> data(patchCount);

        float size = 1.f / precision;
        int depth = static_cast<int>(log2(precision));
        VKRHI_TerrainRenderVertData vertData;

        for (int i = 0; i < precision; i++)
        {
            for (int j = 0; j < precision; j++)
            {
                data[i * precision + j].objectSpaceCoords = glm::vec4(i / static_cast<float>(precision), j / static_cast<float>(precision), size, depth);
            }
        }*/

        /*
        auto const& tree = terrain->GetQuadTree();
        int patchCount = tree.GetLeafCount();
        std::vector<VKRHI_TerrainRenderVertData> data(patchCount);

        int i = 0;
        for (auto&& nodeData : tree)
        {
            glm::vec4 coords (SC::Terrain::QuadTreeDescriptor::Get2DCoords(nodeData.key), 1.f / glm::pow(2.f, nodeData.key.depth), nodeData.key.depth);
            data[i].objectSpaceCoords = coords;
            i++;
        }
        KU_ASSERT(i == tree.GetLeafCount());

        IKey<std::vector<VKRHI_Buffer>> key{ _vertexBuffers.size(), _vertexBuffers };
        VKRHI_Buffer& buffer = _vertexBuffers.emplace_back();

        PrepareBuffer(buffer, patchCount * sizeof(VKRHI_TerrainRenderVertData), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);
        VKRHI_UpdateBufferMemoryDesc desc = {};
        desc.source = { data.data() };
        desc.totalSize = patchCount * sizeof(VKRHI_TerrainRenderVertData);
        _transferQueue.UpdateBufferMemory(buffer, desc);

        terrainRODesc.patchBuffer = key;
        */
    }

    // Textures
	KU_ASSERT(terrain->GetHeightMap());
	if (!RegisterTexture(terrain->GetHeightMap(), terrainRODesc.heightTextureKey, _terrainHeightmapSampler))
	{
		ELOG(LogVulkan, "Couldn't register the terrain heighmap for rendering.");
		return false;
	}

	KU_ASSERT(terrain->GetGrassTexture());
	if (!RegisterTexture(terrain->GetGrassTexture(), terrainRODesc.grassTextureKey))
	{
		ELOG(LogVulkan, "Couldn't register the terrain grass texture for rendering.");
		return false;
	}

	KU_ASSERT(terrain->GetSnowTexture());
	if (!RegisterTexture(terrain->GetSnowTexture(), terrainRODesc.snowTextureKey))
	{
		ELOG(LogVulkan, "Couldn't register the terrain snow texture for rendering.");
		return false;
	}

	KU_ASSERT(terrain->GetCliffTexture());
	if (!RegisterTexture(terrain->GetCliffTexture(), terrainRODesc.cliffTextureKey))
	{
		ELOG(LogVulkan, "Couldn't register the terrain cliff texture for rendering.");
		return false;
	}
	
	_terrainRO.emplace();
	_terrainRO->FromDesc(terrainRODesc);

	return true;
}

bool HelloTriangleApplication::RegisterStaticMeshForRendering(SC::StaticMesh const* staticMesh)
{
	KU_ASSERT(staticMesh);
	if (!staticMesh->GetMesh())
		return false;

	VKRHI_StaticMeshRenderObjectDesc meshRODesc = {};

	if (!RegisterMesh(staticMesh->GetMesh(), meshRODesc.meshRODesc))
	{
		ELOG(LogVulkan, "Couldn't register mesh for rendering.");
		return false;
	}

	if (staticMesh->GetTexture())
	{
		if (!RegisterTexture(staticMesh->GetTexture(), meshRODesc.textureBufferKey))
		{
			ELOG(LogVulkan, "Couldn't register the mesh texture for rendering.");
			return false;
		}
	}

	VKRHI_StaticMeshRenderObject& meshRO = _meshesRO.emplace_back();
	meshRO.FromDesc(meshRODesc);

	return true;
}

bool HelloTriangleApplication::RegisterMesh(Handle<Mesh> mesh, VKRHI_MeshRenderObjectDesc& inOutDesc)
{
	MeshData const& meshData = mesh->GetMeshData();

	size_t vertsSize = meshData.GetVertsDataByteSize();
	inOutDesc.vertexBufferKey = RegisterMeshBuffer(_vertexBuffers, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, meshData.vertices.data(), vertsSize);
	inOutDesc.verticesCount = meshData.vertices.size();

	size_t idsSize = meshData.GetIndicesDataByteSize();
	inOutDesc.indexBufferKey = RegisterMeshBuffer(_indexBuffers, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, meshData.indices.data(), idsSize);
	inOutDesc.indicesCount = meshData.indices.size();

	return true;
}

IKey<std::vector<VKRHI_Buffer>> HelloTriangleApplication::RegisterMeshBuffer(std::vector<VKRHI_Buffer>& bufferList, VkBufferUsageFlags flags, void const* data, size_t dataSize)
{
	IKey<std::vector<VKRHI_Buffer>> key{ bufferList.size(), bufferList };

	VKRHI_Buffer& buffer = bufferList.emplace_back();

	PrepareBuffer(buffer, dataSize, flags);
    VKRHI_UpdateBufferMemoryDesc desc = {};
    desc.source = { data };
    desc.totalSize = dataSize;
	_transferQueue.UpdateBufferMemory(buffer, desc);

	return key;
}

void HelloTriangleApplication::PrepareBuffer(VKRHI_Buffer& buffer, size_t bufferSize, VkBufferUsageFlags flags)
{
	BufferDesc vBufferDesc = {};
	vBufferDesc.allocator = &_graphMemAllocator;
	vBufferDesc.size = bufferSize;
	vBufferDesc.usage = flags;
	vBufferDesc.properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

	buffer.FromDesc(_logicalDevice, vBufferDesc);
}

void HelloTriangleApplication::GetDefaultImageDesc(glm::uvec2 extents, ImageMemoryDesc& memDesc, ImageDesc& imageDesc)
{
	VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;

	memDesc.allocator = &_graphMemAllocator;
	memDesc.extent = extents;
	memDesc.layerCount = 1;
	memDesc.format = format;
	memDesc.tiling = VK_IMAGE_TILING_OPTIMAL;
	memDesc.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
	memDesc.memProperties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

	imageDesc.format = VK_FORMAT_R8G8B8A8_UNORM;
	imageDesc.sampler = _defaultSampler;
	imageDesc.aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT;
	imageDesc.type = VK_IMAGE_VIEW_TYPE_2D;
	imageDesc.layerCount = 1u;
}

bool HelloTriangleApplication::RegisterTexture(Handle<Texture> texture, TextureKeys& OutTexture, VkSampler sampler)
{
	KU_ASSERT(texture);

	if (texture->GetAlbedo())
	{
		if (!RegisterImage(texture->GetAlbedo(), OutTexture.albedo, sampler))
		{
			ELOG(LogVulkan, "Couldn't register the albedo image for rendering.");
			return false;
		}
	}

	if (texture->GetHeightMap())
	{
		if (!RegisterImage(texture->GetHeightMap(), OutTexture.heightMap, sampler))
		{
			ELOG(LogVulkan, "Couldn't register the heighmap image for rendering.");
			return false;
		}
	}

	if (texture->GetNormalMap())
	{
		if (!RegisterImage(texture->GetNormalMap(), OutTexture.normalMap, sampler))
		{
			ELOG(LogVulkan, "Couldn't register the normal map image for rendering.");
			return false;
		}
	}

	return true;
}

bool HelloTriangleApplication::RegisterImage(Handle<Image> image, ImageKey& OutImage, VkSampler sampler)
{
	ImageKey imageKey{ _images.size(), _images };

	glm::uvec2 extents = image->GetExtent();

	ImageMemoryDesc imageMemDesc = {};
	ImageDesc imgDesc = {};
	GetDefaultImageDesc(extents, imageMemDesc, imgDesc);
	imageMemDesc.usage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
	imageMemDesc.mipmapCount = static_cast<uint32_t>(std::floor(std::log2(std::max(extents.x, extents.y)))) + 1;

	VKRHI_ImageMemory& imageMem = _imageMemBuffers.emplace_back();
	imageMem.FromDesc(_logicalDevice, imageMemDesc);

	VKRHI_Image& rhiImage = _images.emplace_back();
	if (sampler)
		imgDesc.sampler = sampler;
	rhiImage.FromImageMemory(_logicalDevice, imageMem, imgDesc);

    VKRHI_UpdateImageMemoryDesc desc = {};
    desc.source = { image->GetBuffer() };
    desc.totalSize = image->GetByteSize();
    desc.format = imgDesc.format;
    desc.layoutIn = VK_IMAGE_LAYOUT_UNDEFINED;
    desc.layoutOut = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    _transferQueue.UpdateImageMemory(imageMem, desc);

	OutImage = imageKey;

	return true;
}

bool HelloTriangleApplication::RegisterCubemapImage(Handle<CubemapImage> cubemapImage, ImageKey& OutTexture)
{
	ImageKey imageKey{ _images.size(), _images };

	ImageMemoryDesc desc = {};
	ImageDesc imgDesc = {};
	GetDefaultImageDesc(cubemapImage->GetExtent(), desc, imgDesc);

	VKRHI_ImageMemory& imageMem = _imageMemBuffers.emplace_back();
	desc.layerCount = 6;
	desc.flags |= VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
	imageMem.FromDesc(_logicalDevice, desc);

	VKRHI_Image& rhiTexture = _images.emplace_back();
	imgDesc.layerCount = 6;
	imgDesc.type = VK_IMAGE_VIEW_TYPE_CUBE;
	imgDesc.sampler = _cubemapSampler;
	rhiTexture.FromImageMemory(_logicalDevice, imageMem, imgDesc);

	VKRHI_Buffer stageBuffer;
	std::vector<void const*> images;
	for (size_t i = 0; i < 6; i++)
		images.push_back(cubemapImage->GetImage(i)->GetBuffer());

    VKRHI_UpdateImageMemoryDesc updateDesc = {};
    updateDesc.layoutIn = VK_IMAGE_LAYOUT_UNDEFINED;
    updateDesc.layoutOut = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    updateDesc.layerCount = 6;
    updateDesc.format = desc.format;
    updateDesc.source = images;
    updateDesc.totalSize = cubemapImage->GetImage(0)->GetByteSize();
    updateDesc.bGenerateMipmaps = false;
    _transferQueue.UpdateImageMemory(imageMem, updateDesc);

	OutTexture = imageKey;

	return true;
}

void HelloTriangleApplication::ClearMeshData()
{
	for (auto&& ro : _meshesRO)
		ro.Shutdown();
	_meshesRO.clear();

	_terrainRO->Shutdown();
	_terrainRO.reset();

    if (_cubemapRO)
        _cubemapRO->Shutdown();
	_cubemapRO.reset();

	for (auto&& buffer : _vertexBuffers)
		buffer.Shutdown();
	_vertexBuffers.clear();

	for (auto&& buffer : _indexBuffers)
		buffer.Shutdown();
	_indexBuffers.clear();

	for (auto&& image : _images)
		image.Shutdown();
	_images.clear();

	for (auto&& buffer : _imageMemBuffers)
		buffer.Shutdown();
	_imageMemBuffers.clear();
}

bool HelloTriangleApplication::CreateDescriptorSetLayouts()
{
	// Global
	KU_VERIFY(_descSetLayouts.global.FromBindings(&_logicalDevice, {
		{0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_ALL},
		{1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_ALL},
		{2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_ALL}
	}));

	// Static Mesh
	KU_VERIFY(_descSetLayouts.staticMesh.object.FromBindings(&_logicalDevice, {
		{0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT}
	}));

	KU_VERIFY(_descSetLayouts.staticMesh.defaultMat.FromBindings(&_logicalDevice, {
		{0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT}
	}));

	// Cubemap
	KU_VERIFY(_descSetLayouts.cubemap.mat.FromBindings(&_logicalDevice, {
		{0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT}
	}));

	// Terrain
	KU_VERIFY(_descSetLayouts.terrain.object.FromBindings(&_logicalDevice, {
        {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT | VK_SHADER_STAGE_MESH_BIT_NV | VK_SHADER_STAGE_TASK_BIT_NV }
	}));

    KU_VERIFY(_descSetLayouts.terrain.meshShaderObject.FromBindings(&_logicalDevice, {
        {0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_TASK_BIT_NV }
        }));

	KU_VERIFY(_descSetLayouts.terrain.mat.FromBindings(&_logicalDevice, {
	    {0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 2, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_MESH_BIT_NV | VK_SHADER_STAGE_TASK_BIT_NV },
	    {1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 2, VK_SHADER_STAGE_FRAGMENT_BIT},
	    {2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 2, VK_SHADER_STAGE_FRAGMENT_BIT},
	    {3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 2, VK_SHADER_STAGE_FRAGMENT_BIT},
	    {4, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT | VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_MESH_BIT_NV | VK_SHADER_STAGE_TASK_BIT_NV }
	}));
	return true;
}

bool HelloTriangleApplication::InitImGUI()
{
#ifdef INCLUDE_EDITOR
	ImGui::CreateContext();

	if (!ImGui_ImplGlfw_InitForVulkan(_window->GetGLFWWindow(), false))
		return false;

	return true;
#endif // INCLUDE_EDITOR
}

bool HelloTriangleApplication::InitImGUIDynamic()
{
#ifdef INCLUDE_EDITOR
	VkDescriptorPoolSize pool_sizes[] =
	{
		{ VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
		{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
		{ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
		{ VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
	};
	VkDescriptorPoolCreateInfo pool_info = {};
	pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	pool_info.maxSets = 1000 * IM_ARRAYSIZE(pool_sizes);
	pool_info.poolSizeCount = (uint32_t)IM_ARRAYSIZE(pool_sizes);
	pool_info.pPoolSizes = pool_sizes;
	vkCreateDescriptorPool(_logicalDevice.GetVkDevice(), &pool_info, nullptr, &_imGUIDescPool);

	ImGui_ImplVulkan_InitInfo info = {};
	info.Instance = _vkInstance;
	info.PhysicalDevice = _mainPhysicalDevice->GetVkPhysicalDevice();
	info.Device = _logicalDevice.GetVkDevice();
	info.QueueFamily = *_mainPhysicalDevice->GetQueueFamilyIndices().graphicsFamily;
	info.Queue = _logicalDevice.GetGraphicQueue();
	info.PipelineCache = VK_NULL_HANDLE;
	info.DescriptorPool = _imGUIDescPool;
	info.Allocator = nullptr;
	info.CheckVkResultFn = nullptr;
	info.FrameCount = static_cast<int32_t>(_frames.size());

	if (!ImGui_ImplVulkan_Init(&info, _editorRenderPass.GetRHIRenderPass()))
		return false;

	VKRHI_CommandBuffer& buffer = _transferQueue.BeginCommandBuffer();
	ImGui_ImplVulkan_CreateFontsTexture(buffer.GetVkCommandBuffer());
	_transferQueue.EndCommandBuffer(buffer);
	ImGui_ImplVulkan_InvalidateFontUploadObjects();

#endif // INCLUDE_EDITOR
	return true;
}

void HelloTriangleApplication::ShutdownImGUI()
{
#ifdef INCLUDE_EDITOR
	ImGui_ImplGlfw_Shutdown();
	/*_rawInputContext.keyDelegate.RemoveListener(&ImGui_ImplGlfw_KeyCallback);
	_rawInputContext.charDelegate.RemoveListener(&ImGui_ImplGlfw_CharCallback);
	_rawInputContext.scrollDelegate.RemoveListener(&ImGui_ImplGlfw_ScrollCallback);
	_rawInputContext.mouseButtonDelegate.RemoveListener(&ImGui_ImplGlfw_MouseButtonCallback);*/
	ImGui::DestroyContext();
#endif // INCLUDE_EDITOR
}

void HelloTriangleApplication::ShutdownImGUIDynamic()
{
#ifdef INCLUDE_EDITOR
	ImGui_ImplVulkan_Shutdown();
	if (_imGUIDescPool)
		vkDestroyDescriptorPool(_logicalDevice.GetVkDevice(), _imGUIDescPool, nullptr);
#endif // INCLUDE_EDITOR
}
