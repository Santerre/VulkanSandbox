#include "Input/InputContext.h"


#include "Debugging/Assert.h"
#include "Utils/GenericAlgorithm.h"
#include "Utils/IteratorUtils.h"

#include <string>
#include <set>
#include <map>
#include <list>
#include <numeric>

using namespace Input;

void InputAction::Trigger()
{
	onAction.Invoke();
}

bool ActionRawInterface::ShouldTriggerAction(RawInputChange const& change) const
{
	return change.value == triggerState;
}

void InputState::UpdateValue(std::vector<InputStates> const& newValues)
{
	for (auto&& value : newValues)
		if (value == InputStates::RELEASED)
		{
			SetValue(InputStates::RELEASED);
			return;
		}

	SetValue(InputStates::PRESSED);
}

void InputState::SetValue(InputStates newValue)
{
	if (_value == newValue)
		return;

	_value = newValue;

	if (_value == InputStates::PRESSED)
		onEnterState.Invoke();
	else if (_value == InputStates::RELEASED)
		onExitState.Invoke();
	else
		KU_ASSERT(0);

	onStateChange.Invoke(_value);
}

void InputState::Update()
{
	if (_value == InputStates::PRESSED)
		whilePressed.Invoke();
}

InputStates StateRawInterface::ConvertStateChange(RawInputChange const& change) const
{
	return change.value;
}

void InputRange::UpdateValue(std::vector<float> const& newValues)
{
	float value = std::accumulate(newValues.begin(), newValues.end(), 0.f);

	_value = value;
	onValueChange.Invoke(_value);
}

void InputRange::Update()
{
	if (_value)
		whileNotZero.Invoke(_value);
}

float RangeRawInputInterface::ConvertRangeChange(RawInputChange const& change) const
{
	return change.value == InputStates::PRESSED ? rangeModification : 0;
}

float RangeRawAxisInterface::ConvertRangeChange(RawAxisChange const& change) const
{
	return change.value * sensibility;
}

InputContext::InputContext(std::string const& name, int32_t priority)
	: _priority(priority)
	, _name(name)
{}

void InputContext::SetActive(bool isActive)
{
	if (isActive != _isActive)
		_isActive = isActive;
}

bool InputContext::operator<(InputContext const& other)
{
	return _priority < other._priority;
}

template<class T>
bool operator<(const RawInput input, std::pair<const RawInput, T> const& pair) { return input < pair.first; }
template<class T>
bool operator<(std::pair<const RawInput, T> const& pair, const RawInput input) { return pair.first < input; }

void InputContext::Update(RawChangeList& changeContext)
{
	std::set<InputId>							shouldTriggerActions;
	std::map<InputId, std::vector<InputStates>>	newStates;
	std::map<InputId, std::vector<float>>		newRangeValues;

	// Gather all interesting raw input changes
	std::vector<std::map<RawInput, RawInputChange>::iterator> consumedInputs;
	for (auto it = changeContext.modifiedInputs.begin(); it != changeContext.modifiedInputs.end(); it++)
	{
		auto&[changedInput, inputChange] = *it;
		bool eventConsumed = false;

		// Gather action changes
		{
			auto [matchingBegin, matchingEnd] = _actionInterfaces.equal_range(changedInput);
			for (auto it = matchingBegin; it != matchingEnd; it++)
			{
				ActionRawInterface const& actionInt = it->second;
				if (actionInt.ShouldTriggerAction(inputChange))
				{
					shouldTriggerActions.insert(actionInt.inputId);
					eventConsumed = true;
				}
			}
		}

		// Gather state changes
		{
			auto[matchingBegin, matchingEnd] = _stateInterfaces.equal_range(changedInput);
			for (auto it = matchingBegin; it != matchingEnd; it++)
			{
				StateRawInterface const& stateInt = it->second;
				InputStates convertedValue = stateInt.ConvertStateChange(inputChange);
				newStates[stateInt.inputId].push_back(convertedValue);
				eventConsumed = true;
			}
		}
		
		// Gather range changes
		{
			auto[matchingBegin, matchingEnd] = _rangeInputInterfaces.equal_range(changedInput);
			for (auto it = matchingBegin; it != matchingEnd; it++)
			{
				RangeRawInputInterface const& rangeInt = it->second;
				float convertedValue = rangeInt.ConvertRangeChange(inputChange);
				newRangeValues[rangeInt.inputId].push_back(convertedValue);
				eventConsumed = true;
			}
		}

		if (eventConsumed)
			consumedInputs.push_back(it);
	}

	// Clear consumed inputs
	for (auto&& consumedInput : consumedInputs)
		changeContext.modifiedInputs.erase(consumedInput);
	
	// Gather all interesting axis changes
	std::vector<std::map<RawAxis, RawAxisChange>::iterator> consumedAxis;
	for (auto it = changeContext.modifiedAxis.begin(); it != changeContext.modifiedAxis.end(); it++)
	{
		auto&[changedInput, inputChange] = *it;
		// Gather range changes
		{
			auto[matchingBegin, matchingEnd] = _rangeAxisInterfaces.equal_range(changedInput);
			for (auto it = matchingBegin; it != matchingEnd; it++)
			{
				RangeRawAxisInterface const& rangeInt = it->second;
				float convertedValue = rangeInt.ConvertRangeChange(inputChange);
				newRangeValues[rangeInt.inputId].push_back(convertedValue);
			}
		}
	}

	// Clear consumed inputs
	for (auto&& consumedAxis : consumedAxis)
		changeContext.modifiedAxis.erase(consumedAxis);

	// Effectively update inputs
	for (auto&& actionId : shouldTriggerActions)
		_actions.at(actionId).Trigger();

	for (auto&&[stateId, newStates] : newStates)
		_states.at(stateId).UpdateValue(newStates);

	for (auto&&[rangeId, newRanges] : newRangeValues)
		_ranges.at(rangeId).UpdateValue(newRanges);

	// Update Inputs
	for (auto&&[stateId, state] : _states)
		state.Update();

	for (auto&&[rangeId, range] : _ranges)
		range.Update();
}


InputAction&		InputContext::GetAction(std::string const& name)
{
	return _actions[static_cast<InputId>(name)];
}

InputAction const&	InputContext::GetAction(std::string const& name) const
{
	return _actions.at(static_cast<InputId>(name));
}

InputState&			InputContext::GetState(std::string const& name)
{
	return _states[static_cast<InputId>(name)];
}

InputState const&	InputContext::GetState(std::string const& name) const
{
	return _states.at(static_cast<InputId>(name));
}

InputRange&			InputContext::GetRange(std::string const& name)
{
	return _ranges[static_cast<InputId>(name)];
}

InputRange const&	InputContext::GetRange(std::string const& name) const
{
	return _ranges.at(static_cast<InputId>(name));
}

InputAction& InputContext::AddAction(std::string const& name, std::vector<ActionInterfaceDesc> const& rawInputInterface)
{
	for (auto&& inputInterface : rawInputInterface)
		_actionInterfaces.insert({ inputInterface.input, { name, inputInterface.triggerState} });

	return _actions[name];
}

InputState& InputContext::AddState(std::string const& name, std::vector<StateInterfaceDesc> const& rawInputInterface)
{
	for (auto&& inputInterface : rawInputInterface)
		_stateInterfaces.insert({ inputInterface.input, { name } });

	return _states[name];
}

InputRange& InputContext::AddRange(std::string const& name, std::vector<InputRangeInterfaceDesc> const& rawInputInterface, std::vector<AxisRangeInterfaceDesc> const& rawAxisInterface)
{
	for (auto&& inputInterface : rawInputInterface)
		_rangeInputInterfaces.insert({ inputInterface.input, { name, inputInterface.rangeModification } });
	for (auto&& inputInterface : rawAxisInterface)
		_rangeAxisInterfaces.insert({ inputInterface.input, { name, inputInterface.sensibility } });


	return _ranges[name];
}
