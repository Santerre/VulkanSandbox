#include "Input/GLFW/GLFWRawInputContext.h"

#include "Debugging/Assert.h"

using namespace Input;

GLFWRawInputContext* currentContext = nullptr;


void GLFWRawInputContext::Shutdown()
{
	currentContext = nullptr;

	ClearRecordedEvents();
	_window = nullptr;
	_cursorDelta = {};
	_cursorLocation = {};
}

bool GLFWRawInputContext::PollInputChanges(PollingResults& results)
{
	if (!_window)
		return false;

	glfwPollEvents();

	results.changeList = _changeList;
	ClearRecordedEvents();

	return true;
}

void GLFWRawInputContext::SetCursorEnabled(bool isEnabled)
{
	glfwSetInputMode(_window, GLFW_CURSOR, isEnabled ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
}

void GLFWRawInputContext::ClearRecordedEvents()
{
	_changeList = {};
}

void GLFWRawInputContext::AddInputChange(int glfwInput, int newState)
{
	RawInput input = GLFWKeyToRawInput(glfwInput);
	if (input != RawInput::NONE)
		AddInputChange(input, newState);
}

void GLFWRawInputContext::AddInputChange(RawInput rawInput, int newState)
{
	InputStates state;
	switch (newState)
	{
	case GLFW_PRESS:
		state = InputStates::PRESSED;
		break;

	case GLFW_RELEASE:
		state = InputStates::RELEASED;
		break;

	case GLFW_REPEAT:
	default:
		return;
	}

	AddInputChange(rawInput, state);
}

void GLFWRawInputContext::AddInputChange(RawInput input, InputStates newState)
{
	RawInputChange change = {};
	change.value = newState;
	_changeList.modifiedInputs[input] = change;
}

void GLFWRawInputContext::AddAxisChange(RawAxis axis, float value)
{
	RawAxisChange change = {};
	change.value = value;
	_changeList.modifiedAxis[axis].value += value;
}