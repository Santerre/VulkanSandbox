#include "Input/Input.h"

#include "Debugging/Assert.h"

#include <string>

using namespace Input;

void InputManager::FromRawInputContext(IRawInputContext* context)
{
	_rawContext = context;
}

void InputManager::CreateContext(InputContext&& context)
{
	std::string name = context.GetName();
	int prio = context.GetPriority();
	KU_VERIFY(_contextMap.Add(name, prio, std::move(context)));
}

bool InputManager::PollEvents()
{
	PollingResults results = {};
	if (!_rawContext->PollInputChanges(results))
		return false;

	for (Input::InputContext& context : _contextMap)
	{
		if (context.IsActive())
		{
			context.Update(/*inout*/results.changeList);

			if (context.ShouldBlockInputs())
				break;
		}
	}
	return true;
}

InputAction	const&	InputManager::GetAction(std::string const& context, std::string actionName) const
{
	return _contextMap.at(context).GetAction(actionName);
}

InputState	const&	InputManager::GetState(std::string const& context, std::string stateName) const
{
	return _contextMap.at(context).GetState(stateName);
}

InputRange	const&	InputManager::GetRange(std::string const& context, std::string rangeName) const
{
	return _contextMap.at(context).GetRange(rangeName);
}

InputAction&	InputManager::GetAction(std::string const& context, std::string actionName)
{
	return _contextMap.at(context).GetAction(actionName);
}

InputState&		InputManager::GetState(std::string const& context, std::string stateName)
{
	return _contextMap.at(context).GetState(stateName);
}

InputRange&		InputManager::GetRange(std::string const& context, std::string rangeName)
{
	return _contextMap.at(context).GetRange(rangeName);
}
