#include "Memory/Allocator.h"

Allocator* Allocator::GetResourceAllocator()
{
	static Allocator ResourceAllocator;
	return &ResourceAllocator;
};

