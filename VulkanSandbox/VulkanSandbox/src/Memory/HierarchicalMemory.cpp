#include "Memory/HierarchicalMemory.h"

#include "Debugging/Assert.h"

#include "Resource/Generator/MeshGenerator.h"

bool HierarchicalMemory::Init()
{
	BindGenerator<MeshGenerator>(Path(std::string(HM_RES_MESH_PATH)));
	return true;
}

void HierarchicalMemory::Shutdown()
{
	memory.clear();
	generators.clear();
}

MemorySlot& HierarchicalMemory::GetSlot(Path const& slotPath)
{
	KU_ASSERT(slotPath.GetVolume() == "M:");

	auto result = memory.find(slotPath.GetString());
	if (result == memory.end())
	{
		MemorySlot& slot = memory[slotPath.GetString()];

		if (!GenerateSlot(slotPath, slot))
		{
			ELOG(LogHierarchicalMemory, "Couldn't generate the memory slot %s", slotPath.GetString());
		}

		return slot;
	}
	return result->second;
}

bool HierarchicalMemory::GenerateSlot(Path const& path, MemorySlot& outSlot)
{
	size_t bestGeneratorWeight = 0;
	Path bestGeneratorRelativePath;
	IGenerator* bestGenerator = nullptr;
	for (auto&&[genPath, generator] : generators)
	{
		Path relPath(path.GetFullRelativePath(), { genPath });
		if (!relPath.IsValid())
			continue;
		size_t size = relPath.GetString().size();
		if (bestGeneratorWeight < size)
		{
			bestGeneratorWeight = size;
			bestGeneratorRelativePath = relPath;
			bestGenerator = generator.get();
		}
	}

	if (!bestGenerator)
	{
		ELOG(LogHierarchicalMemory, "Failed to generate resource %s, no generator binded to the path %s.", path.GetFilename().GetString().c_str(), path.GetString().c_str());
		return false;
	}
	
	return bestGenerator->GenerateSlot(bestGeneratorRelativePath, outSlot);
}
