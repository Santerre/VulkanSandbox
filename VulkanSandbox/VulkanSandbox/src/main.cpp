#include <iostream>
#include <memory>

#include "Applications/HelloTriangleApplication.h"

int main() 
{
	std::unique_ptr<IApplication> App(new HelloTriangleApplication());
	if (App->Init())
	{
		while (!App->ShouldShutdown())
			App->Update();
		App->Shutdown();
	}

	return 0;
}