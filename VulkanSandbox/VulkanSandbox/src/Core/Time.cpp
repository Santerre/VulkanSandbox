#include "Core/Time.h"

#include <numeric>
#include <thread>

void Time::Init()
{
	_mainClock.Start();
	_frameTimeHistory = {};
    _historyIndex = { &_frameTimeHistory, _frameTimeHistory.size() };
}

void Time::Update()
{
	_mainClock.Tick();

	float frameTime = _mainClock.GetLastDelta();

	static float n = 0;
	*_historyIndex = frameTime;
    ++_historyIndex;
}

void Time::Shutdown()
{
}

float Time::GetDeltaTime() const
{
	return _mainClock.GetLastDelta();
}

void Time::WaitFrameTime(std::chrono::microseconds frame_time) const
{
	Clock::TimePoint target = _mainClock.GetLastTick() + frame_time;
	while (true)
	{
		std::chrono::duration rem = target - _mainClock.GetCurrentTime();
		if (rem <= std::chrono::milliseconds(0))
			break;

		if (rem > std::chrono::milliseconds(2))
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

std::array<float, Time::frameTimeHistorySize> Time::GetTimeHistory() const
{
	std::array<float, Time::frameTimeHistorySize> res;
    res[0] = *(_historyIndex);
	std::copy(_historyIndex + 1, _historyIndex, res.begin() + 1);
	return res;
}

void Clock::Start()
{
	_start = GetCurrentTime();
	Tick();
}

void Clock::Tick()
{
	TimePoint newTick = GetCurrentTime();
	_lastDelta = DeltaInSeconds(newTick, _lastTick);
	_lastTick = newTick;
}

float Clock::DeltaInSeconds(TimePoint const& a, TimePoint const& b) const
{
	return fabs(std::chrono::duration<float, std::chrono::seconds::period>(b - a).count());
}

Clock::TimePoint Clock::GetCurrentTime() const
{
	return std::chrono::high_resolution_clock::now();

}

float TimeStats::ComputeMean() const
{
	return std::accumulate(_sortedTimes.begin(), _sortedTimes.end(), 0.f) / static_cast<float>(_sortedTimes.size());
}

float TimeStats::ComputeMin() const
{
	return _sortedTimes[0];
}

float TimeStats::ComputeMax() const
{
	return _sortedTimes[_sortedTimes.size() - 1];
}

float TimeStats::ComputeMedian() const
{
	size_t size = _sortedTimes.size();
	size_t midPoint = size % 2 == 0 ? size / 2 : (size + 1) / 2;
	if (size % 2 == 1)
		return *(_sortedTimes.begin() + midPoint);
	else
	{
		float l = *(_sortedTimes.begin() + midPoint);
		float r = *(_sortedTimes.begin() + midPoint + 1);
		return (l + r) / 2.f;
	}
}