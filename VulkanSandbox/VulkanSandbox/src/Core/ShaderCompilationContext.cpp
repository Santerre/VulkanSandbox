#include "Core/ShaderCompilationContext.h"

#include "Debugging/Assert.h"

void ShaderCompilationContext::SetGlobalMacro(std::string const& name, std::string const& value) 
{ 
    stringGlobalMacros[name] = value; 
}

void ShaderCompilationContext::SetGlobalMacro(std::string const& name, uint32_t value)
{
    intGlobalMacros[name] = value;
}

void ShaderCompilationContext::SetGlobalMacro(std::string const& name, int value)
{ 
    intGlobalMacros[name] = value; 
}

void ShaderCompilationContext::SetGlobalMacro(std::string const& name, float value) 
{ 
    floatGlobalMacros[name] = value; 
}