#include "Core/MeshData.h"

size_t MeshData::GetVertsDataByteSize() const
{
	return vertices.size() * sizeof(Vertex);
}

size_t MeshData::GetIndicesDataByteSize() const
{
	return indices.size() * sizeof(MeshIndex);
}