#include "Core/Window.h"

Window::Window(const char* windowName)
:_windowName(windowName)
{
	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
}

void Window::Open()
{
	_window = glfwCreateWindow(static_cast<int>(_extents.x), static_cast<int>(_extents.y), _windowName, nullptr, nullptr);
}

void Window::Close()
{
	glfwDestroyWindow(_window);
	glfwTerminate();
}

bool Window::ShouldClose() const
{
	if (!_window)
		return false;

	return glfwWindowShouldClose(_window);
}

void Window::SetWindowUserPointer(void* userPointer)
{
	glfwSetWindowUserPointer(GetGLFWWindow(), userPointer);
}

void* Window::GetWindowUserPointer(GLFWwindow* window)
{
	return glfwGetWindowUserPointer(window);
}
