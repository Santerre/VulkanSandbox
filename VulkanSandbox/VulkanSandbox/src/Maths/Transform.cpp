#pragma once

#include "Maths/Transform.h"

#include "Debugging/Assert.h"

#include "glm/gtx/transform.hpp"
#include "glm/gtx/matrix_decompose.hpp"

glm::mat4 Transform::GetTransformMatrix() const
{
	return glm::scale(glm::translate(_position) * glm::mat4_cast(_rotation), _scale);
}

glm::vec3 Transform::TransformPoint(glm::vec3 const& position)
{ 
	return glm::vec3(GetTransformMatrix() * glm::vec4(position, 1));
}

glm::vec3 Transform::TransformVector(glm::vec3 const& vector)
{ 
	return glm::vec3(GetTransformMatrix() * glm::vec4(vector, 0));
}

void Transform::RotationRelativeMove(glm::vec3 const& relativeMove)
{
	_position += _rotation * relativeMove;
}

void Transform::Move(glm::vec3 const& move)
{
	_position += move;
}

void Transform::RotateAbsolute(glm::vec3 const& rotationAxis, float angle)
{
	_rotation = glm::rotate(_rotation, angle, glm::inverse(_rotation) * rotationAxis);
}

void Transform::RotateRelative(glm::vec3 const& rotationAxis, float angle)
{
	_rotation = glm::rotate(_rotation, angle, rotationAxis);
}

void Transform::Rotate(glm::quat const& rotationDelta)
{
	_rotation = rotationDelta * _rotation;
}

void Transform::Scale(glm::vec3 const& deltaScale)
{
	_scale *= deltaScale;
}

void Transform::LookAt(glm::vec3 const& eye, glm::vec3 const& target, glm::vec3 const& up)
{
	glm::mat4 mat = glm::lookAt(eye, target, up);

	glm::vec3 scale;
	glm::vec3 skew;
	glm::vec4 perspective;
	KU_VERIFY(glm::decompose(mat, _scale, _rotation, _position, skew, perspective));
}
