#include "Scene/TestScene.h"

#include <array>

#include "Debugging/Assert.h"

#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "Maths/Utils.h"

#include "Core/Window.h"
#include "Core/RenderSystem.h"

#include "Resource/ResourceMgr.h"
#include "Memory/HierarchicalMemory.h"

#include "Resource/Mesh.h"
#include "Resource/Texture.h"
#include "Resource/CubemapImage.h"

#include "Input/Input.h"

#include "Debugging/Log.h"
#include "Debugging/Profiler.h"

#include "imgui.h"

bool TestScene::FromDesc(SceneDesc const& desc)
{
	if (!Scene::FromDesc(desc))
		return false;

	KU_ASSERT(_context.resourceMgr);

	SetupInput();

	_camera = std::make_unique<SC::Camera>();
	_camera->FromViewportExtents(_context.window->GetExtents());

	//_staticMesh = std::make_unique<SC::StaticMesh>();
	//{
	//	SC::StaticMeshDesc smDesc = {};
    //
	//	//Handle<Mesh> mesh = _context.resourceMgr->Get<Mesh>("M:/" HM_RES_MESH_PATH "/Test/Cube.raw");
	//	//Handle<Texture> texture = _context.resourceMgr->Get<Texture>("D:/Images/TestTexture.png");
    //
	//	Handle<Mesh> mesh = _context.resourceMgr->Get<Mesh>("D:/Models/Chalet/chalet.obj");
	//	//Handle<Mesh> mesh = _context.resourceMgr->Get<Mesh>("D:/Models/Cube/cube.obj");
	//	Handle<Texture> texture = _context.resourceMgr->Get<Texture>("D:/Models/Chalet/chalet_texture.desc");
	//	_staticMesh->GetTransform().RotateAbsolute(glm::vec3(-1.f, 0.f, 0.f), glm::half_pi<float>());
	//	_staticMesh->GetTransform().RotateAbsolute(glm::vec3(0.f, 1.f, 0.f), glm::half_pi<float>());
    //
	//	KU_ASSERT(mesh);
	//	smDesc.mesh = mesh;
	//	smDesc.texture = texture;
    //
	//	if (!_staticMesh->FromDesc(smDesc))
	//		return false;
    //
	//	//_staticMesh->GetTransform().SetPosition(glm::vec3(-238.f, 6.03f, 128.85f));
	//	_staticMesh->GetTransform().SetPosition(glm::vec3(-238.f, 6.03f, 128.85f));
	//}

    //_camera->GetTransform().SetPosition(_staticMesh->GetTransform().GetPosition() + glm::vec3(0.f, 0.7f, 3.f));
	
	_camera->GetTransform().SetPosition(glm::vec3(-1451.31897, 55.7739601, 747.734131));
    _camera->GetTransform().SetRotation(glm::quat(0.508887053f, -0.0410638377f, -0.857109129f, -0.0691459402f));
    _originalPosition = _camera->GetTransform().GetPosition();
	_cubemap = std::make_unique<SC::Cubemap>();
	{
		SC::CubemapDesc desc = {};
		CubemapImage tex = {};
		//desc.texture = _context.resourceMgr->Get<CubemapTexture>("D:/Images/Cubemap/WaterSky/defaultSky.desc");
		desc.image = _context.resourceMgr->Get<CubemapImage>("D:/Images/Cubemap/RockSky/defaultSky.desc");
		KU_ASSERT(desc.image);
		desc.resourceMgr = _context.resourceMgr;
		_cubemap->FromDesc(desc);
	}

	_terrain = std::make_unique<SC::Terrain>();
	{
		SC::TerrainDesc desc = {};
		desc.context = &_context;
		KU_VERIFY(_terrain->FromDesc(desc));

		_terrain->SetGrassTexture(_context.resourceMgr->Get<Texture>("D:/Images/Textures/Terrain/Grass/Texture.desc"));
		_terrain->SetSnowTexture(_context.resourceMgr->Get<Texture>("D:/Images/Textures/Terrain/Snow/Texture.desc"));
		_terrain->SetCliffTexture(_context.resourceMgr->Get<Texture>("D:/Images/Textures/Terrain/Cliff/Texture.desc"));
		_terrain->SetHeightMap(_context.resourceMgr->Get<Texture>("D:/Images/Textures/Terrain/HeightMap/Texture.desc"));
	}

	_ambientLight = std::make_unique<SC::AmbientLight>();
	{
		KU_VERIFY(_ambientLight->Init());
		_ambientLight->SetColor({ 0.476, 0.661, 0.961 });
		_ambientLight->SetIntensity(0.71f);
	}

	_directionalLight = std::make_unique<SC::DirectionalLight>();
	{
		KU_VERIFY(_directionalLight->FromDirection({ 0.556, -0.809, 0.404 }));
		_directionalLight->SetColor({ 220.f / 250.f, 227.f / 250.f, 211.f / 250.f });
		_directionalLight->SetIntensity(1.06f);
	}

    //_subdivTransform.SetPosition({0, 0, 0});
    //_terrain->SetSubdivTarget(&_subdivTransform);

	return true;
}

void TestScene::Update(float deltaTime)
{
	KU_CPU_SCOPED_QUERY(TestSceneUpdate);
	Scene::Update(deltaTime);

	_frameDeltaTime = deltaTime;
	_rotationTime += deltaTime;

    _terrain->SetSubdivTarget(&GetCullCamera()->GetTransform());
	if (_bforceTerrainUpdate)
		_terrain->NotifyTerrainUpdate();
    _terrain->Update(_context);
    if (_bIsRelativeMoveToggled)
	    _camera->GetTransform().RotationRelativeMove(_relativeMove *  exp(_camSpeed) * _frameDeltaTime);
}

void TestScene::Shutdown()
{

}

void TestScene::ChangeRenderingMode(int i)
{
	switch (i)
	{
	case 0:
		_renderingProperties.forceWireframe = false;
		break;
	case 1:
		_renderingProperties.forceWireframe = true;
		break;
	default:
		break;
	}
}

SC::Camera const* TestScene::GetCullCamera() const
{
    if (!_cullCamera)
        return Scene::GetCullCamera();
    else
        return _cullCamera.get();
}


void TestScene::SetupInput()
{
	using namespace Input;
	KU_ASSERT(_context.inputManager);

	{
		InputContext context("Game", 0);

		{
			{
				auto& action = context.AddAction(
					"RenderingMode1",
					{
						{ RawInput::K_F1, InputStates::PRESSED }
					});

				action.onAction.AddListener([this]()
				{
					ChangeRenderingMode(0);
				});
			}

			{
				auto& action = context.AddAction(
					"RenderingMode2",
					{
						{ RawInput::K_F2, InputStates::PRESSED }
					});

				action.onAction.AddListener([this]()
				{
					ChangeRenderingMode(1);
				});
			}
		}

/*		{
			auto& state = context.AddState(
			"State",
			{
				{ RawInput::MOUSE_BUTTON_LEFT }
			});

			state.onEnterState.AddListener([this]()
			{
				//do something
			});

			state.onExitState.AddListener([this]()
			{
				//do something
			});

			state.onStateChange.AddListener([this](InputStates state)
			{
				//do something
			});
		}*/

		{
        	{
				auto& action = context.AddAction(
					"GoToOrigin",
					{
							{ RawInput::K_F1, InputStates::PRESSED }
					});

				action.onAction.AddListener([this]()
				{
                    GetCamera()->GetTransform().SetPosition(_originalPosition);
				});
			}

			{
				auto& range = context.AddRange(
					"MoveForward",
					{
							{ RawInput::K_W, 1.f }
						,	{ RawInput::K_S, -1.f }
					}
					, {
					});

				range.whileNotZero.AddListener([this](float newValue)
				{
                    if (!_bUseRemoteControls)
					    MoveCameraInput(glm::vec3(0, 0, -1), newValue);
				});
			}

			{
				auto& range = context.AddRange(
					"MoveRight",
					{
							{ RawInput::K_D, 1.f }
						,	{ RawInput::K_A, -1.f }
					}
					, {
					});

				range.whileNotZero.AddListener([this](float newValue)
				{
                    if (!_bUseRemoteControls)
					    MoveCameraInput(glm::vec3(1, 0, 0), newValue);
				});
			}
            static float moveSpeed = 0.01f;
            {
                auto& range = context.AddRange(
                    "TargetMoveForward",
                    {
                            { RawInput::K_UP, 1.f }
                        ,	{ RawInput::K_DOWN, -1.f }
                    }
                    , {
                    });

                range.whileNotZero.AddListener([this](float newValue)
                    {
                        _subdivTransform.Move({newValue * moveSpeed, 0, 0});
                    });
            }
            {
                auto& range = context.AddRange(
                    "TargetMoveRight",
                    {
                            { RawInput::K_RIGHT, 1.f }
                        ,	{ RawInput::K_LEFT, -1.f }
                    }
                    , {
                    });

                range.whileNotZero.AddListener([this](float newValue)
                    {
                        _subdivTransform.Move({ 0, newValue * moveSpeed, 0});
                    });
            }

			{
				auto& range = context.AddRange(
					"MoveUp",
					{
							{ RawInput::K_E, 1.f }
						,	{ RawInput::K_Q, -1.f }
					}
					, {
					});

				range.whileNotZero.AddListener([this](float newValue)
				{
                    if (!_bUseRemoteControls)
					    MoveCameraInput(glm::vec3(0, 1, 0), newValue);
				});
			}

			{
				auto& range = context.AddRange(
					"CamRotateUp",
					{
					//	{ RawInput::K_UP, 1.f }
					//	,{ RawInput::K_DOWN, -1.f }
					}
					,{
						{ RawAxis::MOUSE_UP,  1.f }
					});

				range.onValueChange.AddListener([this](float newValue)
				{
					RotateCameraInputRelative(glm::vec3(1, 0, 0), newValue);
				});
			}
			{
				auto& range = context.AddRange(
					"CamRotateRight",
					{
					//	{ RawInput::K_RIGHT, 1.f }
					//	,{ RawInput::K_LEFT, -1.f }
					}
					,{
						{ RawAxis::MOUSE_RIGHT,  1.f }
					});

				range.onValueChange.AddListener([this](float newValue)
				{
					RotateCameraInputAbsolute(glm::vec3(0, -1, 0), newValue);
				});
			}
			{
				auto& range = context.AddRange(
					"CamSpeedUp",
					{
						{ RawInput::K_J, 1.f }
						,{ RawInput::K_K, -1.f }
					}
					, {
						{ RawAxis::MOUSE_SCROLL_UP,  1.f }
					});

				range.onValueChange.AddListener([this](float newValue)
				{
					_camSpeed += newValue;
				});
			}

            // RemoteControls
            {
				auto& action = context.AddAction(
					"MoveForwardRemote",
					{
							{ RawInput::K_W, InputStates::PRESSED }
					});

				action.onAction.AddListener([this]()
				{
                    if (_bUseRemoteControls)
                    {
                        ToggleRelativeMove(glm::vec3(0, 0, -1), _relativeMoveSpeed);
                    }
				});
			}
            {
				auto& action = context.AddAction(
					"MoveBackwardRemote",
					{
							{ RawInput::K_S, InputStates::PRESSED }
					});

				action.onAction.AddListener([this]()
				{
                    if (_bUseRemoteControls)
                    {
                        ToggleRelativeMove(glm::vec3(0, 0, 1), _relativeMoveSpeed);
                    }
				});
			}
            {
				auto& action = context.AddAction(
					"MoveLeftRemote",
					{
							{ RawInput::K_A, InputStates::PRESSED }
					});

				action.onAction.AddListener([this]()
				{
                    if (_bUseRemoteControls)
                    {
                        ToggleRelativeMove(glm::vec3(1, 0, 0), _relativeMoveSpeed);
                    }
				});
			}
            {
				auto& action = context.AddAction(
					"MoveRightRemote",
					{
							{ RawInput::K_D, InputStates::PRESSED }
					});

				action.onAction.AddListener([this]()
				{
                    if (_bUseRemoteControls)
                    {
                        ToggleRelativeMove(glm::vec3(-1, 0, 0), _relativeMoveSpeed);
                    }
				});
			}
            {
				auto& action = context.AddAction(
					"MoveUpRemote",
					{
							{ RawInput::K_Q, InputStates::PRESSED }
					});

				action.onAction.AddListener([this]()
				{
                    if (_bUseRemoteControls)
                    {
                        ToggleRelativeMove(glm::vec3(0, 1, 0), _relativeMoveSpeed);
                    }
				});
			}
            {
				auto& action = context.AddAction(
					"MoveDownRemote",
					{
							{ RawInput::K_E, InputStates::PRESSED }
					});

				action.onAction.AddListener([this]()
				{
                    if (_bUseRemoteControls)
                    {
                        ToggleRelativeMove(glm::vec3(0, -1, 0), _relativeMoveSpeed);
                    }
				});
			}
		}

		_context.inputManager->CreateContext(std::move(context));
	}
}

void TestScene::ToggleRelativeMove( glm::vec3 direction, float normalizedInput )
{
    _bIsRelativeMoveToggled = !_bIsRelativeMoveToggled;
    _relativeMove = direction * normalizedInput;
}

void TestScene::MoveCameraInput(glm::vec3 direction, float normalizedInput)
{
	_camera->GetTransform().RotationRelativeMove(direction * normalizedInput * exp(_camSpeed) * _frameDeltaTime);
}

void TestScene::RotateCameraInputRelative(glm::vec3 axis, float normalizedInput)
{
	_camera->GetTransform().RotateRelative(axis, normalizedInput * _camRotateSpeed);
}

void TestScene::RotateCameraInputAbsolute(glm::vec3 axis, float normalizedInput)
{
	_camera->GetTransform().RotateAbsolute(axis, normalizedInput * _camRotateSpeed);
}

void TestScene::UpdatePropsEditor(float deltaTime)
{
    if ( ImGui::TreeNode( "Input" ) )
    {
        if (ImGui::Checkbox("RemoteControls", &_bUseRemoteControls))
        {
            _bIsRelativeMoveToggled = false;
            _relativeMove = glm::vec3();
        }
        ImGui::DragFloat("CamRotateSpeed", &_camRotateSpeed);
        ImGui::TreePop();

    }

    if (_camera)
    {
        if (ImGui::TreeNode("Camera"))
        {
            bool bFixCullCamera = !!_cullCamera;
            bool bChanged = ImGui::Checkbox("FixCullCamera", &bFixCullCamera);
            if (bChanged)
            {
                if (bFixCullCamera)
                    _cullCamera = std::make_unique<SC::Camera>(*_camera);
                else
                    _cullCamera.reset();
            }
            ImGui::TreePop();
        }
    }

	if (_staticMesh)
	{
		if (ImGui::TreeNode("StaticMesh"))
		{
			UpdateTransformEditor(_staticMesh->GetTransform());
			ImGui::TreePop();
		}
	}

	if (_terrain)
	{
		if (ImGui::TreeNode("Terrain"))
		{
			glm::vec3 loc = _terrain->GetLocation();
			if (ImGui::DragFloat3("Location", glm::value_ptr(loc)))
				_terrain->SetLocation(loc);

			glm::ivec3 extents = _terrain->GetExtents();
			if (ImGui::DragInt3("Extents", glm::value_ptr(extents), 1.f, 0, 200000))
				_terrain->SetExtents(extents);

			ImGui::Checkbox("ForceTerrainUpdate", &_bforceTerrainUpdate);

			float forceTessellationPrecision = _terrain->GetForceTessellationPrecision();
			if (ImGui::DragFloat("ForceTessellationPrecision", &forceTessellationPrecision, 100, 1000))
			{
				_terrain->SetForceTessellationPrecision(forceTessellationPrecision);
				if (forceTessellationPrecision > 0)
				{
					_terrain->SetForceTessellationPrecision(forceTessellationPrecision);
					_terrain->ChangeTessellationEvaluationTechnique(SC::Terrain::TessellationEvaluationTechnique::TargetDistance);
				}
				_terrain->NotifyTerrainUpdate();
			}

			if (forceTessellationPrecision <= 0)
			{
				std::array<const char*, 2> items = { "Factor Based", "Distance Based" };
				size_t current = static_cast<size_t>(_terrain->GetTessellationEvaluationTechnique());
				if (ImGui::BeginCombo("CPU Tessellation technique", items[current]))
				{
					for (size_t i = 0; i < items.size(); i++)
					{
						bool bIsSelected = i == current;

						if (ImGui::Selectable(items[i], bIsSelected))
						{
							current = i;
							_terrain->ChangeTessellationEvaluationTechnique(static_cast<SC::Terrain::TessellationEvaluationTechnique>(current));
						}
					}
					ImGui::EndCombo();
				}
			}

			switch (_terrain->GetTessellationEvaluationTechnique())
			{
			case SC::Terrain::TessellationEvaluationTechnique::TargetDistance:
			{
				SC::Terrain::DistanceBasedTerrainEvaluator& evaluator = _terrain->GetDistanceBasedTerrainEvaluator();
				bool bChanged = false;
				if (forceTessellationPrecision <= 0)
					bChanged |= ImGui::InputInt("Max Tessellation", &evaluator.maxTessellationFactor);
				bChanged |= ImGui::DragFloat("Distance Factor", &evaluator.distanceFactor, 1.0f, 0.0f);
				if (bChanged)
					_terrain->NotifyTerrainUpdate();

				break;
			}
			case SC::Terrain::TessellationEvaluationTechnique::TessellationFactor:
			{
				int factor = _terrain->GetTessellationFactor();
				if (ImGui::InputInt("TessellationFactor", &factor))
					_terrain->SetTessellationFactor(factor);
				break;
			}
			default:
				KU_ASSERT(0);
				break;
			}
            ImGui::Spacing();

			if (ImGui::TreeNode("Material"))
			{
				ImGui::DragFloat("normUpValue", &_terrain->mat.normUpValue, 0.001f, 0, 1);
				ImGui::DragFloat("slopeUpCos", &_terrain->mat.slopeUpCos, 0.001f, 0, 1);
				ImGui::DragFloat("slopeDownCos", &_terrain->mat.slopeDownCos, 0.001f, 0, 1);
				ImGui::DragFloat("upDownBlend", &_terrain->mat.upDownBlend, 0.001f, 0, 1);
				ImGui::DragFloat("cliffUpTexBlend", &_terrain->mat.cliffUpTexBlend, 0.001f, 0, 1);
				ImGui::DragFloat("cliffDownTexBlend", &_terrain->mat.cliffDownTexBlend, 0.001f, 0, 1);
				ImGui::Spacing();

				ImGui::DragFloat2("Tiling", glm::value_ptr(_terrain->mat.tiling));
				glm::vec2 kmTiling = _terrain->mat.heightMapTiling * 1000.f;
				ImGui::DragFloat2("HeightMapTiling(km)", glm::value_ptr(kmTiling));
				_terrain->mat.heightMapTiling = kmTiling * 0.001f;
				ImGui::Spacing();

				ImGui::DragFloat("tessTargetScreenSize", &_terrain->mat.tessTargetScreenSize, 0.1f);
				ImGui::DragFloat("forceTessLevel", &_terrain->mat.forceTessLevel, 1.f, -1.0f, static_cast<float>(_context.renderSystem->GetRenderingCapabilities().maxTessellation));
				ImGui::DragFloat("forceTaskTessLevel", &_terrain->mat.forceTaskTessLevel, 1.f, -1.0f, static_cast<float>(_context.renderSystem->GetRenderingCapabilities().maxTessellation));
				ImGui::DragFloat("forceMeshTessLevel", &_terrain->mat.forceMeshTessLevel, 1.f, -1.0f, 7.f);
				ImGui::Spacing();

				ImGui::DragFloat("fogDist", &_terrain->mat.fogDist);
				ImGui::ColorEdit3("fogColor", glm::value_ptr(_terrain->mat.fogColor));

				ImGui::TreePop();
			}
			ImGui::TreePop();
		}
	}

	if (_directionalLight || _ambientLight)
	{
		if (ImGui::TreeNode("Lights"))
		{
			if (_ambientLight)
			{
				if (ImGui::TreeNode("AmbientLight"))
				{
					UpdateLightEditor(*_ambientLight);
					ImGui::TreePop();
				}
			}

			if (_directionalLight)
			{
				if (ImGui::TreeNode("DirectionalLight"))
				{
					UpdateLightEditor(*_directionalLight);

					glm::vec3 dir = _directionalLight->GetDirection();
					if (ImGui::DragFloat3("Direction", glm::value_ptr(dir), 0.01f))
						_directionalLight->SetDirection(glm::normalize(dir));
					ImGui::TreePop();
				}
			}
			ImGui::TreePop();
		}
	}
}

void TestScene::UpdateTransformEditor(Transform& transform)
{
	glm::vec3 loc = transform.GetPosition();
	if (ImGui::DragFloat3("Position", glm::value_ptr(loc), 0.01f))
		transform.SetPosition(loc);

	glm::vec3 extents = transform.GetScale();
	if (ImGui::DragFloat3("Scale", glm::value_ptr(extents), 0.1f))
		transform.SetScale(extents);
}

void TestScene::UpdateLightEditor(SC::Light& light)
{
	glm::vec3 color = light.GetColor();
	if (ImGui::ColorEdit3("Color", glm::value_ptr(color)))
		light.SetColor(color);

	float intensity = light.GetIntensity();
	if (ImGui::DragFloat("Intensity", &intensity, 0.01f))
		light.SetIntensity(intensity);
}