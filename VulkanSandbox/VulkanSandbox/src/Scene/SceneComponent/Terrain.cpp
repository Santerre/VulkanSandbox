#include "Scene/SceneComponent/Terrain.h"

#include "Scene/SceneLogCategory.h"
#include "Core/Scene.h"
#include "Core/RenderSystem.h"

#include "Memory/Allocator.h"

#include "Utils/QuadTree.h"

#include "Maths/Transform.h"

#include "Debugging/Profiler.h"

#include "glm/glm.hpp"

namespace SC
{

bool Terrain::DistanceBasedTerrainEvaluator::EvaluateNode(TerrainQuadTreeDescriptorTrait::Trait::Node const& node) const
{
    glm::vec3 target = _target;
    glm::vec2 nodeCoords = TerrainQuadTreeDescriptor::Get2DCoords(node.data.key);
    float halfSide = TerrainQuadTreeDescriptor::GetSize(node.data.key) * 0.5f;
    nodeCoords = nodeCoords + halfSide;
    float distance = glm::max(glm::distance(target, glm::vec3(nodeCoords.x, 0, nodeCoords.y)) - glm::sqrt(halfSide * halfSide * 2.f), 0.f);
    distance *= 1.f / distanceFactor;
    auto res = glm::round(glm::mix(static_cast<float>(maxTessellationFactor), 0.f, distance));
    return res <= node.data.key.depth;
}

bool Terrain::DistanceBasedTerrainEvaluator::UpdateTarget(glm::vec3 newTarget)
{ 
    if (_target == newTarget)
        return false;

    _target = newTarget; 
    return true;
}

bool Terrain::FactorBasedTerrainEvaluator::EvaluateNode(TerrainQuadTreeDescriptorTrait::Trait::Node const& node) const
{
    return _tessellationFactor <= node.data.key.depth;
}

bool Terrain::FactorBasedTerrainEvaluator::UpdateTessellationFactor(int newFactor)
{
    if (_tessellationFactor == newFactor)
        return false;

    _tessellationFactor = newFactor;
    return true;
}

using TreeTrait = Terrain::TerrainQuadTreeDescriptor::TreeTrait;
Handle<TreeTrait::TreeBlock> Terrain::TerrainQuadTreeDescriptor::AllocateBlock()
{
    return Allocator::GetResourceAllocator()->Allocate<TreeTrait::TreeBlock>();
}

void Terrain::TerrainQuadTreeDescriptor::DestroyBlock(Handle<TreeTrait::TreeBlock> block)
{
    Allocator::GetResourceAllocator()->Destroy(block);
}

bool Terrain::TerrainQuadTreeDescriptor::EvaluateNode(TreeTrait::Node const& node)
{
    if (_terrainEvaluator)
        return _terrainEvaluator->EvaluateNode(node);
    return false;
}

void Terrain::TerrainQuadTreeDescriptor::InitNode(TreeTrait::Node& node)
{
    for (int i = 0; i < 4; i++)
        node.data.customData.neighbourDepth[i] = node.data.key.depth;
}

float Terrain::TerrainQuadTreeDescriptor::GetSize(TreeTrait::Key const& key)
{
    return GetSizeAtDepth(key.depth);
}

float Terrain::TerrainQuadTreeDescriptor::GetSizeAtDepth(uint8_t depth)
{
    return 1.f / glm::pow(2.f, static_cast<float>(depth));
}

glm::vec2 Terrain::TerrainQuadTreeDescriptor::Get2DCoords(TreeTrait::Key const& key)
{
    glm::vec2 res;

    static const glm::vec2 dirs[] =
    {
        glm::vec2(0.f, 0.f),
        glm::vec2(1.f, 0.f),
        glm::vec2(0.f, 1.f),
        glm::vec2(1.f, 1.f),
    };

    for (int i = 0; i < key.depth; i++)
    {
        uint8_t l = key.RetrieveLocalPosition(i);
        res += dirs[l] * GetSizeAtDepth(i) * 0.5f;
    }
    return res;
}

Terrain::Terrain()
{
    
}

bool Terrain::FromDesc(TerrainDesc const& desc)
{
	_extents = desc.extents;

    _quadTree.GetDescriptor().SetEvaluator(_distanceBasedEvaluator);
    UpdateTerrainData(*desc.context);

	return true;
}

void Terrain::Shutdown()
{

}

void Terrain::Update(SceneContext const& context)
{
    KU_CPU_SCOPED_QUERY(TerrainUpdate);

    bool bNeedUpdate = _bForceTreeUpdate;
    glm::vec3 location = _subdivTargetTransform->GetPosition() - GetLocation();
    glm::vec2 loc2D = glm::vec2(location.x, location.z);
    loc2D = (loc2D + glm::vec2(GetExtents().x, GetExtents().z) * 0.5f) / glm::vec2(GetExtents().x, GetExtents().z);
    if (_tessellationEvaluationTechnique == TessellationEvaluationTechnique::TargetDistance)
        bNeedUpdate |= _distanceBasedEvaluator.UpdateTarget(glm::vec3(loc2D.x, 0, loc2D.y));

    if (_subdivTargetTransform)
    {
        if (bNeedUpdate)
	        UpdateTerrainData(context);
    }
}

void Terrain::UpdateTerrainData(SceneContext const& context)
{
    KU_CPU_SCOPED_QUERY(UpdateTerrain);
    Profiler::Get().CounterAdd("TerrainUpdated", 1);

    if (_forceTessellationPrecision > 0)
    {
        uint32_t maxQuadTreeDepth = static_cast<uint32_t>(_quadTree.GetMaxDepth());
        // TODO : Get tessellation capacity that more properly
        uint32_t _maxDownstreamTesselation = context.renderSystem->GetRenderingCapabilities().maxTessellation;
        float _maxTessellationFactor = _extents.x / _forceTessellationPrecision;
        _maxTessellationFactor /= static_cast<float>(_maxDownstreamTesselation);

        uint32_t quadTreeDepth = static_cast<uint32_t>(glm::ceil(glm::log2(_maxTessellationFactor)));
        quadTreeDepth = glm::clamp(quadTreeDepth, 0u, maxQuadTreeDepth);

        KU_ASSERT(_tessellationEvaluationTechnique == TessellationEvaluationTechnique::TargetDistance);
        _distanceBasedEvaluator.maxTessellationFactor = quadTreeDepth;
    }

    _quadTree.Evaluate();
    _bForceTreeUpdate = false;
    _versionIndex++;
}

void Terrain::ChangeTessellationEvaluationTechnique(TessellationEvaluationTechnique technique)
{
    if (_tessellationEvaluationTechnique == technique)
        return;

    switch (technique)
    {
    case SC::Terrain::TessellationEvaluationTechnique::TessellationFactor:
        _quadTree.GetDescriptor().SetEvaluator(_factorBasedEvaluator);
        break;
    case SC::Terrain::TessellationEvaluationTechnique::TargetDistance:
        _quadTree.GetDescriptor().SetEvaluator(_distanceBasedEvaluator);
        break;
    default:
        KU_ASSERT(0);
        return;
    }

    NotifyTerrainUpdate();
    _tessellationEvaluationTechnique = technique;
}

void Terrain::SetTessellationFactor(int newFactor) 
{ 
    if (_factorBasedEvaluator.UpdateTessellationFactor(newFactor))
        NotifyTerrainUpdate();
}

void Terrain::NotifyTerrainUpdate()
{
    _bForceTreeUpdate = true;
}

} // end of namespace SC