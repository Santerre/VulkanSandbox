#include "Scene/SceneComponent/DirectionalLight.h"

namespace SC
{

bool DirectionalLight::FromDirection(glm::vec3 direction)
{
	_direction = direction;
	return true;
}

}