#include "Scene/SceneComponent/StaticMesh.h"

namespace SC
{

bool StaticMesh::FromDesc(StaticMeshDesc const& desc)
{
	_mesh = desc.mesh;
	_texture = desc.texture;
	return true;
}

void StaticMesh::Shutdown()
{

}

}