#include "Scene/SceneComponent/Camera.h"

namespace SC
{

void Camera::FromViewportExtents(glm::uvec2 viewportExtents)
{
	_projection = glm::perspective(glm::radians(45.0f), viewportExtents.x / static_cast<float>(viewportExtents.y), 0.01f, 5000.0f);
	_projection[1][1] *= -1.f;

	_zeroPosition = glm::lookAt(glm::vec3(0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0));
	_extents = viewportExtents;
}

glm::mat4 Camera::GetViewProjection() const
{
	return _projection * GetView();
}

}
