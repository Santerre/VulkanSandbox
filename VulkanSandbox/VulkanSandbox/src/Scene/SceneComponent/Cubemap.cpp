#include "Scene/SceneComponent/Cubemap.h"

#include "Memory/HierarchicalMemory.h"
#include "Resource/ResourceMgr.h"
#include "Resource/Mesh.h"

namespace SC
{

bool Cubemap::FromDesc(CubemapDesc const& desc)
{
	_image = desc.image;
	_mesh = desc.resourceMgr->Get<Mesh>("M:/" HM_RES_MESH_PATH "/Test/Cube.raw");
	if (!_mesh)
		return false;

	return true;
}

void Cubemap::Shutdown()
{

}

}