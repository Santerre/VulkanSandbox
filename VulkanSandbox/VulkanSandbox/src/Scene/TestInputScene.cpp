#include "Scene/TestInputScene.h"

#include "Debugging/Assert.h"

#include "glm/gtc/matrix_transform.hpp"

#include "Resource/ResourceMgr.h"
#include "Core/Window.h"
#include "Memory/HierarchicalMemory.h"

#include "Resource/Mesh.h"
#include "Resource/Texture.h"
#include "Input/Input.h"

#include "Debugging/Log.h"

bool TestInputScene::FromDesc(SceneDesc const& desc)
{
	if (!Scene::FromDesc(desc))
		return false;

	KU_ASSERT(_context.resourceMgr);

	SetupInput();

	_camera = std::make_unique<SC::Camera>();
	_camera->FromViewportExtents(_context.window->GetExtents());

	_staticMesh = std::make_unique<SC::StaticMesh>();
	{
		SC::StaticMeshDesc smDesc = {};

		//Handle<Mesh> mesh = desc.resourceMgr->Get<Mesh>("M:/" HM_RES_MESH_PATH "/Test/Rectangle.raw");
		//Handle<Texture> texture = desc.resourceMgr->Get<Texture>("D:/Images/TestTexture.png");

		Handle<Mesh> mesh = _context.resourceMgr->Get<Mesh>("D:/Models/Chalet/chalet.obj");
		Handle<Texture> texture = _context.resourceMgr->Get<Texture>("D:/Models/Chalet/chalet.jpg");

		KU_ASSERT(mesh);
		smDesc.mesh = mesh;
		smDesc.texture = texture;

		if (!_staticMesh->FromDesc(smDesc))
			return false;
	}
	return true;
}

void TestInputScene::Update(float deltaTime)
{
	Scene::Update(deltaTime);

/*	rotationTime += deltaTime;

	glm::mat4 rot = glm::rotate(glm::mat4(1.f), glm::radians(-50.f), glm::vec3(1.0f, 0.0f, 0.f));
	_staticMesh->SetTransform(glm::rotate(rot, rotationTime * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
	_camera->SetView(glm::lookAt(glm::vec3(0.f, 4.f, 0.f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)));*/
}

void TestInputScene::Shutdown()
{

}

void TestInputScene::TestOutput(std::string const& str)
{
	WLOG(LogTemp, str.c_str());
}

void TestInputScene::SetupInput()
{
	using namespace Input;
	KU_ASSERT(_context.inputManager);

	{
		InputContext context("Game", 0);
		{
			auto& action = context.AddAction(
			"Test",
			{
					{ RawInput::K_SPACE }
				,	{ RawInput::K_D }
				,	{ RawInput::K_S, InputStates::PRESSED }
			});

			action.onAction.AddListener([this]()
			{
				TestOutput("Test Activated");
			});
		}

		{
			auto& action = context.AddAction(
			"Test2",
			{
					{ RawInput::K_SPACE, InputStates::PRESSED}
				,	{ RawInput::K_D }
				,	{ RawInput::K_W, InputStates::PRESSED }
			});

			action.onAction.AddListener([this]()
			{
				TestOutput("Test2 Activated");
			});
		}

		{
			auto& state = context.AddState(
			"TestState",
			{
						{ RawInput::MOUSE_BUTTON_LEFT }
					,	{ RawInput::MOUSE_BUTTON_RIGHT }
			});

			state.onEnterState.AddListener([this]()
			{
				TestOutput("Left pressed");
			});

			state.onExitState.AddListener([this]()
			{
				TestOutput("Left released");
			});
			
			state.onStateChange.AddListener([this](InputStates state)
			{
				TestOutput(std::string("Left changed : ") + (state == InputStates::PRESSED ? "Pressed" : "Released"));
			});
		}

		{
			auto& range = context.AddRange(
			"TestRange",
			{
					{ RawInput::K_W, 1.f }
				,	{ RawInput::K_S, -1.f }
			},
			{
					{ RawAxis::MOUSE_UP }
			});

			range.onValueChange.AddListener([this](float newValue)
			{
				TestOutput(std::string("Range Update : ") + std::to_string(newValue));
			});
		}

		_context.inputManager->CreateContext(std::move(context));
	}
}