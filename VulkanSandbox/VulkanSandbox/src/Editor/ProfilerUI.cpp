#include "Editor/ProfilerUI.h"

#include <string>

#include "Editor/EditorLogCategory.h"

#include "Resource/ProfileData.h"
#include "Resource/ResourceMgr.h"

#include "imgui.h"

ProfilerFrameView::ProfilerFrameView(Handle<ProfileData> targetedData, ResourceMgr* resourceManager)
    :   _targetedData(targetedData),
        _resourceManager(resourceManager)
{
}

void ProfilerFrameView::Draw()
{
    KU_ASSERT(_targetedData);

    _path.reserve(255);
    if (ImGui::InputText("SaveToCsv", _path.data(), _path.capacity()))
        _path.resize(strlen(_path.data()));

    ImGui::SameLine();
    if (ImGui::Button("Save"))
    {
        _resourceManager->SaveResource(_targetedData, "D:/ProfilingDump/" + _path + ".csv");
        _lastSavedPath = _path;
    }

    auto&& data = _targetedData->GetData();
    for (size_t i = 0; i < data.size(); i++)
    {
        if (ImGui::TreeNode((std::string("Frame") + std::to_string(i)).c_str()))
        {
            ImGui::Text("CPU Dump");
            DrawNode(data[i].cpuDump, data[i].cpuDump.root, true);
            DrawCounters(data[i].cpuDump);

            ImGui::TreePop();
        }
    }
}

void ProfilerFrameView::DrawCounters(Profiler::RecordingScopeDump const& scope)
{
    if (ImGui::TreeNode("Counters"))
    {
        for (auto&& [name, counter] : scope.counters)
            ImGui::Value(name.c_str(), static_cast<int>(counter.value));
        ImGui::TreePop();
    }
}

void ProfilerFrameView::DrawNode(Profiler::RecordingScopeDump const& scope, Profiler::RecordingScopeDump::NodeDump const& nodeDump, bool bIsRoot)
{
    static constexpr double timeUnit = 1000.0;

    Profiler::RecordingScopeDump::RangeDump const& rangeDump = scope.ranges.at(nodeDump.rangeKey);

    #ifdef _DEBUG
    #define KU_PROFILER_FLOAT_PRECISION "2"
    #else 
    #define KU_PROFILER_FLOAT_PRECISION "5"
    #endif

    if (ImGui::TreeNode(rangeDump.name.c_str(), "%s : ""%0." KU_PROFILER_FLOAT_PRECISION "f ms", rangeDump.name.c_str(), rangeDump.totalTime * timeUnit))
    {
        if (!bIsRoot)
        {
            ImGui::Text("Self : %0." KU_PROFILER_FLOAT_PRECISION "f ms", (nodeDump.totalTime - nodeDump.childTime) * timeUnit);
            ImGui::Text("Count : %u", nodeDump.timers.size());
        }
        for (auto&& child : nodeDump.children)
            DrawNode(scope, child, false);
        ImGui::TreePop();
    }
    #undef KU_PROFILER_FLOAT_PRECISION

}

void ProfilerUI::FromDesc(InitDesc const& desc)
{ 
    _resourceManager = desc.resourceManager;
    _onRequestStartProfiling = desc.onRequestStartProfiling;
    _onRequestEndProfiling = desc.onRequestEndProfiling;
}

void ProfilerUI::Shutdown()
{
    _currentIndex = -1;
    _views.clear();
}

void ProfilerUI::AddDisplayedProfileData(Handle<ProfileData> data)
{
    size_t index = _views.size();
    ProfilerFrameView& savedFrame = _views.emplace_back(data, _resourceManager);
    savedFrame.SetPath("Frame" + std::to_string(index));

    _currentIndex = static_cast<int32_t>(index);
}

void ProfilerUI::AskStartProfiling(int32_t frameCount)
{
    _recordFrameLeft = frameCount;
    _onRequestStartProfiling();
}

void ProfilerUI::AskEndProfiling()
{
    _onRequestEndProfiling();
    _recordFrameLeft = 0;
}

void ProfilerUI::Update(bool bProfilerRecording)
{
    if (!bProfilerRecording && _recordFrameLeft)
    {
        WLOG(LogEditor, "Frame left to record not null but the profiler isn't recording");
        _recordFrameLeft = 0;
    }

    if (_recordFrameLeft > 0)
        if (!--_recordFrameLeft)
            AskEndProfiling();
}

void ProfilerUI::Draw()
{
    if (!_isOpen)
        return;

    if (ImGui::Begin("Profiler", &_isOpen))
    {
        if (_recordFrameLeft == 0)
        {
            if (ImGui::Button("Save Frame"))
            {
                AskStartProfiling(1);
            }

            ImGui::SameLine();
            if (ImGui::Button("StartRecord"))
                AskStartProfiling(-1);
        }
        else 
        {
            if (ImGui::Button("EndRecord"))
                AskEndProfiling();
        }

        if (ImGui::TreeNode("Records"))
        {
            for (size_t i = 0; i < _views.size(); i++)
            {
                std::string id = std::string("Record ") + std::to_string(i);
                if (ImGui::TreeNode(id.c_str(), _views[i].GetLastSavedPath() == "" ? id.c_str() : _views[i].GetPath().c_str()))
                {
                    _views[i].Draw();
                    ImGui::TreePop();
                }
            }
            ImGui::TreePop();
        }
    }
    ImGui::End();
}
