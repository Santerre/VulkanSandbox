#include "Editor/ResourceEditor.h"

#include <algorithm>

#include "Debugging/Assert.h"
#include "Editor/EditorLogCategory.h"


void ResourceWindow::Shutdown() 
{ 
    _resourceView->Shutdown();
}

void ResourceWindow::Draw() 
{ 
    ImGui::Begin(_resourceView->GetResource()->GetResource()->GetName().c_str(), &_isOpened);
    _resourceView->Draw(); 
    ImGui::End();
}

void ResourceEditor::Init(ResourceMgr* resManager) 
{
    _resManager = resManager; 
}

void ResourceEditor::Shutdown()
{
    DeleteResourceViews();
    _showResourceQueries.clear();
}

void ResourceEditor::LoadResources( std::vector<std::shared_ptr<IResourceWrapper>> const& resources )
{
    UpdateResourceViews({ resources }, {});
}

void ResourceEditor::AddResource( std::shared_ptr<IResourceWrapper> resource )
{
    auto res = std::find(_viewedResources.begin(), _viewedResources.end(), resource);
    // Resource already exists, no need to add.
    if (res != _viewedResources.end())
        return;

    _viewedResources.push_back(resource);

    UpdateResourceViews({ resource }, {});
}

void ResourceEditor::UpdateResourceViews(std::vector<std::shared_ptr<IResourceWrapper>> const& addedRes, std::vector<std::shared_ptr<IResourceWrapper>> const& removedRes)
{
    DeleteResourceViews(removedRes);
    CreateResourceViews(addedRes);
}

void ResourceEditor::CreateResourceViews(std::vector<std::shared_ptr<IResourceWrapper>> const& addedRes)
{
    if (!addedRes.size())
        return;

    _resourceWindows.reserve(_resourceWindows.size() + addedRes.size());
    for (auto&& wrapper : addedRes)
    {
        auto resource = wrapper->GetResource();
        if (!resource->GetName().size())
            continue;

        std::shared_ptr<AResourceView> ptr;
        switch ( resource->GetType() )
        {
            case EResourceType::GLSLShader:
                ptr = std::static_pointer_cast<AResourceView>(std::make_shared<ResourceView<EResourceType::GLSLShader>>());
                break;
            case EResourceType::SPVShader:
                ptr = std::static_pointer_cast<AResourceView>(std::make_shared<ResourceView<EResourceType::SPVShader>>());
                break;
            case EResourceType::DynamicShader:
                ptr = std::static_pointer_cast<AResourceView>(std::make_shared<ResourceView<EResourceType::DynamicShader>>());
                break;
            case EResourceType::Texture:
                ptr = std::static_pointer_cast<AResourceView>(std::make_shared<ResourceView<EResourceType::Texture>>());
                break;
            case EResourceType::Image:
                ptr = std::static_pointer_cast<AResourceView>(std::make_shared<ResourceView<EResourceType::Image>>());
                break;
            default:
                ptr = std::static_pointer_cast<AResourceView>(std::make_shared<ResourceView<EResourceType::Unknown>>());
                break;
        }
        
        InitContext context = {};
        context.resEditor = this;
        context.resManager = _resManager;
        if (!KU_VERIFY(ptr->Init(context, wrapper)))
        {
            WLOG(LogEditor, "Couldn't load the resource view. Resource : %s", resource->GetName().c_str());
            continue;
        }

        _resourceWindows.push_back(ResourceWindow(ptr));
    }
}

void ResourceEditor::DeleteResourceViews()
{
    for (auto&& res : _resourceWindows)
        res.Shutdown();

    _resourceWindows.clear();
}

void ResourceEditor::DeleteResourceViews(std::vector<std::shared_ptr<IResourceWrapper>> const& removedRes)
{
    if (!removedRes.size())
        return;

    auto removedIt = std::remove_if(_resourceWindows.begin(), _resourceWindows.end(), [&removedRes](ResourceWindow const& res)
    {
        return std::find(removedRes.cbegin(), removedRes.cend(), res.GetResourceView()->GetResource()) != removedRes.cend();
    });

    // Meaning no resource where found
    KU_ASSERT(removedIt != _resourceWindows.end());

    std::for_each(removedIt, _resourceWindows.end(), [](ResourceWindow& res)
    {
        res.Shutdown();
    });
    
    _resourceWindows.erase(removedIt);
}

bool ResourceEditor::Draw()
{
    bool isOpen = true;
    ImGui::Begin("ResourceEditor", &isOpen);

	static char searchInput[512] = "";
	ImGui::InputText("Search", searchInput, 512);
	ImGui::BeginChild("##scrolling");
	std::string searchVal(searchInput);
	for (auto&& window : _resourceWindows)
	{
        std::string const& name = window.GetResourceView()->GetResource()->GetResource()->GetName();
		if (searchVal != "" && name.find(searchVal) == std::string::npos)
			continue;
		if (ImGui::Button(name.c_str()))
			window.Open();

        if (window.IsOpen())
            window.Draw();
	}
	ImGui::EndChild();
    
    ImGui::End();

    // TODO move it out of draw
    ProcessQueries();

    return isOpen;
}

bool ResourceEditor::ShowResourceView( Handle<Resource> res, bool bShow )
{
    auto result = std::find_if(_resourceWindows.begin(), _resourceWindows.end(), [res](ResourceWindow const& window) -> bool
    {
        return window.GetResourceView()->GetResource()->GetResource() == res;
    });

    if (result == _resourceWindows.end())
       return false;

    if (bShow)
        result->Open();
    else
        result->Close();
    return true;
}

void ResourceEditor::QueryShowResource(std::shared_ptr<IResourceWrapper> wrapper)
{
    ShowResourceQuery query {};
    query.resourceWrapper = wrapper;
    _showResourceQueries.push_back(query);
}

void ResourceEditor::ProcessQueries()
{
    for (auto&& query : _showResourceQueries)
    {
        if (!query.resourceWrapper)
        {
            WLOG(LogEditor, "A show resource was empty!");
            continue;
        }

        AddResource(query.resourceWrapper);
        KU_VERIFY(ShowResourceView(query.resourceWrapper->GetResource(), true));
    }

    _showResourceQueries.clear();
}
