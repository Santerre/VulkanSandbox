#include "Debugging/Profiler.h"

#include "Debugging/Assert.h"

#include "RHI/Vulkan/VKRHI_Device.h"
#include "RHI/Vulkan/VKRHI_PhysicalDevice.h"

#include "Platform/GenericPlatform.h"

#include <functional>

Profiler::Timer const& Profiler::RecordingTimer::GetTimer() const
{
    switch (type)
    {
    case RecordingTimerType::CPU:
        return cpuTimer;
    case RecordingTimerType::GPU:
        return gpuTimer.timer;
    default:
        KU_UNIMPLEMENTED_EXCEPTION();
        return cpuTimer;
    }
}

void Profiler::RecordingScope::Node::Cleanup()
{
    timerKeys.clear();
}

void Profiler::RecordingScope::StartRecording()
{
    _currentNode = &_rootNode;
}

void Profiler::RecordingScope::EndRecording()
{
    _currentNode = nullptr;
}

Profiler::RecordingTimer& Profiler::RecordingScope::BeginRangeTimer(std::string const& name)
{
    size_t id = Hash(name);
    RangeKey key = { id, _ranges };

    // if doesn't exist yet, create new child
    if (!PushChildRange(key))
    {
        auto&& [rangeIt, bNew] = _ranges.emplace(std::piecewise_construct, std::forward_as_tuple(id), std::forward_as_tuple());
        if (bNew)
            rangeIt->second.name = name;
    }

    RecordingTimer& timer = _recordingTimers.emplace_back();
    AddTimer({ _recordingTimers.size() - 1, _recordingTimers });
    return timer;
}

Profiler::RecordingTimer& Profiler::RecordingScope::EndRangeTimer()
{
    return PopChildRange().Unlock(_recordingTimers);
}

void Profiler::RecordingScope::CounterSet(std::string const& name, uint64_t value)
{
    _counters[name].value = value;
}

void Profiler::RecordingScope::CounterAdd(std::string const& name, uint64_t delta)
{
    _counters[name].value += delta;
}

void Profiler::RecordingScope::Cleanup()
{
    _rootNode.Cleanup();
}

Profiler::RecordingScopeDump Profiler::RecordingScope::GetDump() const
{
    Profiler::RecordingScopeDump res;
    // Ranges
    auto& rangeDumps = res.ranges;
    rangeDumps.reserve(_ranges.size());
    for (auto&& range : _ranges)
    {
        auto&&[rangeDump, bIsNew] = res.ranges.emplace(std::piecewise_construct, std::forward_as_tuple(range.first), std::forward_as_tuple());
        KU_ASSERT(bIsNew);
        rangeDump->second.name = range.second.name;
    }

    // Counters
    res.counters.reserve(_counters.size());
    for(auto&& [name, counter] : _counters)
    {
        auto&& newCounter = res.counters.emplace(std::piecewise_construct, std::forward_as_tuple(name), std::forward_as_tuple());
        newCounter.first->second.value = counter.value;
    }

    // Nodes
    // Adds a root range for consistency
    std::string rootName = "Root";
    size_t rootKey = _hashFunction(rootName);
    auto const& rootRange = rangeDumps.emplace(std::piecewise_construct, std::forward_as_tuple(rootKey), std::forward_as_tuple());
    rootRange.first->second.name = rootName;

    std::function<RecordingScopeDump::NodeDump(Profiler::RecordingScope::Node const&)> dumpNodeFn;
    dumpNodeFn = [&dumpNodeFn, this, &rangeDumps, &rootKey](Profiler::RecordingScope::Node const& strNode) -> RecordingScopeDump::NodeDump
    {
        RecordingScopeDump::NodeDump res;
        res.totalTime = 0.f;
        res.childTime = 0.f;

        res.children.reserve(strNode.children.size());
        for (auto&& child : strNode.children)
        {
            auto const& childDump = res.children.emplace_back(dumpNodeFn(child));
            res.childTime += childDump.totalTime;
        }

        // Root node is a special case
        if (!strNode.parentNode)
        {
            res.totalTime = res.childTime;
            res.rangeKey = rootKey;
        }
        else
        {
            res.timers.reserve(strNode.children.size());
            for (auto&& key : strNode.timerKeys)
            {
                auto&& timerSrc = key.Unlock(_recordingTimers);
                auto&& timerDump = res.timers.emplace_back();
                timerDump.start = timerSrc.GetTimer().start;
                timerDump.end = timerSrc.GetTimer().end;

                res.totalTime += timerDump.GetDuration();
            }
            
            res.rangeKey = strNode.range.Get(_ranges);
        }

        auto& rangeDump = rangeDumps[res.rangeKey];
        rangeDump.count++;
        rangeDump.totalTime += res.totalTime;
        
        KU_ASSERT(res.totalTime >= res.childTime);

        return res;
    };
    res.root = dumpNodeFn(_rootNode);

    return res;
}

bool Profiler::RecordingScope::PushChildRange(RangeKey rangeKey)
{
    KU_ASSERT(_currentNode);

    for (auto&& child : _currentNode->children)
    {
        if (child.range == rangeKey)
        {
            _currentNode = &child;
            return true;
        }
    }

    _currentNode = &_currentNode->children.emplace_back(_currentNode, rangeKey);
    return false;
}

Profiler::RecordingTimerKey Profiler::RecordingScope::PopChildRange()
{
    KU_ASSERT(_currentNode);
    KU_ASSERT(_currentNode != &_rootNode);

    RecordingTimerKey timerKey = _currentNode->timerKeys[_currentNode->timerKeys.size()-1];
    _currentNode = _currentNode->parentNode;
    return  timerKey;
}

void Profiler::RecordingScope::AddTimer(RecordingTimerKey key)
{
    KU_ASSERT(_currentNode);
    _currentNode->timerKeys.push_back(key);
}

void Profiler::Init()
{
}

void Profiler::Shutdown()
{
    KU_ASSERT(_recordingFrame == -1);
    _recordingFrame = -1;
}

Profiler::FrameHandle Profiler::NewFrame()
{
    ProfilerFrame& frame = _frameBuffer.Push();
    return static_cast<Profiler::FrameHandle>(_frameBuffer.GetHeadID());
}

void Profiler::StartRecord(FrameHandle frameHandle)
{
    KU_ASSERT(_recordingFrame == -1);
    _recordingFrame = frameHandle;

    ProfilerFrame& frame = GetCurrentFrame();
    frame = {};
    frame.frameStart = Platform::GetCPUTimestamp();

    frame.cpuScope.StartRecording();
    frame.gpuScope.StartRecording();
}

void Profiler::EndRecord()
{
    KU_ASSERT(_recordingFrame != -1);

    ProfilerFrame& frame = GetCurrentFrame();
    frame.cpuScope.EndRecording();
    frame.gpuScope.EndRecording();

    _recordingFrame = -1;
}


size_t Profiler::GetCurrentFramePoolOrCreate(VKRHI_Device const& device, size_t recRangeID)
{
    ProfilerFrame& frame = GetCurrentFrame();
    size_t poolIndex = GetPoolID(recRangeID);
    KU_ASSERT(poolIndex < frame.queryPools.size() + 1);

    if (poolIndex >= frame.queryPools.size())
    {
        QueryPoolDesc desc = {};
        desc.count = gpuQueryPoolSize;
        desc.type = VK_QUERY_TYPE_TIMESTAMP;
        VKRHI_QueryPool& pool = frame.queryPools.emplace_back();
        pool.FromDescriptor(device, desc);
    }
    return poolIndex;
}

Profiler::ProfilerFrame& Profiler::GetCurrentFrame() 
{ 
    KU_ASSERT(_recordingFrame != -1);
    return GetFrame(_recordingFrame);
}

void Profiler::BeginCPURange(std::string const& name)
{
    if (_recordingFrame < 0)
        return;

    ProfilerFrame& frame = GetCurrentFrame();

    Profiler::RecordingTimer& timer = frame.cpuScope.BeginRangeTimer(name);
    timer.type = RecordingTimerType::CPU;
    timer.cpuTimer.start = static_cast<double>(Platform::GetCPUTimestamp() - frame.frameStart) / Platform::GetCPUFrequency();
}

void Profiler::EndCPURange()
{
    if (_recordingFrame < 0)
        return;

    ProfilerFrame& frame = GetCurrentFrame();
    Profiler::RecordingTimer& recTimer = frame.cpuScope.EndRangeTimer();
    recTimer.cpuTimer.end = static_cast<double>(Platform::GetCPUTimestamp() - frame.frameStart) / Platform::GetCPUFrequency();
}

void Profiler::CounterSet(std::string const& name, int64_t value)
{
    if (_recordingFrame < 0)
        return;

    ProfilerFrame& frame = GetCurrentFrame();
    frame.cpuScope.CounterSet(name, value);
}

void Profiler::CounterAdd(std::string const& name, int64_t delta)
{
    if (_recordingFrame < 0)
        return;

    ProfilerFrame& frame = GetCurrentFrame();
    frame.cpuScope.CounterAdd(name, delta);
}

void Profiler::CmdBeginGPURange(VKRHI_Device const& device, VkCommandBuffer buffer, std::string const& name, VkPipelineStageFlagBits stage)
{
    if (_recordingFrame < 0)
        return;

    // TODO fix GPU profiler
    return;

    ProfilerFrame& frame = GetCurrentFrame();
    Profiler::RecordingTimer& timer = frame.gpuScope.BeginRangeTimer(name);
    timer.type = RecordingTimerType::GPU;

    // Begin queries on pair indices
    size_t recRangeID = frame.queryPairID * 2;
    size_t poolIndex = GetCurrentFramePoolOrCreate(device, recRangeID);

    VKRHI_QueryPool& pool = frame.queryPools[poolIndex];
    timer.gpuTimer.startQuery.query = pool.CmdWriteTimestamp(buffer, recRangeID % gpuQueryPoolSize, stage);
    timer.gpuTimer.startQuery.localPoolIndex = poolIndex;

    frame.queryPairID++;
}

void Profiler::CmdEndGPURange(VKRHI_Device const& device, VkCommandBuffer buffer, std::string const& name, VkPipelineStageFlagBits stage)
{
    if (_recordingFrame < 0)
        return;

    // TODO fix GPU profiler
    return;

    ProfilerFrame& frame = GetCurrentFrame();
    Profiler::RecordingTimer& recTimer = frame.gpuScope.EndRangeTimer();

    // End queries on inpair indices
    size_t recRangeID = recTimer.gpuTimer.startQuery.query.GetID() + 1;
    KU_ASSERT(recRangeID < gpuQueryPoolSize);
    size_t poolIndex = recTimer.gpuTimer.startQuery.localPoolIndex;

    VKRHI_QueryPool& pool = frame.queryPools[poolIndex];
    recTimer.gpuTimer.endQuery.query = pool.CmdWriteTimestamp(buffer, recRangeID % gpuQueryPoolSize, stage);
    recTimer.gpuTimer.endQuery.localPoolIndex = poolIndex;
}

void Profiler::ResolveGPURanges(FrameHandle frameHandle, VKRHI_Device const& device)
{
    // TODO fix GPU profiler
    return;

    /*
    ProfilerFrame& frame = GetFrame(frameHandle);
    size_t total = frame.queryPairID * 2;

    if (!total)
        return;

    std::vector<uint32_t> results;
    results.reserve(total);

    // Retrieve queries
    for (size_t currentID = 0; currentID < total;)
    {
        size_t remaining = total - currentID;
        size_t itemCount = std::max(remaining, gpuQueryPoolSize);
        frame.queryPools[GetPoolID(currentID)].GetQueryResults<uint32_t>(0, itemCount, results);
        currentID += itemCount;
    }

    // Update ranges
    float period = device.GetPhysicalDevice().GetPhysicalDeviceProperties().deviceProperties.properties.limits.timestampPeriod;
    auto getResultInSec = [&results, period](GPURecordingRange::PoolQuery const& poolQuery) -> double
    {
        return results[poolQuery.localPoolIndex + poolQuery.query.GetID()] / static_cast<double>(period);
    };

    for (auto&& recRange : frame.gpuRecordingRanges)
    {
        Range& range = recRange.recordRange.range.Unlock(frame.ranges);
        recRange.recordRange.start = getResultInSec(recRange.startQuery);
        recRange.recordRange.end = getResultInSec(recRange.endQuery);
        range.duration = recRange.recordRange.end - recRange.recordRange.start;
        range.count++;
    }

    // Clean queries
    for (auto&& pool : frame.queryPools)
    {
        pool.Shutdown();
    }
    frame.queryPools.clear();*/
}

Profiler::ProfilerFrameDump Profiler::GetDumpFromFrame(FrameHandle handle) const
{
    ProfilerFrameDump res;
    ProfilerFrame const& frame = GetFrame(handle);

    res.cpuDump = frame.cpuScope.GetDump();

    return res;
}

Profiler::ProfilerFrameDump Profiler::PopFrame()
{ 
    Profiler::ProfilerFrameDump frameDump = GetDumpFromFrame(static_cast<Profiler::FrameHandle>(_frameBuffer.GetTailID()));
    _frameBuffer.Pop();
    return frameDump;
}
