#include "Platform/GenericPlatform.h"

#include <filesystem>

Path GenericPlatform::SolveFilePath(Path const& path)
{
	Path volumePath;
	if (path.GetVolume().GetString() == "D:")
		volumePath = Path(std::string("Resources"));
	else
	{
		KU_ASSERT(0);
		volumePath = Path(std::string("./"));
	}
	
	return volumePath.Concat(path.GetRelativePath()).Concat(path.GetExtendedName());
}

bool GenericPlatform::CreateDirectory(Path const& path)
{
	std::error_code ec;
	return std::filesystem::create_directories(path.GetStdPath(), ec);
}