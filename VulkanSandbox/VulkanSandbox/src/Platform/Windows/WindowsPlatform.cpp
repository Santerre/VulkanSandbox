#include "Platform/Windows/WindowsPlatform.h"

#include "Platform/GenericPlatform.h"

#include <Windows.h>

Path WindowsPlatform::SolveFilePath(Path const& path)
{
	return GenericPlatform::SolveFilePath(path);
}

#pragma push_macro("CreateDirectory")
#undef CreateDirectory
bool WindowsPlatform::CreateDirectory(Path const& path)
{
	return GenericPlatform::CreateDirectory(path);
}
#pragma pop_macro("CreateDirectory")

uint64_t WindowsPlatform::GetCPUTimestamp()
{
    LARGE_INTEGER li;
    ::QueryPerformanceCounter(&li);
    return li.QuadPart;
}

uint64_t WindowsPlatform::GetCPUFrequency()
{
    LARGE_INTEGER li;
    ::QueryPerformanceFrequency(&li);
    return li.QuadPart;
}