#include "RHI/Vulkan/VKRHI_PhysicalDevice.h"

#include <vector>
#include <algorithm>

bool QueueFamilyIndices::IsComplete() const
{
	return graphicsFamily.has_value() && presentFamily.has_value();
}

VKRHI_DeviceProperties::VKRHI_DeviceProperties()
{
    meshShaderProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_PROPERTIES_NV;
    meshShaderProperties.pNext = nullptr;

    deviceProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    deviceProperties.pNext = &meshShaderProperties;
}

VKRHI_DeviceFeatures::VKRHI_DeviceFeatures()
{
    meshShaderFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_FEATURES_NV;
    meshShaderFeatures.pNext = nullptr;

    deviceFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    deviceFeatures.pNext = &meshShaderFeatures;
}

int VKRHI_PhysicalDevice::Score(std::vector<const char*> const& optionnalDeviceExtensions) const
{
	int score = 0;

	if (_properties.deviceProperties.properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
		score += 1000;

    for (auto&& extension : optionnalDeviceExtensions)
        score += 100;

	score += _properties.deviceProperties.properties.limits.maxImageDimension2D;

	return score;
}

bool VKRHI_PhysicalDevice::IsSuitable() const
{
    auto& deviceFeatures = _supportedFeatures.deviceFeatures.features;
	if (!deviceFeatures.geometryShader)
		return false;
	if (!deviceFeatures.tessellationShader)
		return false;
	if (!deviceFeatures.samplerAnisotropy)
		return false;
	if (!deviceFeatures.fillModeNonSolid)
		return false;
    if (!deviceFeatures.wideLines)
		return false;
	if (!_queueFamilyIndices.IsComplete())
		return false;

	if (_swapChainSupportDetails.formats.empty() || _swapChainSupportDetails.presentModes.empty())
		return false;

	return true;
}

bool VKRHI_PhysicalDevice::SupportsExtensions(std::vector<const char*> const& requiredExtensions) const
{
	uint32_t propertyCount;
	vkEnumerateDeviceExtensionProperties(_device, nullptr, &propertyCount, nullptr);
	std::vector<VkExtensionProperties> extensionProperties(propertyCount);
	vkEnumerateDeviceExtensionProperties(_device, nullptr, &propertyCount, extensionProperties.data());

	for (auto&& requiredExtension : requiredExtensions)
	{
		auto res = std::find_if(extensionProperties.begin(), extensionProperties.end(), [&requiredExtension](VkExtensionProperties const& item) -> bool
		{
			return strcmp(item.extensionName, requiredExtension) == 0;
		});

		if (res == extensionProperties.end())
			return false;
	}

	return true;
}

void VKRHI_PhysicalDevice::FromVkDevice(VkInstance instance, VkPhysicalDevice const& device, VkSurfaceKHR const& surface)
{
	_device = device;

	vkGetPhysicalDeviceProperties2(_device, &_properties.deviceProperties);
	vkGetPhysicalDeviceFeatures2(_device, &_supportedFeatures.deviceFeatures);
	InitQueueFamily(surface);

	LoadSwapChainSupportDetails(surface);
}

void VKRHI_PhysicalDevice::LoadSwapChainSupportDetails(VkSurfaceKHR const& surface)
{
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(_device, surface, &_swapChainSupportDetails.capabilities);

	{
		uint32_t count;
		vkGetPhysicalDeviceSurfaceFormatsKHR(_device, surface, &count, nullptr);
		_swapChainSupportDetails.formats.resize(count);
		vkGetPhysicalDeviceSurfaceFormatsKHR(_device, surface, &count, _swapChainSupportDetails.formats.data());
	}

	{
		uint32_t count;
		vkGetPhysicalDeviceSurfacePresentModesKHR(_device, surface, &count, nullptr);
		_swapChainSupportDetails.presentModes.resize(count);
		vkGetPhysicalDeviceSurfacePresentModesKHR(_device, surface, &count, _swapChainSupportDetails.presentModes.data());
	}
}

VkFormatProperties VKRHI_PhysicalDevice::GetPhysicalDeviceFormatProperties(VkFormat format) const
{
	VkFormatProperties props;
	vkGetPhysicalDeviceFormatProperties(_device, format, &props);
	return props;
}

void VKRHI_PhysicalDevice::InitQueueFamily(VkSurfaceKHR const& surface)
{
	uint32_t queueFamilyCount;
	vkGetPhysicalDeviceQueueFamilyProperties(_device, &queueFamilyCount, nullptr);

	_queueFamilyProperties.resize(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(_device, &queueFamilyCount, _queueFamilyProperties.data());

	for (uint32_t i = 0u; i < queueFamilyCount; i++)
	{
		if (_queueFamilyProperties[i].queueCount > 0 && _queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
			_queueFamilyIndices.graphicsFamily = i;

		VkBool32 isSurfaceSupported = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(_device, i, surface, &isSurfaceSupported);
		if (isSurfaceSupported)
			_queueFamilyIndices.presentFamily = i;

		if (_queueFamilyIndices.IsComplete())
			break;
	}
}

bool VKRHI_PhysicalDevice::SupportsMeshShading() const
{
    if (!_supportedFeatures.meshShaderFeatures.meshShader || !_supportedFeatures.meshShaderFeatures.taskShader)
        return false;

    if (!SupportsExtensions({ VK_NV_MESH_SHADER_EXTENSION_NAME }))
        return false;

    return true;
}
