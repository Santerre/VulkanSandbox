#include "RHI/Vulkan/VKRHI_Semaphore.h"

#include "Debugging/Log.h"
#include "RHI/Vulkan/VKRHI_LogCategory.h"

void VKRHI_Semaphore::Initialize(VKRHI_Device const& device)
{
	VkSemaphoreCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	
	_device = &device;

	if (vkCreateSemaphore(device.GetVkDevice(), &createInfo, nullptr, &_vkSemaphore) != VK_SUCCESS)
	{
		WLOG(LogVulkan, "Couldn't create the semaphore.");
		return;
	}
}

void VKRHI_Semaphore::Shutdown()
{
	if (_device && _vkSemaphore)
		vkDestroySemaphore(_device->GetVkDevice(), _vkSemaphore, nullptr);
	_device = nullptr;
}