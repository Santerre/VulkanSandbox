#include "RHI/Vulkan/VKRHI_GraphicalMemoryAllocator.h"

#include "Debugging/Assert.h"

#include "RHI/Vulkan/VKRHI_Device.h"
#include "RHI/Vulkan/VKRHI_MemoryObject.h"
#include "RHI/Vulkan/VKRHI_PhysicalDevice.h"
#include "RHI/Vulkan/VKRHI_LogCategory.h"

void VKRHI_GraphicalMemoryAllocator::FromDevice(VKRHI_Device& device)
{
	_device = &device;
}

bool VKRHI_GraphicalMemoryAllocator::AllocateMemory(size_t allocationSize, uint32_t memoryTypeBits, VkMemoryPropertyFlags memoryFlags, VKRHI_GraphicalMemorySlot& outMemory)
{
	KU_ASSERT(_device);

	VkMemoryAllocateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	info.allocationSize = allocationSize;
	if (!FindMemoryType(memoryTypeBits, memoryFlags, info.memoryTypeIndex))
	{
		ELOG(LogVulkan, "Couldn't allocate memory : no memory type corresponding on physical device.");
		return false;
	}

	VkDeviceMemory memory;
	if (!vkAllocateMemory(_device->GetVkDevice(), &info, nullptr, &memory) == VK_SUCCESS)
	{
		ELOG(LogVulkan, "Couldn't allocate memory for device.");
		return false;
	}

	outMemory._offset = 0;
	outMemory._size = allocationSize;
	outMemory._vkMemory = memory;
	return true;
}

void VKRHI_GraphicalMemoryAllocator::ReleaseMemory(VKRHI_GraphicalMemorySlot& memory)
{
	KU_ASSERT(_device);
	vkFreeMemory(_device->GetVkDevice(), memory.GetVkMemory(), nullptr);
	memory = {};
}

bool VKRHI_GraphicalMemoryAllocator::FindMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties, uint32_t& outIndexRes) const
{
	KU_ASSERT(_device);

	VkPhysicalDeviceMemoryProperties physicalDeviceProps;
	vkGetPhysicalDeviceMemoryProperties(_device->GetPhysicalDevice().GetVkPhysicalDevice(), &physicalDeviceProps);

	for (uint32_t i = 0u; i < physicalDeviceProps.memoryTypeCount; i++)
	{
		if ((typeFilter & (1 << i)) && (physicalDeviceProps.memoryTypes[i].propertyFlags & properties) == properties)
		{
			outIndexRes = i;
			return true;
		}
	}

	return false;
}
