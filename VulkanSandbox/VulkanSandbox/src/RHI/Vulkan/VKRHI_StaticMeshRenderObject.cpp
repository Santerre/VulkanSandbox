#include "RHI/Vulkan/VKRHI_StaticMeshRenderObject.h"

bool VKRHI_StaticMeshRenderObject::FromDesc(VKRHI_StaticMeshRenderObjectDesc const& desc)
{
	_meshRenderObject.FromDesc(desc.meshRODesc);

	_textureBufferKey = desc.textureBufferKey;

	return true;
}

void VKRHI_StaticMeshRenderObject::Shutdown()
{

}