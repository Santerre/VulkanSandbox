#include "RHI/Vulkan/VKRHI_AvailableDescSetLayouts.h"

#include <algorithm>
#include <numeric>

#include "Debugging/Assert.h"

#include "RHI/Vulkan/VKRHI_Device.h"

VKRHI_DescriptorSetBinding::VKRHI_DescriptorSetBinding(uint32_t _binding, VkDescriptorType _descriptorType, VkShaderStageFlags _stageFlags)
{
	binding = _binding;
	descriptorType = _descriptorType;
	stageFlags = _stageFlags;
}

VKRHI_DescriptorSetBinding::VKRHI_DescriptorSetBinding(uint32_t _binding, VkDescriptorType _descriptorType, uint32_t _arraySize, VkShaderStageFlags _stageFlags)
: VKRHI_DescriptorSetBinding(_binding, _descriptorType, _stageFlags)
{
	arraySize = _arraySize;
}


bool VKRHI_DescriptorSetLayout::FromBindings(VKRHI_Device* device, std::vector<VKRHI_DescriptorSetBinding>&& bindings)
{
	KU_ASSERT(device);
	_device = device;
	_bindings = std::move(bindings);

	return Compile();
}

void VKRHI_DescriptorSetLayout::Shutdown()
{
	if (_device)
		vkDestroyDescriptorSetLayout(_device->GetVkDevice(), _rhiLayout, nullptr);
}

bool VKRHI_DescriptorSetLayout::Compile()
{
	KU_ASSERT(_device);

	size_t size = _bindings.size();
	std::vector<VkDescriptorSetLayoutBinding> vkBindings(size);
	std::transform( _bindings.begin(), _bindings.end(), vkBindings.begin(),
		[](VKRHI_DescriptorSetBinding const& binding) -> VkDescriptorSetLayoutBinding
		{
			VkDescriptorSetLayoutBinding vkBinding = {};
			vkBinding.binding = binding.binding;
			vkBinding.descriptorType = binding.descriptorType;
			vkBinding.stageFlags = binding.stageFlags;
			vkBinding.descriptorCount = binding.arraySize;
			vkBinding.pImmutableSamplers = nullptr;

			return vkBinding;
		}
	);
	
	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = static_cast<uint32_t>(size);
	layoutInfo.pBindings = vkBindings.data();

	if (vkCreateDescriptorSetLayout(_device->GetVkDevice(), &layoutInfo, nullptr, &_rhiLayout) != VK_SUCCESS)
		return false;

	return true;
}

void VKRHI_AvailableDescSetLayouts::Shutdown()
{
	for (auto&& layout : layouts)
		layout.Shutdown();
}

int VKRHI_AvailableDescSetLayouts::CountLayoutsOfType(VkDescriptorType type)
{
	return std::accumulate(layouts.begin(), layouts.end(), 0u, [type](uint32_t acc, VKRHI_DescriptorSetLayout const& desc)
	{
		auto const& vector = desc.GetBindings();
		return acc + std::accumulate(vector.begin(), vector.end(), 0u,
			[type](uint32_t acc, VKRHI_DescriptorSetBinding const& binding)
			{
				return acc + (type == binding.descriptorType ? binding.arraySize : 0u);
			});
	});
}

void VKRHI_AvailablePipelineLayouts::ShutdownLayouts(VKRHI_Device const& device)
{
	if (staticMeshLayout)
    {
		vkDestroyPipelineLayout(device.GetVkDevice(), staticMeshLayout, nullptr);
        staticMeshLayout = VK_NULL_HANDLE;
    }

	if (terrainLayout)
    {
		vkDestroyPipelineLayout(device.GetVkDevice(), terrainLayout, nullptr);
        terrainLayout = VK_NULL_HANDLE;
    }

    if (meshShaderTerrainLayout)
    {
        vkDestroyPipelineLayout(device.GetVkDevice(), meshShaderTerrainLayout, nullptr);
        meshShaderTerrainLayout = VK_NULL_HANDLE;
    }

	if (cubemapLayout)
    {
		vkDestroyPipelineLayout(device.GetVkDevice(), cubemapLayout, nullptr);
        cubemapLayout = VK_NULL_HANDLE;
    }
}