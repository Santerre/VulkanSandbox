#include "RHI/Vulkan/VKRHI_TransferQueue.h"

#include "Debugging/Assert.h"
#include "Debugging/Log.h"

#include "Resource/Image.h"

#include "RHI/Vulkan/VKRHI_LogCategory.h"

#include "RHI/Vulkan/VKRHI_Device.h"
#include "RHI/Vulkan/VKRHI_PhysicalDevice.h"

#include "RHI/Vulkan/VKRHI_MemoryObject.h"
#include "RHI/Vulkan/VKRHI_TypeConverters.h"

void VKRHI_TransferQueue::FromDesc( VKRHI_Device& device, VKRHI_TransferQueueDescriptor const& desc )
{
    _device = &device;
    _queue = desc.queue;
    _graphMemAllocator = desc.graphMemAllocator;

    VkCommandPoolCreateInfo poolCreateInfo = {};
    poolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolCreateInfo.queueFamilyIndex = desc.queueFamilyIndex;
    poolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    if (vkCreateCommandPool(device.GetVkDevice(), &poolCreateInfo, nullptr, &_commandPool) != VK_SUCCESS)
    {
        ELOG(LogVulkan, "Couldn't create the command pool.");
        return;
    }

    _commandBuffer.FromCommandPool(device, _commandPool);
}

void VKRHI_TransferQueue::Shutdown()
{
    KU_ASSERT(_device);

    _commandBuffer.Shutdown();

    if (_commandPool)
        vkDestroyCommandPool(_device->GetVkDevice(), _commandPool, nullptr);
}

void VKRHI_TransferQueue::UpdateBufferMemory(VKRHI_Buffer& buffer, VKRHI_UpdateBufferMemoryDesc const& desc)
{
    VKRHI_CommandBuffer& cmdBuffer = BeginCommandBuffer();

    VKRHI_Buffer stageBuffer = {};
    LoadStageBuffer(stageBuffer, desc.source, desc.totalSize);
    CmdCopyBuffer(cmdBuffer, stageBuffer, buffer);

    EndCommandBuffer(cmdBuffer);

    stageBuffer.Shutdown();
}
void VKRHI_TransferQueue::UpdateImageMemory(VKRHI_ImageMemory& imageMem, VKRHI_UpdateImageMemoryDesc const& desc)
{
    KU_ASSERT(desc.source.size());
    KU_ASSERT(desc.totalSize);
	VKRHI_Buffer stageBuffer;
	LoadStageBuffer(stageBuffer, desc.source, desc.totalSize); 

    VKRHI_CommandBuffer& cmdBuffer = BeginCommandBuffer();
	
	CmdTransitionImageLayout(cmdBuffer, imageMem, desc.format, desc.layoutIn, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	CmdCopyBuffer(cmdBuffer, stageBuffer, imageMem, desc.layerCount);
	if (desc.bGenerateMipmaps && imageMem.GetMipmapCount())
	{
		KU_VERIFY(CmdGenerateMipmaps(cmdBuffer, imageMem, desc.format));
	}
	else
	{
		CmdTransitionImageLayout(cmdBuffer, imageMem, desc.format, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, desc.layoutOut);
	}
    EndCommandBuffer(cmdBuffer);

	stageBuffer.Shutdown();
}

VKRHI_CommandBuffer& VKRHI_TransferQueue::BeginCommandBuffer()
{
    _commandBuffer.BeginOneTime();
    return _commandBuffer;
}

void VKRHI_TransferQueue::EndCommandBuffer(VKRHI_CommandBuffer& cmdBuffer)
{
    cmdBuffer.End();
        
    SubmitTransfer();
    WaitTransfer();
}

void VKRHI_TransferQueue::LoadStageBuffer(VKRHI_Buffer& stageBuffer, void const* source, size_t size)
{
    LoadStageBuffer(stageBuffer, std::vector<void const*>{ source }, size);
}

void VKRHI_TransferQueue::LoadStageBuffer(VKRHI_Buffer& stageBuffer, std::vector<void const*> const& sources, size_t sourceLength)
{
    size_t sourcesCount = sources.size();

    BufferDesc stageBufferDesc = {};
    stageBufferDesc.allocator = _graphMemAllocator;
    stageBufferDesc.size = sourcesCount * sourceLength;
    stageBufferDesc.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    stageBufferDesc.properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

    KU_ASSERT(_device);
    stageBuffer.FromDesc(*_device, stageBufferDesc);

    void* data;
    stageBuffer.MapMemory(&data);
    for (size_t i = 0u; i < sourcesCount; i++)
        memcpy((char*)data + i * sourceLength, sources[i], sourceLength);
    stageBuffer.UnmapMemory();
}

void VKRHI_TransferQueue::CmdCopyBuffer(VKRHI_CommandBuffer& cmdBuffer, VKRHI_Buffer& src, VKRHI_Buffer& dst)
{
    VkBufferCopy region = {};
    region.size = src.GetByteSize();
    vkCmdCopyBuffer(cmdBuffer.GetVkCommandBuffer(), src.GetVkBuffer(), dst.GetVkBuffer(), 1, &region);
}

void VKRHI_TransferQueue::CmdCopyBuffer(VKRHI_CommandBuffer& cmdBuffer, VKRHI_Buffer& src, VKRHI_ImageMemory& dst, uint32_t layerCount)
{
    VkBufferImageCopy region = {};

    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.layerCount = layerCount;

    region.imageOffset = { 0, 0, 0 };
    region.imageExtent = VKRHI_TypeConverters::GlmToVk(glm::uvec3{ dst.GetExtents(), 1 });

    vkCmdCopyBufferToImage(cmdBuffer.GetVkCommandBuffer(), src.GetVkBuffer(), dst.GetVkImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
}

bool VKRHI_TransferQueue::CmdTransitionImageLayout(VKRHI_CommandBuffer& cmdBuffer, VKRHI_ImageMemory& imageMem, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout)
{
    if (oldLayout == newLayout)
        return true;

    VkImageMemoryBarrier barrier = {};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.image = imageMem.GetVkImage();

    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = imageMem.GetMipmapCount();
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = imageMem.GetLayerCount();

    if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
        barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
    }
    else
    {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    }

    VkPipelineStageFlags srcStageMask;
    VkPipelineStageFlags dstStageMask;

    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        dstStageMask = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        srcStageMask = VK_PIPELINE_STAGE_TRANSFER_BIT;
        dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        dstStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    }
    else
    {
        ELOG(LogVulkan, "Image layout transfer failed : The given layout transition is unknown.");
        return false;
    }

    vkCmdPipelineBarrier(
        cmdBuffer.GetVkCommandBuffer(), srcStageMask, dstStageMask, 0,
        0, nullptr,
        0, nullptr,
        1, &barrier);

    return true;
}

bool VKRHI_TransferQueue::CmdGenerateMipmaps(VKRHI_CommandBuffer& cmdBuffer, VKRHI_ImageMemory& imageMem, VkFormat imageFormat)
{
    {
        // Check if image format supports linear blitting
        VkFormatProperties formatProperties = _device->GetPhysicalDevice().GetPhysicalDeviceFormatProperties(imageFormat);
        if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)) 
        {
            ELOG(LogVulkan, "texture imageMem format does not support linear blitting.");
            return false;
        }

        glm::uvec2 extents = imageMem.GetExtents();

        VkImageMemoryBarrier barrier = {};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.image = imageMem.GetVkImage();
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = imageMem.GetLayerCount();
        barrier.subresourceRange.levelCount = 1;

        uint32_t mipWidth = extents.x;
        uint32_t mipHeight = extents.y;
        for (uint32_t i = 1; i < imageMem.GetMipmapCount(); i++)
        {
            barrier.subresourceRange.baseMipLevel = i - 1;
            barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;


            vkCmdPipelineBarrier(cmdBuffer.GetVkCommandBuffer(),
                                  VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
                                  0, nullptr,
                                  0, nullptr,
                                  1, &barrier);

            VkImageBlit blit = {};
            blit.srcOffsets[0] = { 0, 0, 0 };
            blit.srcOffsets[1] = { static_cast<int32_t>(mipWidth), static_cast<int32_t>(mipHeight), 1 };
            blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            blit.srcSubresource.mipLevel = i - 1;
            blit.srcSubresource.baseArrayLayer = 0;
            blit.srcSubresource.layerCount = imageMem.GetLayerCount();
            blit.dstOffsets[0] = { 0, 0, 0 };
            blit.dstOffsets[1] = { static_cast<int32_t>(mipWidth > 1 ? mipWidth / 2 : 1), static_cast<int32_t>(mipHeight > 1 ? mipHeight / 2 : 1), 1 };
            blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            blit.dstSubresource.mipLevel = i;
            blit.dstSubresource.baseArrayLayer = 0;
            blit.dstSubresource.layerCount = imageMem.GetLayerCount();

            vkCmdBlitImage(cmdBuffer.GetVkCommandBuffer(),
                            imageMem.GetVkImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                            imageMem.GetVkImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                            1, &blit,
                            VK_FILTER_LINEAR);

            barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
            barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

            vkCmdPipelineBarrier(cmdBuffer.GetVkCommandBuffer(),
                                  VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
                                  0, nullptr,
                                  0, nullptr,
                                  1, &barrier);

            if (mipWidth > 1) mipWidth /= 2;
            if (mipHeight > 1) mipHeight /= 2;
        }

        barrier.subresourceRange.baseMipLevel = imageMem.GetMipmapCount() - 1;
        barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        vkCmdPipelineBarrier(cmdBuffer.GetVkCommandBuffer(),
                              VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
                              0, nullptr,
                              0, nullptr,
                              1, &barrier);
    }
    return true;
}

void VKRHI_TransferQueue::SubmitTransfer()
{
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &_commandBuffer.GetVkCommandBuffer();

    KU_VERIFY(vkQueueSubmit(_queue, 1, &submitInfo, VK_NULL_HANDLE) == VK_SUCCESS);
}

void VKRHI_TransferQueue::WaitTransfer()
{
    KU_VERIFY(vkQueueWaitIdle(_queue) == VK_SUCCESS);
}