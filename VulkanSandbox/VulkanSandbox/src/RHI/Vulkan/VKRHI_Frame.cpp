#include "RHI/Vulkan/VKRHI_Frame.h"

#include <array>

#include "imgui.h"
#include "imgui_impl_vulkan.h"

#include "Debugging/Assert.h"
#include "Debugging/Log.h"

#include "Core/Scene.h"

#include "Utils/IteratorUtils.h"

#include "RHI/Vulkan/VKRHI_TypeConverters.h"
#include "RHI/Vulkan/VKRHI_Device.h"
#include "RHI/Vulkan/VKRHI_SwapChain.h"
#include "RHI/Vulkan/VKRHI_LogCategory.h"
#include "RHI/Vulkan/VKRHI_MemoryObject.h"
#include "RHI/Vulkan/VKRHI_Image.h"
#include "RHI/Vulkan/VKRHI_RenderPass.h"
#include "RHI/Vulkan/VKRHI_TransferQueue.h"

#include "RHI/Vulkan/VKRHI_StaticMeshRenderObject.h"
#include "RHI/Vulkan/VKRHI_TerrainRenderObject.h"
#include "RHI/Vulkan/VKRHI_CubemapRenderObject.h"

#include "Debugging/Profiler.h"

bool VKRHI_Frame::FromVkFrameDescriptor(VKRHI_Device& device, VKRHI_FrameDescriptor& frameDescriptor)
{
	_swapChain = frameDescriptor.swapChain;
	_imageIndex = frameDescriptor.imageIndex;

	_device = &device;

	BufferDesc vBufferDesc = {};
	vBufferDesc.allocator = frameDescriptor.graphMemAllocator;
	vBufferDesc.size = 10000000 * sizeof(VKRHI_TerrainRenderVertData);
	vBufferDesc.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
	vBufferDesc.properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
	_terrainPatchBuffer.FromDesc(*_device, vBufferDesc);

	if (!CreateFramebuffers(frameDescriptor))
		return false;
	if (!CreateDescriptorSets(frameDescriptor))
		return false;
	if (!CreateCommandBuffers(frameDescriptor))
		return false;

	KU_ASSERT(_device);

	_renderFinishedSemaphore.Initialize(*_device);
	_imageRenderingFence.Initialize(*_device);

    _renderingOptions = frameDescriptor.renderingOptions;

	// Record static buffers
	KU_VERIFY(RecordCubemapSubCommandBuffer(frameDescriptor));
	KU_VERIFY(RecordOpaqueSubCommandBuffer(frameDescriptor));

	return true;
}

void VKRHI_Frame::Shutdown()
{
	for (auto objectUniformBuffer : _objectUniformBuffers)
		objectUniformBuffer.Shutdown();
	_globalUniformBuffer.Shutdown();
	_globalCamUniformBuffer.Shutdown();
	_globalLightUniformBuffer.Shutdown();

	_terrainPatchBuffer.Shutdown();
	_terrainUniformBuffer.Shutdown();
    _terrainMatUniformBuffer.Shutdown();

	_renderFinishedSemaphore.Shutdown();
	_imageRenderingFence.Shutdown();

	DestroyCommandBuffers();

    FreeAllDescriptorSets();
    _descriptorPool = VK_NULL_HANDLE;

	if (_opaqueFramebuffer)
		vkDestroyFramebuffer(_device->GetVkDevice(), _opaqueFramebuffer, nullptr);

	if (_editorFramebuffer)
		vkDestroyFramebuffer(_device->GetVkDevice(), _editorFramebuffer, nullptr);

	_device = nullptr;
	_swapChain = nullptr;
	_imageIndex = 0;
}

bool VKRHI_Frame::CreateFramebuffers(VKRHI_FrameDescriptor const& frameDescriptor)
{
	if (!KU_VERIFY(CreateFramebuffer(frameDescriptor, *frameDescriptor.opaqueRenderPass, _opaqueFramebuffer)))
	{
		ELOG(LogVulkan, "Couldn't create the object framebuffer");
		return false;
	}

#ifdef INCLUDE_EDITOR
	if (!KU_VERIFY(CreateFramebuffer(frameDescriptor, *frameDescriptor.editorRenderPass, _editorFramebuffer)))
	{
		ELOG(LogVulkan, "Couldn't create the editor framebuffer");
		return false;
	}
#endif

	return true;
}


bool VKRHI_Frame::CreateFramebuffer(VKRHI_FrameDescriptor const& frameDescriptor, VKRHI_RenderPass const& pass, VkFramebuffer& outFramebuffer)
{
	KU_ASSERT(_swapChain);
	glm::uvec2 frameExtents = pass.GetAreaExtents();

	std::vector<Handle<VKRHI_Image>> images = _swapChain->GetImageViews();
	KU_ASSERT(_imageIndex >= 0 && images.size() > _imageIndex);
	Handle<VKRHI_Image> image = images[_imageIndex];

	KU_ASSERT(frameDescriptor.depthBuffer);

	std::vector<VkImageView> views{ image->GetImageView() };
	if (pass.HasDepthStencilAttachment())
		views.push_back(frameDescriptor.depthBuffer->GetImageView());

	VkFramebufferCreateInfo framebufferInfo = {};
	framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferInfo.renderPass = pass.GetRHIRenderPass();
	framebufferInfo.attachmentCount = static_cast<uint32_t>(views.size());
	framebufferInfo.pAttachments = views.data();
	framebufferInfo.width = frameExtents.x;
	framebufferInfo.height = frameExtents.y;
	framebufferInfo.layers = 1;

	return vkCreateFramebuffer(_device->GetVkDevice(), &framebufferInfo, nullptr, &outFramebuffer) == VK_SUCCESS;
}

bool VKRHI_Frame::CreateDescriptorSets(VKRHI_FrameDescriptor const& frameDescriptor)
{
    _descriptorPool = frameDescriptor.descriptorPool;

	if (!CreateGlobalDescriptorSet(frameDescriptor))
		return false;

	if (!CreateMeshesDescriptorSet(frameDescriptor))
		return false;

	if (!CreateTerrainDescriptorSet(frameDescriptor))
		return false;

	if (!CreateCubemapDescriptorSet(frameDescriptor))
		return false;

	return true;
}

void VKRHI_Frame::FreeAllDescriptorSets()
{
    std::vector<VkDescriptorSet> descriptorSets(std::move(_objectDescriptorSets));
    _objectDescriptorSets.clear();

    if (_defaultMatDescriptorSet)
    {
        descriptorSets.push_back(_defaultMatDescriptorSet);
        _defaultMatDescriptorSet = VK_NULL_HANDLE;
    }

    if (_cubemapMatDescriptorSet)
    {
        descriptorSets.push_back(_cubemapMatDescriptorSet);
        _cubemapMatDescriptorSet = VK_NULL_HANDLE;
    }
        
    if (_terrainDescriptorSet)
    {
        descriptorSets.push_back(_terrainDescriptorSet);
        _terrainDescriptorSet = VK_NULL_HANDLE;
    }
    if (_terrainMeshShaderDescriptorSet)
    {
        descriptorSets.push_back(_terrainMeshShaderDescriptorSet);
        _terrainMeshShaderDescriptorSet = VK_NULL_HANDLE;
    }
    if (_terrainMatDescriptorSet)
    {
        descriptorSets.push_back(_terrainMatDescriptorSet);
        _terrainMatDescriptorSet = VK_NULL_HANDLE;
    }
        
    if (_globalDescriptorSet)
    {
        descriptorSets.push_back(_globalDescriptorSet);
        _globalDescriptorSet = VK_NULL_HANDLE;
    }

    FreeDescriptorSets(descriptorSets);
}

bool VKRHI_Frame::AllocateDescriptorSets(std::vector<VkDescriptorSetLayout> const& layouts, VkDescriptorSet* outDescSet)
{
	if (!layouts.size())
		return true;

	VkDescriptorSetAllocateInfo setAllocInfo = {};
	setAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	setAllocInfo.descriptorPool = _descriptorPool;
	setAllocInfo.descriptorSetCount = static_cast<uint32_t>(layouts.size());
	setAllocInfo.pSetLayouts = layouts.data();

	if (vkAllocateDescriptorSets(_device->GetVkDevice(), &setAllocInfo, outDescSet) != VK_SUCCESS)
		return false;
	return true;
}

bool VKRHI_Frame::AllocateNDescriptorSets(size_t count, VKRHI_DescriptorSetLayout const& layout, VkDescriptorSet* outDescSet)
{
	std::vector<VkDescriptorSetLayout> layouts(count, layout.GetRHILayout());
	return AllocateDescriptorSets(layouts, outDescSet);
}

bool VKRHI_Frame::FreeDescriptorSets(std::vector<VkDescriptorSet>& sets) const
{
	if (vkFreeDescriptorSets(_device->GetVkDevice(), _descriptorPool, static_cast<uint32_t>(sets.size()), sets.data()) != VK_SUCCESS)
		return false;
	return true;
}

bool VKRHI_Frame::AllocUniformBuffer(VKRHI_FrameDescriptor const& frameDescriptor, uint32_t size, VKRHI_Buffer& outBuffer)
{
	BufferDesc desc = {};
	desc.size = size;
	desc.allocator = frameDescriptor.graphMemAllocator;
	desc.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
	desc.properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

	KU_ASSERT(_device);
	if (!outBuffer.FromDesc(*_device, desc))
		return false;
	return true;
}

VkWriteDescriptorSet VKRHI_Frame::GetUBOWriter(VkDescriptorSet set, VKRHI_Buffer const& ubo, VkDescriptorBufferInfo& outInfoBuffer) const
{
    outInfoBuffer = {};
    outInfoBuffer = ubo.GetWholeBufferInfo();

	VkWriteDescriptorSet writeSet = {};

	writeSet = {};
	writeSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeSet.dstSet = set;
	writeSet.dstBinding = 0;
	writeSet.dstArrayElement = 0;
	writeSet.descriptorCount = 1;
	writeSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	writeSet.pBufferInfo = &outInfoBuffer;
	return writeSet;
}

VkWriteDescriptorSet VKRHI_Frame::GetImageWriter(VkDescriptorSet set, VKRHI_Image const& image, VkDescriptorImageInfo& outInfoBuffer) const
{
    outInfoBuffer = {};
    outInfoBuffer.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    outInfoBuffer.imageView = image.GetImageView();
    outInfoBuffer.sampler = image.GetImageSampler();

	VkWriteDescriptorSet writeSet = {};
	writeSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeSet.dstSet = set;
	writeSet.dstBinding = 0;
	writeSet.dstArrayElement = 0;
	writeSet.descriptorCount = 1;
	writeSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	writeSet.pImageInfo = &outInfoBuffer;
	return writeSet;
}

VkWriteDescriptorSet VKRHI_Frame::GetImageWriter(VkDescriptorSet set, std::vector<VKRHI_Image const*> const& images, std::vector<VkDescriptorImageInfo>& outInfoBuffer) const
{
	KU_ASSERT(outInfoBuffer.size() == images.size());

	std::transform(images.begin(), images.end(), outInfoBuffer.begin(),
		[](VKRHI_Image const* image)
	{
		VkDescriptorImageInfo infoBuffer = {};
		infoBuffer.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		infoBuffer.imageView = image->GetImageView();
		infoBuffer.sampler = image->GetImageSampler();
		return infoBuffer;
	});

	VkWriteDescriptorSet writeSet = {};
	writeSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeSet.dstSet = set;
	writeSet.dstBinding = 0;
	writeSet.dstArrayElement = 0;
	writeSet.descriptorCount = static_cast<uint32_t>(images.size());
	writeSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	writeSet.pImageInfo = outInfoBuffer.data();
	return writeSet;
}

bool VKRHI_Frame::CreateMeshesDescriptorSet(VKRHI_FrameDescriptor const& frameDescriptor)
{
	// Objects
	size_t count = frameDescriptor.scene->GetStaticMeshsCount();
	_objectDescriptorSets.resize(count);
	if (!AllocateNDescriptorSets(
			count,
			frameDescriptor.descSetLayouts->staticMesh.object,
			_objectDescriptorSets.data()))
	{
		return false;
	}

	{
		size_t count = _objectDescriptorSets.size();
		std::vector<VkWriteDescriptorSet> sets(count);
		std::vector<VkDescriptorBufferInfo> infos(count);
		_objectUniformBuffers.resize(count);
		for (size_t i = 0; i < count; i++)
		{
			AllocUniformBuffer(frameDescriptor, sizeof(MeshUniform), _objectUniformBuffers[i]);
			
			sets[i] = GetUBOWriter(_objectDescriptorSets[i], _objectUniformBuffers[i], infos[i]);
		}
		vkUpdateDescriptorSets(_device->GetVkDevice(), static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);
	}
    
    if (count)
    {
	    // Materials
	    if (!AllocateDescriptorSets(
			    { frameDescriptor.descSetLayouts->staticMesh.defaultMat.GetRHILayout() },
			    &_defaultMatDescriptorSet))
	    {
		    return false;
	    }

        std::vector<VkWriteDescriptorSet> sets(1);
	
        auto& image = (*frameDescriptor.meshesRO)[0].GetTextureBufferKey().albedo.Unlock(*frameDescriptor.images);
        VkDescriptorImageInfo imageInfo = {};
        sets[0] = GetImageWriter(_defaultMatDescriptorSet, image, imageInfo);
	
        vkUpdateDescriptorSets(_device->GetVkDevice(), static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);
    }

	return true;
}

bool VKRHI_Frame::CreateTerrainDescriptorSet(VKRHI_FrameDescriptor const& frameDescriptor)
{
	// Object
	if (!AllocateDescriptorSets(
		{ frameDescriptor.descSetLayouts->terrain.object.GetRHILayout() },
		&_terrainDescriptorSet))
	{
		return false;
	}

	{
		std::vector<VkWriteDescriptorSet> sets(1);
		AllocUniformBuffer(frameDescriptor, sizeof(TerrainUniform), _terrainUniformBuffer);

		VkDescriptorBufferInfo bufferInfo;
		sets[0] = GetUBOWriter(_terrainDescriptorSet, _terrainUniformBuffer, bufferInfo);

		vkUpdateDescriptorSets(_device->GetVkDevice(), static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);
	}

    // Mesh Shader Buffer
    if (frameDescriptor.renderingOptions.useMeshShading)
    {
        // Object
        if (!AllocateDescriptorSets(
            { frameDescriptor.descSetLayouts->terrain.meshShaderObject.GetRHILayout() },
            &_terrainMeshShaderDescriptorSet))
        {
            return false;
        }

        KU_ASSERT(_device->IsMeshShadingEnabled());

        std::vector<VkWriteDescriptorSet> sets(1);

        VkDescriptorBufferInfo bufferInfo;
        sets[0] = GetUBOWriter(_terrainMeshShaderDescriptorSet, _terrainPatchBuffer, bufferInfo);
        sets[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

        vkUpdateDescriptorSets(_device->GetVkDevice(), static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);
    }

	// Material
	if (!AllocateDescriptorSets(
		{ frameDescriptor.descSetLayouts->terrain.mat.GetRHILayout() },
		&_terrainMatDescriptorSet))
	{
		return false;
	}

	{
		TextureKeys const& heightMap = frameDescriptor.terrainRO->GetHeightTextureKey();
		TextureKeys const& grassTexture = frameDescriptor.terrainRO->GetGrassTextureKey();
		TextureKeys const& snowTexture = frameDescriptor.terrainRO->GetSnowTextureKey();
		TextureKeys const& cliffTexture = frameDescriptor.terrainRO->GetCliffTextureKey();

		std::array<VkWriteDescriptorSet, 5> sets;
		std::vector<VkDescriptorImageInfo> infos[4] = {};
		std::array<TextureKeys const*, 4> images = { &heightMap, &grassTexture, &snowTexture, &cliffTexture };
		for (size_t i = 0; i < images.size(); i++)
		{
			infos[i].resize(2);
			VKRHI_Image const* mainImage;
			if (i == 0)
				mainImage = &images[i]->heightMap.Unlock(*frameDescriptor.images);
			else
				mainImage = &images[i]->albedo.Unlock(*frameDescriptor.images);

			VKRHI_Image const* normal = &images[i]->normalMap.Unlock(*frameDescriptor.images);

			sets[i] = GetImageWriter(_terrainMatDescriptorSet, { mainImage, normal }, infos[i]);
			sets[i].dstBinding = static_cast<uint32_t>(i);
		}

		AllocUniformBuffer(frameDescriptor, sizeof(SC::TerrainMatUniform), _terrainMatUniformBuffer);

		VkDescriptorBufferInfo matBufferInfo;
		sets[4] = GetUBOWriter(_terrainMatDescriptorSet, _terrainMatUniformBuffer, matBufferInfo);
		sets[4].dstBinding = 4;

		vkUpdateDescriptorSets(_device->GetVkDevice(), static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);
	}

	return true;
}

bool VKRHI_Frame::CreateCubemapDescriptorSet(VKRHI_FrameDescriptor const& frameDescriptor)
{
	if (!AllocateDescriptorSets(
		{ frameDescriptor.descSetLayouts->cubemap.mat.GetRHILayout() },
		&_cubemapMatDescriptorSet))
	{
		return false;
	}

    if (!frameDescriptor.cubemapRO)
        return true;

	std::vector<VkWriteDescriptorSet> sets(1);
	VKRHI_Image const& image = frameDescriptor.cubemapRO->GetImageBufferKey().Unlock(*frameDescriptor.images);

	VkDescriptorImageInfo imageInfo;
	sets[0] = GetImageWriter(_cubemapMatDescriptorSet, image, imageInfo);
	vkUpdateDescriptorSets(_device->GetVkDevice(), static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);

	return true;
}

bool VKRHI_Frame::CreateGlobalDescriptorSet(VKRHI_FrameDescriptor const& frameDescriptor)
{
	if (!AllocateDescriptorSets(
		{ frameDescriptor.descSetLayouts->global.GetRHILayout() },
		&_globalDescriptorSet))
	{
		return false;
	}

	AllocUniformBuffer(frameDescriptor, sizeof(GlobalUniform), _globalUniformBuffer);
	AllocUniformBuffer(frameDescriptor, sizeof(GlobalCamUniform), _globalCamUniformBuffer);
	AllocUniformBuffer(frameDescriptor, sizeof(GlobalLightUniform), _globalLightUniformBuffer);

	std::array<VkWriteDescriptorSet, 3> sets;
	VkDescriptorBufferInfo infoGlobal;
	sets[0] = GetUBOWriter(_globalDescriptorSet, _globalUniformBuffer, infoGlobal);
	
	VkDescriptorBufferInfo infoCam;
	sets[1] = GetUBOWriter(_globalDescriptorSet, _globalCamUniformBuffer, infoCam);
	sets[1].dstBinding = 1;

	VkDescriptorBufferInfo infoLight;
	sets[2] = GetUBOWriter(_globalDescriptorSet, _globalLightUniformBuffer, infoLight);
	sets[2].dstBinding = 2;

	vkUpdateDescriptorSets(_device->GetVkDevice(), static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);

	return true;
}

void VKRHI_Frame::CmdBindMeshRenderObject(VKRHI_FrameDescriptor const& frameDescriptor, VkCommandBuffer commandBuffer, VKRHI_MeshRenderObject const& meshRenderObject)
{
	VKRHI_Buffer const& vertBuffer = meshRenderObject.GetVertexBufferKey().Unlock(*frameDescriptor.vertexBuffers);
	VkBuffer vertsBuffers[] = { vertBuffer.GetVkBuffer() };
	VkDeviceSize offsets[] = { 0 };
	vkCmdBindVertexBuffers(commandBuffer, 0, 1, vertsBuffers, offsets);

	VKRHI_Buffer const& indexBuffer = meshRenderObject.GetIndexBufferKey().Unlock(*frameDescriptor.indexBuffers);
	vkCmdBindIndexBuffer(commandBuffer, indexBuffer.GetVkBuffer(), 0, VK_INDEX_TYPE_UINT32);
}

void VKRHI_Frame::PrepareDataForDrawing(FrameRenderContext& context)
{
	Scene const* scene = context.frameDescriptor.scene;
	KU_ASSERT(scene);

	// Objects
	std::vector<SC::StaticMesh const*> meshes = { context.frameDescriptor.scene->GetStaticMesh() };
	for (size_t i = 0; i < _objectUniformBuffers.size(); i++)
	{
		MeshUniform objectUniform;
		objectUniform.model = meshes[i]->GetTransformMat();

		void* memory;
		_objectUniformBuffers[0].MapMemory(&memory);
		memcpy(memory, &objectUniform, sizeof(MeshUniform));
		_objectUniformBuffers[0].UnmapMemory();
	}

	// Terrain
    SC::Terrain const* terrain = scene->GetTerrain();
    if (terrain)
	{
		TerrainUniform terrainUniform;
		terrainUniform.location = terrain->GetLocation();
		terrainUniform.extents = terrain->GetExtents();

        void* memory;
		_terrainUniformBuffer.MapMemory(&memory);
		memcpy(memory, &terrainUniform, sizeof(TerrainUniform));
		_terrainUniformBuffer.UnmapMemory();

		_terrainMatUniformBuffer.MapMemory(&memory);
		memcpy(memory, &terrain->mat, sizeof(SC::TerrainMatUniform));
		_terrainMatUniformBuffer.UnmapMemory();

		// Tries to predict tessellation
		float totalTessellation = -1.0;
		if (context.frameDescriptor.renderingOptions.useMeshShading)
		{
			uint32_t subPatchTessellation = context.frameDescriptor.renderingOptions.subPatchTessellation;
			Profiler::Get().CounterAdd("TerrainSubPatchTessellation", subPatchTessellation);

			float taskTessellation = static_cast<float>(terrain->mat.forceTaskTessLevel);
			float meshTessellation = static_cast<float>(terrain->mat.forceMeshTessLevel);
			if (taskTessellation > 0)
				Profiler::Get().CounterAdd("TerrainTaskTessellation", static_cast<int64_t>(taskTessellation));
			if (meshTessellation > 0)
				Profiler::Get().CounterAdd("TerrainMeshTessellation", static_cast<int64_t>(meshTessellation));

			if (terrain->mat.forceTessLevel > 0)
				totalTessellation = terrain->mat.forceTessLevel * subPatchTessellation;

			if (taskTessellation > 0 && meshTessellation > 0)
				totalTessellation = taskTessellation * meshTessellation * subPatchTessellation;
		}
		else
		{
			if (terrain->mat.forceTessLevel > 0)
			{
				totalTessellation = terrain->mat.forceTessLevel;
				Profiler::Get().CounterAdd("TerrainTCSTessellation", static_cast<int64_t>(totalTessellation));
			}
		}
		if (totalTessellation)
			Profiler::Get().CounterAdd("TerrainGPUTotalTessellation", static_cast<int64_t>(totalTessellation));
        auto& ro = context.frameDescriptor.terrainRO;
        KU_ASSERT(ro);

		static bool bForceUpdate = false;
		if (bForceUpdate || terrain->GetVersionIndex() > _terrainVersionIndex)
		{
			_terrainVersionIndex = terrain->GetVersionIndex();

			uint32_t patchCount = terrain->GetQuadTree().GetLeafCount();
			uint32_t treeByteSize = patchCount * sizeof(VKRHI_TerrainRenderVertData);
			std::vector<VKRHI_TerrainRenderVertData> data(patchCount);

			int i = 0;
			for (auto&& nodeData : terrain->GetQuadTree())
			{
				glm::vec4 coords(SC::Terrain::TerrainQuadTreeDescriptor::Get2DCoords(nodeData.key), SC::Terrain::TerrainQuadTreeDescriptor::GetSize(nodeData.key), nodeData.key.depth);
				data[i++].objectSpaceCoords = coords;
			}

			KU_ASSERT(_terrainPatchBuffer.GetByteSize() > treeByteSize);
			//if (_terrainPatchBuffer.GetByteSize() < patchCount * sizeof(VKRHI_TerrainRenderVertData))
			//{ 
			//    _terrainPatchBuffer.Shutdown();	
			//    KU_ASSERT(_terrainPatchBuffer.GetByteSize());
			//    BufferDesc vBufferDesc = {};
			//    vBufferDesc.allocator = context.graphMemAlloc;
			//    vBufferDesc.size = patchCount * sizeof(VKRHI_TerrainRenderVertData) * 10;
			//    vBufferDesc.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
			//    vBufferDesc.properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
			//    
			//    _terrainPatchBuffer.FromDesc(*_device, vBufferDesc);
			//}

			VKRHI_UpdateBufferMemoryDesc desc = {};
			desc.source = { data.data() };
			desc.totalSize = treeByteSize;
			context.transferQueue->UpdateBufferMemory(_terrainPatchBuffer, desc);

			_terrainPatchCount = patchCount;
		}
	}

	// Global
	{
		GlobalUniform globalBuffer;
		globalBuffer.view = glm::inverse(scene->GetCamera()->GetView());
		globalBuffer.projection = scene->GetCamera()->GetProjection();

        globalBuffer.cullView = glm::inverse(scene->GetCullCamera()->GetView());

		void* memory;
		_globalUniformBuffer.MapMemory(&memory);
		memcpy(memory, &globalBuffer, sizeof(globalBuffer));
		_globalUniformBuffer.UnmapMemory();

		GlobalCamUniform globalCamBuffer;
		globalCamBuffer.eye = scene->GetCamera()->GetTransform().GetPosition();
		globalCamBuffer.camExtents = scene->GetCamera()->GetViewportExtents();

		memory = nullptr;
		_globalCamUniformBuffer.MapMemory(&memory);
		memcpy(memory, &globalCamBuffer, sizeof(GlobalCamUniform));
		_globalCamUniformBuffer.UnmapMemory();

		GlobalLightUniform globalLightBuffer;
		globalLightBuffer.ambient.color = scene->GetAmbientLight()->GetColor();
		globalLightBuffer.ambient.intensity = scene->GetAmbientLight()->GetIntensity();

		globalLightBuffer.directional.color = scene->GetDirectionalLight()->GetColor();
		globalLightBuffer.directional.intensity = scene->GetDirectionalLight()->GetIntensity();
		globalLightBuffer.directional.direction = scene->GetDirectionalLight()->GetDirection();

		memory = nullptr;
		_globalLightUniformBuffer.MapMemory(&memory);
		memcpy(memory, &globalLightBuffer, sizeof(GlobalLightUniform));
		_globalLightUniformBuffer.UnmapMemory();
	}
}

bool VKRHI_Frame::CreateCommandBuffers(VKRHI_FrameDescriptor const& frameDescriptor)
{
	if (!_mainCommandBuffer.FromCommandPool(*_device, frameDescriptor.commandPool))
	{
		ELOG(LogVulkan, "Couldn't initialize the main command buffers");
		return false;
	}

#ifdef INCLUDE_EDITOR
	// Editor
	{
		if (!_editorCommandBuffer.FromCommandPool(*_device, frameDescriptor.commandPool))
		{
			ELOG(LogVulkan, "Couldn't allocate the editor command buffers");
			return false;
		}
	}
#endif // INCLUDE_EDITOR

	for (auto&& subBuffers : { &_cubemapSubCommandBuffer, &_terrainSubCommandBuffer, &_opaqueSubCommandBuffer })
	{
		subBuffers->FromCommandPool(*_device, frameDescriptor.commandPool, VK_COMMAND_BUFFER_LEVEL_SECONDARY);

		VkCommandBufferInheritanceInfo inheritanceInfo{};
		inheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
		inheritanceInfo.renderPass = frameDescriptor.opaqueRenderPass->GetRHIRenderPass();
		inheritanceInfo.subpass = 0u;
		inheritanceInfo.framebuffer = _opaqueFramebuffer;
		inheritanceInfo.occlusionQueryEnable = false;
		subBuffers->SetSecondaryBufferInheritanceInfo(inheritanceInfo);
	}

	return true;
}

void VKRHI_Frame::DestroyCommandBuffers()
{
	std::array<VKRHI_CommandBuffer*, 5> buffers = { &_mainCommandBuffer
												  , &_editorCommandBuffer
												  , &_cubemapSubCommandBuffer
												  , &_terrainSubCommandBuffer
												  , &_opaqueSubCommandBuffer};

	for(auto&& buf : buffers)
		buf->Shutdown();
}

bool VKRHI_Frame::RecordCubemapSubCommandBuffer(VKRHI_FrameDescriptor const& frameDescriptor)
{
	VKRHI_CommandBuffer& commandBuffer = _cubemapSubCommandBuffer;
	if (!commandBuffer.BeginSecondary())
	{
		ELOG(LogVulkan, "Couldn't begin the cubemap secondary command buffer.");
		return false;
	}

	VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();

	// Cubemap
	if (frameDescriptor.cubemapRO)
	{
		KU_ASSERT(frameDescriptor.vertexBuffers);
		KU_ASSERT(frameDescriptor.indexBuffers);

		vkCmdBindPipeline(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, frameDescriptor.cubemapPipeline);

		VKRHI_MeshRenderObject const& renderObject = frameDescriptor.cubemapRO->GetMesh();
		CmdBindMeshRenderObject(frameDescriptor, vkCommandBuffer, renderObject);

		std::array<VkDescriptorSet, 2> sets =
		{
			_globalDescriptorSet,
			_cubemapMatDescriptorSet,
		};

		vkCmdBindDescriptorSets(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, frameDescriptor.availablePipelineLayouts.cubemapLayout, 0, static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);
		vkCmdDrawIndexed(vkCommandBuffer, static_cast<uint32_t>(renderObject.GetIndexCount()), 1, 0, 0, 0);
	}
	commandBuffer.End();

	return true;
}

bool VKRHI_Frame::RecordTerrainSubCommandBuffer(VKRHI_FrameDescriptor const& frameDescriptor)
{
	VKRHI_CommandBuffer& commandBuffer = _terrainSubCommandBuffer;
	if (!commandBuffer.BeginSecondary())
	{
		ELOG(LogVulkan, "Couldn't begin the terrain secondary command buffer.");
		return false;
	}

	VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();

	Profiler::Get().CounterAdd("TerrainCount", 1);
	Profiler::Get().CounterAdd("TerrainTotalPatchCount", _terrainPatchCount);

    auto const& renderObject = frameDescriptor.terrainRO;
    if (_renderingOptions.useMeshShading)
    {
        KU_ASSERT(_device->IsMeshShadingEnabled());
        vkCmdBindPipeline(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, frameDescriptor.meshShaderTerrainPipeline);

        std::array<VkDescriptorSet, 4> sets =
        {
            _globalDescriptorSet,
            _terrainMatDescriptorSet,
            _terrainDescriptorSet,
            _terrainMeshShaderDescriptorSet,
        };

        vkCmdBindDescriptorSets(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, frameDescriptor.availablePipelineLayouts.meshShaderTerrainLayout, 0, static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);

        int32_t instanceCount = _terrainPatchCount;
        int32_t maxDrawCount = _device->GetPhysicalDevice().GetPhysicalDeviceProperties().meshShaderProperties.maxDrawMeshTasksCount;
        for (int32_t i = 0; i < instanceCount;)
        {
			int32_t renderCount = glm::min(instanceCount, maxDrawCount);
			Profiler::Get().CounterAdd("TerrainMeshShaderDrawCount", 1);
			Profiler::Get().CounterAdd("TerrainMeshShaderDrawInstances", renderCount);
            _device->GetFunc().vkCmdDrawMeshTasksNV(vkCommandBuffer, renderCount, i);
            i += renderCount;
        }
    }
    else
    {

        KU_ASSERT(frameDescriptor.vertexBuffers);
        KU_ASSERT(frameDescriptor.indexBuffers);

        vkCmdBindPipeline(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, frameDescriptor.terrainPipeline);

        //VKRHI_Buffer const& vertBuffer = renderObject->GetPatchBuffer().Unlock(*context.frameDescriptor.vertexBuffers);
        VkBuffer vertsBuffers[] = { _terrainPatchBuffer.GetVkBuffer() };
        VkDeviceSize offsets[] = { 0 };
        vkCmdBindVertexBuffers(vkCommandBuffer, 0, 1, vertsBuffers, offsets);

        /*VKRHI_Buffer const& indexBuffer = meshRenderObject.GetIndexBufferKey().Unlock(*context.frameDescriptor.indexBuffers);
        vkCmdBindIndexBuffer(vkCommandBuffer, indexBuffer.GetVkBuffer(), 0, VK_INDEX_TYPE_UINT32);*/

        std::array<VkDescriptorSet, 3> sets =
        {
            _globalDescriptorSet,
            _terrainMatDescriptorSet,
            _terrainDescriptorSet,
        };

        vkCmdBindDescriptorSets(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, frameDescriptor.availablePipelineLayouts.terrainLayout, 0, static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);
        //vkCmdDrawIndexed(vkCommandBuffer, static_cast<uint32_t>(renderObject.GetIndexCount()), 1, 0, 0, 0);
		Profiler::Get().CounterAdd("TerrainTessellationPatchCount", _terrainPatchCount);
		vkCmdDraw(vkCommandBuffer, _terrainPatchCount, 1, 0, 0);
	}
	commandBuffer.End();

    return true;
}

bool VKRHI_Frame::RecordOpaqueSubCommandBuffer(VKRHI_FrameDescriptor const& frameDescriptor)
{
	VKRHI_CommandBuffer& commandBuffer = _opaqueSubCommandBuffer;
	if (!commandBuffer.BeginSecondary())
	{
		ELOG(LogVulkan, "Couldn't begin the opaque secondary command buffer.");
		return false;
	}

	VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();

	// Forward shading
	{
		size_t objectBufferCount = frameDescriptor.scene->GetStaticMeshsCount();

		KU_ASSERT(frameDescriptor.vertexBuffers);
		KU_ASSERT(frameDescriptor.indexBuffers);
		for (size_t i = 0; i < objectBufferCount; i++)
		{
			vkCmdBindPipeline(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, frameDescriptor.opaquePipeline);

			VKRHI_MeshRenderObject const& renderObject = (*frameDescriptor.meshesRO)[i].GetMesh();
			CmdBindMeshRenderObject(frameDescriptor, vkCommandBuffer, renderObject);

			std::array<VkDescriptorSet, 3> sets =
			{
				_globalDescriptorSet,
				_defaultMatDescriptorSet,
				_objectDescriptorSets[i],
			};

			vkCmdBindDescriptorSets(vkCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, frameDescriptor.availablePipelineLayouts.staticMeshLayout, 0, static_cast<uint32_t>(sets.size()), sets.data(), 0, nullptr);
			vkCmdDrawIndexed(vkCommandBuffer, static_cast<uint32_t>(renderObject.GetIndexCount()), 1, 0, 0, 0);
		}
	}

	commandBuffer.End();

	return true;
}

bool VKRHI_Frame::RecordMainCommandBuffer(VKRHI_FrameDescriptor const& frameDescriptor)
{
	VKRHI_CommandBuffer& commandBuffer = _mainCommandBuffer;
	if (!commandBuffer.Begin())
	{
		ELOG(LogVulkan, "Couldn't begin the main command buffer.");
		return false;
	}
	VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();

	frameDescriptor.opaqueRenderPass->CmdBeginRenderPass(vkCommandBuffer, _opaqueFramebuffer);
	
	std::array<VkCommandBuffer, 3> subCommandBuffers = { _cubemapSubCommandBuffer.GetVkCommandBuffer(),
														 _terrainSubCommandBuffer.GetVkCommandBuffer(),
														 _opaqueSubCommandBuffer.GetVkCommandBuffer() };
	vkCmdExecuteCommands(vkCommandBuffer, static_cast<uint32_t>(subCommandBuffers.size()), subCommandBuffers.data());

	frameDescriptor.opaqueRenderPass->CmdEndRenderPass(vkCommandBuffer);

	commandBuffer.End();

	return true;
}

bool VKRHI_Frame::RecordEditorCommandBuffer(VKRHI_FrameDescriptor const& frameDescriptor)
{
#ifdef INCLUDE_EDITOR
	if (!_editorCommandBuffer.Begin())
	{
		ELOG(LogVulkan, "Couldn't begin the editor command buffer.");
		return false;
	}

    VkCommandBuffer vkBuffer = _editorCommandBuffer.GetVkCommandBuffer();;

	frameDescriptor.editorRenderPass->CmdBeginRenderPass(vkBuffer, _editorFramebuffer);
	ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), vkBuffer, _imageIndex);
	frameDescriptor.editorRenderPass->CmdEndRenderPass(vkBuffer);

	_editorCommandBuffer.End();

#endif
	return true;
}

void VKRHI_Frame::Draw(FrameRenderContext& context)
{
	PrepareDataForDrawing(context);

	KU_ASSERT(_device && _swapChain);

	KU_VERIFY(RecordTerrainSubCommandBuffer(context.frameDescriptor));
	KU_VERIFY(RecordMainCommandBuffer(context.frameDescriptor));
	std::vector<VkCommandBuffer> finalCommandList;
	finalCommandList.push_back(_mainCommandBuffer.GetVkCommandBuffer());

	if (context.shouldDrawEditor)
	{
		if (KU_VERIFY(RecordEditorCommandBuffer(context.frameDescriptor)))
			finalCommandList.push_back(_editorCommandBuffer.GetVkCommandBuffer());
	}

	std::vector<VkSemaphore> waitSemaphores;
	if (context.backbufferDrawableSemaphore)
		waitSemaphores.push_back(context.backbufferDrawableSemaphore->GetVkSemaphore());

	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.waitSemaphoreCount = static_cast<uint32_t>(waitSemaphores.size());
	submitInfo.pWaitSemaphores = waitSemaphores.data();
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = static_cast<uint32_t>(finalCommandList.size());
	submitInfo.pCommandBuffers = finalCommandList.data();
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = &_renderFinishedSemaphore.GetVkSemaphore();

	_imageRenderingFence.Reset();
	if (vkQueueSubmit(_device->GetGraphicQueue(), 1, &submitInfo, _imageRenderingFence.GetVkFence()) != VK_SUCCESS)
	{
		ELOG(LogVulkan, "Couldn't load the graphics queue.");
		return;
	}
}

void VKRHI_Frame::Present()
{
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = &_renderFinishedSemaphore.GetVkSemaphore();
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &_swapChain->GetVkSwapChain();
	presentInfo.pImageIndices = &_imageIndex;
	presentInfo.pResults = nullptr;

	vkQueuePresentKHR(_device->GetPresentQueue(), &presentInfo);
}

void VKRHI_Frame::WaitDraw()
{
	_imageRenderingFence.Wait();
	//_imageRenderingFence.Reset();
}