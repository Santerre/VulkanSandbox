#include "RHI/Vulkan/VKRHI_ShaderCompiler.h"

#include <set>

#include "Platform/GenericPlatform.h"
#include "Resource/Resource.h"

#include "Resource/Parsers/TextParser.h"

#include "Debugging/Log.h"

#include "shaderc/shaderc.hpp"

class ShaderIncluder : public shaderc::CompileOptions::IncluderInterface
{
public:
    ShaderIncluder() = delete;
    ShaderIncluder(Path const& includePath)
    : _includePath(includePath)
    {
    }

    virtual ~ShaderIncluder() = default;

    struct WrappedPathResults : shaderc_include_result
    {
        bool operator==(WrappedPathResults const& other) const { return path == other.path && internalIndex == other.internalIndex; }
        std::string path;
        std::string contentStr;
        size_t      internalIndex;
    };

//IncluderInterface interface
public:

    // Handles shaderc_include_resolver_fn callbacks.
    virtual shaderc_include_result* GetInclude(const char* requested_source,
                                               shaderc_include_type type,
                                               const char* requesting_source,
                                               size_t include_depth) override
    {
        WrappedPathResults& result = _results.emplace_front();
        result.path = _includePath.Concat(Path(std::string(requested_source))).GetString();
        result.source_name = result.path.c_str();
        result.source_name_length = result.path.size();

        DiskFileLoader loader(result.path, false);
        auto reader = loader.OpenRead();
        if ( reader )
        {
            TextParser::UnserializeText(*reader, result.contentStr);
        }
        else
        {
            result.contentStr = "Couldn't find the include";
            result.source_name = "";
            result.source_name_length = 0;
        }
        loader.CloseRead();

        result.content = result.contentStr.data();
        result.content_length = result.contentStr.size();

        result.internalIndex = _results.size();

        shaderc_include_result *res = &(*_results.begin());

        return res;
    }

    // Handles shaderc_include_result_release_fn callbacks.
    virtual void ReleaseInclude(shaderc_include_result* data) override
    {
        WrappedPathResults* wrappedData = static_cast<WrappedPathResults*>(data);
        _results.remove(*wrappedData);
    }

private:
    Path _includePath;
    std::list<WrappedPathResults> _results;
};

/// GLSL Shader
void VKRHI_ShaderCompiler::FromDesc(InitDesc const& desc)
{
    _application = desc.application;
    _resourceMgr = desc.resourceMgr;
    _defaultCompileOptions = desc.defaultCompileOptions;
}

shaderc_optimization_level ToShaderCOptimisationLevel( VKRHI_ShaderCompiler::CompilationOptimization optimization )
{
    switch ( optimization )
    {
    case VKRHI_ShaderCompiler::CompilationOptimization::O0:
        return shaderc_optimization_level::shaderc_optimization_level_zero;
    case VKRHI_ShaderCompiler::CompilationOptimization::O1:
        return shaderc_optimization_level::shaderc_optimization_level_performance;
    case VKRHI_ShaderCompiler::CompilationOptimization::O2:
        return shaderc_optimization_level::shaderc_optimization_level_performance;
    case VKRHI_ShaderCompiler::CompilationOptimization::O3:
        return shaderc_optimization_level::shaderc_optimization_level_performance;
    default:
        ELOG(LogShaderCompilation, "Unknown shader optimization level");
        return shaderc_optimization_level::shaderc_optimization_level_zero;
    }
}

Handle<SPVShader> VKRHI_ShaderCompiler::CompileShader(std::shared_ptr<ResourceWrapper<GLSLShader>> targetShader, CompileOptions const& options, ShaderType shaderType)
{
    shaderc::Compiler compiler;
    shaderc::CompileOptions shadercOptions;

    //for (auto&& properties : shaderProperties)
    //{
    //    if (_shaderAttributes.find(properties.first) == _shaderAttributes.end())
    //        WLOG(LogResource, "No %s property in the dynamic shader %s. Ignoring.", properties.first.c_str(), _name.c_str());
    //    else
    //        shadercOptions.AddMacroDefinition(properties.first, properties.second);
    //}

    shadercOptions.SetOptimizationLevel(ToShaderCOptimisationLevel(options.optimisation));
    shadercOptions.SetTargetEnvironment(shaderc_target_env::shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_1);
    shadercOptions.SetWarningsAsErrors();

    // /!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/
    // TODO : REMOVE - Added because the optimisation of the terrain mesh shader contains spirV error. Need to investigate.
    // /!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/!\/
    if (shaderType == ShaderType::Mesh)
        shadercOptions.SetOptimizationLevel(shaderc_optimization_level::shaderc_optimization_level_zero);
    for ( auto&& properties : options.macroList )
        shadercOptions.AddMacroDefinition(properties.first, properties.second);

    Path osFilePath =  GenericPlatform::SolveFilePath(options.includePath);
    shadercOptions.SetIncluder(std::unique_ptr<shaderc::CompileOptions::IncluderInterface>(new ShaderIncluder(osFilePath)));
    Path targetPath = targetShader->GetResourcePath();

    std::stringstream finalSourceCode;
    std::stringstream source(targetShader->GetTrueResource()->GetShaderData());
    if (options.prefixCode.size() > 0)
    {
        // TODO skip first lines a safer way
        std::string line;
        size_t skippedLine = 0;
        while (std::getline(source, line))
        {
            size_t firstCharId = line.find_first_not_of(' ');
            const std::vector<std::string> skippedLines
            {
                "#pragma",
                "#extension",
                "#version",
            };

            bool found = firstCharId == line.npos;
            if (!found)
            {
                for (auto&& skip : skippedLines)
                {
                    found |= line.compare(firstCharId, skip.size(), skip) == 0;
                    if (found)
                        break;
                }
            }

            if (!found)
                break;

            skippedLine++;
            finalSourceCode << line << std::endl;
        }
        finalSourceCode << std::endl;
        for (auto&& code : options.prefixCode)
            finalSourceCode << code << std::endl;
        finalSourceCode << "#line " << skippedLine << std::endl;
        finalSourceCode << line << std::endl;
    }
    source >> finalSourceCode.rdbuf();

    shaderc::SpvCompilationResult results = compiler.CompileGlslToSpv(finalSourceCode.str(), shaderc_shader_kind::shaderc_glsl_infer_from_source, targetPath.GetString().c_str(), shadercOptions);
   
    if ( results.GetCompilationStatus() != shaderc_compilation_status::shaderc_compilation_status_success )
    {
        ELOG(LogShaderCompilation, "Failed to compile %s to SPV : %s", targetPath.GetString().c_str(), results.GetErrorMessage().c_str());
        return {};
    }

    std::vector<uint32_t> data;
    data.reserve(1024);
    std::copy(results.cbegin(), results.cend(), std::back_inserter(data));
    
    Path name = Path(std::string("M:/Shader/Compiled/")).Concat(targetShader->GetResourcePath().GetFullRelativePath()).ChangeExtension(SPVShader::GetShaderDefaultExtension(shaderType));

    Handle<SPVShader> shader = _resourceMgr->CreateResource<SPVShader>();
    shader->FromData(name.GetString(), shaderType, std::move(data));
    ResourceInitContext context;
    context.application = _application;
    shader->Init(context);

    return shader;
}