#include "RHI/Vulkan/VKRHI_Image.h"

#include "RHI/Vulkan/VKRHI_Device.h"

bool VKRHI_Image::FromImageMemory(class VKRHI_Device const& device, VKRHI_ImageMemory const& source, ImageDesc const& descriptor)
{
	_imageMemory = &source;

	return FromVkImage(device, source.GetVkImage(), descriptor, source.GetMipmapCount());
}

bool VKRHI_Image::FromVkImage(VKRHI_Device const& device, VkImage source, ImageDesc const& descriptor, uint32_t mipmapCount)
{
	_device = &device;

	_sampler = descriptor.sampler;

	_format = descriptor.format;

	VkImageViewCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	createInfo.image = source;
	createInfo.viewType = descriptor.type;
	createInfo.format = descriptor.format;

	createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

	createInfo.subresourceRange.aspectMask = descriptor.aspectFlags;
	createInfo.subresourceRange.layerCount = descriptor.layerCount;
	createInfo.subresourceRange.baseArrayLayer = 0;
	createInfo.subresourceRange.levelCount = mipmapCount;
	createInfo.subresourceRange.baseMipLevel = 0;

	if (vkCreateImageView(device.GetVkDevice(), &createInfo, nullptr, &_imageView) != VK_SUCCESS)
		return false;
	return true;
}

void VKRHI_Image::Shutdown()
{
	if (_device && _imageView)
	{
		vkDestroyImageView(_device->GetVkDevice(), _imageView, nullptr);
	}
}