#include "RHI/Vulkan/PipelineTemplate/BasePipelineTemplate.h"

#include <vector>

#include "Debugging/Assert.h"
#include "RHI/Vulkan/VKRHI_TypeConverters.h"
#include "RHI/Vulkan/VKRHI_RenderPass.h"


void VKRHI_BasePipelineTemplate::CreateDefaultDescriptor(VKRHI_BasePipelineCreateDesc const& createDesc, VKRHI_PipelineDescriptor& outDesc)
{
	KU_ASSERT(createDesc.shaderModules);

	// Input assembly
	outDesc.vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

	outDesc.vertexInputInfo.vertexAttributeDescriptionCount = 0;
	outDesc.vertexInputInfo.pVertexAttributeDescriptions = nullptr;
	outDesc.vertexInputInfo.vertexBindingDescriptionCount = 0;
	outDesc.vertexInputInfo.pVertexBindingDescriptions = nullptr;

	outDesc.vertexInputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	outDesc.vertexInputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	outDesc.vertexInputAssemblyInfo.primitiveRestartEnable = VK_FALSE;
	
	// Viewport
	outDesc.viewport.x = 0.f;
	outDesc.viewport.y = 0.f;
	outDesc.viewport.width = static_cast<float>(createDesc.surfaceExtents.x);
	outDesc.viewport.height = static_cast<float>(createDesc.surfaceExtents.y);
	outDesc.viewport.minDepth = 0.0f;
	outDesc.viewport.maxDepth = 1.0f;
	
	outDesc.scissor.offset = { 0, 0 };
	outDesc.scissor.extent = VKRHI_TypeConverters::GlmToVk(createDesc.surfaceExtents);
	
	outDesc.viewportStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	outDesc.viewportStateInfo.viewportCount = 1;
	outDesc.viewportStateInfo.pViewports = &outDesc.viewport;
	outDesc.viewportStateInfo.scissorCount = 1;
	outDesc.viewportStateInfo.pScissors = &outDesc.scissor;
	
	// Rasterizer
	outDesc.rasterizationCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	
	outDesc.rasterizationCreateInfo.depthClampEnable = VK_FALSE;
	outDesc.rasterizationCreateInfo.rasterizerDiscardEnable = VK_FALSE;
	
	outDesc.rasterizationCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
	outDesc.rasterizationCreateInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	
	outDesc.rasterizationCreateInfo.depthBiasEnable = VK_FALSE;
	outDesc.rasterizationCreateInfo.depthBiasConstantFactor = 0.0f;
	outDesc.rasterizationCreateInfo.depthBiasClamp = 0.0f;
	outDesc.rasterizationCreateInfo.depthBiasSlopeFactor = 0.0f;
	
	outDesc.rasterizationCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
	outDesc.rasterizationCreateInfo.lineWidth = 1.0f;
	
	// Multisampling
	outDesc.multisamplingStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	outDesc.multisamplingStateCreateInfo.sampleShadingEnable = VK_FALSE;
	outDesc.multisamplingStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	outDesc.multisamplingStateCreateInfo.minSampleShading = 1.0f;
	outDesc.multisamplingStateCreateInfo.pSampleMask = nullptr;
	outDesc.multisamplingStateCreateInfo.alphaToCoverageEnable = VK_FALSE;
	outDesc.multisamplingStateCreateInfo.alphaToOneEnable = VK_FALSE;
	
	// Color blending
	outDesc.colorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT
		| VK_COLOR_COMPONENT_G_BIT
		| VK_COLOR_COMPONENT_B_BIT
		| VK_COLOR_COMPONENT_A_BIT;
	
	outDesc.colorBlendAttachmentState.blendEnable = VK_FALSE;
	outDesc.colorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	outDesc.colorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	outDesc.colorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
	outDesc.colorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	outDesc.colorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	outDesc.colorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;
	
	outDesc.colorBlendCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	outDesc.colorBlendCreateInfo.logicOpEnable = VK_FALSE;
	outDesc.colorBlendCreateInfo.logicOp = VK_LOGIC_OP_COPY;
	outDesc.colorBlendCreateInfo.attachmentCount = 1;
	outDesc.colorBlendCreateInfo.pAttachments = &outDesc.colorBlendAttachmentState;
	for (int i = 0; i < 4; i++)
		outDesc.colorBlendCreateInfo.blendConstants[i] = 0.0f;

	// Dynamic State
	//VkDynamicState dynamicStates[] = {
	//	VK_DYNAMIC_STATE_VIEWPORT,
	//	VK_DYNAMIC_STATE_LINE_WIDTH
	//};
	//VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo;
	//dynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	//dynamicStateCreateInfo.dynamicStateCount = 2;
	//dynamicStateCreateInfo.pDynamicStates = dynamicStates;

	outDesc.depthStencilInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	outDesc.depthStencilInfo.depthTestEnable = VK_TRUE;
	outDesc.depthStencilInfo.depthWriteEnable = VK_TRUE;
	outDesc.depthStencilInfo.depthCompareOp = VK_COMPARE_OP_LESS;
	outDesc.depthStencilInfo.depthBoundsTestEnable = VK_FALSE;
	outDesc.depthStencilInfo.minDepthBounds = 0.f;
	outDesc.depthStencilInfo.maxDepthBounds = 1.f;

	outDesc.depthStencilInfo.stencilTestEnable = VK_FALSE;
	outDesc.depthStencilInfo.front = {};
	outDesc.depthStencilInfo.back = {};

	outDesc.pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	outDesc.pipelineInfo.stageCount = static_cast<uint32_t>(createDesc.shaderModules->size());
	outDesc.pipelineInfo.pStages = createDesc.shaderModules->data();
	outDesc.pipelineInfo.pVertexInputState = &outDesc.vertexInputInfo;
	outDesc.pipelineInfo.pInputAssemblyState = &outDesc.vertexInputAssemblyInfo;
	outDesc.pipelineInfo.pViewportState = &outDesc.viewportStateInfo;
	outDesc.pipelineInfo.pRasterizationState = &outDesc.rasterizationCreateInfo;
	outDesc.pipelineInfo.pMultisampleState = &outDesc.multisamplingStateCreateInfo;
	outDesc.pipelineInfo.pDepthStencilState = &outDesc.depthStencilInfo;
	outDesc.pipelineInfo.pColorBlendState = &outDesc.colorBlendCreateInfo;
	outDesc.pipelineInfo.pDynamicState = nullptr;

	outDesc.pipelineInfo.layout = createDesc.pipelineLayout;

	outDesc.pipelineInfo.renderPass = createDesc.renderPass->GetRHIRenderPass();
	outDesc.pipelineInfo.subpass = 0;

	outDesc.pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
	outDesc.pipelineInfo.basePipelineIndex = -1;
}