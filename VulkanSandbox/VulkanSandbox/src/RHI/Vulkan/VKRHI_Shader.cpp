#include "RHI/Vulkan/VKRHI_Shader.h"

#include "RHI/Vulkan/VKRHI_Device.h"

#include "RHI/Vulkan/VKRHI_TypeConverters.h"

bool VKRHI_Shader::FromSpirVData(VKRHI_Device& device, std::vector<uint32_t> const& data, ShaderType shaderStage)
{
	_device = &device;
	VkShaderModuleCreateInfo moduleCreateInfo = {};
	moduleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	moduleCreateInfo.codeSize = data.size() * 4;
	moduleCreateInfo.pCode = data.data();
	if (!vkCreateShaderModule(device.GetVkDevice(), &moduleCreateInfo, nullptr, &_shaderModule) == VK_SUCCESS)
		return false;

	_shaderStage = VKRHI_TypeConverters::GenToVK(shaderStage);

	_stageCreateInfo = {};
	_stageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	_stageCreateInfo.stage = _shaderStage;
	_stageCreateInfo.module = _shaderModule;
	_stageCreateInfo.pName = "main";

	return true;
}

void VKRHI_Shader::Shutdown()
{
	if (!_device)
		return;

	vkDestroyShaderModule(_device->GetVkDevice(), _shaderModule, nullptr);
}
