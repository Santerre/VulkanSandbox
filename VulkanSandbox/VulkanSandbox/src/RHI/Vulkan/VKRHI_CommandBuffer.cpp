#include "RHI/Vulkan/VKRHI_CommandBuffer.h"

#include "RHI/Vulkan/VKRHI_Device.h"

#include "Debugging/Assert.h"

bool VKRHI_CommandBuffer::FromCommandPool(VKRHI_Device& device, VkCommandPool pool, VkCommandBufferLevel level)
{
    VkCommandBufferAllocateInfo allocateInfo = {};

    _device = &device;
    _commandPool = pool;

    allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocateInfo.commandPool = pool;
	allocateInfo.level = level;
	allocateInfo.commandBufferCount = 1;

    return vkAllocateCommandBuffers(device.GetVkDevice(), &allocateInfo, &_buffer) == VK_SUCCESS;
}

void VKRHI_CommandBuffer::Shutdown()
{
    if (_buffer != VK_NULL_HANDLE)
        vkFreeCommandBuffers(_device->GetVkDevice(), _commandPool, 1, &_buffer);

    KU_ASSERT(!_bIsRecording);
    _buffer = VK_NULL_HANDLE;
    _bIsRecording = false;
}

void VKRHI_CommandBuffer::SetSecondaryBufferInheritanceInfo( VkCommandBufferInheritanceInfo const& inheritanceInfo )
{
    _inheritanceInfo = inheritanceInfo;
}

bool VKRHI_CommandBuffer::BeginOneTime()
{
    KU_ASSERT(!IsRecording());

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    if (vkBeginCommandBuffer(_buffer, &beginInfo) != VK_SUCCESS)
        return false;
    
    _bIsRecording = true;
    return true;
}

bool VKRHI_CommandBuffer::Begin()
{
    KU_ASSERT(!IsRecording());

    VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = 0;
	beginInfo.pInheritanceInfo = nullptr;

    if (vkBeginCommandBuffer(_buffer, &beginInfo) != VK_SUCCESS)
        return false;
    
    _bIsRecording = true;
    return true;
}

bool VKRHI_CommandBuffer::BeginSecondary()
{
    KU_ASSERT(!IsRecording());

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
    beginInfo.pInheritanceInfo = &_inheritanceInfo;

    vkBeginCommandBuffer(_buffer, &beginInfo);
    
    _bIsRecording = true;
    return true;
}

void VKRHI_CommandBuffer::End()
{
    KU_ASSERT(IsRecording());
    vkEndCommandBuffer(_buffer);

    _bIsRecording = false;
}