#include "RHI/Vulkan/VKRHI_SwapChain.h"

#include <memory>

#include "glm/vec2.hpp"

#include "Memory/Allocator.h"
#include "Debugging/Assert.h"

#include "RHI/Vulkan/VKRHI_TypeConverters.h"
#include "RHI/Vulkan/VKRHI_Device.h"
#include "RHI/Vulkan/VKRHI_Semaphore.h"

bool VKRHI_SwapChain::FromDesc(VKRHI_Device& device, VKRHI_SwapChain::InitDesc const& desc)
{
	_device = &device;

	VKRHI_PhysicalDevice physicalDevice = device.GetPhysicalDevice();
	_supportDetails = physicalDevice.GetSwapChainSupportDetails();

	_surfaceFormat = ChooseSwapSurfaceFormat(_supportDetails.formats);
	VkPresentModeKHR presentMode = ChooseSwapPresentMode(_supportDetails.presentModes);
	_imageExtents = ChooseSwapExtent(_supportDetails.capabilities, desc.windowExtents);
	uint32_t imageCount = ChooseImageCount(_supportDetails.capabilities, desc.desiredImageCount);

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = desc.surface;

	createInfo.minImageCount = imageCount;
	createInfo.imageExtent = VKRHI_TypeConverters::GlmToVk(_imageExtents);
	createInfo.presentMode = presentMode;
	createInfo.imageFormat = _surfaceFormat.format;
	createInfo.preTransform = _supportDetails.capabilities.currentTransform;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	createInfo.clipped = VK_TRUE;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.oldSwapchain = VK_NULL_HANDLE;

	QueueFamilyIndices indices = physicalDevice.GetQueueFamilyIndices();
	uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };

	if (indices.graphicsFamily != indices.presentFamily) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
	}


	KU_ASSERT(_device);
	if (vkCreateSwapchainKHR(_device->GetVkDevice(), &createInfo, nullptr, &_swapChain) != VK_SUCCESS)
		return false;

	RetrieveSwapChainImages(imageCount);

	CreateSwapChainViews(_surfaceFormat.format);

	return true;
}

glm::uvec2	VKRHI_SwapChain::GetSurfaceImageCountMinMax() const
{
	return { _supportDetails.capabilities.minImageCount, 
			 _supportDetails.capabilities.maxImageCount };
}

bool VKRHI_SwapChain::AquireNextImage(VKRHI_Semaphore const& semaphore, uint32_t& outFrameID)
{
	if (!_device)
		return false;

	VkResult result = vkAcquireNextImageKHR(_device->GetVkDevice(), _swapChain, std::numeric_limits<uint64_t>::max(), semaphore.GetVkSemaphore(), VK_NULL_HANDLE, &outFrameID);

	KU_ASSERT(result == VK_SUCCESS);
	return result == VK_SUCCESS;
}

void VKRHI_SwapChain::Shutdown()
{
	if (_device && _swapChain)
		vkDestroySwapchainKHR(_device->GetVkDevice(), _swapChain, nullptr);

	for (auto&& imageView : _swapChainImageViews)
	{
		imageView->Shutdown();
		ReleaseRes(imageView);
	}
}

VkSurfaceFormatKHR VKRHI_SwapChain::ChooseSwapSurfaceFormat(std::vector<VkSurfaceFormatKHR> const& supportedFormats) const
{
	if (supportedFormats.size() == 1 && supportedFormats[0].format == VK_FORMAT_UNDEFINED)
		return { VK_FORMAT_R8G8B8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };

	for (auto&& format : supportedFormats)
	{
		if (format.format == VK_FORMAT_R8G8B8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
			return format;
	}

	return supportedFormats[0];
}

VkPresentModeKHR VKRHI_SwapChain::ChooseSwapPresentMode(std::vector<VkPresentModeKHR> const& supportedPresentModes) const
{
	VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;
	for (auto&& supportedMode : supportedPresentModes)
	{
		if (supportedMode == VK_PRESENT_MODE_MAILBOX_KHR)
			return supportedMode;

		if (supportedMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
			bestMode = supportedMode;
	}

	return bestMode;
}

glm::uvec2 VKRHI_SwapChain::ChooseSwapExtent(VkSurfaceCapabilitiesKHR const& surfaceCapabilities, glm::uvec2 const& windowExtents) const
{
	if (surfaceCapabilities.currentExtent.width == std::numeric_limits<uint32_t>::max())
	{
		glm::uvec2 actualExtents = glm::min(VKRHI_TypeConverters::VkToGlm(surfaceCapabilities.maxImageExtent), windowExtents);
		actualExtents = glm::max(VKRHI_TypeConverters::VkToGlm(surfaceCapabilities.minImageExtent), actualExtents);
		return actualExtents;
	}

	return VKRHI_TypeConverters::VkToGlm(surfaceCapabilities.currentExtent);
}

uint32_t VKRHI_SwapChain::ChooseImageCount(VkSurfaceCapabilitiesKHR const& surfaceCapabilities, uint32_t desiredImageCount) const
{
	uint32_t imageCount = std::max(desiredImageCount, surfaceCapabilities.minImageCount);
	if (surfaceCapabilities.maxImageCount > 0)
		imageCount = std::min(imageCount, surfaceCapabilities.maxImageCount);

	return imageCount;
}

void	VKRHI_SwapChain::RetrieveSwapChainImages(uint32_t imagesCount)
{
	KU_ASSERT(_device);

	uint32_t maxImagesCount;
	vkGetSwapchainImagesKHR(_device->GetVkDevice(), _swapChain, &maxImagesCount, nullptr);
	KU_ASSERT(maxImagesCount >= imagesCount);
	KU_ASSERT(imagesCount > 0);

	uint32_t finalImageCount = imagesCount;
	_swapChainImages.resize(finalImageCount);
	vkGetSwapchainImagesKHR(_device->GetVkDevice(), _swapChain, &finalImageCount, _swapChainImages.data());
	KU_ASSERT(finalImageCount == imagesCount);
}

void	VKRHI_SwapChain::CreateSwapChainViews(VkFormat format)
{
	KU_ASSERT(_device);

	size_t imageCount = _swapChainImages.size();
	_swapChainImageViews.resize(imageCount);
	for (size_t i = 0u; i < imageCount; i++)
	{
		ImageDesc descriptor = {};
		descriptor.format = format;
		descriptor.aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT;
		_swapChainImageViews[i] = AllocRes(VKRHI_Image);
		_swapChainImageViews[i]->FromVkImage(*_device, _swapChainImages[i], descriptor);
	}
}
