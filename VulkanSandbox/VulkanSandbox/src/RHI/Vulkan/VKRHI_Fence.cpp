#include "RHI/Vulkan/VKRHI_Fence.h"

#include "Debugging/Log.h"
#include "Debugging/Assert.h"
#include "RHI/Vulkan/VKRHI_LogCategory.h"

void VKRHI_Fence::Initialize(VKRHI_Device const& device)
{
	VkFenceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	createInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	_device = &device;

	if (vkCreateFence(device.GetVkDevice(), &createInfo, nullptr, &_vkFence) != VK_SUCCESS)
	{
		WLOG(LogVulkan, "Couldn't create the fence.");
		return;
	}
}

void VKRHI_Fence::Shutdown()
{
	if (_device && _vkFence)
		vkDestroyFence(_device->GetVkDevice(), _vkFence, nullptr);
	_device = nullptr;
}

void VKRHI_Fence::Wait()
{
	KU_ASSERT(_device);
	vkWaitForFences(_device->GetVkDevice(), 1, &_vkFence, VK_TRUE, std::numeric_limits<uint64_t>::max());
}

void VKRHI_Fence::Reset()
{
	KU_ASSERT(_device);
	vkResetFences(_device->GetVkDevice(), 1, &_vkFence);
}