#include "RHI/Vulkan/VKRHI_RenderObject.h"

#include "RHI/Vulkan/VKRHI_Device.h"


bool VKRHI_MeshRenderObject::FromDesc(VKRHI_MeshRenderObjectDesc const& desc)
{
	_vertexBufferKey = desc.vertexBufferKey;
	_indexBufferKey = desc.indexBufferKey;

	_vertsCount = desc.verticesCount;
	_indexCount = desc.indicesCount;

	return true;
}
