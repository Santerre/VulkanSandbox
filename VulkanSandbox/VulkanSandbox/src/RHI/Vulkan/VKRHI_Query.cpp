#include "RHI/Vulkan/VKRHI_Query.h"

#include "RHI/Vulkan/VKRHI_Device.h"

#include "Debugging/Assert.h"
#include "Debugging/Log.h"

bool VKRHI_QueryPool::FromDescriptor(VKRHI_Device const& device, QueryPoolDesc desc)
{
    VkQueryPoolCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
    info.queryType = desc.type;
    info.queryCount = static_cast<uint32_t>(desc.count);

    if (vkCreateQueryPool(device.GetVkDevice(), &info, nullptr, &_vkPool) != VK_SUCCESS)
    {
        ELOG(LogVulkan, "Couldn't create the vulkan pool.");
        return false;
    }

    _allocatedCount = desc.count;
    return true;
}

void VKRHI_QueryPool::Shutdown()
{
    if (_vkPool)
        vkDestroyQueryPool(_device->GetVkDevice(), _vkPool, nullptr);
}

VKRHI_Query VKRHI_QueryPool::CmdWriteTimestamp(VkCommandBuffer commandBuffer, size_t queryId, VkPipelineStageFlagBits stage)
{
    KU_ASSERT(queryId > 0 && queryId < _allocatedCount);

    vkCmdWriteTimestamp(commandBuffer, stage, _vkPool, static_cast<uint32_t>(queryId));

    VKRHI_Query query;
    query._index = queryId;
    return query;
}

VKRHI_Query VKRHI_QueryPool::CmdBeginQuery(VkCommandBuffer commandBuffer, size_t queryId)
{
    KU_ASSERT(queryId > 0 && queryId < _allocatedCount);

    vkCmdBeginQuery(commandBuffer, _vkPool, static_cast<uint32_t>(queryId), 0);

    VKRHI_Query query;
    query._index = queryId;
    return query;
}

void VKRHI_QueryPool::CmdEndQuery(VkCommandBuffer commandBuffer, VKRHI_Query const& query)
{
    vkCmdEndQuery(commandBuffer, _vkPool, static_cast<uint32_t>(query._index));
}

void VKRHI_QueryPool::CmdReset(VkCommandBuffer commandBuffer, size_t startQuery, size_t queryCount)
{
    vkCmdResetQueryPool(commandBuffer, _vkPool, static_cast<uint32_t>(startQuery), static_cast<uint32_t>(queryCount));
}
