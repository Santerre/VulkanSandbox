#include "RHI/Vulkan/VKRHI_MemoryObject.h"

#include "Debugging/Log.h"
#include "Debugging/Assert.h"

#include "RHI/Vulkan/VKRHI_PhysicalDevice.h"
#include "RHI/Vulkan/VKRHI_LogCategory.h"
#include "RHI/Vulkan/VKRHI_GraphicalMemoryAllocator.h"
#include "RHI/Vulkan/VKRHI_TypeConverters.h"


bool VKRHI_MemoryObject::FromDesc(VKRHI_Device& device, MemoryObjectDesc const& bufferDesc)
{
	_device = &device;

	KU_ASSERT(bufferDesc.allocator);
	_allocator = bufferDesc.allocator;
	if (!_allocator)
		return false;

	KU_ASSERT(bufferDesc.memRequirement);
	if (!_allocator->AllocateMemory(static_cast<size_t>(bufferDesc.memRequirement->size), bufferDesc.memRequirement->memoryTypeBits, bufferDesc.properties, _memorySlot))
		return false;

	return true;
}

void VKRHI_MemoryObject::MapMemory(void** outMappedMemory)
{
	KU_ASSERT(_device);
	vkMapMemory(_device->GetVkDevice(), _memorySlot.GetVkMemory(), _memorySlot.GetOffset(), _memorySlot.GetSize(), 0, outMappedMemory);
}

void VKRHI_MemoryObject::UnmapMemory()
{
	vkUnmapMemory(_device->GetVkDevice(), _memorySlot.GetVkMemory());
}

void VKRHI_MemoryObject::Shutdown()
{
	if (!_device)
		return;
	_allocator->ReleaseMemory(_memorySlot);

	_device = nullptr;
}


bool VKRHI_Buffer::FromDesc(VKRHI_Device& device, BufferDesc const& bufferDesc)
{
	_device = &device;

	VkBufferCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	createInfo.size = static_cast<VkDeviceSize>(bufferDesc.size);
	createInfo.usage = bufferDesc.usage;
	createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateBuffer(device.GetVkDevice(), &createInfo, nullptr, &_buffer) != VK_SUCCESS)
		return false;

	_size = bufferDesc.size;

	VkMemoryRequirements requirements;
	MemoryObjectDesc desc = {};
	desc.allocator = bufferDesc.allocator;
	desc.properties = bufferDesc.properties;
	vkGetBufferMemoryRequirements(device.GetVkDevice(), _buffer, &requirements);
	desc.memRequirement = &requirements;
	if (!VKRHI_MemoryObject::FromDesc(device, desc))
		return false;

	vkBindBufferMemory(_device->GetVkDevice(), _buffer, GetMemSlot().GetVkMemory(), GetMemSlot().GetOffset());
	return true;
}

void VKRHI_Buffer::Shutdown()
{
	if (_device && _buffer)
		vkDestroyBuffer(_device->GetVkDevice(), _buffer, nullptr);

	VKRHI_MemoryObject::Shutdown();
}

VkDescriptorBufferInfo VKRHI_Buffer::GetBufferInfo(uint64_t offset, uint64_t range) const
{
	VkDescriptorBufferInfo bufferInfo = {};
	bufferInfo.buffer = GetVkBuffer();
	bufferInfo.offset = static_cast<VkDeviceSize>(offset);
	bufferInfo.range = static_cast<VkDeviceSize>(range);

	return bufferInfo;
}

VkDescriptorBufferInfo VKRHI_Buffer::GetWholeBufferInfo() const
{
	return GetBufferInfo(0, VK_WHOLE_SIZE);
}

size_t VKRHI_Buffer::GetByteSize() const
{
	return _size;
}

bool VKRHI_ImageMemory::FromDesc(VKRHI_Device& device, ImageMemoryDesc const& bufferDesc)
{
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.flags = bufferDesc.flags;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent = VKRHI_TypeConverters::GlmToVk(glm::uvec3{ bufferDesc.extent, 1 });
	imageInfo.mipLevels = bufferDesc.mipmapCount;
	imageInfo.arrayLayers = bufferDesc.layerCount;
	imageInfo.format = bufferDesc.format;
	imageInfo.tiling = bufferDesc.tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = bufferDesc.usage;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateImage(device.GetVkDevice(), &imageInfo, nullptr, &_image) != VK_SUCCESS)
		return false;

	VkMemoryRequirements requirements;
	vkGetImageMemoryRequirements(device.GetVkDevice(), _image, &requirements);

	MemoryObjectDesc desc = {};
	desc.allocator = bufferDesc.allocator;
	desc.memRequirement = &requirements;
	desc.properties = bufferDesc.memProperties;
	if (!VKRHI_MemoryObject::FromDesc(device, desc))
		return false;

	vkBindImageMemory(device.GetVkDevice(), _image, GetMemSlot().GetVkMemory(), GetMemSlot().GetOffset());

	_layerCount = bufferDesc.layerCount; 
	_mipmapCount = bufferDesc.mipmapCount;
	_extents = bufferDesc.extent;
	return true;
}

void VKRHI_ImageMemory::Shutdown()
{
	if (_image)
		vkDestroyImage(_device->GetVkDevice(), _image, nullptr);

	VKRHI_MemoryObject::Shutdown();
}

size_t VKRHI_ImageMemory::GetByteSize() const
{
	return GetMemSlot().GetSize();
}
