#include "RHI/Vulkan/VKRHI_TerrainRenderObject.h"

bool VKRHI_TerrainRenderObject::FromDesc(VKRHI_TerrainRenderObjectDesc const& desc)
{
	_heightTextureKey = desc.heightTextureKey;
	_grassTextureKey =	desc.grassTextureKey;
	_snowTextureKey =	desc.snowTextureKey;
	_cliffTextureKey =	desc.cliffTextureKey;

	return true;
}

void VKRHI_TerrainRenderObject::Shutdown() 
{
}
