#include "RHI/Vulkan/VKRHI_RenderPass.h"

#include <algorithm>

#include "glm/gtc/type_ptr.hpp"

#include "Utils/IteratorUtils.h"
#include "Maths/Utils.h"

#include "Debugging/Assert.h"

#include "RHI/Vulkan/VKRHI_TypeConverters.h"

VKRHI_SubpassDependencyDesc::VKRHI_SubpassDependencyDesc()
{
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
}

VKRHI_PassAttachmentDesc::VKRHI_PassAttachmentDesc(uint32_t index, VkFormat format)
{
	attachment.format = format;
	attachment.samples = VK_SAMPLE_COUNT_1_BIT;
	attachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachment.finalLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	attachmentReference.attachment = index;
	attachmentReference.layout = VK_IMAGE_LAYOUT_UNDEFINED;
}

bool VKRHI_RenderPass::FromDesc(VKRHI_Device& device, VKRHI_RenderPassDesc&& desc)
{
	std::vector<std::vector<VkAttachmentReference>> colorAttachmentsPool;
    size_t subpassesCount = std::max(desc.subpasses.size(),  size_t(1u));
    colorAttachmentsPool.reserve(subpassesCount);

    std::vector<VkSubpassDescription> subpasses;
    subpasses.reserve(subpassesCount);

    auto createSubpassDesc = [&colorAttachmentsPool](std::vector<VKRHI_PassAttachmentDesc>& colorAttachments, std::optional<VKRHI_PassAttachmentDesc> const& depthStencilAttachments) -> auto
    {
        using VkAttachmentRefIt = FwdMemberIterator<std::vector<VKRHI_PassAttachmentDesc>, &VKRHI_PassAttachmentDesc::attachmentReference>;
        auto& colorAttachment = colorAttachmentsPool.emplace_back();
	    std::copy(VkAttachmentRefIt(colorAttachments.begin()), VkAttachmentRefIt(colorAttachments.end()), std::back_inserter(colorAttachment));

	    VkSubpassDescription subpassDescription = {};
	    subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

	    if (colorAttachment.size())
	    {
		    subpassDescription.colorAttachmentCount = static_cast<uint32_t>(colorAttachment.size());
		    subpassDescription.pColorAttachments = colorAttachment.data();
	    }

	    if (depthStencilAttachments.has_value())
		    subpassDescription.pDepthStencilAttachment = &depthStencilAttachments.value().attachmentReference;

        return subpassDescription;
    };

    if (desc.subpasses.size())
    {
        for (auto&& pass : desc.subpasses)
	        subpasses.push_back(createSubpassDesc(pass.colorAttachments, pass.depthStencilAttachment));
    }
    else
    {
        VKRHI_SubpassDescription subpassDesc {};
        subpassDesc.colorAttachments = desc.colorAttachments;
        subpassDesc.depthStencilAttachment = desc.depthStencilAttachment;
        subpassDesc.contents = VkSubpassContents::VK_SUBPASS_CONTENTS_INLINE;
	    subpasses.push_back(createSubpassDesc(subpassDesc.colorAttachments, subpassDesc.depthStencilAttachment));
        desc.subpasses.push_back(subpassDesc);
    }
        
	std::vector<VkAttachmentDescription> attachments;
	using VkAttachmentIt = FwdMemberIterator<std::vector<VKRHI_PassAttachmentDesc>, &VKRHI_PassAttachmentDesc::attachment>;
	std::copy(VkAttachmentIt(desc.colorAttachments.begin()), VkAttachmentIt(desc.colorAttachments.end()), std::back_inserter(attachments));
	if (desc.depthStencilAttachment)
		attachments.push_back(desc.depthStencilAttachment->attachment);

	std::vector<VkSubpassDependency> subpassDependencies;
	using VkSubpassIt = FwdMemberIterator<std::vector<VKRHI_SubpassDependencyDesc>, &VKRHI_SubpassDependencyDesc::dependency>;
	std::copy(VkSubpassIt(desc.dependencies.begin()), VkSubpassIt(desc.dependencies.end()), std::back_inserter(subpassDependencies));

	VkRenderPassCreateInfo passCreateInfo = {};
	passCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	passCreateInfo.subpassCount = static_cast<uint32_t>(subpasses.size());
	passCreateInfo.pSubpasses = subpasses.data();
	passCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	passCreateInfo.pAttachments = attachments.data();
	passCreateInfo.dependencyCount = static_cast<uint32_t>(subpassDependencies.size());
	passCreateInfo.pDependencies = subpassDependencies.data();

	if (vkCreateRenderPass(device.GetVkDevice(), &passCreateInfo, nullptr, &_rhiRenderPass) != VK_SUCCESS)
		return false;

	_device = &device;
	_renderPassProperties = std::move(desc);
	return true;
}

void VKRHI_RenderPass::Shutdown()
{
	if (_device && _rhiRenderPass)
		vkDestroyRenderPass(_device->GetVkDevice(), _rhiRenderPass, nullptr);

	_device = nullptr;
	_renderPassProperties = {};
}


void VKRHI_RenderPass::CmdBeginRenderPass(VkCommandBuffer buffer, VkFramebuffer framebuffer)
{
	VkRenderPassBeginInfo renderBeginInfo = {};
	renderBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderBeginInfo.renderPass = _rhiRenderPass;
	renderBeginInfo.framebuffer = framebuffer;
	renderBeginInfo.renderArea.offset = { 0, 0 };
	renderBeginInfo.renderArea.extent = VKRHI_TypeConverters::GlmToVk(_renderPassProperties.renderAreaExtents);

	std::vector<VkClearValue> clearValues;
	std::transform(
		_renderPassProperties.colorAttachments.begin(), _renderPassProperties.colorAttachments.end(),
		std::back_inserter(clearValues),
		[](VKRHI_PassAttachmentDesc desc)
	{
		VkClearValue value;
		value.color = { UNROLL_VEC4(desc.clearColor) };
		return value;
	});

	if (_renderPassProperties.depthStencilAttachment)
	{
		auto& depthStencilValue = clearValues.emplace_back();
		auto const& color = _renderPassProperties.depthStencilAttachment->clearColor;
		depthStencilValue.depthStencil = { color.x, static_cast<uint32_t>(color.y) };
 	}

	renderBeginInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
	renderBeginInfo.pClearValues = clearValues.data();

	vkCmdBeginRenderPass(buffer, &renderBeginInfo, _renderPassProperties.subpasses[0].contents);
    _currentSubpass = 0;
}

void VKRHI_RenderPass::CmdEndRenderPass(VkCommandBuffer buffer)
{
    KU_ASSERT(_currentSubpass == _renderPassProperties.subpasses.size() - 1);

	vkCmdEndRenderPass(buffer);
    _currentSubpass = -1;
}

void VKRHI_RenderPass::CmdNextSubpass(VkCommandBuffer buffer)
{
    KU_ASSERT(_currentSubpass != -1);
    KU_ASSERT(_currentSubpass < static_cast<int>(_renderPassProperties.subpasses.size()));

	vkCmdNextSubpass(buffer, _renderPassProperties.subpasses[_currentSubpass].contents);
    _currentSubpass++;
}
