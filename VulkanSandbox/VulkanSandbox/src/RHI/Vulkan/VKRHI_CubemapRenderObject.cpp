#include "RHI/Vulkan/VKRHI_CubemapRenderObject.h"

bool VKRHI_CubemapRenderObject::FromDesc(VKRHI_CubemapRenderObjectDesc const& desc)
{
	_meshRenderObject.FromDesc(desc.meshRODesc);

	_imageBufferKey = desc.imageBufferKey;

	return true;
}

void VKRHI_CubemapRenderObject::Shutdown()
{

}