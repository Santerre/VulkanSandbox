#include "Debug/VKRHI_DebugExtension.h"

#include "Debug/Log.h"

DEF_LOG_CATEGORY(VulkanValidation);

void VKRHI_DebugExtension::Init()
{

}

void VKRHI_DebugExtension::Shutdown()
{

}

void VKRHI_DebugExtension::Register()
{

}

VKAPI_ATTR VkBool32 VKAPI_CALL VKRHI_DebugExtension::DebugCallback(
	VkDebugReportFlagsEXT flags,
	VkDebugReportObjectTypeEXT objType,
	uint64_t obj,
	size_t location,
	int32_t code,
	const char* layerPrefix,
	const char* msg,
	void* userData)
{
	switch (flags)
	{
	case VK_DEBUG_REPORT_INFORMATION_BIT_EXT:
		VLOG(LogVulkanValidation, msg);
		break;
	case VK_DEBUG_REPORT_WARNING_BIT_EXT:
		WLOG(LogVulkanValidation, msg);
		break;
	case VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT:
		WLOG(LogVulkanValidation, msg);
		break;
	case VK_DEBUG_REPORT_ERROR_BIT_EXT:
		ELOG(LogVulkanValidation, msg);
		break;
	case VK_DEBUG_REPORT_DEBUG_BIT_EXT:
		LOG(LogVulkanValidation, msg);
		break;
	default:
		break;
	}

	return VK_FALSE;
}