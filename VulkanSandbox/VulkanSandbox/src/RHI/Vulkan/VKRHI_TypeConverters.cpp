#include "RHI/Vulkan/VKRHI_TypeConverters.h"

glm::uvec2 VKRHI_TypeConverters::VkToGlm(VkExtent2D const& extents)
{
	return glm::uvec2{ extents.width, extents.height };
}

VkExtent2D VKRHI_TypeConverters::GlmToVk(glm::uvec2 const& extents)
{
	return VkExtent2D{ extents.x, extents.y };
}

glm::uvec3 VKRHI_TypeConverters::VkToGlm(VkExtent3D const& extents)
{
	return glm::uvec3{ extents.width, extents.height, extents.depth };
}

VkExtent3D VKRHI_TypeConverters::GlmToVk(glm::uvec3 const& extents)
{
	return VkExtent3D{ extents.x, extents.y, extents.z };
}

VkShaderStageFlagBits VKRHI_TypeConverters::GenToVK(ShaderType shaderType)
{
    switch ( shaderType )
    {
    case ShaderType::Vertex:
		return VK_SHADER_STAGE_VERTEX_BIT;
    case ShaderType::TessellationControl:
		return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
    case ShaderType::TessellationEvaluation:
		return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
    case ShaderType::Geometry:
		return VK_SHADER_STAGE_GEOMETRY_BIT;
    case ShaderType::Fragment:
		return VK_SHADER_STAGE_FRAGMENT_BIT;
    case ShaderType::Compute:
        return VK_SHADER_STAGE_COMPUTE_BIT;
    case ShaderType::Task:
        return VK_SHADER_STAGE_TASK_BIT_NV;
    case ShaderType::Mesh:
        return VK_SHADER_STAGE_MESH_BIT_NV;
    default:
        KU_ASSERT(0);
        return VK_SHADER_STAGE_ALL_GRAPHICS;
    }
}
