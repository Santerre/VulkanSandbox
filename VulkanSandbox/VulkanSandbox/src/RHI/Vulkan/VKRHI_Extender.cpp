#include "include/RHI/Vulkan/VKRHI_Extender.h"

void VKRHI_Extender::Init()
{

}

bool VKRHI_Extender::RequestExtension(const char* extentionName)
{
	const auto[it, res] = _requestedExtensions.insert(extentionName);
	return res;
}

bool VKRHI_Extender::RequestLayer(const char* layerName)
{
	const auto[it, res] = _requestedLayers.insert(layerName);
	return res;
}

void VKRHI_Extender::TestExtensionItems()
{
	uint32_t availableLayersCount = 0;
	vkEnumerateInstanceLayerProperties(&availableLayersCount, nullptr);
	std::vector<VkLayerProperties> availableLayers(availableLayersCount);
	vkEnumerateInstanceLayerProperties(&availableLayersCount, availableLayers.data());
	availableLayers.sort();
	size_t wantedLayersCount = _requiredLayers.size();
	if (wantedLayersCount)
		VLOG(LogVulkan, u8"---- Loading %u validation layer ----", wantedLayersCount);
	else
		VLOG(LogVulkan, u8"No validation layers queried");

	for (auto layer : _requiredLayers.size())
	{
		bool isFound = false;
		for (uint32_t j = 0; j < availableLayersCount; j++)
		{
			if (strcmp(_requiredLayers[i], availableLayers[j].layerName) == 0)
			{
				isFound = true;
				break;
			}
		}

		if (!isFound)
			_requiredLayers.erase(_requiredLayers.begin() + i);
		else
	}
}

void VKRHI_Extender::InitExtensions()
{

}

void VKRHI_Extender::PrintExtensionSummary(std::string const& title, std::map<std::string, VKRHI_ExtenderItem> const& map)
{
	PrintExtensionSummary(title, map.begin(), map.end());
}
