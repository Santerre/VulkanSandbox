#include "RHI/Vulkan/VKRHI_Device.h"

#include <vector>
#include <set>

#include "RHI/Vulkan/VKRHI_PhysicalDevice.h"
#include "Debugging/Assert.h"

bool VKRHI_Device::FromPhysicalDevice(VKRHI_PhysicalDevice const& physicalDevice, std::vector<const char*> const& validationLayers, std::vector<const char*> const& _extensions)
{
	_physicalDevice = &physicalDevice;

	QueueFamilyIndices indices = physicalDevice.GetQueueFamilyIndices();

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<uint32_t> uniqueQueueIndexes = { indices.graphicsFamily.value(), indices.presentFamily.value() };
	for (auto&& index : uniqueQueueIndexes)
	{
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.queueFamilyIndex = index;

		float queuePriority = 1.f;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pQueueCreateInfos = queueCreateInfos.data();

	createInfo.enabledExtensionCount = static_cast<uint32_t>(_extensions.size());
	createInfo.ppEnabledExtensionNames = _extensions.data();

	createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
	createInfo.ppEnabledLayerNames = validationLayers.data();

    VkPhysicalDeviceFeatures physicalDeviceFeatures = {};

    VkPhysicalDeviceMeshShaderFeaturesNV meshShadingFeatures = {};
    meshShadingFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_FEATURES_NV;
    if (physicalDevice.SupportsMeshShading())
    {
        meshShadingFeatures.meshShader = VK_TRUE;
        meshShadingFeatures.taskShader = VK_TRUE;
        _meshShadingEnabled = true;

        createInfo.pNext = &meshShadingFeatures;
    }

	physicalDeviceFeatures.samplerAnisotropy = VK_TRUE;
	physicalDeviceFeatures.tessellationShader = VK_TRUE;
	physicalDeviceFeatures.fillModeNonSolid = VK_TRUE;

	createInfo.pEnabledFeatures = &physicalDeviceFeatures;

	if (vkCreateDevice(physicalDevice.GetVkPhysicalDevice(), &createInfo, nullptr, &_device) != VK_SUCCESS)
		return false;

	vkGetDeviceQueue(_device, indices.graphicsFamily.value(), 0, &_graphicQueue);
	vkGetDeviceQueue(_device, indices.presentFamily.value(), 0, &_presentQueue);

    InitFunctions(_deviceFunctions);

	return true;
}

void VKRHI_Device::Shutdown()
{
	if (_device)
		vkDestroyDevice(_device, nullptr);
}

void VKRHI_Device::Wait()
{
	vkDeviceWaitIdle(_device);
}

VKRHI_PhysicalDevice const& VKRHI_Device::GetPhysicalDevice() const
{
	KU_ASSERT(_physicalDevice);
	return *_physicalDevice;
}

void VKRHI_Device::InitFunctions(VKRHI_DeviceFunctions& outFunctions)
{
#define LOAD_FUNC(name) outFunctions.name = (PFN_##name)vkGetDeviceProcAddr(_device, #name);

    LOAD_FUNC(vkCmdDrawMeshTasksNV);
}