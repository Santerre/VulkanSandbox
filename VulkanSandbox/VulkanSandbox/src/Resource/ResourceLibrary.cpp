#include "Resource/ResourceLibrary.h"

void ResourceLibrary::AddResource( std::string const& path, std::shared_ptr<IResourceWrapper> res )
{
    KU_ASSERT(!HasResource(path));
    _resources[path] = res;
}

std::shared_ptr<IResourceWrapper> ResourceLibrary::GetResource( std::string const& path ) const
{
    auto res = _resources.find(path);
    return res == _resources.end() ? std::shared_ptr<IResourceWrapper>{} : res->second;
}

bool ResourceLibrary::HasResource( std::string const& path ) const
{
    return _resources.find(path) != _resources.end();
}

void ResourceLibrary::UnregisterResource( std::string const& path )
{
    auto res = _resources.find(path);
    KU_ASSERT(res != _resources.end());
    if (res != _resources.end())
        _resources.erase(res);
}

void ResourceLibrary::Clear()
{
    _resources.clear();
}