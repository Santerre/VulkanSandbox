#include "Resource/Loaders/MemoryLoader.h"

#include <algorithm>

#include "Debugging/Assert.h"

#include "Memory/HierarchicalMemory.h"

MemoryBuffer::MemoryBuffer(char_type* start, std::streamsize size)
{
	Open(start, size);
}

void MemoryBuffer::Open(char_type* start, std::streamsize size)
{ 
	setg(start, start, start + size);
}


void MemoryBuffer::Close()
{
	// nothing to do yet
}

std::basic_streambuf<char>::pos_type MemoryBuffer::seekoff(
	off_type off,
	std::ios_base::seekdir way,
	std::ios_base::openmode which)
{
	switch (way)
	{
	case std::ios_base::beg:
		setg(eback(), eback() + off, egptr());
		break;
	case std::ios_base::cur:
		gbump(static_cast<int>(off));
		break;
	case std::ios_base::end:
		setg(eback(), egptr() - off, egptr());
		break;
	default:
		return pos_type(off_type(-1));
	}

	return pos_type(gptr() - eback());
}

std::basic_streambuf<char>::pos_type MemoryBuffer::seekpos(pos_type off, std::ios_base::openmode which)
{
	return pos_type(seekoff(off, std::ios_base::beg, which));
}

std::streamsize MemoryBuffer::showmanyc()
{
	return std::streamsize(egptr() - eback());
}


std::streamsize MemoryBuffer::xsgetn(char_type* s, std::streamsize n)
{
	std::streamsize count = std::min(n, std::streamsize(egptr() - gptr()));
	memcpy(s, gptr(), static_cast<size_t>(count));
	gbump(static_cast<int>(count));
	return std::streamsize(count);
}

std::basic_streambuf<char>::int_type MemoryBuffer::underflow()
{
	if (gptr() < egptr() || gptr() >= eback())
		return traits_type::eof();

	return traits_type::to_int_type(*gptr());
}

MemoryLoader::MemoryLoader(HierarchicalMemory& memory, Path const& path)
	: _memory(memory)
	, _path(path)
{
}

std::istream* MemoryLoader::OpenRead()
{
	KU_ASSERT(!_inFile);

	MemorySlot& buffer = _memory.GetSlot(_path);
	std::vector<char>& data = buffer.GetData();
	if (data.empty())
		return nullptr;

	_memoryBuffer = std::make_unique<MemoryBuffer>(data.data(), std::streamsize(data.size()));

	_inFile = std::make_unique<std::istream>(_memoryBuffer.get());

	return _inFile.get();
}

void MemoryLoader::CloseRead()
{
	if (_memoryBuffer)
		_memoryBuffer->Close();

	if (_inFile)
		_inFile = nullptr;

	if (_memoryBuffer)
		_memoryBuffer = nullptr;
}

std::ostream* MemoryLoader::OpenWrite()
{
	// not implemented yet
	KU_ASSERT(0);
	return nullptr;
}

void MemoryLoader::CloseWrite()
{
	// nothing to do yet
}
