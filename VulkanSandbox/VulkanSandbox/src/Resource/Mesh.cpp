#include "Resource/Mesh.h"

#include "Resource/ResourceStreamInfo.h"

// Todo - Remove : Vulkan specific
#include "RHI/Vulkan/VKRHI_Device.h"

#include "tiny_obj_loader.h"

void Mesh::Init(ResourceInitContext const& Context)
{
	_isComplete = true;
}

void Mesh::Shutdown()
{
	_isComplete = false;
}

bool	Parser<Mesh>::Unserialize(Handle<Mesh> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	stream.seekg(0);
	if (info.path->GetExtension() == ".raw")
	{
		handle->_name = info.path->GetString();
		size_t vertsCount;
		LoadStreamArrayAs(stream, &vertsCount, 1);

		handle->_data.vertices.resize(vertsCount);
		LoadStreamArrayAs(stream, handle->_data.vertices.data(), vertsCount);
		size_t indicesCount;
		LoadStreamArrayAs(stream, &indicesCount, 1);
		handle->_data.indices.resize(indicesCount);
		LoadStreamArrayAs(stream, handle->_data.indices.data(), indicesCount);
	}
	else if (info.path->GetExtension() == ".obj")
	{
		tinyobj::attrib_t attrib;
		std::vector<tinyobj::shape_t> shapes;
		std::vector<tinyobj::material_t> materials;
		std::string warn, err;

		if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, &stream))
		{
			ELOG(LogResource, "Couldn't read the obj %s.", info.path->GetString().c_str());
			return false;
		}
		
		for (const auto& shape : shapes) {
			for (const auto& index : shape.mesh.indices) {
				Vertex vertex = {};
				vertex.position = 
				{
					attrib.vertices[3 * index.vertex_index + 0],
					attrib.vertices[3 * index.vertex_index + 1],
					attrib.vertices[3 * index.vertex_index + 2]
				};

				vertex.uv = 
				{
					attrib.texcoords[2 * index.texcoord_index + 0],
					1.0 - attrib.texcoords[2 * index.texcoord_index + 1]
				};

				vertex.color = { 1.0f, 1.0f, 1.0f };

				handle->_data.vertices.push_back(vertex);
				handle->_data.indices.push_back(static_cast<uint32_t>(handle->_data.indices.size()));

			}
		}
	}
	else
	{
		return false;
	}

	return true;
}

bool	Parser<Mesh>::Reload(Handle<Mesh> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	return Unserialize(handle, info, stream);
}