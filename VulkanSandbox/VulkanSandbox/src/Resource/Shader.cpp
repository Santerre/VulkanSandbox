#include "Resource/Shader.h"

#include <vector>
#include <memory>
#include <algorithm>

#include "Debugging/Assert.h"
#include "Debugging/Log.h"

#include "Resource/ResourceStreamInfo.h"
#include "Resource/Parsers/TextParser.h"

#include "RHI/Vulkan/VKRHI_Device.h"

#include "RHI/Vulkan/VKRHI_LogCategory.h"

#include "Core/Path.h"

void GLSLShader::Init(ResourceInitContext const& context)
{
	_isComplete = true;
    _application = context.application;

}

void GLSLShader::Shutdown()
{
	_source.clear();
}

std::string GLSLShader::GetShaderDefaultExtension(ShaderType type)
{
    switch ( type )
    {
    case ShaderType::Vertex:
        return ".vert";
    case ShaderType::TessellationControl:
        return ".tesc";
    case ShaderType::TessellationEvaluation:
        return ".tese";
    case ShaderType::Geometry:
        return ".geom";
    case ShaderType::Fragment:
        return ".frag";
    case ShaderType::Compute:
        return ".comp";
    case ShaderType::Task:
        return ".task";
    case ShaderType::Mesh:
        return ".mesh";
    case ShaderType::None:
    default:
        KU_ASSERT(false);
        return "";
    }
}

bool Parser<GLSLShader>::Unserialize(Handle<GLSLShader> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	handle->_name = info.path->GetExtendedName().GetString();
    TextParser::UnserializeText(stream, handle->_source);

	return true;
}

bool Parser<GLSLShader>::Reload(Handle<GLSLShader> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	return Unserialize(handle, info, stream);
}


bool Parser<GLSLShader>::Serialize(Handle<GLSLShader> handle, ResourceStreamInfo const& info, std::ostream& stream)
{
    TextParser::SerializeText(stream, handle->_source);
    return true;
}

/// SPV Shader
void SPVShader::FromData( std::string const& name, ShaderType type, std::vector<uint32_t>&& str )
{
    _name = name;
    _shaderType = type;
    _shaderData = std::move(str);
}

void SPVShader::Init(ResourceInitContext const& context)
{
	if (_shaderData.size())
	{
		KU_ASSERT(_rhiShader);
		if (!_rhiShader->FromSpirVData(context.application->GetRenderingContext(), _shaderData, _shaderType))
		{
			ELOG(LogResource, "Failed To load the shader %s from file data", _name.c_str());
			return;
		}
	}

	_isComplete = true;
}

void SPVShader::Shutdown()
{
	_rhiShader->Shutdown();
}

std::string  SPVShader::GetShaderDefaultExtension(ShaderType type)
{
    return GLSLShader::GetShaderDefaultExtension(type) + "spv";
}

bool Parser<SPVShader>::Unserialize(Handle<SPVShader> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	handle->_name = info.path->GetExtendedName().GetString();
	handle->_shaderType = ShaderParserUtils::PathToShaderType(*info.path);

	LoadAllStreamData(stream, handle->_shaderData);

	return true;
}

bool Parser<SPVShader>::Reload(Handle<SPVShader> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	return Unserialize(handle, info, stream);
}

bool Parser<SPVShader>::Serialize(Handle<SPVShader> handle, ResourceStreamInfo const& info, std::ostream& stream)
{
    WriteAllStreamData(stream, handle->_shaderData);
    return true;
}

/// Dynamic Shader
void	DynamicShaderDesc::Serialize( Document& outDocument ) const
{
	AddValue<std::string>(outDocument, outDocument, "Path", shaderPath);
	AddValue<std::map<std::string, std::string>>(outDocument, outDocument, "Attributes", shaderAttributes);
}

bool	DynamicShaderDesc::Unserialize( Document& document )
{
	shaderPath = RetrieveValue<std::string>(document["Path"]);
    shaderAttributes = RetrieveValue<std::map<std::string, std::string>>(document["Attributes"]);

    return true;
}

ShaderType ShaderParserUtils::PathToShaderType(Path const& path)
{
	std::string extension = path.GetExtension().GetString();
	if (extension.find(".vert") == 0)
		return ShaderType::Vertex;
	else if (extension.find(".geom") == 0)
		return ShaderType::Geometry;
	else if (extension.find(".tesc") == 0)
		return ShaderType::TessellationControl;
	else if (extension.find(".tese") == 0)
		return ShaderType::TessellationEvaluation;
	else if (extension.find(".frag") == 0)
		return ShaderType::Fragment;
    else if (extension.find(".task") == 0)
        return ShaderType::Task;
    else if (extension.find(".mesh") == 0)
        return ShaderType::Mesh;
	else
	{
		KU_ASSERT(0);
		return ShaderType::Vertex;
	}
}
