#include "Core/Path.h"

#include <regex>
#include "Debugging/Assert.h"

Path::Path(std::string const& pathStr)
{
	FromPathStr(pathStr);
}

Path::Path(std::filesystem::path const& path)
	: _path(path)
	, _isValid(true)
{
}

Path::Path(Path const& context, Path const& origin)
{
	(*this) = GetRelativePath(context, origin);
}

bool Path::operator==(Path const& other) const
{
	return _path == other._path;
}

bool Path::operator==(std::string const& str) const
{
	return _path == str;
}

Path Path::Concat(Path const& b) const
{
	return _path / b._path;
}

Path Path::GetVolume() const
{ 
	return { _path.root_name() }; 
}

Path Path::GetDirectory() const
{
	return { _path.parent_path() };
}

Path Path::GetFullRelativePath() const
{
	return { _path.relative_path() };
}

Path Path::GetRelativePath() const 
{ 
	return { _path.lexically_relative(_path.root_path()).parent_path() };
}

Path Path::GetFilename() const 
{ 
	return { _path.stem().string() }; 
}

Path Path::GetExtension() const 
{ 
	return { _path.extension().string() }; 
}

Path Path::GetExtendedName() const 
{ 
	return { _path.filename().string() };
}

std::string Path::GetString() const 
{ 
	return _path.string(); 
}

void Path::FromPathStr(std::string const& pathStr)
{
	_path = std::filesystem::path(pathStr);
	_isValid = true;
}

Path Path::GetRelativePath(Path const& context, Path const& origin)
{
	std::error_code code;
	Path result(std::filesystem::relative(context._path, origin._path, code));
	result._isValid = !code;
	return result;
}

Path Path::ChangeExtension(std::string newExtension ) const
{
    // TODO optimise
    return GetDirectory().Concat(GetFilename().GetString() + newExtension);
}