#include "Resource/Parsers/TextParser.h"

#include <string>
#include <sstream>
#include <fstream>
#include <streambuf>

bool	TextParser::UnserializeText(std::istream& stream, std::string& res)
{
    res.assign((std::istreambuf_iterator<char>(stream)),
                std::istreambuf_iterator<char>());

    size_t i = 0;
    while( (i = res.find("\r\n")) != std::string::npos)
    {
        res.replace(i, 2, "\n");
    }
    return true;
}

bool	TextParser::SerializeText(std::ostream& stream, std::string const& src)
{
    stream << src;
    return true;
}