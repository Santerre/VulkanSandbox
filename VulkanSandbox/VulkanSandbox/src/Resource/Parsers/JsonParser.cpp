#include "Resource/Parsers/JsonParser.h"

#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/writer.h"
#include "rapidjson/prettywriter.h"

#include "Resource/Parsers/ISerializable.h"

bool	JsonParser::UnserializeObject(std::istream& stream, ISerializable* object)
{
	Document document;
	rapidjson::IStreamWrapper isw(stream);
	document.ParseStream(isw);
	return object->Unserialize(document);
}

bool	JsonParser::SerializeObject(std::ostream& stream, ISerializable const* object)
{
	Document document;
	document.SetObject();
	object->Serialize(document);
	rapidjson::OStreamWrapper osw(stream);
	rapidjson::PrettyWriter writer(osw);
	return document.Accept(writer);
}