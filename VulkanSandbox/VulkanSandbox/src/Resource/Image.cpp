#include "Resource/Texture.h"

#include "Debugging/Log.h"

#include "Resource/ResourceStreamInfo.h"

void Image::Init(ResourceInitContext const& Context)
{
	_isComplete = _buffer != nullptr;
}

void Image::Shutdown()
{
	_buffer = nullptr;
	_extent = glm::ivec2();
	_texChannels = 0;
	_isComplete = false;
}

size_t Image::GetByteSize() const
{
	return static_cast<size_t>(_extent.x * _extent.y * 4);
}

bool	Parser<Image>::Unserialize(Handle<Image> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	std::vector<unsigned char> streamBuf;
	LoadAllStreamData<unsigned char>(stream, streamBuf);

    handle->_name = info.path->GetString();
	handle->_buffer = stbi_load_from_memory(streamBuf.data(), static_cast<int>(streamBuf.size()), 
		&handle->_extent.x, &handle->_extent.y, &handle->_texChannels, STBI_rgb_alpha);

	if (!handle->_buffer)
	{
		ELOG(LogResource, "Couldn't load resource %s", info.path->GetString().c_str());
		return false;
	}

	return true;
}

bool	Parser<Image>::Reload(Handle<Image> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	return Unserialize(handle, info, stream);
}