#include "Resource/DynamicShader.h"

#include "RHI/ShaderCompiler.h"
#include "Core/ShaderCompilationContext.h"

// TODO Remove when possible
#include "Resource/Generator/MeshGenerator.h"
#include <sstream>

void DynamicShader::FromGLSLShader( std::shared_ptr<ResourceWrapper<GLSLShader>> wrapper, ResourceInitContext context)
{
	_name = GetDefaultNameFromGLSL(wrapper);

    _shaderType = ShaderParserUtils::PathToShaderType(Path(wrapper->GetResourcePath()));
    _targetShader = wrapper;
    Init(context);
}

void DynamicShader::Init(ResourceInitContext const& context)
{
	_isComplete = true;
    _application = context.application;
}

void DynamicShader::Shutdown()
{
    _shaderType = ShaderType::None;
    _targetShader = {};
    _generatedShaderCount = 0;
    _shaderAttributes.clear();
}

Handle<SPVShader>   DynamicShader::RequestSPVShader(ShaderCompilationContext const& context, ShaderCompiler& compiler)
{
    KU_ASSERT(_targetShader);

    ShaderCompiler::CompileOptions options = compiler.GetDefaultCompileOptions();
    options.macroList = _shaderAttributes;

    // TODO put it somewhere else
    for (auto&& [name, value] : context.GetIntGlobalMacros())
        options.macroList.emplace(name, std::to_string(value));
    for (auto&& [name, value] : context.GetIntGlobalMacros())
        options.macroList.emplace(name, std::to_string(value));

    {
        auto&& map = context.GetStringGlobalMacros();
        options.macroList.insert(map.begin(), map.end());
    }

    // TODO allow this in a cleaner way
    if (_prependPatchMeshDepth > 0)
    {
        PatchBuilder::InitDesc desc;
        desc.tessellationDepth = _prependPatchMeshDepth;
        PatchBuilder builder;
        builder.FromDesc(desc);

        std::vector<Vertex> verts(builder.GetVerticesCount());
        std::vector<MeshIndex> indices(builder.GetIndicesCount());

        builder.Build(verts.data(), indices.data());

        std::ostringstream patchDataStream;
        patchDataStream << "const uint KU_PatchData_PositionCount = " << verts.size() << ";" << std::endl;
        patchDataStream << "const vec2 KU_PatchData_Positions[KU_PatchData_PositionCount] = vec2[KU_PatchData_PositionCount](" << std::endl;
        for (size_t i = 0; i < verts.size(); i++)
            patchDataStream << "    vec2(" << verts[i].position.x << ", " << verts[i].position.z << (i == verts.size() - 1 ? ")" : "),") << std::endl;
        patchDataStream << ");" << std::endl;
        patchDataStream << "const uint KU_PatchData_IndexCount = " << indices.size() << ";" << std::endl;
        patchDataStream << "const uint KU_PatchData_Indices[KU_PatchData_IndexCount] = uint[KU_PatchData_IndexCount](" << std::endl;
        for (size_t i = 0; i < indices.size(); i++)
            patchDataStream << "    " << indices[i] << (i == indices.size() - 1 ? "" : ",") << std::endl;
        patchDataStream << ");" << std::endl;
        options.prefixCode = { patchDataStream.str()};
        options.macroList.insert( { "KU_DEFINE_PATCH_DATA", "1" } );
    }

    Handle<SPVShader> shader = compiler.CompileShader(_targetShader, options, _shaderType);

    if (!shader)
    {
        ELOG(LogShaderCompilation, "%s : Couldn't compile shader %s", GetName().c_str(), _targetShader->GetResourcePath().GetString().c_str());
        return nullptr;
    }

    return shader;
}

std::string  DynamicShader::GetShaderDefaultExtension( ShaderType type )
{
    return GLSLShader::GetShaderDefaultExtension(type) + ".desc";
}

std::string  DynamicShader::GetDefaultNameFromGLSL(std::shared_ptr<ResourceWrapper<GLSLShader>> shader)
{
    KU_ASSERT(shader);
    return shader->GetResourcePath().GetString() + ".desc";
}

bool Parser<DynamicShader>::Unserialize(Handle<DynamicShader> handle, ResourceStreamInfo const& info, std::istream& stream)
{
    KU_ASSERT(info.resourceMgr);

	handle->_name = info.path->GetExtendedName().GetString();
    {
        DynamicShaderDesc desc;
        if (!UnserializeObject(stream, &desc))
        { 
		    ELOG(LogResource, "%s : An error happened while unserializing.", handle->GetName().c_str());
            return false;
	    }

        handle->_targetShader = info.resourceMgr->GetWrapper<GLSLShader>(desc.shaderPath);
        if (!handle->_targetShader)
        { 
	        ELOG(LogResource, "%s : Couldn't find shader at %s", handle->GetName().c_str(), desc.shaderPath.c_str());
            return false;
        }

        handle->_shaderAttributes = std::move(desc.shaderAttributes);
	    handle->_shaderType = ShaderParserUtils::PathToShaderType(desc.shaderPath);
    }

	return true;
}

bool Parser<DynamicShader>::Reload(Handle<DynamicShader> handle, ResourceStreamInfo const& info, std::istream& stream)
{
    // TODO clear correctly
	return Unserialize(handle, info, stream);
}

bool Parser<DynamicShader>::Serialize(Handle<DynamicShader> handle, ResourceStreamInfo const& info, std::ostream& stream)
{
    DynamicShaderDesc desc;
    desc.shaderPath = handle->_targetShader->GetResourcePath().GetString();
    desc.shaderAttributes = handle->_shaderAttributes;
    SerializeObject(stream, &desc);

    return true;
}
