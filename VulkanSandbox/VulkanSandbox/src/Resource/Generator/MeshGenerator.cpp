#include "Resource/Generator/MeshGenerator.h"

#include "Memory/HierarchicalMemory.h"

bool TriangleBuilder::Build(Vertex* outVertices, MeshIndex* outIndices)
{
	new (outVertices) Vertex[GetVerticesCount()]
	{
		{{0.0f, -0.5f, 0.0f},	{1.0f, 0.0f, 0.0f},	{1.0f, 0.0f}},
		{{0.5f, 0.5f, 0.0f},	{0.0f, 1.0f, 0.0f},	{0.0f, 0.0f}},
		{{-0.5f, 0.5f, 0.0f},	{0.0f, 0.0f, 1.0f},	{0.0f, 1.0f}}
	};
	new (outVertices) MeshIndex[GetIndicesCount()]
	{
		0, 1, 2
	};

	return true;
}

bool RectangleBuilder::Build(Vertex* outVertices, MeshIndex* outIndices)
{
	new (outVertices) Vertex[GetVerticesCount()]
	{
		{{-0.5f, -0.5f, 0.0f},	{1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
		{{0.5f, -0.5f, 0.0f},	{0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
		{{0.5f, 0.5f, 0.0f},	{0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
		{{-0.5f, 0.5f, 0.0f},	{1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}}
	};
	new (outIndices) MeshIndex[GetIndicesCount()]
	{
		0, 1, 2, 2, 3, 0
	};
	return true;
}


bool CubeBuilder::Build(Vertex* outVertices, MeshIndex* outIndices)
{
	new (outVertices) Vertex[GetVerticesCount()]
	{
		{ {-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f} }, // +Y (top face)
		{ {0.5f, 0.5f, -0.5f},	{1.0f, 1.0f, 0.0f}, {0.0f, 0.0f} },
		{ {0.5f, 0.5f,  0.5f},	{1.0f, 1.0f, 1.0f}, {0.0f, 1.0f} },
		{ {-0.5f, 0.5f,  0.5f}, {0.0f, 1.0f, 1.0f}, {1.0f, 1.0f} },

		{ {-0.5f, -0.5f,  0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 0.0f} }, // -Y (bottom face)
		{ {0.5f, -0.5f,  0.5f},  {1.0f, 0.0f, 1.0f}, {0.0f, 0.0f} },
		{ {0.5f, -0.5f, -0.5f},  {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f} },
		{ {-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f} },
	};
	new (outIndices) MeshIndex[GetIndicesCount()]
	{
		0, 2, 1,
		0, 3, 2,

		4, 6, 5,
		4, 7, 6,

		3, 5, 2,
		3, 4, 5,

		2, 6, 1,
		2, 5, 6,

		1, 6, 7,
		1, 7, 0,

		0, 4, 3,
		0, 7, 4,
	};
	return true;
}

void GridBuilder::FromDesc(Desc const& desc)
{
	_extents = desc.extents;
	_isForTesselation = desc.isForTesselation;
}

size_t GridBuilder::GetVerticesCount() const 
{
	return (_extents.x + 1) * (_extents.y + 1); 
}

size_t GridBuilder::GetIndicesCount() const 
{ 
	return (_isForTesselation ? 4u : 6u) * _extents.x * _extents.y; 
}

bool GridBuilder::Build(Vertex* outVertices, MeshIndex* outIndices)
{
	glm::vec2 startPosition = { -glm::vec2(_extents) / 2.f };
	for (unsigned int i = 0u; i < _extents.x + 1; i++)
	{
		for (unsigned int j = 0u; j < _extents.y + 1; j++)
		{
			auto pos = startPosition + glm::vec2(i, j);
			outVertices[i + j * (_extents.x + 1)].position = glm::vec3(pos.x / _extents.x, 0, pos.y / _extents.y);
			outVertices[i + j * (_extents.x + 1)].color = glm::vec3(0.1, 0.1, 0.1);
			outVertices[i + j * (_extents.x + 1)].uv = glm::vec2(static_cast<float>(i) / static_cast<float>(_extents.x), static_cast<float>(j) / static_cast<float>(_extents.y));
		}
	}

	unsigned int currentIndex = 0;
	for (unsigned int i = 0u; i < _extents.x; i++)
	{
		for (unsigned int j = 0u; j < _extents.y; j++)
		{
			auto xCount = _extents.x + 1;
			outIndices[currentIndex++] = i + 1 + (j) * xCount;
			outIndices[currentIndex++] = i + (j)* xCount;
			outIndices[currentIndex++] = i + 1 + (j + 1) * xCount;
			if (!_isForTesselation)
			{
				outIndices[currentIndex++] = i + 1 + (j + 1) * xCount;
				outIndices[currentIndex++] = i + (j)* xCount;
			}
			outIndices[currentIndex++] = i + (j + 1) * xCount;
		}
	}
	return true;
}

bool MeshGenerator::GenerateSlot(Path const& relPath, MemorySlot& slot)
{
	std::unique_ptr<MeshBuilder> builder;

	if (relPath.GetExtendedName() == "Triangle.raw")
		builder = std::make_unique<TriangleBuilder>();
	else if (relPath.GetExtendedName() == "Rectangle.raw")
		builder = std::make_unique<RectangleBuilder>();
	else if (relPath.GetExtendedName() == "Cube.raw")
		builder = std::make_unique<CubeBuilder>();
	else if (relPath.GetExtendedName() == "16x16Grid.raw")
	{
		builder = std::make_unique<GridBuilder>();
		GridBuilder* gridBuilder = static_cast<GridBuilder*>(builder.get());
		GridBuilder::Desc desc;
		desc.extents = { 16u, 16u };
		gridBuilder->FromDesc(desc);
	}
	else if (relPath.GetExtendedName() == "16x16TesGrid.raw")
	{
		builder = std::make_unique<GridBuilder>();
		GridBuilder* gridBuilder = static_cast<GridBuilder*>(builder.get());
		GridBuilder::Desc desc;
		desc.extents = { 1000u, 1000u };
		desc.isForTesselation = true;
		gridBuilder->FromDesc(desc);
	}

	if (!KU_VERIFY(builder))
		return false;

	size_t vertsCount = builder->GetVerticesCount();
	size_t indexCount = builder->GetIndicesCount();

	slot.Internal_Allocate(2 * sizeof(size_t) + vertsCount * sizeof(Vertex) + indexCount * sizeof(MeshIndex));

	size_t offset = 0;
	size_t* vertsCountSlot = slot.Internal_GetDataPointerAs<size_t>(offset);
	*vertsCountSlot = vertsCount;
	offset += sizeof(size_t);

	Vertex* vertsSlot = slot.Internal_GetDataPointerAs<Vertex>(offset);
	offset += sizeof(Vertex) * vertsCount;

	size_t* indexCountSlot = slot.Internal_GetDataPointerAs<size_t>(offset);
	offset += sizeof(size_t);
	*indexCountSlot = indexCount;
	MeshIndex* indicesSlot = slot.Internal_GetDataPointerAs<MeshIndex>(offset);
	offset += sizeof(MeshIndex) * indexCount;

	return builder->Build(vertsSlot, indicesSlot);
}


void	PatchBuilder::FromDesc(InitDesc const& desc)
{
	KU_ASSERT(desc.tessellationDepth > 0);
	_tessellationDepth = desc.tessellationDepth;
}

size_t PatchBuilder::GetGridSideCount() const
{
	return static_cast<size_t>(glm::pow(2, _tessellationDepth)) + 1;
}

size_t	PatchBuilder::GetVerticesCount() const
{
	size_t gridSideCount = GetGridSideCount();
	return static_cast<size_t>(glm::pow(gridSideCount, 2) - (gridSideCount - 2) * 4);
}

size_t	PatchBuilder::GetIndicesCount() const
{
	size_t gridSideCount = GetGridSideCount();
	KU_ASSERT(gridSideCount > 1);
	size_t triangleCount = (gridSideCount - 2) * 4 + static_cast<size_t>(glm::pow(std::max<size_t>(gridSideCount, 3) - 3, 2)) * 2;
	return triangleCount * 3;
}

bool	PatchBuilder::Build(Vertex* outVertices, MeshIndex* outIndices)
{
	// Build Grid
	size_t gridSideCount = GetGridSideCount();
	KU_ASSERT(gridSideCount > 1);
	std::vector<uint32_t> indexGrid(gridSideCount * gridSideCount);

	auto getID = [&gridSideCount](size_t x, size_t y, bool bSwitch = false) -> size_t
	{
		if (bSwitch)
			return x * gridSideCount + y;
		else
			return y * gridSideCount + x;
	};


	// Grid shell
	size_t vIndex = 0;
	size_t iIndex = 0;
	auto pushVertex = [&gridSideCount, &vIndex, &indexGrid, &outVertices](size_t id)
	{
		float x = (id % gridSideCount) / static_cast<float>(gridSideCount - 1);
		float y = (id / gridSideCount) / static_cast<float>(gridSideCount - 1);
		Vertex &vert = outVertices[vIndex];
		vert.position = { x , 0, y };
		vert.color = { 0, 0, 0 };
		vert.uv = { x, y };

		indexGrid[id] = static_cast<MeshIndex>(vIndex++);
	};

	auto tryPushTriangle = [&iIndex, &outIndices](MeshIndex a, MeshIndex b, MeshIndex c) -> bool
	{
		if (a == b || b == c || c == a)
			return false;
		outIndices[iIndex++] = a;
		outIndices[iIndex++] = b;
		outIndices[iIndex++] = c;
		return true;
	};

	auto edgeLoop = [&indexGrid, &gridSideCount, &vIndex, &getID, &pushVertex](size_t base, bool bVertical)
	{
		size_t startVIndex = vIndex;
		for (size_t i = 0; i < gridSideCount; i++)
		{
			size_t id = getID(i, base, bVertical);

			// push for base vertex
			if (i == base)
				if (bVertical)
					pushVertex(id);
				else
					continue;
			// Skip base opposite
			else if (i == gridSideCount - 1 - base)
				if (bVertical)
					continue;
				else
					pushVertex(id);
			// Else use base index
			else
				indexGrid[id] = static_cast<MeshIndex>(startVIndex);
		}
	};
	edgeLoop(0, true);
	edgeLoop(0, false);
	edgeLoop(gridSideCount - 1, true);
	edgeLoop(gridSideCount - 1, false);

	// Inner grid
	for (size_t j = 1; j < gridSideCount - 1; j++)
	{
		for (size_t i = 1; i < gridSideCount - 1; i++)
		{
			pushVertex(getID(i, j));
		}
	}

	// Build Triangles
	for (size_t j = 1; j < gridSideCount; j++)
	{
		for (size_t i = 0; i < gridSideCount - 1; i++)
		{
			{
				MeshIndex a = indexGrid[getID(i, j - 1)];
				MeshIndex b = indexGrid[getID(i, j)];
				MeshIndex c = indexGrid[getID(i+1, j)];
				tryPushTriangle(a, b, c);
			}
			{
				MeshIndex a = indexGrid[getID(i, j - 1)];
				MeshIndex b = indexGrid[getID(i + 1, j)];
				MeshIndex c = indexGrid[getID(i + 1, j - 1)];
				tryPushTriangle(a, b, c);
			}
		}
	}
	KU_ASSERT(vIndex == GetVerticesCount());
	KU_ASSERT(iIndex == GetIndicesCount());

	return true;
}