#include "Resource/CubemapImage.h"

#include <algorithm>

#include "Debugging/Log.h"

#include "Resource/ResourceStreamInfo.h"
#include "Resource/ResourceMgr.h"

void CubemapImageDesc::Serialize(Document& outDocument) const
{
	AddValue<std::string>(outDocument, outDocument, "X", X);
	AddValue<std::string>(outDocument, outDocument, "nX", nX);
	AddValue<std::string>(outDocument, outDocument, "Y", nY);
	AddValue<std::string>(outDocument, outDocument, "nY", Y);
	AddValue<std::string>(outDocument, outDocument, "Z", Z);
	AddValue<std::string>(outDocument, outDocument, "nZ", nZ);
}

bool		CubemapImageDesc::Unserialize(Document& document)
{
	X = RetrieveValue<std::string>(document["X"]);
	nX = RetrieveValue<std::string>(document["nX"]);
	Y = RetrieveValue<std::string>(document["Y"]);
	nY = RetrieveValue<std::string>(document["nY"]);
	Z = RetrieveValue<std::string>(document["Z"]);
	nZ = RetrieveValue<std::string>(document["nZ"]);

	return true;
}

void CubemapImage::Init(ResourceInitContext const& Context)
{
	_isComplete = true;
	for (auto&& image : _images)
	{
		if (!image || !image->IsComplete())
		{
			_isComplete = false;
		}
		else if (image->GetExtent().x != image->GetExtent().y)
		{
			ELOG(LogResource, "Invalid extent for cubemap image, the width and height components must be equal : %s - %s", image->GetName().c_str(), GetName().c_str());
			_isComplete = false;
		}
		else if (_extents.x == 0)
		{
			_extents = image->GetExtent();
		}
		else if (image->GetExtent() != _extents)
		{ 
			ELOG(LogResource, "Invalid extent for cubemap image, each image extents must be equal : %s - %s", image->GetName().c_str(), GetName().c_str());
			_isComplete = false;
		}

		if (!_isComplete)
			return;
	}
}

void CubemapImage::Shutdown()
{
	for (auto&& image : _images)
		image.Reset();

	_extents = {};
	_isComplete = false;
}

bool	Parser<CubemapImage>::Unserialize(Handle<CubemapImage> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	CubemapImageDesc desc = {};

	if (!UnserializeObject(stream, &desc))
		return false;

    handle->_name = info.path->GetString();
	for (int i = 0; i < 6; i++)
	{
		Path filePath = info.path->GetDirectory().Concat(desc.imageNames[i]);
		handle->_images[i] = info.resourceMgr->Get<Image>(filePath.GetString());
		if (!handle->_images[i])
			return false;
	}

	return true;
}

bool	Parser<CubemapImage>::Serialize(Handle<CubemapImage> handle, ResourceStreamInfo const& info, std::ostream& stream)
{
	CubemapImageDesc desc = {};

	KU_ASSERT(handle);

	for (int i = 0; i < 6; i++)
	{
		if (handle->_images[i])
			desc.imageNames[i] = handle->_images[i]->GetName();
		else
			desc.imageNames[i] = "";
	}

	if (!SerializeObject(stream, &desc))
		return false;

	return true;
}

bool	Parser<CubemapImage>::Reload(Handle<CubemapImage> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	return Unserialize(handle, info, stream);
}