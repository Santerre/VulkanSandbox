#include "Resource/Loader.h"

#include "Debugging/Assert.h"
#include "Platform/GenericPlatform.h"
void ILoader::Shutdown()
{
	CloseRead();
	CloseWrite();
}

DiskFileLoader::DiskFileLoader(Path path, bool bResolve)
:   _path(path),
    _bResolve(bResolve)
{

}

std::istream* DiskFileLoader::OpenRead()
{
	KU_ASSERT(_path.IsValid());

    Path osPath = _bResolve ? Platform::SolveFilePath(_path) : _path;
	_inFile = std::make_unique<std::ifstream>(osPath.GetString(), std::ios::binary);
	if (_inFile->is_open())
		return _inFile.get();

	_inFile = nullptr;
	return nullptr;
}

void	DiskFileLoader::CloseRead()
{
	if (_inFile == nullptr)
		return;

	if (_inFile->is_open())
		_inFile->close();
}

std::ostream* DiskFileLoader::OpenWrite()
{
	Path solvedPath = Platform::SolveFilePath(_path).GetString();
    Platform::CreateDirectory(solvedPath.GetRelativePath());
	_outFile = std::make_unique<std::ofstream>(solvedPath.GetString());
	if (_outFile->is_open())
		return _outFile.get();

	_outFile = nullptr;
	return nullptr;
}

void DiskFileLoader::CloseWrite()
{
	if (_outFile && _outFile->is_open())
		_outFile->close();

	_outFile = nullptr;
}

std::istream*	DummyLoader::OpenRead()
{
	return nullptr;
}

void			DummyLoader::CloseRead()
{
}

std::ostream*	DummyLoader::OpenWrite()
{
	return nullptr;
}

void			DummyLoader::CloseWrite()
{
}
