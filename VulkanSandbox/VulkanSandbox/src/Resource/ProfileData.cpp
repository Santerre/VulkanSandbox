#include "Resource/ProfileData.h"

#include <set>

#include "Resource/ResourceStreamInfo.h"

#include "Debugging/Profiler.h"
#include "Debugging/Assert.h"

void ProfileData::Init(ResourceInitContext const& Context)
{
	_isComplete = true;
}

void ProfileData::Shutdown()
{
	_isComplete = false;
}

void ProfileData::AddDump(Profiler::ProfilerFrameDump const& dump)
{
	_dumps.push_back(dump);
}

bool	Parser<ProfileData>::Unserialize(Handle<ProfileData> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	KU_UNIMPLEMENTED_EXCEPTION();
	return false;
}

bool	Parser<ProfileData>::Reload(Handle<ProfileData> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	KU_UNIMPLEMENTED_EXCEPTION();
	return Unserialize(handle, info, stream);
}

bool	Parser<ProfileData>::Serialize(Handle<ProfileData> handle, ResourceStreamInfo const& info, std::ostream& stream)
{
	std::vector<std::string> orderedColums;
	std::unordered_map<std::string, size_t> columns;
	for (auto&& dump : handle->_dumps)
	{
		for (auto&& [name, counter] : dump.cpuDump.counters)
			if (columns.find(name) == columns.end())
			{
				columns[name] = columns.size();
				orderedColums.push_back("Counter." + name);
			}

		for (auto&& [id, range] : dump.cpuDump.ranges)
			if (columns.find(range.name) == columns.end())
			{
				columns[range.name] = columns.size();
				orderedColums.push_back("Range." + range.name);
			}
	}

	for (auto&& column : orderedColums)
		stream << column << ",";
	stream << std::endl;

	for (auto&& dump : handle->_dumps)
	{
		std::vector<std::string> row(columns.size());
		for (auto&& val : row)
			val = "NaN";

		for (auto&& [name, counter] : dump.cpuDump.counters)
			row[columns.at(name)] = std::to_string(counter.value);

		for (auto&& [id, range] : dump.cpuDump.ranges)
			row[columns.at(range.name)] = std::to_string(range.totalTime);

		for (auto&& value : row)
			stream << value << ",";
		stream << std::endl;
	}
	return true;
}
