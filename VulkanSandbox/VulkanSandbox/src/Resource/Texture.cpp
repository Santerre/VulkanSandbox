#include "Resource/Texture.h"

#include <array>

#include "Core/Path.h"
#include "Resource/ResourceMgr.h"
#include "Resource/ResourceStreamInfo.h"
#include "Resource/Image.h"

void TextureDesc::Serialize(Document& outDocument) const
{
	AddValue<std::string>(outDocument, outDocument, "albedo", albedo);
	AddValue<std::string>(outDocument, outDocument, "heightMap", heightMap);
	AddValue<std::string>(outDocument, outDocument, "normalMap", normalMap);
}

bool		TextureDesc::Unserialize(Document& document)
{
	albedo = RetrieveValue<std::string>(document["albedo"]);
	heightMap = RetrieveValue<std::string>(document["heightMap"]);
	normalMap = RetrieveValue<std::string>(document["normalMap"]);

	return true;
}

void Texture::Init(ResourceInitContext const& Context)
{
	_isComplete = true;
}

void Texture::Shutdown()
{
	_albedo.Reset();
	_heightMap.Reset();
	_normalMap.Reset();
}

Handle<Image>	Parser<Texture>::LoadImage(ResourceStreamInfo const& info, std::string const& name)
{
	if (name != "")
	{
		Path filePath = info.path->GetDirectory().Concat(name);
		return info.resourceMgr->Get<Image>(filePath.GetString());
	}
	return {};
}

bool	Parser<Texture>::Unserialize(Handle<Texture> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	TextureDesc desc = {};

	if (!UnserializeObject(stream, &desc))
		return false;

    handle->_name = info.path->GetString();

	handle->_albedo = LoadImage(info, desc.albedo);
	handle->_heightMap = LoadImage(info, desc.heightMap);
	handle->_normalMap = LoadImage(info, desc.normalMap);

	return true;
}

bool	Parser<Texture>::Serialize(Handle<Texture> handle, ResourceStreamInfo const& info, std::ostream& stream)
{
	TextureDesc desc = {};

	KU_ASSERT(handle);

	std::array<Handle<Image>, 3> images = { handle->_albedo, handle->_normalMap, handle->_heightMap };
	std::array<std::string*, 3> imageNames = { &desc.albedo, &desc.normalMap, &desc.heightMap };
	for (size_t i = 0; i < images.size(); i++)
	{
		if (images[i])
			*imageNames[i] = images[i]->GetName();
		else
			*imageNames[i] = "";
	}

	if (!SerializeObject(stream, &desc))
		return false;

	return true;
}

bool	Parser<Texture>::Reload(Handle<Texture> handle, ResourceStreamInfo const& info, std::istream& stream)
{
	return Unserialize(handle, info, stream);
}
