#include "Resource/ResourceMgr.h"

void ResourceMgr::Init(IApplication* application, ResourceLibrary* library) 
{
	_application = application;
    _library = library;
}

void ResourceMgr::ReleaseAllResources()
{
	for (auto&& item : _resources)
		item->Release();

    _library->Clear();
    _resources.clear();
	VLOG(LogResourceMgr, "All resources cleared.");
}

void ResourceMgr::Shutdown()
{
    KU_ASSERT(_resources.size() == 0);
	VLOG(LogResourceMgr, "Shutdown resource manager.");
    if (_resources.size())
	    VLOG(LogResourceMgr, "  !! %u resources weren't destroyed !!", _resources.size());
}

std::shared_ptr<ILoader> ResourceMgr::GetLoaderAtPath( std::string const& pathStr ) const
{
	Path const& path(pathStr);
	Path volume = path.GetVolume();
	if (volume == "D:")
	{
		return std::make_shared<DiskFileLoader>(path);
	}
	else if (volume == "M:")
	{
		return std::make_shared<MemoryLoader>(_application->GetHierarchicalMemory(), path);
	}
	else
	{
		WLOG(LogResourceMgr, "The following asset has a invalid path and couldn't be loaded : %s", pathStr.c_str());
		return std::make_shared<DummyLoader>();
	}
}

std::vector<std::shared_ptr<IResourceWrapper>> ResourceMgr::GetAllResources() const
{
    std::vector<std::shared_ptr<IResourceWrapper>> res;
    res.reserve(_resources.size());

    for (auto resWrapper : _resources)
        res.push_back(resWrapper);

    return res;
}