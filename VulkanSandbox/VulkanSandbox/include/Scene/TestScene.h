#pragma once

#include "Core/Scene.h"

class TestScene : public Scene
{
	virtual bool FromDesc(SceneDesc const& desc) override;
	virtual void Update(float deltaTime) override;
	virtual void Shutdown() override;

    virtual SC::Camera const* GetCullCamera() const override;

private:
	void SetupInput();
	void MoveCameraInput(glm::vec3 direction, float normalizedInput);
	void RotateCameraInputRelative(glm::vec3 axis, float normalizedInput);
	void RotateCameraInputAbsolute(glm::vec3 axis, float normalizedInput);
	void ChangeRenderingMode(int i);

public:
	virtual void UpdatePropsEditor(float deltaTime) override;
private:
	void UpdateTransformEditor(Transform& transform);
	void UpdateLightEditor(SC::Light& light);

private:
    Transform _subdivTransform;

	float _rotationTime = 0;

	glm::vec3 _originalPosition;

	float _camSpeed = log(100.f);
	float _camRotateSpeed = 1.f;

	float _frameDeltaTime;

    std::unique_ptr<SC::Camera> _cullCamera;

// Remote Controls mode
private:
    void ToggleRelativeMove( glm::vec3 direction, float normalizedInput );

private:
	bool _bUseRemoteControls = false;
	bool _bforceTerrainUpdate = false;

    bool _bIsRelativeMoveToggled;
    glm::vec3 _relativeMove;
    float _relativeMoveSpeed = 1;
};