#pragma once

#include "External/Glm.h"

#include "Maths/Transform.h"

namespace SC
{

class Camera
{
public:
	void FromViewportExtents(glm::uvec2 viewportExtents);

	glm::mat4 GetViewProjection() const;

	glm::mat4 GetView() const 
	{ 
		return _zeroPosition * _transform.GetTransformMatrix();
	}

	Transform& GetTransform() { return _transform; }
	Transform const& GetTransform() const { return _transform; }
	glm::mat4 const& GetProjection() const { return _projection; }
	glm::uvec2 const& GetViewportExtents() const { return _extents; }

	void SetProjection(glm::mat4 const& projection) { _projection = projection; }

private:
	Transform _transform;
	glm::mat4 _zeroPosition;
	glm::mat4 _projection;

	glm::uvec2 _extents;
};

}
