#pragma once

#include "External/Glm.h"

#include "Scene/SceneComponent/Light.h"

namespace SC
{
class DirectionalLight : public Light
{
public:
	bool FromDirection(glm::vec3 direction);

	glm::vec3 const& GetDirection() const { return _direction; }
	void SetDirection(glm::vec3 const& direction) { _direction = direction; }

private:
	glm::vec3 _direction;
};

}
