#pragma once

#include "glm/glm.hpp"

#include "Utils/QuadTree.h"

#include "Memory/Handle.h"
#include "Maths/Transform.h"

class Terrain;

class ResourceMgr;
class Texture;
struct SceneContext;

namespace SC
{
	struct TerrainDesc
	{
        SceneContext const* context;
		glm::vec3 extents = { 2000000, 129, 2000000};
	};

	struct TerrainMatUniform
	{
		float normUpValue = 0.285f;
		float slopeUpCos = 0.907f;
		float slopeDownCos = 0.542f;

		float upDownBlend = 0.047f;

		float cliffUpTexBlend = 0.022f;
		float cliffDownTexBlend = 0.017f;

		float tessTargetScreenSize = 10;

		float fogDist = 1000;

		glm::vec2 tiling = glm::vec2(0.1f, 0.1f);
		glm::vec2 heightMapTiling = glm::vec2(0.0005f, 0.0005f);

		glm::vec3 fogColor = { 0.760, 0.838, 1.0 };

        float forceTessLevel = -1;
        float forceTaskTessLevel = -1;
        float forceMeshTessLevel = -1;
    };

	class Terrain
	{
	public:

        enum struct TessellationEvaluationTechnique
        {
            TessellationFactor = 0,
            TargetDistance
        };
        
        struct Leaf
        {
            float neighbourDepth[4];
        };

        struct TerrainQuadTreeDescriptorTrait
        {
            using Trait = QuadTreeTrait<TerrainQuadTreeDescriptorTrait>;
            using NodeData = Leaf;
        };

        struct ITerrainTessEvaluator
        {
            virtual bool EvaluateNode(TerrainQuadTreeDescriptorTrait::Trait::Node const& node) const = 0;
        };

        struct DistanceBasedTerrainEvaluator : public ITerrainTessEvaluator
        {
        public:
            virtual bool EvaluateNode(TerrainQuadTreeDescriptorTrait::Trait::Node const& node) const override;

        public:
            bool UpdateTarget(glm::vec3 newTarget);

            float distanceFactor = 0.004f;
            int maxTessellationFactor = 15;

        private:
            glm::vec3 _target = {};
        };

        struct FactorBasedTerrainEvaluator final : public ITerrainTessEvaluator
        {
        public:
            virtual bool EvaluateNode(TerrainQuadTreeDescriptorTrait::Trait::Node const& node) const override;

        public:
            bool UpdateTessellationFactor(int newFactor);
            int GetTessellationFactor() const { return _tessellationFactor; }

        private:
            int _tessellationFactor = 1;
        };

		struct TerrainQuadTreeDescriptor
		{
            using TreeTrait = TerrainQuadTreeDescriptorTrait::Trait;

        // Interface
        public:
            using NodeData = TerrainQuadTreeDescriptorTrait::NodeData;

            Handle<typename TreeTrait::TreeBlock> AllocateBlock();
            void DestroyBlock(Handle<typename TreeTrait::TreeBlock> block );
            bool EvaluateNode(typename TreeTrait::Node const& node);
            void InitNode(TreeTrait::Node& node);

        // Utils
        public:

            static float GetSize(TreeTrait::Key const& key);
            static float GetSizeAtDepth(uint8_t depth);
            static glm::vec2 Get2DCoords(TerrainQuadTreeDescriptorTrait::Trait::Key const& key);

            void        SetEvaluator(ITerrainTessEvaluator const& evaluator) { _terrainEvaluator = &evaluator; }
            ITerrainTessEvaluator const*  GetEvaluator() { return _terrainEvaluator; }

        private:
            ITerrainTessEvaluator const* _terrainEvaluator;
        };

        Terrain();

		bool FromDesc(TerrainDesc const& desc);
		void Shutdown();

		void Update(SceneContext const& context);

		glm::vec3 const&	GetLocation() const { return _location; }
		glm::uvec3 const&	GetExtents() const { return _extents; }

		void	SetLocation(glm::vec3 location) { _location = location; }
		void	SetExtents(glm::uvec3 extents) { _extents = extents; }

		void	SetGrassTexture(Handle<Texture> texture) { _grassTexture = texture; }
		void	SetSnowTexture(Handle<Texture> texture) { _snowTexture = texture; }
		void	SetCliffTexture(Handle<Texture> texture) { _cliffTexture = texture; }
		void	SetHeightMap(Handle<Texture> texture) { _heightMap = texture; }

		Handle<Texture>		    GetGrassTexture() const { return _grassTexture; }
		Handle<Texture>		    GetSnowTexture() const { return _snowTexture; }
		Handle<Texture>		    GetCliffTexture() const { return _cliffTexture; }
		Handle<Texture>		    GetHeightMap() const { return _heightMap; }

        QuadTree<TerrainQuadTreeDescriptor> const&	GetQuadTree() const { return _quadTree; }

		TerrainMatUniform mat;

        // TODO make it automatic
		void	SetSubdivTarget(Transform const* subdivTransform) { _subdivTargetTransform = subdivTransform; }

        void	ChangeTessellationEvaluationTechnique(TessellationEvaluationTechnique technique);
        TessellationEvaluationTechnique GetTessellationEvaluationTechnique() const { return _tessellationEvaluationTechnique; }

        void SetTessellationFactor(int newFactor);
        int GetTessellationFactor() const { return _factorBasedEvaluator.GetTessellationFactor(); }

        void SetForceTessellationPrecision(float forceTessellationPrecision) { _forceTessellationPrecision = forceTessellationPrecision; }
        float GetForceTessellationPrecision() const { return _forceTessellationPrecision; }

        DistanceBasedTerrainEvaluator& GetDistanceBasedTerrainEvaluator() { return _distanceBasedEvaluator; }

        void NotifyTerrainUpdate();

        // TODO handle updates a better way
        int32_t GetVersionIndex() const { return _versionIndex; }

	private:
		void UpdateTerrainData(SceneContext const& context);

	private:
		glm::vec3		_location;
		glm::uvec3		_extents;
		Transform		_transform;

		Handle<Texture> _grassTexture;
		Handle<Texture> _snowTexture;
		Handle<Texture> _cliffTexture;

		Handle<Texture> _heightMap;

        QuadTree<TerrainQuadTreeDescriptor>  _quadTree;
        
        bool _bForceTreeUpdate = false;
        // TODO handle updates a better way
        int32_t _versionIndex = 0;

        // Tessellation Evaluation
        float                           _forceTessellationPrecision = -1;
        DistanceBasedTerrainEvaluator   _distanceBasedEvaluator;
        FactorBasedTerrainEvaluator     _factorBasedEvaluator;

        TessellationEvaluationTechnique _tessellationEvaluationTechnique = TessellationEvaluationTechnique::TargetDistance;

        //temp
		Transform const* _subdivTargetTransform;
	};
}