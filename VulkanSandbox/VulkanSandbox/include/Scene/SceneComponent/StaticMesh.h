#pragma once

#include "glm/glm.hpp"

#include "Memory/Handle.h"

#include "Maths/Transform.h"

class Mesh;
class Texture;

namespace SC
{

struct StaticMeshDesc
{
	Handle<Mesh> mesh;
	Handle<Texture> texture;
};

class StaticMesh
{
public:
	bool FromDesc(StaticMeshDesc const& desc);
	void Shutdown();

	glm::mat4 GetTransformMat() const { return _transform.GetTransformMatrix(); }
	Transform const& GetTransform() const { return _transform; }
	Transform& GetTransform() { return _transform; }

	Handle<Mesh> GetMesh() const { return _mesh; }
	Handle<Texture> GetTexture() const { return _texture; }

private:
	Handle<Mesh>	_mesh;
	Handle<Texture>	_texture;
	Transform		_transform;
};

}