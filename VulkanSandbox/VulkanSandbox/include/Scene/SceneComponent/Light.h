#pragma once

#include "glm/glm.hpp"

namespace SC
{
class Light
{
public:
	glm::vec3 const& GetColor() const { return _color; }
	void SetColor(glm::vec3 const& color) { _color = color; }

	float GetIntensity() const { return _intensity; }
	void SetIntensity(float intensity) { _intensity = intensity; }

private:
	glm::vec3	_color = glm::vec3(1, 1, 1);
	float		_intensity = 1.f;
};
}