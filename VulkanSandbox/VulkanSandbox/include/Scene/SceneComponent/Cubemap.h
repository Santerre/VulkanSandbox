#pragma once

#include "glm/glm.hpp"

#include "Memory/Handle.h"
#include "Resource/Mesh.h"
#include "Maths/Transform.h"

class CubemapImage;

class ResourceMgr;

namespace SC
{

struct CubemapDesc
{
	ResourceMgr*		resourceMgr = nullptr;
	Handle<CubemapImage>	image;
};

class Cubemap
{
public:
	bool FromDesc(CubemapDesc const& desc);
	void Shutdown();

	Handle<Mesh> GetMesh() const { return _mesh; }
	Handle<CubemapImage> GetImage() const { return _image; }

private:
	Handle<Mesh>			_mesh;
	Handle<CubemapImage>	_image;
};

}