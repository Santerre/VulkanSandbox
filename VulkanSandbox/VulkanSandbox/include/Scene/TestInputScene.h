#pragma once

#include <string>

#include "Core/Scene.h"

class TestInputScene : public Scene
{
	virtual bool FromDesc(SceneDesc const& desc) override;
	virtual void Update(float deltaTime) override;
	virtual void Shutdown() override;

private:
	static void TestOutput(std::string const& str);
	void SetupInput();

private:
	float rotationTime = 0;
};