#pragma once

#include "Input/IRawInputContext.h"
#include "Input/RawInput.h"

#include "Utils/Delegate.h"

#include "GLFW/glfw3.h"

#include <array>

namespace Input
{

namespace Detail
{
	static constexpr std::array<RawInput, GLFW_KEY_LAST + 1> BuildInputConvertMap()
	{
		std::array<RawInput, GLFW_KEY_LAST + 1> temp = {};
		temp[GLFW_KEY_W] = RawInput::K_W;
		temp[GLFW_KEY_S] = RawInput::K_S;
		temp[GLFW_KEY_A] = RawInput::K_A;
		temp[GLFW_KEY_D] = RawInput::K_D;
		temp[GLFW_KEY_Q] = RawInput::K_Q;
		temp[GLFW_KEY_E] = RawInput::K_E;
		temp[GLFW_KEY_UP] = RawInput::K_UP;
		temp[GLFW_KEY_F1] = RawInput::K_F1;
		temp[GLFW_KEY_F2] = RawInput::K_F2;
		temp[GLFW_KEY_F3] = RawInput::K_F3;
		temp[GLFW_KEY_F4] = RawInput::K_F4;
		temp[GLFW_KEY_F5] = RawInput::K_F5;
		temp[GLFW_KEY_DOWN] = RawInput::K_DOWN;
		temp[GLFW_KEY_LEFT] = RawInput::K_LEFT;
		temp[GLFW_KEY_RIGHT] = RawInput::K_RIGHT;
		temp[GLFW_KEY_SPACE] = RawInput::K_SPACE;
		temp[GLFW_KEY_GRAVE_ACCENT] = RawInput::K_TILD;
		temp[GLFW_KEY_J] = RawInput::K_J;
		temp[GLFW_KEY_K] = RawInput::K_K;

		return temp;
	}
}

struct CursorLocation
{
	double x;
	double y;
};

class GLFWRawInputContext : public IRawInputContext
{
public:
	using RetrieveSelfCallback = GLFWRawInputContext*(*)(GLFWwindow*);

	template <RetrieveSelfCallback retrieveSelfFromWindow>
	void FromGLFWWindow(GLFWwindow* window);

	void Shutdown();

	virtual bool PollInputChanges(PollingResults& results) override;

	virtual void SetCursorEnabled(bool isEnabled) override;

	void ClearRecordedEvents();

private:
	void AddInputChange(int glfwInput, int newState);
	void AddInputChange(RawInput glfwInput, int newState);
	void AddInputChange(RawInput input, InputStates newState);
	void AddAxisChange(RawAxis axis, float value);

public:
	using KeyDelegate =			Delegate<void(GLFWwindow*, int, int, int, int)>;
	using CharDelegate =		Delegate<void(GLFWwindow*, unsigned int)>;
	using ScrollDelegate =		Delegate<void(GLFWwindow*, double, double)>;
	using MouseButtonDelegate =	Delegate<void(GLFWwindow*, int, int, int)>;

	KeyDelegate				keyDelegate;
	CharDelegate			charDelegate;
	ScrollDelegate			scrollDelegate;
	MouseButtonDelegate		mouseButtonDelegate;

private:
	template <RetrieveSelfCallback retrieveSelfFromWindow>
	static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	template <RetrieveSelfCallback retrieveSelfFromWindow>
	static void CursorPositionCallback(GLFWwindow* window, double xpos, double ypos);
	template <RetrieveSelfCallback retrieveSelfFromWindow>
	static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
	template <RetrieveSelfCallback retrieveSelfFromWindow>
	static void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
	template <RetrieveSelfCallback retrieveSelfFromWindow>
	static void CharCallback(GLFWwindow* window, unsigned int c);

	// returns the mapped raw input, NONE if the glfwInput isn't mapped
	static constexpr RawInput GLFWKeyToRawInput(int glfwInput) 
	{
		return inputToGLFWMap[glfwInput];
	}

private:
	static constexpr std::array<RawInput, GLFW_KEY_LAST + 1> inputToGLFWMap = Detail::BuildInputConvertMap();

private:
	GLFWwindow*		_window = nullptr;

	RawChangeList	_changeList;
	CursorLocation	_cursorLocation = {};
	CursorLocation	_cursorDelta = {};

	static constexpr  float	_mouseUpNormSpeed = 1000;
	static constexpr  float	_mouseRightNormSpeed = 1000;

	static constexpr  float	_scrollUpNormSpeed = 2;
	static constexpr  float	_scrollRightNormSpeed = 2;
};


template <GLFWRawInputContext::RetrieveSelfCallback retrieveSelfFromWindow>
void GLFWRawInputContext::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	GLFWRawInputContext* me = retrieveSelfFromWindow(window);
	me->keyDelegate.Invoke(window, key, scancode, action, mods);

	if (key >= 0)
		me->AddInputChange(key, action);
}

template <GLFWRawInputContext::RetrieveSelfCallback retrieveSelfFromWindow>
void GLFWRawInputContext::CursorPositionCallback(GLFWwindow* window, double xpos, double ypos)
{
	GLFWRawInputContext* me = retrieveSelfFromWindow(window);
	me->_cursorDelta = { xpos - me->_cursorLocation.x,  ypos - me->_cursorLocation.y };
	me->_cursorLocation = { xpos, ypos };

	if (me->_cursorDelta.x != 0)
		me->AddAxisChange(RawAxis::MOUSE_RIGHT, static_cast<float>(me->_cursorDelta.x / me->_mouseRightNormSpeed));

	if (me->_cursorDelta.y != 0)
		me->AddAxisChange(RawAxis::MOUSE_UP, static_cast<float>(-me->_cursorDelta.y / me->_mouseUpNormSpeed));
}

template <GLFWRawInputContext::RetrieveSelfCallback retrieveSelfFromWindow>
void GLFWRawInputContext::MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	GLFWRawInputContext* me = retrieveSelfFromWindow(window);
	me->mouseButtonDelegate.Invoke(window, button, action, mods);

	if (button == GLFW_MOUSE_BUTTON_RIGHT)
		me->AddInputChange(RawInput::MOUSE_BUTTON_RIGHT, action);
	else if(button == GLFW_MOUSE_BUTTON_LEFT)
		me->AddInputChange(RawInput::MOUSE_BUTTON_LEFT, action);
	else if (button == GLFW_MOUSE_BUTTON_MIDDLE)
		me->AddInputChange(RawInput::MOUSE_BUTTON_MIDDLE, action);
}

template <GLFWRawInputContext::RetrieveSelfCallback retrieveSelfFromWindow>
void GLFWRawInputContext::ScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
	GLFWRawInputContext* me = retrieveSelfFromWindow(window);
	me->scrollDelegate.Invoke(window, xoffset, yoffset);

	if (xoffset)
		me->AddAxisChange(RawAxis::MOUSE_SCROLL_RIGHT, static_cast<float>(xoffset) / me->_scrollRightNormSpeed);
	if (yoffset)
		me->AddAxisChange(RawAxis::MOUSE_SCROLL_UP, static_cast<float>(yoffset) / me->_scrollUpNormSpeed);
}

template <GLFWRawInputContext::RetrieveSelfCallback retrieveSelfFromWindow>
void GLFWRawInputContext::CharCallback(GLFWwindow* window, unsigned int c)
{
	GLFWRawInputContext* me = retrieveSelfFromWindow(window);
	me->charDelegate.Invoke(window, c);
}

template <GLFWRawInputContext::RetrieveSelfCallback retrieveSelfFromWindow>
void GLFWRawInputContext::FromGLFWWindow(GLFWwindow* window)
{
	_window = window;

	glfwSetKeyCallback(_window, GLFWRawInputContext::KeyCallback<retrieveSelfFromWindow>);
	glfwSetCursorPosCallback(_window, GLFWRawInputContext::CursorPositionCallback<retrieveSelfFromWindow>);
	glfwSetMouseButtonCallback(_window, GLFWRawInputContext::MouseButtonCallback<retrieveSelfFromWindow>);
	glfwSetScrollCallback(_window, GLFWRawInputContext::ScrollCallback<retrieveSelfFromWindow>);
	glfwSetCharCallback(_window, GLFWRawInputContext::CharCallback<retrieveSelfFromWindow>);

	glfwGetCursorPos(window, &_cursorLocation.x, &_cursorLocation.y);
}

}
