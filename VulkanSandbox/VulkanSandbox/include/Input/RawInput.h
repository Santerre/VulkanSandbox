#pragma once

#include <cinttypes>
#include <map>

#include "InputStates.h"

namespace Input
{

enum struct RawInput : uint16_t
{
	NONE = 0u
	, K_W
	, K_S
	, K_A
	, K_D
	, K_Q
	, K_J
	, K_K
	, K_E
	, K_UP
	, K_DOWN
	, K_LEFT
	, K_RIGHT
	, K_SPACE
	, K_TILD
	, K_F1
	, K_F2
	, K_F3
	, K_F4
	, K_F5
	, MOUSE_BUTTON_LEFT
	, MOUSE_BUTTON_MIDDLE
	, MOUSE_BUTTON_RIGHT
	, MAX
};

enum struct RawAxis : uint16_t
{
	  NONE = 0u
	, MOUSE_UP
	, MOUSE_RIGHT
	, MOUSE_SCROLL_RIGHT
	, MOUSE_SCROLL_UP
	, MAX
};

struct RawInputChange
{
	InputStates value;
};

struct RawAxisChange
{
	float value = 0.f;
};

struct RawChangeList
{
	std::map<RawInput, RawInputChange>	modifiedInputs;
	std::map<RawAxis, RawAxisChange>	modifiedAxis;
};

}