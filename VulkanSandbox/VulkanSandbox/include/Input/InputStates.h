#pragma once

enum struct InputStates
{
	NONE = 0,
	PRESSED,
	RELEASED,
	MAX
};