#pragma once

#include <map>
#include <vector>
#include <string>

#include "Utils/Delegate.h"

#include "Input/RawInput.h"

namespace Input
{

using InputId = std::string;

struct InputAction
{
	void Trigger();

	Delegate<void(void)>	onAction;
};

struct ActionRawInterface
{
public:
	ActionRawInterface(InputId _id, InputStates _triggerState = InputStates::RELEASED) 
		: inputId(_id), triggerState(_triggerState) {}

	bool ShouldTriggerAction(RawInputChange const& change) const;

public:
	InputId inputId;

	InputStates triggerState;
};

struct InputState
{
public:
	void Update();
	void UpdateValue(std::vector<InputStates> const& newValues);

	InputStates GetState() const { return _value; }

	Delegate<void()>			whilePressed;

	Delegate<void()>			onEnterState;
	Delegate<void()>			onExitState;
	Delegate<void(InputStates)>	onStateChange;

private:
	void SetValue(InputStates newValue);

	InputStates _value = InputStates::RELEASED;

};

struct StateRawInterface
{
public:
	InputStates ConvertStateChange(RawInputChange const& change) const;

public:
	InputId inputId;
};

struct InputRange
{
	void Update();
	void UpdateValue(std::vector<float> const& newValues);

	float GetRange() const { return _value; }

	Delegate<void(float)>	whileNotZero;

	Delegate<void(float)>	onValueChange;

private:
	float _value = 0.f;
};

struct RangeRawInputInterface
{
public:
	float ConvertRangeChange(RawInputChange const& change) const;

public:
	InputId inputId;

	float	rangeModification;
};

struct RangeRawAxisInterface
{
public:
	float ConvertRangeChange(RawAxisChange const& change) const;

public:
	InputId inputId;

	float	sensibility;
};

struct ActionInterfaceDesc
{
	RawInput input;
	InputStates triggerState = InputStates::RELEASED;
};

struct StateInterfaceDesc
{
	RawInput input;
};

struct InputRangeInterfaceDesc
{
	RawInput input;
	float rangeModification = 1.f;
};

struct AxisRangeInterfaceDesc
{
	RawAxis input;
	float sensibility = 1.f;
};

class InputContext
{
public:
	InputContext(std::string const& name, int32_t priority);

	void Update(RawChangeList& changeContext);

	std::string const&	GetName() { return _name; }
	int32_t				GetPriority() { return _priority; }

	InputAction&		GetAction(std::string const& name);
	InputAction const&	GetAction(std::string const& name) const;

	InputState&			GetState(std::string const& name);
	InputState const&	GetState(std::string const& name) const;

	InputRange&			GetRange(std::string const& name);
	InputRange const&	GetRange(std::string const& name) const;

	InputAction&  AddAction(std::string const& name, std::vector<ActionInterfaceDesc> const& rawInputInterface);
	InputState& AddState(std::string const& name, std::vector<StateInterfaceDesc> const& rawInputInterface);
	
	// Add a range with all the associated input and axis. 
	// Each input and axis has a float value to sepecifie the sensitivity/delta of respectively the input/axis.
	InputRange& AddRange(std::string const& name, std::vector<InputRangeInterfaceDesc> const& rawInputInterface, std::vector<AxisRangeInterfaceDesc> const& rawAxisInterface);

	bool operator<(InputContext const& other);

	void SetActive(bool isActive);
	bool IsActive() const { return _isActive; }

	void SetBlockInputs(bool shouldBlockInputs) { _shouldBlockInputs = shouldBlockInputs; }
	bool ShouldBlockInputs() const { return _shouldBlockInputs; }

private:
	bool _shouldBlockInputs = false;
	bool _isActive = true;

	std::string						_name;
	int32_t							_priority = 0;

	std::map<InputId, InputAction>	_actions;
	std::map<InputId, InputState>	_states;
	std::map<InputId, InputRange>	_ranges;

	std::multimap<RawInput,	ActionRawInterface>		_actionInterfaces;
	std::multimap<RawInput,	StateRawInterface>		_stateInterfaces;
	std::multimap<RawInput,	RangeRawInputInterface>	_rangeInputInterfaces;
	std::multimap<RawAxis,	RangeRawAxisInterface>	_rangeAxisInterfaces;
};

} // namespace Input
