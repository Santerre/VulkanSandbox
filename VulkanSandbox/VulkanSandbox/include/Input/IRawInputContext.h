#pragma once

#include "Input/RawInput.h"

#include <map>

namespace Input
{

struct PollingResults
{
	RawChangeList changeList;
};

class IRawInputContext
{
public:
	virtual bool PollInputChanges(PollingResults& results) = 0;

	virtual void SetCursorEnabled(bool isEnabled) = 0;
};

} //Input