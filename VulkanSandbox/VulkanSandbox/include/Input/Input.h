#pragma once

#include <map>

#include "Input/InputContext.h"
#include "Input/IRawInputContext.h"

#include "Utils/TwoWayMap.h"

namespace Input
{

class InputManager
{
public:
	void FromRawInputContext(IRawInputContext* context);
	void CreateContext(InputContext&& context);

	InputContext& GetContext(std::string const& name) { return _contextMap.at(name); }

	void Shutdown() {}

	bool PollEvents();

	InputAction	const&	GetAction(std::string const& context, std::string actionName) const;
	InputState	const&	GetState(std::string const& context, std::string stateName) const;
	InputRange	const&	GetRange(std::string const& context, std::string rangeName) const;

	InputAction&	GetAction(std::string const& context, std::string actionName);
	InputState&		GetState(std::string const& context, std::string stateName);
	InputRange&		GetRange(std::string const& context, std::string rangeName);

private:
	TwoWayMap<std::string, int32_t, InputContext>  _contextMap;

	IRawInputContext* _rawContext = nullptr;
};

} // namespace Input
