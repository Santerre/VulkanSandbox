#include "Core/IApplication.h"

#include <memory>
#include <set>
#include <vector>
#include <list>
#include <queue>

#include "Core/Window.h"
#include "Core/MeshData.h"
#include "Core/Scene.h"
#include "Core/RenderSystem.h"
#include "Core/ShaderCompilationContext.h"

#include "Memory/HierarchicalMemory.h"

#include "Resource/ResourceMgr.h"
#include "Core/Time.h"

#include "RHI/ShaderCompiler.h"

#include "RHI/Vulkan/VKRHI_Device.h"
#include "RHI/Vulkan/VKRHI_SwapChain.h"
#include "RHI/Vulkan/VKRHI_PhysicalDevice.h"
#include "RHI/Vulkan/VKRHI_LogCategory.h"
#include "RHI/Vulkan/VKRHI_Semaphore.h"
#include "RHI/Vulkan/VKRHI_Frame.h"
#include "RHI/Vulkan/VKRHI_StaticMeshRenderObject.h"
#include "RHI/Vulkan/VKRHI_TerrainRenderObject.h"
#include "RHI/Vulkan/VKRHI_CubemapRenderObject.h"
#include "RHI/Vulkan/VKRHI_MemoryObject.h"
#include "RHI/Vulkan/VKRHI_GraphicalMemoryAllocator.h"
#include "RHI/Vulkan/VKRHI_AvailableDescSetLayouts.h"
#include "RHI/Vulkan/VKRHI_RenderPass.h"
#include "RHI/Vulkan/VKRHI_TransferQueue.h"

#include "Input/Input.h"
#include "Input/GLFW/GLFWRawInputContext.h"

#include "Debugging/Profiler.h"

#include "Editor/ResourceEditor.h"
#include "Editor/ProfilerUI.h"


class HelloTriangleApplication : public IApplication
{
public:
	virtual bool Init() override;
	virtual void Update() override;
	virtual void Shutdown() override;

	virtual bool ShouldShutdown() const override;

	virtual const char* GetApplicationName() const override { return "Software GPU tessellation demo"; }
	virtual VKRHI_Device& GetRenderingContext() override { return _logicalDevice; }
	virtual class HierarchicalMemory& GetHierarchicalMemory() { return _hierarchicalMemory; }

	void SetShouldShutdown(bool value);

private:
	bool InitVulkan();
	bool CreateScene();
	void RequestExtensions(std::vector<const char*>& layers) const;
	void RequestLayers(std::vector<const char*>& extensions) const;
	bool CreateSurface();
	bool PickPhysicalDevice();
	bool CreateLogicalDevice();
	bool CreateCommandPool();
	bool CreateSwapChain();
	bool CreateDescriptorPool();
	bool CreateDepthResources();
	bool CreateTextureSamplers();
	bool CreateRenderPass();
	bool RegisterSceneForRendering();
	bool CreateGraphicsPipeline();
    void FillFrameDescriptor(VKRHI_FrameDescriptor& descriptor);
	bool CreateFrames();

    bool InitShaderCompilation();
	bool SetDeviceCompilationMacros(VKRHI_Device const& device);

	void DestroyTextureSamplers();
    void DestroyPipelines();

	void CreateDynamicRenderingObjects();
	void DestroyDynamicRenderingObjects();

    void RefreshSceneRendering();

	bool InitInput();
	void ToggleEditor();

	void SimulationUpdate(float deltaTime);

	VKRHI_Frame& GetFrame(size_t frameID) { return _frames[frameID]; }
    void ResolveFrame(size_t frameID);
    void DrawFrame(size_t frameID, VKRHI_Semaphore const& backbufferDrawableSemaphore);

	void FlushRendering();
	void WaitRenderDevice();

	bool FindSupportedFormats(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features, VkFormat& outFormat) const;
	bool FindSupportedDepthFormats(VkFormat& outFormat) const;
	bool HasStencilComponent(VkFormat format) const;

    size_t GetFrameCount() { return _frames.size(); }

	// Returns the maximum number of frames that can be aquired
	size_t GetMaxAquiredFrames() { return _swapChain.GetSurfaceImageCountMinMax().r; }

	std::vector<const char*> DetermineLayers();

	std::vector<const char*> const& GetRequiredValidationLayers() const;

    Handle<SPVShader> LoadShader( std::string const& shaderPath );
	std::vector<VkPipelineShaderStageCreateInfo> LoadShaders(std::vector<std::string> const& shaderPaths);

	ResourceMgr							_resourceMgr;
	ResourceLibrary						_resourceLibrary;
	HierarchicalMemory					_hierarchicalMemory;

	RenderSystem						_renderSystem;
										
	ShaderCompilationContext			_baseShaderCompilationContext;
	ShaderCompilationContext			_shaderCompilationContext;
	ShaderCompiler						_shaderCompiler;

	VkInstance							_vkInstance;
	VKRHI_PhysicalDevice*				_mainPhysicalDevice = nullptr;
	std::vector<VKRHI_PhysicalDevice>	_physicalDevices;

	VKRHI_Device						_logicalDevice;
	VKRHI_SwapChain						_swapChain;
	VkSurfaceKHR						_surface;

	VKRHI_RenderPass					_opaqueRenderPass;

	VkPipeline							_opaquePipeline;
	VkPipeline							_terrainPipeline;
    VkPipeline							_meshShaderTerrainPipeline;
    VkPipeline							_cubemapPipeline;

	VkCommandPool						_commandPool;
	VkDescriptorPool					_descriptorPool;

	VKRHI_GraphicalMemoryAllocator		_graphMemAllocator;
	
    VKRHI_TransferQueue					_transferQueue;

    std::queue<size_t>        			_inFlightFrames;
	VKRHI_Semaphore						_backbufferDrawableSemaphore;
	std::vector<VKRHI_Frame>			_frames;
	VKRHI_ImageMemory					_depthBufferMem;
	VKRHI_Image							_depthBuffer;

	bool _isShutdownPending = false;
	std::unique_ptr<class Window>		_window;

	std::vector<const char*>			_requiredExtensions;
	std::vector<const char*>			_requiredLayers;

	std::vector<const char*>			_instanceLayers;

	bool								_enableDebugLayer = false;

	std::vector<const char*>			_requiredDeviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
    std::vector<const char*>			_optionalDeviceExtensions = { VK_NV_MESH_SHADER_EXTENSION_NAME };

private:
	static void DestroyDebugUtilsMessengerEXT(
		VkInstance instance, 
		VkDebugUtilsMessengerEXT callback, 
		const VkAllocationCallbacks* pAllocator);

	static VkResult CreateDebugUtilsMessengerEXT(
		VkInstance instance, 
		const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, 
		const VkAllocationCallbacks* pAllocator, 
		VkDebugUtilsMessengerEXT* pCallback);

	static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData);

	void SetupDebugCallback();

private:
	VkDebugUtilsMessengerEXT _debugCallback;

// Rendering resources
private:
	bool RegisterCubemapForRendering(SC::Cubemap const* mesh);
	bool RegisterTerrainForRendering(SC::Terrain const* mesh);
	bool RegisterStaticMeshForRendering(SC::StaticMesh const* mesh);

	bool RegisterMesh(Handle<Mesh> mesh, VKRHI_MeshRenderObjectDesc& inOutDesc);
	void PrepareBuffer(VKRHI_Buffer& buffer, size_t bufferSize, VkBufferUsageFlags flags);

	void GetDefaultImageDesc(glm::uvec2 extents, ImageMemoryDesc& memDesc, ImageDesc& imageDesc);
	bool RegisterTexture(Handle<Texture> texture, TextureKeys& inOutDesc, VkSampler = VK_NULL_HANDLE);
	bool RegisterImage(Handle<class Image> image, ImageKey& inOutDesc, VkSampler sampler = VK_NULL_HANDLE);
	bool RegisterCubemapImage(Handle<CubemapImage> image, ImageKey& inOutDesc);

	bool CreateDescriptorSetLayouts();

	void ClearMeshData();

	IKey<std::vector<VKRHI_Buffer>> RegisterMeshBuffer(std::vector<VKRHI_Buffer>& bufferList, VkBufferUsageFlags flags, void const* data, size_t dataSize);

	VKRHI_AvailableDescSetLayouts		_descSetLayouts;
	VKRHI_AvailablePipelineLayouts      _availablePipelineLayouts;

	std::vector<VKRHI_StaticMeshRenderObject>	_meshesRO;
	std::optional<VKRHI_TerrainRenderObject>	_terrainRO;
	std::optional<VKRHI_CubemapRenderObject>	_cubemapRO;

	std::vector<VKRHI_Buffer>			_vertexBuffers;
	std::vector<VKRHI_Buffer>			_indexBuffers;

	std::vector<VKRHI_Image>			_images;
	std::list<VKRHI_ImageMemory>		_imageMemBuffers;

	VkSampler							_defaultSampler;
	VkSampler							_cubemapSampler;
	VkSampler							_terrainHeightmapSampler;

// Simulation
private:
	Input::InputManager			_inputMgr;
	Input::GLFWRawInputContext	_rawInputContext;

	std::unique_ptr<Scene>		_scene;

	Time						_time;
	TimeStats					_timeStats;
	static constexpr size_t		_statRange = 100;

// Editor
private:
	bool InitImGUI();
	bool InitImGUIDynamic();
	void ShutdownImGUI();
	void ShutdownImGUIDynamic();

	void EditorUpdate(float deltaTime);

private:
	bool				_isInEditMode = false;

	VKRHI_RenderPass	_editorRenderPass;
#ifdef INCLUDE_EDITOR
private:
	bool _showGuiOutOfEditor = false;

	VkDescriptorPool	_imGUIDescPool;

    bool                _showResEditor;
	ResourceEditor      _resourceEditor;

	ProfilerUI			_profilerUI;
#endif // INCLUDE_EDITOR

	bool				_requestFrameRefresh = false;
	bool				_bProfilerRecording = false;
	bool				_bProfilerUIRequestStartProfiling = false;
	bool				_bProfilerUIRequestEndProfiling = false;

private:
	Handle<ProfileData> _recordingProfileData;
// Rendering
private:
	// Need pipeline/sceneRO update
    bool _useMeshShading = false;
    bool _forceWireframe = false;
    bool _terrainDebugDisplay = false;
    bool _forceSolidClear = false;

	// Need swapchain update
	int  _maxBackBufferCount = 3;

	// Need no update
	int  _maxFramesInFlight = -1;
	bool _bImmediatlyWaitRender = false;
	int  _targetFrameRate = 60;

    glm::vec3 _solidClearColor = glm::vec3(0.0, 0.0, 0.0);

    std::vector<std::shared_ptr<IResourceWrapper>> _addedResources;
};
