#pragma once

#include <string>
#include <filesystem>

class Path
{
public:
	Path() = default;
	Path(std::string const& pathStr);
	Path(std::filesystem::path const& path);
	Path(Path const& context, Path const& origin);

	bool operator==(Path const& other) const;
	bool operator==(std::string const& str) const;

	void FromPathStr(std::string const& pathStr);

	Path GetVolume() const;

	Path GetDirectory() const;
	Path GetFullRelativePath() const;
	Path GetRelativePath() const;
	Path GetFilename() const;
	Path GetExtension() const;

	Path GetExtendedName() const;

    Path ChangeExtension(std::string newExtension) const;

	std::string GetString() const;

	bool				IsValid() const { return _isValid; }

	Path Concat(Path const& b) const;

	static Path GetRelativePath(Path const& context, Path const& origin);

	std::filesystem::path const& GetStdPath() const { return _path; }

private:
	std::filesystem::path _path;

	bool		_isValid = false;
};
