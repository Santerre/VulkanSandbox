#pragma once

#include <memory>

#include "Scene/SceneComponent/Camera.h"
#include "Scene/SceneComponent/StaticMesh.h"
#include "Scene/SceneComponent/Cubemap.h"
#include "Scene/SceneComponent/Terrain.h"
#include "Scene/SceneComponent/DirectionalLight.h"
#include "Scene/SceneComponent/Light.h"
#include "Scene/SceneComponent/DirectionalLight.h"
#include "Scene/SceneComponent/AmbientLight.h"

namespace Input
{
	class InputManager;
}

class RenderSystem;

struct SceneContext
{
	// TODO - remove rendersystem from here
	RenderSystem const*  	renderSystem;	   
	class ResourceMgr*		resourceMgr;
	Input::InputManager*	inputManager;

	class Window*				window;
};

struct SceneDesc
{
	SceneContext context;
};

struct RenderingProperties
{
	bool forceWireframe = false;
};

class Scene
{
public:
	virtual bool FromDesc(SceneDesc const& desc);
	virtual void UpdatePropsEditor(float deltaTime) {}
	virtual void Update(float deltaTime) {}
	virtual void Shutdown() {}

	SC::Camera* GetCamera() { return _camera.get(); }
	SC::StaticMesh* GetStaticMesh() { return _staticMesh.get(); }

	SC::Camera const* GetCamera() const { return _camera.get(); }
    virtual SC::Camera const* GetCullCamera() const { return GetCamera(); }
    SC::StaticMesh const* GetStaticMesh() const { return _staticMesh.get(); }

	SC::Terrain const* GetTerrain() const { return _terrain.get(); }
	SC::Cubemap const* GetCubemap() const { return _cubemap.get(); }

	SC::AmbientLight const* GetAmbientLight() const { return _ambientLight.get(); }
	SC::DirectionalLight const* GetDirectionalLight() const { return _directionalLight.get(); }


	RenderingProperties const& GetRenderingProperties() const { return _renderingProperties; }

	size_t GetStaticMeshsCount() const { return _staticMesh ? 1 : 0; }

protected:
	std::unique_ptr<SC::Camera>				_camera;

	std::unique_ptr<SC::StaticMesh>			_staticMesh;
	std::unique_ptr<SC::Cubemap>			_cubemap;
	std::unique_ptr<SC::Terrain>			_terrain;

	std::unique_ptr<SC::DirectionalLight>	_directionalLight;
	std::unique_ptr<SC::AmbientLight>		_ambientLight;

	RenderingProperties				_renderingProperties;

protected:
	SceneContext _context;
};