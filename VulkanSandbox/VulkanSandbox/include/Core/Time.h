#pragma once

#include <chrono>

#include <array>
#include <algorithm>

#include "Utils/IteratorUtils.h"

class Clock
{
public:
	using TimePoint = std::chrono::time_point<std::chrono::high_resolution_clock>;
	void Start();
	void Tick();

	float		GetFromStart() const	{ return DeltaInSeconds(_start, _lastTick); }
	TimePoint	GetLastTick() const		{ return _lastTick; }
	float		GetLastDelta() const	{ return _lastDelta; }
	TimePoint	GetCurrentTime() const;

private:
	float DeltaInSeconds(TimePoint const& a, TimePoint const& b) const;

	TimePoint _start;
	TimePoint _lastTick;
	float _lastDelta;
};

class Time
{
public:
	void	Init();
	void	Update();
	void	Shutdown();

	float	GetDeltaTime() const;
	void	WaitFrameTime(std::chrono::microseconds frame_time) const;

public:
	constexpr static int frameTimeHistorySize = 500;

	std::array<float, frameTimeHistorySize> GetTimeHistory() const;

private:
	Clock _mainClock;

	std::array<float, frameTimeHistorySize> _frameTimeHistory;
    LoopIterator<decltype(_frameTimeHistory)> _historyIndex;
};

class TimeStats
{
public:
	template<typename It>
	void Update(It begin, It end);

	float GetMin() const { return _minTime; }
	float GetMax() const { return _maxTime; };
	float GetMean() const { return _meanTime;};
	float GetMedian() const { return _medianTime; };

private:
	float ComputeMin() const;
	float ComputeMax() const;
	float ComputeMean() const;
	float ComputeMedian() const;

private:
	std::vector<float> _sortedTimes;

	float _meanTime	  = 0.f;
	float _minTime	  = 0.f;
	float _maxTime	  = 0.f;
	float _medianTime = 0.f;
};


template<typename It>
void TimeStats::Update(It begin, It end)
{
	_sortedTimes.assign(begin, end);
	std::sort(_sortedTimes.begin(), _sortedTimes.end());

	_minTime = ComputeMin();
	_maxTime = ComputeMax();
	_meanTime = ComputeMean();
	_medianTime = ComputeMedian();
}
