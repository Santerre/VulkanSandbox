#pragma once

#include <vector>
#include <map>
#include <string>

class ShaderCompilationContext
{
public:
    std::map<std::string, std::string> const& GetStringGlobalMacros() const { return stringGlobalMacros; }
    std::map<std::string, int> const& GetIntGlobalMacros() const { return intGlobalMacros; }
    std::map<std::string, float> const& GetFloatGlobalMacros() const { return floatGlobalMacros; }

    void SetGlobalMacro(std::string const& name, std::string const& value);
    void SetGlobalMacro(std::string const& name, uint32_t value);
    void SetGlobalMacro(std::string const& name, int value);
    void SetGlobalMacro(std::string const& name, float value);
    
private:
    std::map<std::string, std::string>  stringGlobalMacros;
    std::map<std::string, int>          intGlobalMacros;
    std::map<std::string, float>        floatGlobalMacros;
};