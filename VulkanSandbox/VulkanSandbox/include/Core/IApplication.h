#pragma once

// Todo - Vulkan specific
#include "vulkan/vulkan.h"

#include "Core/FrameRenderContext.h"

class IApplication
{
public:
	virtual bool Init() = 0;
	virtual void Update() = 0;
	virtual void Shutdown() = 0;

	virtual bool ShouldShutdown() const = 0;

	virtual const char* GetApplicationName() const = 0;

	// Todo - Vulkan specific
	virtual class VKRHI_Device& GetRenderingContext() = 0;

	virtual class HierarchicalMemory& GetHierarchicalMemory() = 0;
};