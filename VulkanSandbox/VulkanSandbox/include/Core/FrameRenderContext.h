#pragma once

#include <vector>
#include <list>

#include "vulkan/vulkan.h"

#include "RHI/Vulkan/VKRHI_MemoryObject.h"
#include "RHI/Vulkan/VKRHI_AvailableDescSetLayouts.h"
#include "RHI/Vulkan/VKRHI_CommandBuffer.h"

class VKRHI_RenderPass;
class VKRHI_StaticMeshRenderObject;
class VKRHI_TerrainRenderObject;
class VKRHI_CubemapRenderObject;

struct VKRHI_FrameRenderingOptions
{
    bool useMeshShading = false;

    // TODO - move that in the terrain
    uint32_t subPatchTessellation = 1;
};

// Todo - Remove : Vulkan specific
struct VKRHI_FrameDescriptor
{
public:
    VkCommandPool									commandPool;
    VkDescriptorPool								descriptorPool;
    VKRHI_RenderPass*								opaqueRenderPass;
    VKRHI_RenderPass*								editorRenderPass;

    class VKRHI_SwapChain*							swapChain = nullptr;
    uint32_t										imageIndex;
    class VKRHI_Image*								depthBuffer = nullptr;

    VkPipeline										opaquePipeline;
    VkPipeline										terrainPipeline;
    VkPipeline										meshShaderTerrainPipeline;
    VkPipeline										cubemapPipeline;
    VKRHI_AvailablePipelineLayouts					availablePipelineLayouts;

    class VKRHI_GraphicalMemoryAllocator*			graphMemAllocator;

    VKRHI_AvailableDescSetLayouts*					descSetLayouts;

    // Scene data
public:
    class Scene const*									scene;

    std::vector<VKRHI_StaticMeshRenderObject> const*	meshesRO;
    VKRHI_TerrainRenderObject const*					terrainRO;
    VKRHI_CubemapRenderObject const*					cubemapRO;
    std::vector<class VKRHI_Buffer> const*				vertexBuffers;
    std::vector<class VKRHI_Buffer> const*				indexBuffers;

    std::vector<class VKRHI_Image> const*				images;
    std::list<class VKRHI_ImageMemory> const*			imageMemBuffers;

    // Frame type
public:
    VKRHI_FrameRenderingOptions                         renderingOptions;
};

struct FrameRenderContext
{
    // Todo - Remove : Vulkan specific
    VKRHI_FrameDescriptor                   frameDescriptor;

    class VKRHI_TransferQueue*              transferQueue = nullptr;
    const class VKRHI_Semaphore*            backbufferDrawableSemaphore = nullptr;
	bool					                shouldDrawEditor = false;
};