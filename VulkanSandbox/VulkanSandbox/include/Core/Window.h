#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "glm/vec2.hpp"

class Window
{
public:
	Window(const char* windowName);

	void Open();
	void Close();

	bool ShouldClose() const;

	GLFWwindow* GetGLFWWindow() const { return _window; }

	glm::uvec2 const& GetExtents() { return _extents; }

	void SetWindowUserPointer(void* userPointer);
	static void* GetWindowUserPointer(GLFWwindow* window);

private:
	glm::uvec2	_extents = { 1440u, 900u };
	
	const char* _windowName;

	GLFWwindow* _window;
};

