#pragma once

#include <cstdint>

// TODO : Temporary mockup, move the rendering here
class RenderSystem
{
public:
    // TODO : Temporary mockup
    struct RenderingCapabilities
    {
        uint32_t maxTessellation;
    };

public:
    void UpdateRenderingCapabilities(RenderingCapabilities const& renderingCapabilities) { _renderingCapabilities = renderingCapabilities; }

public:
    RenderingCapabilities const& GetRenderingCapabilities() const { return _renderingCapabilities; }

private:
    RenderingCapabilities _renderingCapabilities;
};