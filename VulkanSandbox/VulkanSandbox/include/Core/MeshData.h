#pragma once

#include "glm/glm.hpp"

#include <vector>

struct Vertex
{
	glm::vec3 position;
	glm::vec3 color;
	glm::vec2 uv;
};

using MeshIndex = uint32_t;

struct MeshData
{
	std::vector<Vertex> vertices;
	std::vector<MeshIndex> indices;

	size_t GetVertsDataByteSize() const;
	size_t GetIndicesDataByteSize() const;
};