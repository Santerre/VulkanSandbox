#pragma once

#include <vector>
#include "glm/vec2.hpp"
#include "vulkan/vulkan.h"

#include "Memory/Handle.h"

#include "RHI/Vulkan/VKRHI_PhysicalDevice.h"
#include "RHI/Vulkan/VKRHI_Device.h"

#include "RHI/Vulkan/VKRHI_Image.h"

class VKRHI_SwapChain
{
public:
	struct InitDesc
	{
		VkSurfaceKHR surface;
		uint32_t     desiredImageCount = 3;
		glm::uvec2   windowExtents;
	};

public:
	bool					FromDesc(VKRHI_Device& device, InitDesc const& desc);
	void					Shutdown();


	VKRHI_Device*			GetDevice() { return _device; };
	VKRHI_Device const*		GetDevice() const { return _device; };

	glm::uvec2 const&		GetSurfaceExtents() const { return _imageExtents; }
	VkSurfaceFormatKHR		GetSurfaceFormat() const { return _surfaceFormat; }
	glm::uvec2				GetSurfaceImageCountMinMax() const;
	VkSwapchainKHR const&	GetVkSwapChain() const { return _swapChain; }

	bool					AquireNextImage(class VKRHI_Semaphore const& semaphore, uint32_t& outFrameID);

	std::vector<Handle<VKRHI_Image>> const& GetImageViews() const { return _swapChainImageViews; }

private:
	VkSurfaceFormatKHR		ChooseSwapSurfaceFormat(std::vector<VkSurfaceFormatKHR> const& supportedFormats) const;
	VkPresentModeKHR		ChooseSwapPresentMode(std::vector<VkPresentModeKHR> const& supportedPresentMode) const;
	glm::uvec2				ChooseSwapExtent(VkSurfaceCapabilitiesKHR const& surfaceCapabilities, glm::uvec2 const& windowExtents) const;
	uint32_t				ChooseImageCount(VkSurfaceCapabilitiesKHR const& surfaceCapabilities, uint32_t desiredImageCount) const;

	/// Retrive a number of imagesCount swapchain images from API. imagesCount must be a valid count of swapchain images.
	void					RetrieveSwapChainImages(uint32_t imagesCount);
	void					CreateSwapChainViews(VkFormat format);


	VKRHI_Device*						_device;
	VkSwapchainKHR						_swapChain;

	VKRHI_SwapChainSupportDetails       _supportDetails;

	std::vector<VkImage>				_swapChainImages;
	std::vector<Handle<VKRHI_Image>>	_swapChainImageViews;

	glm::uvec2							_imageExtents;
	VkSurfaceFormatKHR					_surfaceFormat;
};