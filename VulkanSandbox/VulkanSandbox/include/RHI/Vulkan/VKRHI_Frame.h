#pragma once

#include <vector>
#include <list>

#include "vulkan/vulkan.h"

#include "glm/glm.hpp"

#include "RHI/Vulkan/VKRHI_Semaphore.h"
#include "RHI/Vulkan/VKRHI_Fence.h"
#include "RHI/Vulkan/VKRHI_MemoryObject.h"
#include "RHI/Vulkan/VKRHI_AvailableDescSetLayouts.h"
#include "RHI/Vulkan/VKRHI_CommandBuffer.h"

#include "Core/FrameRenderContext.h"

class VKRHI_RenderPass;

class VKRHI_MeshRenderObject;
class VKRHI_StaticMeshRenderObject;
class VKRHI_TerrainRenderObject;
class VKRHI_CubemapRenderObject;
class VKRHI_Frame
{
	struct TerrainUniform
	{
		glm::vec3	extents;
		float padding;
		glm::vec3	location;
	};

    struct TerrainMeshShadingPerPatchUniform
    {
        glm::vec4 coords;
        //int subdivision;
    };

	struct MeshUniform
	{
		glm::mat4 model;
	};

	struct GlobalUniform
	{
		glm::mat4 view;
		glm::mat4 projection;

        glm::mat4 cullView;
    };

	struct GlobalCamUniform
	{
		glm::vec2 camExtents;
		float padding[2];
		glm::vec3 eye;
	};

		struct GlobalLightUniform
		{
			struct AmbientLightUniform
			{
				glm::vec3	color;
				float		intensity;
			} ambient;

			struct DirectionalLightUniform
			{
				glm::vec3	color;
				float		intensity;
				glm::vec3	direction;
			} directional;
		};

public:
	bool FromVkFrameDescriptor(class VKRHI_Device& device, VKRHI_FrameDescriptor& frameDescriptor);

	void Shutdown();

	void PrepareDataForDrawing(FrameRenderContext& Context);
	
    void Draw(FrameRenderContext& Context);
	void Present();
	void WaitDraw();

// Command buffers
private:
	bool CreateCommandBuffers(VKRHI_FrameDescriptor const& frameDescriptor);
	void DestroyCommandBuffers();

	bool RecordCubemapSubCommandBuffer(VKRHI_FrameDescriptor const& frameDescriptor);
	bool RecordTerrainSubCommandBuffer(VKRHI_FrameDescriptor const& frameDescriptor);
	bool RecordOpaqueSubCommandBuffer(VKRHI_FrameDescriptor const& frameDescriptor);
	bool RecordMainCommandBuffer(VKRHI_FrameDescriptor const& frameDescriptor);
	bool RecordEditorCommandBuffer(VKRHI_FrameDescriptor const& frameDescriptor);

private:
	bool CreateFramebuffers(VKRHI_FrameDescriptor const& frameDescriptor);
	bool CreateFramebuffer(VKRHI_FrameDescriptor const& frameDescriptor, VKRHI_RenderPass const& pass, VkFramebuffer& outFramebuffer);
	void CmdBindMeshRenderObject(VKRHI_FrameDescriptor const& frameDescriptor, VkCommandBuffer commandBuffer, VKRHI_MeshRenderObject const& meshRenderObject);

private:
	class VKRHI_Device*				 _device = nullptr;
                                     
	class VKRHI_SwapChain*			 _swapChain = nullptr;
	uint32_t						 _imageIndex;
                                     
	VKRHI_Fence						 _imageRenderingFence;
	VKRHI_Semaphore					 _renderFinishedSemaphore;
                                     
	VkFramebuffer					 _opaqueFramebuffer;
	VkFramebuffer					 _editorFramebuffer;

    VkCommandPool                    _commandPool;

    VKRHI_CommandBuffer			     _mainCommandBuffer;
	VKRHI_CommandBuffer			     _editorCommandBuffer;

	VKRHI_CommandBuffer			     _cubemapSubCommandBuffer;
	VKRHI_CommandBuffer			     _terrainSubCommandBuffer;
	VKRHI_CommandBuffer			     _opaqueSubCommandBuffer;

// Settings
private:
    VKRHI_FrameRenderingOptions      _renderingOptions;

// Descriptor sets
private:
	bool AllocateDescriptorSets(std::vector<VkDescriptorSetLayout> const& layouts, VkDescriptorSet* outDescSet);
	bool AllocateNDescriptorSets(size_t count, VKRHI_DescriptorSetLayout const& layout, VkDescriptorSet* outDescSet);
	bool FreeDescriptorSets(std::vector<VkDescriptorSet>& sets) const;

	bool AllocUniformBuffer(VKRHI_FrameDescriptor const& frameDescriptor, uint32_t size, VKRHI_Buffer& outBuffer);

	bool CreateDescriptorSets(VKRHI_FrameDescriptor const& frameDescriptor);
	void FreeAllDescriptorSets();
	
	VkWriteDescriptorSet	GetUBOWriter(VkDescriptorSet set, class VKRHI_Buffer const& ubo, VkDescriptorBufferInfo& outInfoBuffer) const;
	VkWriteDescriptorSet	GetImageWriter(VkDescriptorSet set, class VKRHI_Image const& image, VkDescriptorImageInfo& outInfoBuffer) const;
	VkWriteDescriptorSet	GetImageWriter(VkDescriptorSet set, std::vector<VKRHI_Image const*> const& images, std::vector<VkDescriptorImageInfo>& outInfoBuffer) const;

	bool CreateMeshesDescriptorSet(VKRHI_FrameDescriptor const& frameDescriptor);
	bool CreateCubemapDescriptorSet(VKRHI_FrameDescriptor const& frameDescriptor);
	bool CreateGlobalDescriptorSet(VKRHI_FrameDescriptor const& frameDescriptor);
	bool CreateTerrainDescriptorSet(VKRHI_FrameDescriptor const& frameDescriptor);

    VkDescriptorPool                _descriptorPool;

	std::vector<VKRHI_Buffer>		_objectUniformBuffers;
	std::vector<VkDescriptorSet>	_objectDescriptorSets;
	VkDescriptorSet					_defaultMatDescriptorSet;

	VkDescriptorSet					_cubemapMatDescriptorSet;

    uint32_t                        _terrainPatchCount;
	// TODO create a real dynamic object system
	int32_t							_terrainVersionIndex = -1;
	VKRHI_Buffer					_terrainPatchBuffer;
	VKRHI_Buffer					_terrainUniformBuffer;
    VKRHI_Buffer					_terrainMatUniformBuffer;
	VkDescriptorSet					_terrainDescriptorSet;
    VkDescriptorSet					_terrainMeshShaderDescriptorSet;
    VkDescriptorSet					_terrainMatDescriptorSet;

	VKRHI_Buffer					_globalUniformBuffer;
	VKRHI_Buffer					_globalCamUniformBuffer;
	VKRHI_Buffer					_globalLightUniformBuffer;
	VkDescriptorSet					_globalDescriptorSet;

    std::vector<VKRHI_Buffer>		_dynamicBuffers;

};