#pragma once

#include <vector>

#include "vulkan/vulkan.h"

#include "Resource/ResourceEnums.h"

class VKRHI_Shader
{
public:
	bool FromSpirVData(class VKRHI_Device& device, std::vector<uint32_t> const& data, ShaderType shaderStage);

	void Shutdown();

	VkPipelineShaderStageCreateInfo const& GetShaderCreateInfo() const { return _stageCreateInfo; }

private:
	VKRHI_Device* _device;

	VkShaderModule _shaderModule;
	VkShaderStageFlagBits _shaderStage;
	VkPipelineShaderStageCreateInfo _stageCreateInfo;
};