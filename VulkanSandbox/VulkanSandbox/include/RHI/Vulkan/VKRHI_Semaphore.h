#pragma once

#include "vulkan/vulkan.h"

#include "RHI/Vulkan/VKRHI_Device.h"

class VKRHI_Semaphore
{
public:
	void Initialize(VKRHI_Device const& device);
	void Shutdown();

	VkSemaphore const& GetVkSemaphore() const { return _vkSemaphore; };

private:
	VKRHI_Device const* _device = nullptr;
	VkSemaphore _vkSemaphore;
};