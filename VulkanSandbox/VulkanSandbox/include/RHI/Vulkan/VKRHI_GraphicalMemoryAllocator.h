#pragma once

#include "vulkan/vulkan.h"

struct VKRHI_GraphicalMemorySlot
{
	friend class VKRHI_GraphicalMemoryAllocator;

public:
	VkDeviceMemory	GetVkMemory() const { return _vkMemory; }
	size_t			GetOffset() const { return _offset; }
	size_t			GetSize() const { return _size; }

private:
	VkDeviceMemory	_vkMemory = VK_NULL_HANDLE;
	size_t			_offset = 0;
	size_t			_size = 0;
};

class VKRHI_GraphicalMemoryAllocator
{
public:
	void FromDevice(class VKRHI_Device& device);
	bool AllocateMemory(size_t allocationSize, uint32_t memoryTypeBits, VkMemoryPropertyFlags memoryFlags, VKRHI_GraphicalMemorySlot& outMemory);
	void ReleaseMemory(VKRHI_GraphicalMemorySlot& outMemory);

	void Shutdown() {}

private:
	bool FindMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties, uint32_t& outIndexRes) const;

	class VKRHI_Device* _device = nullptr;
	VkCommandPool pool;
};