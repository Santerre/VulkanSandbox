#pragma once

// Todo - Remove : Vulkan specific
#include "vulkan/vulkan.h"

#include "RHI/Vulkan/VKRHI_MemoryObject.h"
#include "Debugging/Assert.h"

struct ImageDesc
{
	// Todo - Remove : Vulkan specific
	VkFormat			format;
	VkSampler			sampler;
	VkImageAspectFlags	aspectFlags;
	VkImageViewType		type = VK_IMAGE_VIEW_TYPE_2D;

	uint32_t			layerCount = 1u;
};

class VKRHI_Image
{
public:
	bool FromImageMemory(class VKRHI_Device const& device, VKRHI_ImageMemory const& source, ImageDesc const& descriptor);
	bool FromVkImage(class VKRHI_Device const& device, VkImage source, ImageDesc const& descriptor, uint32_t mipmapCount = 1u);

	void Shutdown();

	VkImageView const& GetImageView() const { return _imageView; }
	VKRHI_ImageMemory const& GetImageMemory() const { KU_ASSERT(_imageMemory); return *_imageMemory; }
	VkSampler const& GetImageSampler() const { return _sampler; }
	VkFormat GetFormat() const { return _format; }

private:
	class VKRHI_Device const*	_device;
	VKRHI_ImageMemory const*	_imageMemory = nullptr;
	VkImageView					_imageView;
	VkSampler					_sampler;
	VkFormat					_format;
};