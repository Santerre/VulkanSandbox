#pragma once

#include <map>
#include <string>

enum struct EStatus
{
	Pending = 0,
	Available,
	Unavailable
};

struct VKRHI_ExtenderItem
{
	std::string name;
	EStatus status = EStatus::Pending;
};

using ExtenderItemMap = std::map<std::string, VKRHI_ExtenderItem>;

class VKRHI_Extender
{
	void Init();

	bool RequestExtension(const char* extentionName);
	bool RequestLayer(const char* layerName);

	ExtenderItemMap const& GetRequestedExtensions() const { return _requestedExtensions; }
	ExtenderItemMap const& GetRequestedLayers() const { return _requestedLayers; }

private:

	void InitLayers();
	void InitExtensions();

	void PrintExtensionSummary(std::string const& title, std::map<std::string, VKRHI_ExtenderItem> const& map);

	template<class InputIterator>
	void PrintExtensionSummary(std::string const& title, InputIterator begin, InputIterator end);

private:
	ExtenderItemMap _requestedExtensions;
	ExtenderItemMap _requestedLayers;
};

template<class InputIterator, class Functor>
void VKRHI_Extender::TestExtensionItem(InputIterator begin, InputIterator end, Functor func)
{
	std::for_each(begin, end(), [&begin, &end](Item it)
	{
		it func(it);
		// dostuff
	});
}

template<class InputIterator, class Functor>
void VKRHI_Extender::TestExtensionItem(ExtenderItemMap const& map, InputIterator begin, InputIterator end, Functor func)
{
	InputIterator foundIt = begin;
	map.for_each(map.begin, map.end(), [&foundIt, &end](auto const& [name, itemToFind])
	{
		InputIterator it = std::find_if(foundIt, end, [func, &itemToFind]()
		{
			return itemToFind.name == func(item);
		});
		if (it == end)
		{
			itemToFind.status = EStatus::Unavailable;
		}
		else
		{
			itemToFind.status = EStatus::Available;
			foundIt = it;
		}
	});
}

template<class InputIterator>
void VKRHI_Extender::PrintExtensionSummary(std::string const& title, InputIterator begin, InputIterator end)
{
	VLOG(LogVulkan, "--------------");
	for_each(begin, end, [&title](auto const&[name, item])
	{
		switch (EStatus)
		{
		case EStatus::Pending:
			LOG(LogVulkan, "Pending : %s", item.name);
			break;
		case EStatus::Available:
			LOG(LogVulkan, "Available : %s", item.name);
			break;
		case EStatus::Unavailable:
			LOG(LogVulkan, "Unavailable : %s", item.name);
			break;
		default:
			break;
		}

	});
	VLOG(LogVulkan, "--------------");
}
