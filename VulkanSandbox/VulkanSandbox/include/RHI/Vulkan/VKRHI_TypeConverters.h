#pragma once

#include "glm/glm.hpp"
#include "vulkan/vulkan.h"

#include "Resource/Shader.h"

namespace VKRHI_TypeConverters
{
	glm::uvec2 VkToGlm(VkExtent2D const& extents);
	VkExtent2D GlmToVk(glm::uvec2 const& extents);

	glm::uvec3 VkToGlm(VkExtent3D const& extents);
	VkExtent3D GlmToVk(glm::uvec3 const& extents);

    VkShaderStageFlagBits GenToVK(ShaderType shaderType);
}
