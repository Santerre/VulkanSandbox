#pragma once

#include "vulkan/vulkan.h"

class VKRHI_Device;

class VKRHI_CommandBuffer
{
public:
    bool FromCommandPool(VKRHI_Device& device, VkCommandPool pool, VkCommandBufferLevel level = VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY);
    void Shutdown();

    void                    SetSecondaryBufferInheritanceInfo( VkCommandBufferInheritanceInfo const& inheritanceInfo );
    VkCommandBuffer&        GetVkCommandBuffer() { return _buffer; }

    bool                    IsRecording() { return _bIsRecording; }

    bool                    BeginOneTime();
    bool                    Begin();
    bool                    BeginSecondary();

    void                    End();  

private:
    VkCommandBuffer                 _buffer;
    VkCommandPool                   _commandPool;
    VKRHI_Device*                   _device;

    VkCommandBufferInheritanceInfo  _inheritanceInfo;

    bool            _bIsRecording = false;
};
