#pragma once

#include "vulkan/vulkan.h"

#include <vector>
#include <array>

struct VKRHI_DescriptorSetBinding
{
	VKRHI_DescriptorSetBinding(uint32_t binding, VkDescriptorType descriptorType, VkShaderStageFlags stageFlags);
	VKRHI_DescriptorSetBinding(uint32_t binding, VkDescriptorType descriptorType, uint32_t arraySize, VkShaderStageFlags stageFlags);

	VkDescriptorType	descriptorType;
	VkShaderStageFlags  stageFlags;
	uint32_t			binding = 0u;
	uint32_t			arraySize = 1u;
};

struct VKRHI_DescriptorSetLayout
{
	bool FromBindings(class VKRHI_Device* device, std::vector<VKRHI_DescriptorSetBinding>&& bindings);
	void Shutdown();

	std::vector<VKRHI_DescriptorSetBinding> const GetBindings() const { return _bindings; }
	VkDescriptorSetLayout GetRHILayout() const { return _rhiLayout; }
	VKRHI_Device* GetDevice() const { return _device; }

private:
	bool Compile();

	std::vector<VKRHI_DescriptorSetBinding> _bindings;

	VKRHI_Device* _device;
	VkDescriptorSetLayout _rhiLayout;
};

struct VKRHI_AvailableDescSetLayouts
{
public:
	VKRHI_AvailableDescSetLayouts() {};
	~VKRHI_AvailableDescSetLayouts() {};

	void Shutdown();

	int CountLayoutsOfType(VkDescriptorType type);

public:
	union
	{
		std::array<VKRHI_DescriptorSetLayout, 7> layouts;

		struct
		{
			struct
			{
				VKRHI_DescriptorSetLayout						object;
				VKRHI_DescriptorSetLayout						defaultMat;
			} staticMesh;

			struct
			{
				VKRHI_DescriptorSetLayout						object;
                VKRHI_DescriptorSetLayout						meshShaderObject;
                VKRHI_DescriptorSetLayout						mat;
			} terrain;

			struct
			{
				VKRHI_DescriptorSetLayout						mat;
			} cubemap;

			VKRHI_DescriptorSetLayout							global;
		};
	};
};

struct VKRHI_AvailablePipelineLayouts
{
public:
	void ShutdownLayouts(VKRHI_Device const& device);

public:
	VkPipelineLayout staticMeshLayout;
	VkPipelineLayout terrainLayout;
    VkPipelineLayout meshShaderTerrainLayout;
    VkPipelineLayout cubemapLayout;
};