#pragma once

#pragma once

#include <vector>

#include "vulkan/vulkan.h"
#include "Memory/Key.h"

#include "RHI/Vulkan/VKRHI_RenderObject.h"

#include "glm/vec4.hpp"

struct VKRHI_TerrainRenderVertData
{
    glm::vec4 objectSpaceCoords;
};

struct VKRHI_TerrainRenderObjectDesc
{
	TextureKeys	heightTextureKey;
	TextureKeys	grassTextureKey;
	TextureKeys	snowTextureKey;
	TextureKeys	cliffTextureKey;
};

class VKRHI_TerrainRenderObject
{
public:
	bool FromDesc(VKRHI_TerrainRenderObjectDesc const& desc);

	void Shutdown();

	TextureKeys		GetHeightTextureKey() const { return _heightTextureKey; }
	TextureKeys		GetGrassTextureKey() const { return _grassTextureKey; }
	TextureKeys		GetSnowTextureKey() const { return _snowTextureKey; }
	TextureKeys		GetCliffTextureKey() const { return _cliffTextureKey; }

private:
	TextureKeys				_heightTextureKey;
	TextureKeys				_grassTextureKey;
	TextureKeys				_snowTextureKey;
	TextureKeys				_cliffTextureKey;
};
