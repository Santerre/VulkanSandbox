#pragma once

#include "vulkan/vulkan.h"

class VKRHI_DebugExtension
{
	void Init();
	void Shutdown();

	void Register();

	static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objType,
		uint64_t obj,
		size_t location,
		int32_t code,
		const char* layerPrefix,
		const char* msg,
		void* userData);


}