#pragma once

#include <vector>
#include "vulkan/vulkan.h"
#include "RHI/Vulkan/VKRHI_LogCategory.h"
#include "RHI/Vulkan/VKRHI_Device.h"

struct QueryPoolDesc
{
   // TODO remove vulkan specific
   VkQueryType type;
   size_t count;
};

class VKRHI_QueryPool;

class VKRHI_Query
{
    friend VKRHI_QueryPool;
public:
    size_t GetID() const { return _index; }

private:
    size_t _index = 0;
};

class VKRHI_QueryPool
{
public:
    bool FromDescriptor(VKRHI_Device const& device, QueryPoolDesc desc);
    void Shutdown();

    VKRHI_Query CmdWriteTimestamp(VkCommandBuffer commandBuffer, size_t queryId, VkPipelineStageFlagBits stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT);
    VKRHI_Query CmdBeginQuery(VkCommandBuffer commandBuffer, size_t queryId);
    void CmdEndQuery(VkCommandBuffer commandBuffer, VKRHI_Query const& query);

    template<typename T>
    std::vector<T> GetQueryResults(VKRHI_Query beginQuery, size_t count);

    // Append the results to the end of the array
    template<typename T>
    void GetQueryResults(size_t startQueryID, size_t count, std::vector<T>& inOutRes);

    void CmdReset(VkCommandBuffer commandBuffer, size_t startQuery, size_t queryCount);

private:
    size_t          _allocatedCount;
    VkQueryPool     _vkPool;

    VKRHI_Device*   _device = nullptr;
};

template<typename T>
std::vector<T> VKRHI_QueryPool::GetQueryResults(VKRHI_Query beginQuery, size_t count)
{
    return GetQueryResults(beginQuery.GetID(), count);
}

template<typename T>
void VKRHI_QueryPool::GetQueryResults(size_t startQueryID, size_t count, std::vector<T>& inOutRes)
{
    size_t startIndex = inOutRes.size();
    inOutRes.resize(inOutRes.size() + count);

    if (vkGetQueryPoolResults(_device->GetVkDevice(), _vkPool, static_cast<uint32_t>(startQueryID), static_cast<uint32_t>(count), count * sizeof(T), inOutRes.data() + startIndex, 0, 0) != VK_SUCCESS)
    {
        ELOG(LogVulkan, "Couldn't create the vulkan pool");
        inOutRes.clear();
    }
}
