#pragma once

#include <string>

#include "Resource/ResourceMgr.h"
#include "Resource/Shader.h"

DEF_LOG_CATEGORY(ShaderCompilation);

class VKRHI_ShaderCompiler
{
public:
    enum struct CompilationOptimization
    {
        O0 = 0,
        O1,
        O2,
        O3
    };

    struct CompileOptions
    {
        CompilationOptimization optimisation = CompilationOptimization::O3;

        // TODO move it to another place
        std::vector<std::string> prefixCode;

        std::string includePath;
        std::map<std::string, std::string> macroList;
    };

    struct InitDesc
    {
        IApplication   *application;
        ResourceMgr    *resourceMgr;
        CompileOptions  defaultCompileOptions;
    };

public:
    void FromDesc(InitDesc const& desc);
    void Shutdown(){}

    CompileOptions const& GetDefaultCompileOptions() const { return _defaultCompileOptions; }
    Handle<SPVShader>     CompileShader(std::shared_ptr<ResourceWrapper<GLSLShader>> targetShader, CompileOptions const& options, ShaderType shaderType);

private:
    CompileOptions  _defaultCompileOptions;
    ResourceMgr    *_resourceMgr;
    IApplication   *_application;
};