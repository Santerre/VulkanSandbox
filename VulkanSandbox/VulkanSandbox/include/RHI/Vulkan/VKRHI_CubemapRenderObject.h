#pragma once

#include <vector>

#include "vulkan/vulkan.h"
#include "Memory/Key.h"

#include "RHI/Vulkan/VKRHI_RenderObject.h"

struct VKRHI_CubemapRenderObjectDesc
{
	VKRHI_MeshRenderObjectDesc	meshRODesc;

	ImageKey					imageBufferKey;
};

class VKRHI_CubemapRenderObject
{
public:
	bool FromDesc(VKRHI_CubemapRenderObjectDesc const& desc);

	void Shutdown();

	ImageKey		GetImageBufferKey() const { return _imageBufferKey; }

	VKRHI_MeshRenderObject const&	GetMesh() const { return _meshRenderObject; }

private:
	VKRHI_MeshRenderObject	_meshRenderObject;
	ImageKey				_imageBufferKey;
};
