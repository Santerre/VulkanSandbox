#pragma once

#include <optional>
#include <vector>

#include "vulkan/vulkan.h"

struct QueueFamilyIndices
{
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;

	bool IsComplete() const;
};

struct VKRHI_SwapChainSupportDetails
{
	VkSurfaceCapabilitiesKHR		capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR>	presentModes;
};

struct VKRHI_DeviceProperties
{
    VKRHI_DeviceProperties();

    VkPhysicalDeviceProperties2 deviceProperties = {};
    VkPhysicalDeviceMeshShaderPropertiesNV meshShaderProperties = {};
};

struct VKRHI_DeviceFeatures
{
    VKRHI_DeviceFeatures();

    VkPhysicalDeviceFeatures2 deviceFeatures;
    VkPhysicalDeviceMeshShaderFeaturesNV meshShaderFeatures;
};

class VKRHI_PhysicalDevice
{
public:
	void FromVkDevice(VkInstance vkInstance, VkPhysicalDevice const& device, VkSurfaceKHR const& surface);
	void InitQueueFamily(VkSurfaceKHR const& surface);

	int  Score(std::vector<const char*> const& optionnalDeviceExtensions) const;
	bool IsSuitable() const;

	bool SupportsExtensions(std::vector<const char*> const& requiredExtensions) const;

	VkPhysicalDevice const& GetVkPhysicalDevice() const { return _device; }
	QueueFamilyIndices const& GetQueueFamilyIndices() const { return _queueFamilyIndices; }
	VKRHI_SwapChainSupportDetails const& GetSwapChainSupportDetails() const { return _swapChainSupportDetails; }

    VKRHI_DeviceProperties const& GetPhysicalDeviceProperties() const { return _properties; }
    std::vector<VkQueueFamilyProperties> const& GetQueueFamilyProperties() const { return _queueFamilyProperties; }

    VkFormatProperties GetPhysicalDeviceFormatProperties(VkFormat format) const;

    bool SupportsMeshShading() const;

private:
	void LoadSwapChainSupportDetails(VkSurfaceKHR const& surface);

	VkPhysicalDevice	                 _device;
                                         
    VKRHI_DeviceFeatures                 _supportedFeatures;
    VKRHI_DeviceProperties               _properties;
	QueueFamilyIndices	                 _queueFamilyIndices;
                                         
	VKRHI_SwapChainSupportDetails        _swapChainSupportDetails;

    std::vector<VkQueueFamilyProperties> _queueFamilyProperties;
};