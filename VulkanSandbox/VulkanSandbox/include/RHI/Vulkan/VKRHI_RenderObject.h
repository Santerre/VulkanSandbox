#pragma once

#include <array>
#include <vector>

#include "vulkan/vulkan.h"

#include "Memory/Key.h"

#include "Core/MeshData.h"

using VertexBufferKey = IKey<std::vector<class VKRHI_Buffer>>;
using DynamicVertexBufferKey = IKey<std::vector<class VKRHI_Buffer>>;
using IndexBufferKey = IKey<std::vector<class VKRHI_Buffer>>;
using ImageKey = IKey<std::vector<class VKRHI_Image>>;

struct TextureKeys
{
	ImageKey albedo;
	ImageKey heightMap;
	ImageKey normalMap;
};

struct VKRHI_MeshRenderObjectDesc
{
	VertexBufferKey vertexBufferKey;
	size_t			verticesCount;

	IndexBufferKey	indexBufferKey;
	size_t			indicesCount;
};

class VKRHI_MeshRenderObject
{
public:
    enum AttributeBit
    {
        None     = 0x00,
        Position = 0x01,
        Color    = 0x02,
        UV       = 0x04,
        All      = ~0x0,
    };

	bool FromDesc(VKRHI_MeshRenderObjectDesc const& desc);

	static constexpr VkVertexInputBindingDescription GetInputBindingDescription()
    {
        VkVertexInputBindingDescription desc{};
	    desc.binding = 0;
	    desc.stride = sizeof(Vertex);
	    desc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        return desc;
    }

	static std::vector<VkVertexInputAttributeDescription> GetInputAttributeDescriptions(AttributeBit attributes = AttributeBit::All)
    {
	    std::vector<VkVertexInputAttributeDescription> attribDescs{};

        if (attributes & AttributeBit::Position)
        {
            VkVertexInputAttributeDescription desc{};
            desc.binding = 0;
	        desc.location = 0;
	        desc.format = VK_FORMAT_R32G32B32_SFLOAT;
	        desc.offset = offsetof(Vertex, position);
            attribDescs.push_back(std::move(desc));
        }

        if (attributes & AttributeBit::Color)
        {
            VkVertexInputAttributeDescription desc{};
	        desc.binding = 0;
	        desc.location = 1;
	        desc.format = VK_FORMAT_R32G32B32_SFLOAT;
	        desc.offset = offsetof(Vertex, color);
            attribDescs.push_back(std::move(desc));
        }

        if (attributes & AttributeBit::UV)
        {
            VkVertexInputAttributeDescription desc{};
	        desc.binding = 0;
	        desc.location = 2;
	        desc.format = VK_FORMAT_R32G32_SFLOAT;
	        desc.offset = offsetof(Vertex, uv);
            attribDescs.push_back(std::move(desc));
        }
        return attribDescs;
    }

	VertexBufferKey	GetVertexBufferKey() const { return _vertexBufferKey; }
	IndexBufferKey	GetIndexBufferKey() const { return _indexBufferKey; }

	size_t	GetVertsCount() const { return _vertsCount; }
	size_t	GetIndexCount() const { return _indexCount; }

private:

	VertexBufferKey	_vertexBufferKey;
	IndexBufferKey	_indexBufferKey;

	size_t			_vertsCount;
	size_t			_indexCount;
};