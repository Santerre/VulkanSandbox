#pragma once

#include "vulkan/vulkan.h"

#include "glm/glm.hpp"

#include "RHI/Vulkan/VKRHI_Device.h"
#include "RHI/Vulkan/VKRHI_GraphicalMemoryAllocator.h"

struct MemoryObjectDesc
{
	// Todo - Remove : Vulkan specific
	VkMemoryRequirements* memRequirement;
	VkMemoryPropertyFlags properties;
	VKRHI_GraphicalMemoryAllocator* allocator = nullptr;
};

class VKRHI_MemoryObject
{
public:
	bool FromDesc(VKRHI_Device& device, MemoryObjectDesc const& bufferDesc);
	virtual void Shutdown();

	void			MapMemory(void** outMappedMemory);
	void			UnmapMemory();
	VKRHI_GraphicalMemorySlot const& GetMemSlot() const { return _memorySlot; }

protected:
	VKRHI_Device*							_device;

private:
	VKRHI_GraphicalMemorySlot				_memorySlot;
	class VKRHI_GraphicalMemoryAllocator*	_allocator;
};

struct BufferDesc
{
	// Todo - Remove : Vulkan specific
	size_t size;
	VkBufferUsageFlags usage;
	VkMemoryPropertyFlags properties;
	VKRHI_GraphicalMemoryAllocator* allocator = nullptr;
};

class VKRHI_Buffer : public VKRHI_MemoryObject
{
public:
	bool FromDesc(VKRHI_Device& device, BufferDesc const& bufferDesc);
	virtual void Shutdown() override;

	VkBuffer GetVkBuffer() const { return _buffer; }
	VkDescriptorBufferInfo GetBufferInfo(uint64_t offset, uint64_t range) const;
	VkDescriptorBufferInfo GetWholeBufferInfo() const;

	size_t			GetByteSize() const;

private:
	using VKRHI_MemoryObject::FromDesc;

	size_t			_size;

	VkBuffer		_buffer;
	VKRHI_Device*	_device = nullptr;
};

struct ImageMemoryDesc
{
	VKRHI_GraphicalMemoryAllocator* allocator = nullptr;

	glm::uvec2 extent;
	uint32_t layerCount = 1;

	uint32_t mipmapCount = 1;

	// Todo - Remove : Vulkan specific
	VkFormat format;
	VkImageCreateFlags flags;
	VkImageTiling tiling;
	VkImageUsageFlags usage;
	VkMemoryPropertyFlags memProperties;
};

class VKRHI_ImageMemory : public VKRHI_MemoryObject
{
public:
	bool FromDesc(VKRHI_Device& device, ImageMemoryDesc const& bufferDesc);
	virtual void Shutdown() override;


	uint32_t GetLayerCount() const { return _layerCount; }
	uint32_t GetMipmapCount() const { return _mipmapCount; }
	glm::uvec2 GetExtents() const { return _extents; }

	VkImage GetVkImage() const { return _image; }

	size_t			GetByteSize() const;

private:
	using VKRHI_MemoryObject::FromDesc;

	uint32_t _layerCount = 1;
	uint32_t _mipmapCount = 1;
	glm::uvec2 _extents;
	VkImage _image;
};