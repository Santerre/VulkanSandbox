#pragma once

#include <vector>

#include "vulkan/vulkan.h"
#include "Memory/Key.h"

#include "RHI/Vulkan/VKRHI_RenderObject.h"

struct VKRHI_StaticMeshRenderObjectDesc
{
	VKRHI_MeshRenderObjectDesc	meshRODesc;

	TextureKeys					textureBufferKey;
};

class VKRHI_StaticMeshRenderObject
{
public:
	bool FromDesc(VKRHI_StaticMeshRenderObjectDesc const& desc);

	void Shutdown();

	TextureKeys		GetTextureBufferKey() const { return _textureBufferKey; }

	VKRHI_MeshRenderObject const&	GetMesh() const { return _meshRenderObject; }

private:
	VKRHI_MeshRenderObject	_meshRenderObject;
	TextureKeys				_textureBufferKey;
};
