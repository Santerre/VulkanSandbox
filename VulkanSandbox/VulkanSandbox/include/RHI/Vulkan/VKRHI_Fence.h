#pragma once

#include "vulkan/vulkan.h"

#include "RHI/Vulkan/VKRHI_Device.h"

class VKRHI_Fence
{
public:
	void			Initialize(VKRHI_Device const& device);
	void			Shutdown();

	void			Wait();
	void			Reset();
	VkFence const&	GetVkFence() const { return _vkFence; };

private:
	VKRHI_Device const* _device = nullptr;
	VkFence _vkFence;
};