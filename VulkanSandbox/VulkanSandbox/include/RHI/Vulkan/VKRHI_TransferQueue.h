#pragma once

#include "vulkan/vulkan.h"

#include <vector>

#include "RHI/Vulkan/VKRHI_CommandBuffer.h"

class VKRHI_Buffer;
class VKRHI_ImageMemory;
class VKRHI_Device;
class VKRHI_GraphicalMemoryAllocator;

struct VKRHI_TransferQueueDescriptor
{
    VkQueue     queue;
    uint32_t    queueFamilyIndex;

    // Allocator used to craete staging buffers.
    VKRHI_GraphicalMemoryAllocator* graphMemAllocator;
};

struct VKRHI_UpdateBufferMemoryDesc
{
    std::vector<void const*> source;
    size_t totalSize;
};

struct VKRHI_UpdateImageMemoryDesc : VKRHI_UpdateBufferMemoryDesc
{
    bool            bGenerateMipmaps = true;
    uint32_t        layerCount = 1u;
    VkFormat        format;
    VkImageLayout   layoutIn;
    VkImageLayout   layoutOut;
};

class VKRHI_TransferQueue
{
public:
    void FromDesc( VKRHI_Device& device, VKRHI_TransferQueueDescriptor const& desc );
    void Shutdown();

    void UpdateBufferMemory(VKRHI_Buffer& buffer, VKRHI_UpdateBufferMemoryDesc const& desc);
    void UpdateImageMemory(VKRHI_ImageMemory& imageMem, VKRHI_UpdateImageMemoryDesc const& desc);

    bool CmdTransitionImageLayout(VKRHI_CommandBuffer& buffer, VKRHI_ImageMemory& imageMem, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);
    
    VKRHI_CommandBuffer& BeginCommandBuffer();
    void EndCommandBuffer(VKRHI_CommandBuffer& commandBuffer);

private:
    void LoadStageBuffer(VKRHI_Buffer& buffer, void const* source, size_t size);
    void LoadStageBuffer(VKRHI_Buffer& stageBuffer, std::vector<void const*> const& sources, size_t sourceLength);
    bool CmdGenerateMipmaps(VKRHI_CommandBuffer& buffer, VKRHI_ImageMemory& imageMem, VkFormat imageFormat);

    void CmdCopyBuffer(VKRHI_CommandBuffer& buffer, VKRHI_Buffer& src, VKRHI_Buffer& dst);
    void CmdCopyBuffer(VKRHI_CommandBuffer& buffer, VKRHI_Buffer& src, VKRHI_ImageMemory& dst, uint32_t layerCount = 1);

    void SubmitTransfer();
    void WaitTransfer();

private:
    VKRHI_Device*   _device;
    /// Allocator used to craete staging buffers.
    VKRHI_GraphicalMemoryAllocator* _graphMemAllocator;

    VkQueue         _queue;
    VkCommandPool   _commandPool;
    VKRHI_CommandBuffer   _commandBuffer;
};