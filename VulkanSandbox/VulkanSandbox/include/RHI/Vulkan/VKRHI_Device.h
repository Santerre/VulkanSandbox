#pragma once

#include <vector>

#include "vulkan/vulkan.h"

class VKRHI_DeviceFunctions
{
public:
    PFN_vkCmdDrawMeshTasksNV vkCmdDrawMeshTasksNV = nullptr;
};

class VKRHI_Device
{
public:
	bool FromPhysicalDevice(class VKRHI_PhysicalDevice const& physicalDevice, std::vector<const char*> const& validationLayers, std::vector<const char*> const& _requiredExtensions);

	void Shutdown();

	void Wait();

	VkDevice					        GetVkDevice() const { return _device; }
	class VKRHI_PhysicalDevice const&   GetPhysicalDevice() const;

	VkQueue						        GetGraphicQueue() { return _graphicQueue; };
	VkQueue						        GetPresentQueue() { return _presentQueue; };

    bool                                IsMeshShadingEnabled() { return _meshShadingEnabled; }

    VKRHI_DeviceFunctions const&        GetFunc() { return _deviceFunctions; }

private:
    void InitFunctions(VKRHI_DeviceFunctions& outFunctions);

private:
	class VKRHI_PhysicalDevice const* _physicalDevice;

	VkDevice	_device;

	VkQueue		_graphicQueue;
	VkQueue		_presentQueue;

    bool        _meshShadingEnabled = false;

    VKRHI_DeviceFunctions _deviceFunctions;
};