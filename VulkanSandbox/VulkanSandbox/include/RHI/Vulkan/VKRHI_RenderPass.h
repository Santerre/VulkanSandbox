#pragma once

#include <vector>
#include <optional>

#include "vulkan/vulkan.h"

#include "glm/glm.hpp"

#include "VKRHI_Device.h"

class VKRHI_SubpassDependencyDesc
{
public:
	VKRHI_SubpassDependencyDesc();
	VkSubpassDependency dependency = {};
};

class VKRHI_PassAttachmentDesc
{
public:
	VKRHI_PassAttachmentDesc(uint32_t index, VkFormat format);

	VkAttachmentDescription attachment = {};
	VkAttachmentReference	attachmentReference = {};
	glm::vec4				clearColor = { 0.0f, 0.0f, 0.0f, 1.0f };
};

class VKRHI_SubpassDescription
{
public:
    std::vector<VKRHI_PassAttachmentDesc>   colorAttachments = {};
    std::optional<VKRHI_PassAttachmentDesc> depthStencilAttachment;

    VkSubpassContents                       contents = VkSubpassContents::VK_SUBPASS_CONTENTS_INLINE;
};

class VKRHI_RenderPassDesc
{
public:

    // no subpass will automatically create a default subpass
	std::vector<VKRHI_SubpassDescription> subpasses = {};
	std::vector<VKRHI_PassAttachmentDesc> colorAttachments = {};
	std::optional<VKRHI_PassAttachmentDesc> depthStencilAttachment;
	std::vector<VKRHI_SubpassDependencyDesc> dependencies = { VKRHI_SubpassDependencyDesc() };

	glm::uvec2 renderAreaExtents;
};

class VKRHI_RenderPass
{
public:
	bool FromDesc(VKRHI_Device& device, VKRHI_RenderPassDesc&& desc);
	void Shutdown();

	void CmdBeginRenderPass(VkCommandBuffer buffer, VkFramebuffer framebuffer);
	void CmdEndRenderPass(VkCommandBuffer buffer);
    void CmdNextSubpass(VkCommandBuffer buffer);

	VkRenderPass	GetRHIRenderPass() const { return _rhiRenderPass; }
	glm::uvec2		GetAreaExtents() const { return _renderPassProperties.renderAreaExtents; }
	bool			HasDepthStencilAttachment() const { return !!_renderPassProperties.depthStencilAttachment; }

private:
	VKRHI_RenderPassDesc _renderPassProperties;

	VKRHI_Device* _device = nullptr;
	VkRenderPass _rhiRenderPass = {};

    int _currentSubpass;
};