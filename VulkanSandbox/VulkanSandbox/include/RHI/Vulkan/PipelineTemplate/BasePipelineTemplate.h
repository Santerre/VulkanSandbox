#pragma once

#include "vulkan/vulkan.h"

#include <vector>
#include "External/Glm.h"

struct VKRHI_BasePipelineCreateDesc
{
	glm::uvec2										surfaceExtents;
	VkPipelineLayout								pipelineLayout;
	class VKRHI_RenderPass*							renderPass;
	std::vector<VkPipelineShaderStageCreateInfo>*	shaderModules;
};

struct VKRHI_PipelineDescriptor
{
	VKRHI_PipelineDescriptor() = default;

	VKRHI_PipelineDescriptor(VKRHI_PipelineDescriptor const&) = delete;
	VKRHI_PipelineDescriptor(VKRHI_PipelineDescriptor&&) = delete;

	VKRHI_PipelineDescriptor& operator=(VKRHI_PipelineDescriptor const&) = delete;
	VKRHI_PipelineDescriptor& operator=(VKRHI_PipelineDescriptor&&) = delete;

	VkPipelineVertexInputStateCreateInfo			vertexInputInfo = {};
	VkPipelineInputAssemblyStateCreateInfo			vertexInputAssemblyInfo = {};

	VkViewport										viewport = {};
	VkRect2D										scissor = {};
	VkPipelineViewportStateCreateInfo				viewportStateInfo = {};

	VkPipelineRasterizationStateCreateInfo rasterizationCreateInfo = {};

	VkPipelineMultisampleStateCreateInfo multisamplingStateCreateInfo = {};
	VkPipelineColorBlendAttachmentState colorBlendAttachmentState = {};

	VkPipelineColorBlendStateCreateInfo colorBlendCreateInfo = {};

	VkPipelineDepthStencilStateCreateInfo depthStencilInfo = {};

	VkGraphicsPipelineCreateInfo pipelineInfo = {};
};

class VKRHI_BasePipelineTemplate
{
public:
	static void CreateDefaultDescriptor(VKRHI_BasePipelineCreateDesc const& desc, VKRHI_PipelineDescriptor& outDesc);
};