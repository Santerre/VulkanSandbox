#pragma once

#include <string>
#include <stack>
#include <unordered_map>
#include <map>
#include <vector>
#include <list>
#include <array>

#include "Memory/Key.h"
#include "RHI/Vulkan/VKRHI_Query.h"
#include "Utils/RingBuffer.h"

class Profiler
{
public:
    using FrameHandle = int32_t;

    struct RecordingScopeDump
    {
        struct RangeDump
        {
            std::string name;
            size_t      count = 0;
            double      totalTime = 0.f;
        };

        struct CounterDump
        {
            uint64_t value;
        };

        struct TimerDump
        {
            double start;
            double end;

            constexpr double GetDuration() const { return end - start; }
        };

        struct NodeDump
        {
            using RangeDumpKey = Key<std::unordered_map<size_t, RangeDump>, size_t>;

            size_t                  rangeKey;
            std::vector<TimerDump>  timers;
            double                  childTime;
            double                  totalTime;

            std::vector<NodeDump>   children;
        };

        NodeDump root;

        std::unordered_map<size_t, RangeDump> ranges;
        std::unordered_map<std::string, CounterDump> counters;
    };

    struct Range
    {
    public:
        std::string name;
    };
    using RangeKey = Key<std::unordered_map<size_t, Range>, size_t>;

    struct Counter
    {
    public:
        int64_t value;
    };

    struct Timer
    {
        double start;
        double end;
    };

    enum struct RecordingTimerType
    {
        CPU,
        GPU
    };

    using CPURecordingTimer = Timer;

    struct GPURecordingTimer
    {
        struct PoolQuery
        {
            VKRHI_Query     query;
            // TODO make it key
            size_t          localPoolIndex;
        };

        Timer  timer;

        PoolQuery     startQuery;
        PoolQuery     endQuery;
    };

    struct RecordingTimer
    {
        RecordingTimer(){};

        RecordingTimerType type;

        Timer const& GetTimer() const;

        union
        {
            CPURecordingTimer cpuTimer;
            GPURecordingTimer gpuTimer;
        };
    };
    using RecordingTimerKey = IKey<std::vector<RecordingTimer>>;

    struct RecordingScope
    {
    public:
        struct Node
        {
        public:
            Node() = default;
                
            Node(Node* _parentNode, RangeKey _key)
            :   parentNode(_parentNode),
                range(_key)
            {}

            // Clear timer related values, keep structure
            void Cleanup();

        public:
            RangeKey                        range;
            std::vector<RecordingTimerKey>  timerKeys;

            Node*           parentNode = nullptr;
            std::list<Node> children;
        };

    public:
        RecordingScope(RecordingTimerType type)
        : _timerType(type)
        {}

        void StartRecording();
        void EndRecording();

        RecordingTimer& BeginRangeTimer(std::string const& name);
        Profiler::RecordingTimer& EndRangeTimer();

        void CounterSet(std::string const& name, uint64_t value);
        void CounterAdd(std::string const& name, uint64_t delta);

        void AddTimer(RecordingTimerKey key);

        size_t Hash(std::string const& s) { return _hashFunction(s); }

        // Clear timer related values, keep structure
        void Cleanup();

        RecordingScopeDump GetDump() const;

    private:
        bool PushChildRange(RangeKey key);
        RecordingTimerKey PopChildRange();

    private:
        std::string _name;

        Node        _rootNode;

        Node        *_currentNode = nullptr;

        std::vector<RecordingTimer>         _recordingTimers;
        std::unordered_map<size_t, Range>   _ranges;

        std::unordered_map<std::string, Counter> _counters;

        std::hash<std::string>        _hashFunction;
        RecordingTimerType            _timerType;
    };

    using GPURecordingTimerKey = IKey<std::vector<GPURecordingTimer>>;

    struct ScopedCPURange
    {
    public:
        ScopedCPURange(Profiler& profiler, std::string const& name)
            : _profiler(profiler),
            _name(name)
        {
            _profiler.BeginCPURange(_name);
        }

        ~ScopedCPURange()
        {
            _profiler.EndCPURange();
        }

    private:
        Profiler&       _profiler;
        std::string     _name;
    };

    struct ScopedCmdGPURange
    {
    public:
        ScopedCmdGPURange(Profiler& profiler, VKRHI_Device& device, VkCommandBuffer cmdBuffer, std::string const& name)
            : _profiler(profiler),
            _name(name),
            _device(device),
            _cmdBuffer(cmdBuffer)
        {
            _profiler.CmdBeginGPURange(_device, _cmdBuffer, _name);
        }

        ~ScopedCmdGPURange()
        {
            _profiler.CmdEndGPURange(_device, _cmdBuffer, _name);
        }

    private:
        std::string     _name;
        VKRHI_Device&   _device;
        VkCommandBuffer _cmdBuffer;
        Profiler&       _profiler;
    };

    struct ProfilerFrameDump
    {
        //TODO dump more scopes
        RecordingScopeDump cpuDump;
    };

    struct ProfilerFrame
    {
        uint64_t frameStart;

        RecordingScope cpuScope = RecordingScope(RecordingTimerType::CPU);
        RecordingScope gpuScope = RecordingScope(RecordingTimerType::GPU);

        size_t                       queryPairID;
        std::vector<VKRHI_QueryPool> queryPools;
    };

public:
    static Profiler& Get() 
    { 
        static Profiler profiler;
        return profiler;
    }

    Profiler() : _frameBuffer(3){}

    void Init();
    void Shutdown();

    FrameHandle NewFrame();

    void StartRecord(FrameHandle frameHandle);
    void EndRecord();

    void BeginCPURange(std::string const& name);
    void EndCPURange();

    // Set the counter to value
    void CounterSet(std::string const& name, int64_t value);
    // Create a new counter to delta if it doesnt exist, else add delta to its value
    void CounterAdd(std::string const& name, int64_t delta);

    void CmdBeginGPURange(VKRHI_Device const& device, VkCommandBuffer buffer, std::string const& name, VkPipelineStageFlagBits stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT);
    void CmdEndGPURange(VKRHI_Device const& device, VkCommandBuffer buffer, std::string const& name, VkPipelineStageFlagBits stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT);

    void ResolveGPURanges(FrameHandle frameHandle, VKRHI_Device const& device);

    ProfilerFrameDump PopFrame();

private:
    size_t GetPoolID(size_t recRangeID) { return recRangeID / gpuQueryPoolSize; }
    size_t GetCurrentFramePoolOrCreate(VKRHI_Device const& device, size_t recRangeID);

    ProfilerFrame& GetFrame(FrameHandle frameHandle) { return _frameBuffer[static_cast<size_t>(frameHandle)]; }
    ProfilerFrame const& GetFrame(FrameHandle frameHandle) const { return _frameBuffer[static_cast<size_t>(frameHandle)]; }
    ProfilerFrame& GetCurrentFrame();

    ProfilerFrameDump GetDumpFromFrame(FrameHandle handle) const;

private:
    FrameHandle _recordingFrame = -1;
    RingBuffer<ProfilerFrame> _frameBuffer;

private:
    // Must be pair
    static constexpr size_t gpuQueryPoolSize = 100;
    static_assert(gpuQueryPoolSize % 2 == 0);
};

#define KU_CPU_SCOPED_QUERY(name) Profiler::ScopedCPURange scopedCPUrange_##name(Profiler::Get(), #name)
#define KU_CMD_GPU_SCOPED_QUERY(device, buffer, name) Profiler::ScopedCmdGPURange scopedGPUrange_##name(Profiler::Get(), device, buffer, #name)
