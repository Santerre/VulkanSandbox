#pragma once

#ifdef _DEBUG

#include <assert.h>

#define KU_DEBUG_BREAK() __debugbreak()

#define KU_ASSERT(expr) (!!(expr) || (KU_DEBUG_BREAK(), false))
#define KU_VERIFY(expr) KU_ASSERT(expr)

#else //_DEBUG

#define KU_ASSERT(expr) ((void)0)
#define KU_VERIFY(expr) (expr)

#endif 

#define KU_UNIMPLEMENTED_EXCEPTION() KU_ASSERT(0);
