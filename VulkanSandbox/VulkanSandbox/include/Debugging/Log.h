#pragma once

#include <iostream>
#include <string>
#include <sstream>

#define DEF_LOG_CATEGORY(categoryName) struct Log##categoryName { static const char* GetName() { return #categoryName; } }

DEF_LOG_CATEGORY(Temp);

class Logger
{
public:
	template<class T, class... Args>
	static void LogError(const char * format, Args... args)
	{
		std::cerr << "Error   | " << FormatLog<T>(format, args...) << std::endl;
	}

	template<class T, class... Args>
	static void LogWarning(const char* format, Args... args)
	{
		std::cout << "Warning | " << FormatLog<T>(format, args...) << std::endl;
	}

	template<class T, class... Args>
	static void Log(const char* format, Args... args)
	{
		std::cout << "Log     | " << FormatLog<T>(format, args...) << std::endl;
	}

	template<class T, class... Args>
	static void LogVerbose(const char* format, Args... args)
	{
		std::cout << "Verbose | " << FormatLog<T>(format, args...) << std::endl;
	}

private:
	template<class T, class... Args>
	static std::string FormatLog(const char* format, Args... args)
	{
		static const size_t bufferCount = 512;
		char text[bufferCount];

		snprintf(text, bufferCount, format, args...);
		std::stringstream ss;
		ss << T::GetName() << " : " << text;
		return ss.str();
	}
};

#if _DEBUG

#define ELOG(category, format, ...) Logger::LogError<category>(format, __VA_ARGS__)
#define WLOG(category, format, ...) Logger::LogWarning<category>(format, __VA_ARGS__)
#define LOG(category, format, ...) Logger::Log<category>(format, __VA_ARGS__)
#define VLOG(category, format, ...) Logger::LogVerbose<category>(format, __VA_ARGS__)

#else //_DEBUG

#define ELOG(category, format, ...)(void)0
#define WLOG(category, format, ...)(void)0
#define LOG(category, format, ...) (void)0
#define VLOG(category, format, ...)(void)0

#endif //_DEBUG
