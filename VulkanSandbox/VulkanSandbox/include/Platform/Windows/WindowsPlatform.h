#pragma once

#include <string>

class WindowsPlatform
{
public:
	static class Path SolveFilePath(class Path const& path);
	static bool CreateDirectory(class Path const& path);

    static uint64_t GetCPUTimestamp();
    static uint64_t GetCPUFrequency();
};