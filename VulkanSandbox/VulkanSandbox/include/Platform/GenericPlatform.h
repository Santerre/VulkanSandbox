#pragma once

#include <string>

#include "Core/Path.h"
#include "Debugging/Assert.h"

#include "Platform/Windows/WindowsPlatform.h"

using Platform = WindowsPlatform;


class GenericPlatform
{
public:
	static Path SolveFilePath(Path const& path);
	static bool CreateDirectory(Path const& path);
};
