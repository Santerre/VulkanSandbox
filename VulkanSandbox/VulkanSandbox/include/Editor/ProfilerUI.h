#pragma once

#include <vector>
#include <set>
#include <functional>

#include "Memory/Key.h"
#include "Debugging/Profiler.h"

#include "Memory/Handle.h"

class ProfileData;
class ResourceMgr;

class ProfilerFrameView
{
public:
    ProfilerFrameView(Handle<ProfileData> targetedData, ResourceMgr* resourceManager);

    void Draw();

    void SetPath(std::string const& path) { _path = path; }
    std::string const& GetLastSavedPath() const { return _path; }
    std::string const& GetPath() const { return _path; }

private:
    void DrawNode(Profiler::RecordingScopeDump const& scope, Profiler::RecordingScopeDump::NodeDump const& nodeDump, bool bIsRoot);
    void DrawCounters(Profiler::RecordingScopeDump const& scope);

private:
    using RangeKey = IKey<std::vector<Profiler::Range>>;

    ResourceMgr*        _resourceManager = nullptr;

    std::string         _path;
    std::string         _lastSavedPath;
    Handle<ProfileData> _targetedData;
};

class ProfilerUI
{
public:
    struct InitDesc
    {
        ResourceMgr*            resourceManager = nullptr;
        std::function<void()>   onRequestStartProfiling;
        std::function<void()>   onRequestEndProfiling;
    };

    void FromDesc(InitDesc const& desc);
    void Shutdown();

    void Update(bool bProfilerRecording);
    void Draw();

    void AddDisplayedProfileData(Handle<ProfileData> frame);

    void AskStartProfiling(int32_t frameCount);
    void AskEndProfiling();

    bool IsOpen() const { return _isOpen; }
    void SetOpen(bool bOpen) { _isOpen = bOpen; }

private:
    std::function<void()>           _onRequestStartProfiling;
    std::function<void()>           _onRequestEndProfiling;

    int32_t                         _recordFrameLeft = 0;
private:
    bool                            _isOpen = false;

    Profiler const*                 _profiler;

    int32_t                         _currentIndex = -1;

    ResourceMgr*                    _resourceManager = nullptr;
    std::vector<ProfilerFrameView>  _views;

    std::string                     _profileFrameDefaultPath;
};
