#pragma once

#include <vector>
#include <memory>

#include "Memory/Handle.h"
#include "Resource/Resource.h"
#include "Resource/ResourceMgr.h"

// Default resources
#include "imgui.h"

//Shaders
#include "Resource/Shader.h"
#include "Resource/DynamicShader.h"

//Textures
#include "Resource/Texture.h"


class AResourceView;

class ResourceWindow
{
public:
    ResourceWindow(std::shared_ptr<AResourceView> ptr)
    : _resourceView(ptr)
    {}
 
    void Shutdown();
    void Draw();


    void Open() { _isOpened = true; }
    void Close() { _isOpened = false; }
    bool IsOpen() const { return _isOpened; }

    std::shared_ptr<AResourceView> GetResourceView() const { return _resourceView; }

private:
    bool                            _isOpened = false;
    std::shared_ptr<AResourceView>  _resourceView;
};

class ResourceEditor
{
public:
    struct ShowResourceQuery
    {
        std::shared_ptr<IResourceWrapper> resourceWrapper;
    };

public:
    void Init(ResourceMgr* resMgr);
    void Shutdown();

    void LoadResources(std::vector<std::shared_ptr<IResourceWrapper>> const& resources);
    // Add a resource to the list, will not fail if the resource already exists.
    void AddResource(std::shared_ptr<IResourceWrapper> resource);

public:
    bool Draw();

    bool AreResourcesLoaded() { return _viewedResources.size(); }

    bool ShowResourceView(Handle<Resource> res, bool bShow);

    void QueryShowResource(std::shared_ptr<IResourceWrapper> wrapper);
    void ProcessQueries();

private:
    void UpdateResourceViews(std::vector<std::shared_ptr<IResourceWrapper>> const& addedRes, std::vector<std::shared_ptr<IResourceWrapper>> const& removedRes);
    void CreateResourceViews(std::vector<std::shared_ptr<IResourceWrapper>> const& addedRes);

    void DeleteResourceViews();
    void DeleteResourceViews(std::vector<std::shared_ptr<IResourceWrapper>> const& removedRes);

    std::vector<ResourceWindow> _resourceWindows;
    std::vector<std::shared_ptr<IResourceWrapper>> _viewedResources;

private:
    bool                            _isOpened = false;
    ResourceMgr*                    _resManager;
    std::vector<ShowResourceQuery>  _showResourceQueries;
};

struct InitContext
{
    ResourceEditor* resEditor;
    ResourceMgr* resManager;
};

class AResourceView
{
public:
    virtual bool Init(InitContext const& context, std::shared_ptr<IResourceWrapper> resource)
    {
        _viewedResource = resource;
        return true;
    }

    virtual void Draw() = 0;

    virtual void Shutdown() {}

    std::shared_ptr<IResourceWrapper> GetResource() const { return _viewedResource; }

    template<EResourceType ET>
    auto GetResourceAs() { return _viewedResource->GetResource(); }

private:
    std::shared_ptr<IResourceWrapper> _viewedResource;
};

template<EResourceType>
class ResourceView : public AResourceView
{
    virtual bool Init(InitContext const& context, std::shared_ptr<IResourceWrapper> resource) override
    {
        return AResourceView::Init(context, resource);
    }

    virtual void Draw() override
    {
        /// TODO the name may not be unique
        ImGui::Text("No editor implemented yet.");
    }
};

template<>
class ResourceView<EResourceType::GLSLShader> : public AResourceView
{
public:
    virtual bool Init( InitContext const& context, std::shared_ptr<IResourceWrapper> resource ) override
    {
        if (!AResourceView::Init(context, resource))
            return false;

        _resEditor = context.resEditor;
        _resManager = context.resManager;

        Handle<GLSLShader> res = GetResourceAs<EResourceType::GLSLShader>();
        shaderSource.reserve(res->GetShaderData().size() * 4);
        shaderSource = res->GetShaderData();

        _bModified = false;
        _bNeedSave = false;

        return true;
    }

    virtual void Draw() override
    {
        ImGui::Text("GLSLShader");
        std::shared_ptr<ResourceWrapper<GLSLShader>> wrapper = std::static_pointer_cast<ResourceWrapper<GLSLShader>>(GetResource());
        Handle<GLSLShader> res = GetResourceAs<EResourceType::GLSLShader>();
        
        if (_bModified)
        {
            if (ImGui::Button( "Apply" ))
            {
                res->SetShaderData(shaderSource);
                _bModified = false;
            }
        }

        if (_bNeedSave)
        {
            if (ImGui::Button( "Save" ))
            {
                res->SetShaderData(shaderSource);
                _bModified = false;
                if (_resManager->SaveResource(res))
                    _bNeedSave = false;
            }
        }

        if ( ImGui::Button( "Open DynamicShader" ) )
        {
            Path dynamicShaderPath = Path(DynamicShader::GetDefaultNameFromGLSL(wrapper));
            KU_ASSERT(_resManager);
            auto dynamicWrapper = _resManager->GetWrapper<DynamicShader>(dynamicShaderPath.GetString());
            if (!dynamicWrapper)
            {
                auto dynamicShader = _resManager->CreateResourceWrapper<DynamicShader>();
                ResourceInitContext context;
                context.application = res->GetApplication();
                dynamicShader->GetTrueResource()->FromGLSLShader(wrapper, context);
                _resManager->RegisterResource(dynamicShaderPath.GetString(), dynamicShader);
            }
            _resEditor->QueryShowResource(dynamicWrapper);
        }

        if (ImGui::InputTextMultiline("Source", shaderSource.data(), shaderSource.capacity(), ImVec2(600, 500)))
        {
            shaderSource.resize(strlen(shaderSource.data()));
            _bModified = true;
            _bNeedSave = true;
        }
    }

private:
    ResourceEditor*  _resEditor;
    ResourceMgr* _resManager;

    bool _bModified = false;
    bool _bNeedSave = false;

    std::string shaderSource;
};

template<>
class ResourceView<EResourceType::SPVShader> : public AResourceView
{
    virtual bool Init(InitContext const& context, std::shared_ptr<IResourceWrapper> resource) override
    {
        _resEditor = context.resEditor;
        _resManager = context.resManager;
        return AResourceView::Init(context, resource);
    }

    virtual void Draw() override
    {
        ImGui::Text("SPVShader");
        std::shared_ptr<IResourceWrapper> wrapper = GetResource();
        Handle<SPVShader> res = GetResourceAs<EResourceType::SPVShader>();

        if (ImGui::Button( "Open original shader" ))
        {
            _error = "";

            Path path = wrapper->GetResourcePath();
            if (!path.IsValid())
            {
                _error = "Cannot retrieve original shader";
            }
            else
            {
                Path glslPath = path.ChangeExtension( GLSLShader::GetShaderDefaultExtension(res->GetShaderType()));
                KU_ASSERT(_resManager);
                auto wrapper = _resManager->GetWrapper<GLSLShader>(glslPath.GetString());
                _resEditor->QueryShowResource(wrapper);
            }
        }

        if (!_error.empty())
            ImGui::Text(_error.c_str());
    }

private:
    ResourceEditor*  _resEditor;
    ResourceMgr* _resManager;

    std::string     _error = "";
};

template<>
class ResourceView<EResourceType::DynamicShader> : public AResourceView
{
public:
    virtual bool Init(InitContext const& context, std::shared_ptr<IResourceWrapper> resource) override
    {
        _resEditor = context.resEditor;
        _resManager = context.resManager;

        Handle<DynamicShader> res = resource->GetResource();
        KU_ASSERT(res && res->GetTargetShader());
        shaderSubview.Init(context, res->GetTargetShader());

        return AResourceView::Init(context, resource);
    }

    virtual void Draw() override
    {
        ImGui::Text("DynamicShader");

        std::shared_ptr<IResourceWrapper> wrapper = GetResource();
        Handle<DynamicShader> res = GetResourceAs<EResourceType::DynamicShader>();

        if (ImGui::Button( "Save" ))
            _resManager->SaveResource(res);

        if ( ImGui::Button( "Open base shader" ) )
        {
            _resEditor->QueryShowResource(res->GetTargetShader());
        }

        std::map<std::string, std::string> attributeChange;

        bool requireUpdate = false;
        
        if (ImGui::TreeNode("Attributes"))
        {
            std::map<std::string, std::string> newMap;
            auto const& attributes = res->GetShaderAttributes();
            {
                if ( ImGui::Button( "New Attribute" ) )
                {
                    int i = 0;
                    std::string name = "NEW_ATTRIBUTE";
                    while (attributes.find(name) != attributes.end())
                    {
                        name = "NEW_ATTRIBUTE" + std::to_string(i);
                        i++;
                    }
                    newMap[name] = "";
                    requireUpdate = true;
                }

                int i = 0;
                for (auto&& [name, value] : attributes)
                {
                    ImGui::BeginGroup();
                    ImGui::PushID(i);
                    std::string tempName = "";
                    {
                        tempName.reserve(255);
                        tempName = name;
                        if (ImGui::InputText("Name", tempName.data(), tempName.capacity()))
                            requireUpdate = true;
                        tempName.resize(strlen(tempName.data()));
                    }
                    ImGui::SameLine();

                    std::string tempValue = "";
                    {
                        tempValue.reserve(255);
                        tempValue = value;
                        if (ImGui::InputText("Value", tempValue.data(), tempValue.capacity()))
                            requireUpdate = true;
                        tempValue.resize(strlen(tempValue.data()));
                    }

                    if (ImGui::Button("-"))
                        requireUpdate = true;
                    else
                        newMap[tempName] = tempValue;
                    ImGui::EndGroup();
                    ImGui::PopID();
                    i++;

                }

                if (requireUpdate)
                    res->SetShaderAttributes(std::move(newMap));
            }
            ImGui::TreePop();
        }

        if (ImGui::TreeNode("Prefix String"))
        {
            int32_t prependPatchMeshDepth = res->GetPrependPatchMeshDepth();
            if (ImGui::InputInt("prependPatchMeshDepth", &prependPatchMeshDepth))
                res->SetPrependPatchMeshDepth(prependPatchMeshDepth);
            ImGui::TreePop();
        }

        if ( ImGui::TreeNode( "GLSL shader" ) )
        {
            shaderSubview.Draw();
            ImGui::TreePop();
        }
    }

    virtual void Shutdown() override
    {
        shaderSubview.Shutdown();

        AResourceView::Shutdown();
    }

private:
    ResourceEditor* _resEditor;
    ResourceMgr*    _resManager;

    ResourceView<EResourceType::GLSLShader> shaderSubview;
};

template<>
class ResourceView<EResourceType::Texture> : public AResourceView
{
    virtual void Draw() override
    {
        ImGui::Text("Texture");
    }
};

template<>
class ResourceView<EResourceType::Image> : public AResourceView
{
    virtual void Draw() override
    {
        ImGui::Text("Image");
    }
};