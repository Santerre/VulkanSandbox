#pragma once

#include <map>

#include "IteratorUtils.h"

template <class Key1, class Key2, class Value>
class TwoWayMap
{
public:
	constexpr bool Add(Key1 const& key, Key2 const& key2, Value&& value)
	{
		auto[prioContextIt, emplace1IsSuccess] = _mainMap.emplace(key2, std::move(value));
		if (!emplace1IsSuccess)
			return false;

		auto[nameContextIt, emplace2IsSuccess] = _redirectMap.emplace(key, key2);
		if (!emplace2IsSuccess)
			return false;
		return true;
	}

	constexpr bool Add(Key1 const& key, Key2 const& key2, Value const& value)
	{
		auto[prioContextIt, emplace1IsSuccess] = _mainMap.emplace(key2, value);
		if (!emplace1IsSuccess)
			return false;

		auto[nameContextIt, emplace2IsSuccess] = _redirectMap.emplace(key, key2);
		if (!emplace2IsSuccess)
			return false;
		return true;
	}

	Value const& at(Key1 key) const { return _mainMap.at(_redirectMap.at(key)); }
	Value const& at(Key2 key) const { return _mainMap.at(key); }

	Value& at(Key1 key) { return _mainMap.at(_redirectMap.at(key)); }
	Value& at(Key2 key) { return _mainMap.at(key); }

	Value& operator[](Key1 key) { return _mainMap[_redirectMap[key]]; }
	Value& operator[](Key2 key) { return _mainMap[key]; }

	ValueMemberIterator<Key2, Value> begin() { return ValueMemberIterator<Key2, Value>(_mainMap.begin()); }
	ValueMemberIterator<Key2, Value> end() { return ValueMemberIterator<Key2, Value>(_mainMap.end()); }

private:
	std::map<Key1, Key2>	_redirectMap;
	std::map<Key2, Value>	_mainMap;
};
