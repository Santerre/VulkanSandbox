#pragma once

#include <vector>

template<class T>
class BufferView
{
public:
    BufferView(std::vector<char>& source) :
        _memory(source.data()),
        _size(source.size())
    {
    }

    BufferView(char* memory, size_t size) :
        _memory(memory),
        _size(size)
    {
    }

    template<class... Args>
    T& Construct(size_t index, Args&&... args)
    {
        return *(new(&Get(index)) T(std::forward<Args>(args)...));
    }

    template<class... Args>
    void Destroy(size_t index)
    {
        Get(index).~T();
    }

    T& Get(size_t i) { return *reinterpret_cast<T*>(_memory + i * sizeof(T)); }
    T const& Get(size_t i) const { return *reinterpret_cast<T*>(_memory + i * sizeof(T)); }

    T& operator[](size_t i) { return Get(i); }
    T const& operator[](size_t i) const { return Get(i); }

private:
    char *_memory = nullptr;
    size_t _size = 0;
};
