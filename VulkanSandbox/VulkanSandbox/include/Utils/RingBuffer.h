#pragma once

#include <vector>
#include "Debugging/Assert.h"

#include "Utils/BufferView.h"

template<typename T>
class RingBuffer final
{
public:
    RingBuffer(size_t reserveSize);
    ~RingBuffer();

    RingBuffer(RingBuffer const& other) = delete;
    RingBuffer(RingBuffer&& other) = delete;

    RingBuffer& operator=(RingBuffer const& other) = delete;
    RingBuffer& operator=(RingBuffer&& other) = delete;

    template<class... Args>
    T& Push(Args&&... args);
    void Pop();

    size_t GetHeadID() const { return Loop(static_cast<int>(_lastID) - 1); }
    size_t GetTailID() const { return _firstID; }
    size_t Size() const;

    T& operator[](size_t id) { return _view[Loop(id)]; }
    T const& operator[](size_t id) const { return _view[Loop(id)]; }

private:
    size_t Loop(int id) const;
    size_t Loop(size_t id) const;

    size_t _lastID = 0;
    size_t _firstID = 0;
    size_t _reserveSize;

    std::vector<char> _buffer;
    BufferView<T> _view;
};

template<typename T>
RingBuffer<T>::RingBuffer(size_t reserveSize):
    _reserveSize(reserveSize),
    _buffer(reserveSize * sizeof(T)),
    _view(_buffer)
{
}

template<typename T>
RingBuffer<T>::~RingBuffer()
{
    while(Size())
        Pop();
}

template<typename T>
size_t RingBuffer<T>::Size() const
{
    if (_firstID > _lastID)
        return _reserveSize - (_firstID - _lastID);
    else
        return _lastID - _firstID;
}

template<typename T>
size_t RingBuffer<T>::Loop(size_t id) const 
{ 
    return id % _reserveSize; 
}

template<typename T>
size_t RingBuffer<T>::Loop(int id) const
{
    int res = id % static_cast<int>(_reserveSize);
    return static_cast<size_t>(res >= 0 ? res : res + _reserveSize);
}

template<typename T>
template<class... Args>
T& RingBuffer<T>::Push(Args&&... args)
{
    auto&& object = _view.Construct(_lastID, std::forward<Args>(args)...);
    _lastID = Loop(++_lastID);
    KU_ASSERT(_firstID != _lastID);

    return object;
}

template<typename T>
void RingBuffer<T>::Pop()
{
    KU_ASSERT(_firstID != _lastID);
    size_t temp = _firstID;
    _firstID = Loop(++_firstID);
    _view.Destroy(temp);
}
