#pragma once

#include <array>
#include <functional>

#include "Debugging/Assert.h"

#include "Memory/Allocator.h"

namespace QuadTreeUtils
{

struct Key
{
    uint8_t depth;
    uint32_t path;

    Key() = default;

    Key(uint32_t _path, uint8_t _depth)
    :   path(_path),
        depth(_depth)
    {
    }

    operator uint64_t() const
    {
        uint64_t res = depth;
        res = (res << uint64_t(32u)) | static_cast<uint64_t>(path);
        return res;
    }

    bool operator==(Key const& other) const
    {
        return depth == other.depth && path == other.path;
    }

    static uint8_t RetrieveLocalPosition(uint32_t _path, uint8_t _depth)
    {
        return (_path >> ((15u - _depth) * 2u)) & 0b11u;
    }

    uint8_t RetrieveLocalPosition(uint8_t _depth) const
    {
        return RetrieveLocalPosition(path, _depth);
    }
};

template<class Data>
struct NodeData
{
    Key key;
    Data customData;
};

template<class Data, int nodeCount>
struct TreeBlock;

template<class Data, int nodeCount>
struct Node
{
    NodeData<Data>			                    data;
    bool			                            bHasChild = false;
    Handle<TreeBlock<Data, nodeCount>>	        block = nullptr;
};

template<class Data, int nodeCount>
struct TreeBlock
{
    TreeBlock()
        : childs{}
    {

    }

    using Node = Node<Data, nodeCount>;
    union
    {
        Node childs[nodeCount];

        struct
        {
            Node top;
            Node left;
            Node down;
            Node right;
        };
    };
};

} // namespace QuadTree

template <typename QuadTreeDescriptor>
struct QuadTreeTrait
{
    using TreeBlock = QuadTreeUtils::TreeBlock<typename QuadTreeDescriptor::NodeData, 4>;
    using Node = QuadTreeUtils::Node<typename QuadTreeDescriptor::NodeData, 4>;
    using NodeData = QuadTreeUtils::NodeData<typename QuadTreeDescriptor::NodeData>;
    using Key = typename QuadTreeUtils::Key;
};

/// The QuadTreeDescriptor is a structure used to define the quad tree.
/// It must contain at least the following functions
/// TODO
/// 
///
/// The Quad tree node child index are define as
///     | 0 | 1 |
///     | 2 | 3 |
template <typename QuadTreeDescriptor>
class QuadTree
{
    // Structs
public:
    using Trait = QuadTreeTrait<QuadTreeDescriptor>;

    static constexpr int childCount = 4;

    struct ConstIterator
    {

    public:
        ConstIterator() = default;
        ~ConstIterator() = default;

        ConstIterator(ConstIterator const& other) = default;
        ConstIterator& operator=(ConstIterator const& other) = default;

        ConstIterator(QuadTree<QuadTreeDescriptor> const* tree)
        {
            _quadTree = tree;
            _node = tree->GetNodeAt(0u);
        }

        ConstIterator(QuadTree<QuadTreeDescriptor> const* tree, typename Trait::Key key)
        {
            _quadTree = tree;
            _node = tree->GetNodeAt(key);
        }

        bool operator==(ConstIterator const& other) const
        {
            return _node == other._node;
        }

        bool operator!=(ConstIterator const& other) const
        {
            return !(*this == other);
        }

        typename Trait::NodeData const& operator*() const
        {
            KU_ASSERT(_node);

            return _node->data;
        }

        typename Trait::NodeData const& operator->() const
        {
            return *this;
        }

        ConstIterator const& operator++()
        {
            KU_ASSERT(_node);

            typename Trait::Key currentKey = _node->data.key;
            KU_ASSERT(currentKey.depth <= GetMaxDepth());

            uint8_t insertLoc = (GetMaxDepth() - currentKey.depth) * 2;
            // The static cast are a trix to fix the fact that if insertLoc > sizeof(1) is 1 << insertLoc gives an undefined behavior
            currentKey.path += static_cast<decltype(currentKey.path)>(static_cast<uint64_t>(1) << insertLoc);
            KU_ASSERT((currentKey.path & (~0 << insertLoc)) == currentKey.path);

            // No next node (restarted from begining)
            if (!currentKey.path)
            {
                Clear();
                return *this;
            }

            _node = _quadTree->GetNodeAt(currentKey.path);;
            return *this;
        }

        void Clear()
        {
            _node = nullptr;
            _quadTree = nullptr;
        }

    private:
        QuadTree<QuadTreeDescriptor> const* _quadTree;

        typename Trait::Node const* _node = nullptr;
    }; // struct ConstIterator


    // Methods
public:
    QuadTree() = default;

    QuadTree(QuadTreeDescriptor const& descriptor)
    {
        _descriptor = descriptor;

        InitNode(_root);
    }

    QuadTree(QuadTreeDescriptor&& descriptor)
    {
        _descriptor = descriptor;

        InitNode(_root);
    }

    virtual ~QuadTree()
    {
        Clear();
    }

    static constexpr uint8_t GetMaxDepth()
    {
        // Path is composed of pair of bits, max depth is the count of bit pairs in the path.
        return (sizeof(Trait::Key::path)  * 8u) / 2u;
    }

    void Clear()
    {
        ClearNode(_root);
    }

    void Evaluate()
    {
        _leafCount = 1;
        EvaluateNode(_root);
    }

    int GetLeafCount() const { return _leafCount; }

    QuadTreeDescriptor& GetDescriptor() { return _descriptor; }
    QuadTreeDescriptor const& GetDescriptor() const { return _descriptor; }

public:
    ConstIterator cbegin() const
    {
        return ConstIterator(this);
    }

    ConstIterator cend() const
    {
        return ConstIterator();
    }

    ConstIterator begin() const
    {
        return ConstIterator(this);
    }

    ConstIterator end() const
    {
        return ConstIterator();
    }

private:

    void EvaluateNode(typename Trait::Node& node, int depth = 0)
    {
        node.bHasChild = !_descriptor.EvaluateNode(node);
        if (node.bHasChild)
        {
            if (!node.block)
                AllocNode(node);

            // If the node is successfully evaluated, the 4 childs become leaf, minus the current node no longer a leaf. 
            _leafCount += 3;

            for (int i = 0; i < childCount; i++)
                EvaluateNode(node.block->childs[i], depth + 1);
        }
    }

    static bool GetNeighbourPosition(uint8_t position, uint8_t neighbour, uint8_t& outNeighbourPosition)
    {
        glm::ivec2 vp(position % 2, position / 2);

        static const glm::ivec2 dirs[] = 
        {
            glm::ivec2(-1, 0),
            glm::ivec2(0,    -1),
            glm::ivec2(1, 0),
            glm::ivec2(0, 1),
        };
        glm::ivec2 tvp = vp + dirs[neighbour];

        outNeighbourPosition = glm::mod(tvp.x, 2) + glm::mod(tvp.y, 2) * 2;

        int globalRes = tvp.x + tvp.y * 2;
        return globalRes > 3 || globalRes < 0;
    }

    // returns the deepest evaluated node at the specified path, without going deeper that depth. 
    // That means that the node found may have a different key than the one specified.
    typename Trait::Node const* GetNodeAt(typename Trait::Key key) const
    {
        typename Trait::Node const* node = &_root;
        for (uint8_t i = 0; i < key.depth; i++)
        {
            if (!node->bHasChild)
                return node;

            node = &node->block->childs[key.RetrieveLocalPosition(i)];
            KU_ASSERT(node);
        }
        return node;
    }


    // returns the evaluated node at the specified path. 
    typename Trait::Node const* GetNodeAt(uint32_t path) const
    {
        typename Trait::Key key(path, GetMaxDepth());

        return GetNodeAt(key);
    }

    typename Trait::Node const* GetNeighbour(typename Trait::Key key, uint8_t neighbour) const
    {
        typename Trait::Key tempKey(key);
        bool bIsOut = true;
        while (tempKey.depth && bIsOut)
        {
            uint8_t pos = RetrieveLocalPosition(tempKey.path, tempKey.depth);
            uint8_t nLocalPos;
            bIsOut = GetNeighbourPosition(pos, neighbour, /*out*/nLocalPos);
            KU_ASSERT(nLocalPos < 4u);
            uint8_t insertLocation = ~tempKey.depth * 2u;

            tempKey.path = tempKey.path & !(0b11u << insertLocation) | (nLocalPos << insertLocation);

            tempKey.depth--;
        }

        if (bIsOut)
            return nullptr;

        typename Trait::Key finalKey;
        finalKey.path = tempKey.path;
        finalKey.depth = key.depth;

        return GetNodeAt(finalKey);
    }

    //                   |1|
    // Neighbours ids: |0|x|2|
    //                   |3|
    typename Trait::Node const& GetNeighbour(typename Trait::Node const& in, int id) const
    {
        return GetNeighbour(in.key, id);
    }

    void InitNode(typename Trait::Node& node, typename Trait::Key key)
    {
        node.data.key = key;
        _descriptor.InitNode(node);
    }

	void AllocNode(typename Trait::Node& node)
	{
		node.block = _descriptor.AllocateBlock();
        for (int i = 0; i < childCount; i++)
            InitNode(node.block->childs[i], ComputeKey(node.data.key, i));
	}

	void ClearNode(typename Trait::Node& node)
	{
		if (!node.block)
			return;

		for (int i = 0; i < childCount; i++)
			ClearNode(node.block->childs[i]);

        node.bHasChild = false;
        _descriptor.DestroyBlock(node.block);
	}

    static typename Trait::Key ComputeKey(typename Trait::Key parentKey, uint8_t index)
    {
        KU_ASSERT(index < 4);
        uint8_t depth = parentKey.depth + 1;
        return Trait::Key(parentKey.path | static_cast<uint32_t>(index) << (GetMaxDepth() - depth) * 2, depth);
    }

    // Members
private:
    typename Trait::Node _root;
    QuadTreeDescriptor _descriptor;

    int _leafCount = 1;
};


/*static int table[][10];

template<class T, int Depth>
class QuadTree
{
	using QuadTreeUtils;

	T const& GetValue() const
	{
		return _subtrees[0].GetValue();
	}

	void Evaluate(std::function<bool(int coordX, int coordY)> function)
	{
		EvaluateInternal(function, 0, 0);
	}

	void Evaluate(std::function<bool(int coordX, int coordY)> function)
	{
		ForeachEval(function, 0, 0);
	}

	bool IsLeaf() const
	{
		return _isLeaf;
	}

private:

	bool EvaluateInternal(std::function<bool(int coordX, int coordY)> function, int coordX, int coordY)
	{
		_isLeaf = !function(coordX, coordY);
		if (!_isLeaf)
		{
			for (int i = 0; i < 4; i++)
				_subtree[i].EvaluateInternal(function, coordX + ((2 * (i % 2)) << Depth), coordY + ((2 * (i / 2)) << Depth));
		}

		return _isLeaf;
	}

	void ForeachEvalInternal(std::function<void(int coordX, int coordY)> function, int coordX, int coordY)
	{
		if (!_isLeaf)
		{
			for (int i = 0; i < 4; i++)
				_subtreeDoForeachEvaluated(function, coordX + ((2 * (i % 2)) << Depth), coordY + ((2 * (i / 2)) << Depth));
		}
	}

private:
	bool _isLeaf = false;
	typename std::array<typename QuadTree<typename T, Depth - 1>, 4> _subtrees;
};

template<class T>
class QuadTree<T, 0>
{
	T leaf;

	bool IsLeaf() const
	{
		return true;
	}

	void EvaluateInternal(std::function<bool(int coordX, int coordY)> function, int coordX, int coordY)
	{
		return false;
	}

	T const& GetValue() const
	{
		return leaf;
	}
};

template<class T, int Depth>
struct QuadTreeUtils
{
	static constexpr float GetSideSize<T, Depth>() { return 2 << Depth; }
};
*/
