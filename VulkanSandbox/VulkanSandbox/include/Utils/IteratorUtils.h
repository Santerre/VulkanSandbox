#pragma once

#include <iterator>
#include <map>
#include <functional>

#include <Debugging/Assert.h>

template<class Child, class Iterator, class IteratorTrait = typename std::iterator_traits<Iterator>>
class DereferenceIteratorBase
{
public:
	typedef Iterator iterator_type;
	typedef typename IteratorTrait::iterator_category		iterator_category;
	typedef typename IteratorTrait::value_type				value_type;
	typedef typename IteratorTrait::difference_type			difference_type;

	DereferenceIteratorBase() {}
	explicit DereferenceIteratorBase(Iterator it) : current(it) {}
	DereferenceIteratorBase(const DereferenceIteratorBase& it) : current(it.current) {}
	DereferenceIteratorBase& operator= (const DereferenceIteratorBase& it)
	{
		current = it.current;
	}

	bool operator==(Child const& other) const { return this->current == other.current; }
	bool operator!=(Child const& other) const { return !(*this == other); }

	iterator_type base() const { return current; }
	Child& operator++() { ++current; return *static_cast<Child*>(this); }
	Child& operator--() { --current; return *static_cast<Child*>(this); }
	Child& operator++(int) { Child temp = *static_cast<Child*>(this); ++current; return temp; }
	Child& operator--(int) { Child temp = *static_cast<Child*>(this); --current; return temp; }
	Child operator+ (difference_type n) const { return Child(current + n); }
	Child operator- (difference_type n) const { return Child(current - n); }
	Child operator+= (difference_type n) { current += n; return *static_cast<Child*>(this); }
	Child operator-= (difference_type n) { current -= n; return *static_cast<Child*>(this); }

	difference_type operator-(DereferenceIteratorBase const& other) const { return current - other.current; }

protected:
	Iterator current;
};

// MemberIterator
// Iterator dereferencing on member
template<class Iterator,
	auto typename Iterator::value_type::*member>
	class MemberIterator : public DereferenceIteratorBase<MemberIterator<Iterator, member>, Iterator>
{
public:

	typedef DereferenceIteratorBase<MemberIterator<Iterator, member>, Iterator> base;
	typedef typename decltype((*base::current).*member)								pointer;
	typedef std::add_lvalue_reference_t<typename decltype((*base::current).*member)>	reference;

	MemberIterator() {}
	explicit MemberIterator(Iterator it) : base(it) {}

	pointer operator->() const { return **this; }
	reference operator*() const { return (*base::current).*member; }


	reference operator=(reference ref) { return (*base::current).*member = ref; }
};

template<typename Container, auto member>
using FwdMemberIterator = MemberIterator<typename Container::iterator, member>;

template<typename Container, auto member>
using ConstMemberIterator = MemberIterator<typename Container::const_iterator, member>;

template<typename Key, typename Value>
using KeyMemberIterator = ConstMemberIterator<std::map<Key, Value>, &std::pair<const Key, Value>::first>;

template<typename Key, typename Value>
using ValueConstMemberIterator = ConstMemberIterator<std::map<Key, Value>, &std::pair<const Key, Value>::second>;

template<typename Key, typename Value>
using ValueMemberIterator = FwdMemberIterator<std::map<Key, Value>, &std::pair<const Key, Value>::second>;

// Transform iterator 
// Apply a function when accessed.
template<class Iterator, class Return>
class TransformIterator;

template<class Iterator, class Return>
class TransformIterator<Iterator, Return(Iterator)> : public DereferenceIteratorBase<TransformIterator<Iterator, Return(Iterator)>, Iterator>
{
public:

	TransformIterator() = default;
	explicit TransformIterator(Iterator it, std::function<Return(Iterator)> function) : DereferenceIteratorBase(it), _function(function) {}

	using pointer = Return;
	using reference = std::add_lvalue_reference_t<Return>;
	using base = DereferenceIteratorBase<TransformIterator<Iterator, Return(Iterator)>, Iterator>;

	pointer operator->() const { return **this; }
	reference operator*() const { return function(base::current); }
private:
	std::function<Return(Iterator)> _function;
};

template<class Iterator, class PointerType = typename Iterator::value_type>
struct PointerAssignIteratorTrait
{
	using iterator_category = typename Iterator::iterator_category;
	using value_type = PointerType;
	using difference_type = ptrdiff_t;
};

// PointerIterator 
// during assignement operationm, assigns the pointer to the given value
template<class Iterator, class PointerType = typename Iterator::value_type>
class PointerAssignIterator : public DereferenceIteratorBase<PointerAssignIterator<Iterator, PointerType>, Iterator, PointerAssignIteratorTrait<Iterator, PointerType>>
{
public:

	using Base = DereferenceIteratorBase<PointerAssignIterator<Iterator, PointerType>, Iterator, PointerAssignIteratorTrait<Iterator, PointerType>>;

	using typename Base::iterator_category;
	using typename Base::value_type;
	using typename Base::difference_type;

	PointerAssignIterator() {}
	explicit PointerAssignIterator(Iterator it) : Base(it) {}

	typedef std::add_pointer_t<PointerType>			pointer;
	typedef std::add_lvalue_reference_t<PointerType>	reference;

	PointerAssignIterator* operator->() { return **this; }
	PointerAssignIterator& operator*() { return *this; }

	PointerAssignIterator operator=(std::remove_pointer_t<PointerType>& ref) { *Base::current = &ref; return *this; }
};

// Idle iterator 
// Does nothing, used as placeholder
template<class T>
class IdleIterator
{
public:
	typedef T															iterator_type;
	typedef typename std::random_access_iterator_tag					iterator_category;
	typedef typename T													value_type;
	typedef typename ptrdiff_t											difference_type;

	value_type operator=(value_type const& value) const { return value; }

	template <class Iter> bool operator==(IdleIterator<Iter> const& other) const { return true; }
	template <class Iter> bool operator!=(IdleIterator<Iter> const& other) const { return !(*this == other); }

	IdleIterator& operator*() { return *this; }

	IdleIterator& operator++() { return *this; }
	IdleIterator& operator--() { return *this; }
	IdleIterator& operator++(int) { return *this; }
	IdleIterator& operator--(int) { return *this; }
	IdleIterator operator+ (difference_type n) const { return *this; }
	IdleIterator operator- (difference_type n) const { return *this; }
	IdleIterator operator+= (difference_type n) { return *this; }
	IdleIterator operator-= (difference_type n) { return *this; }
};

template<class T>
class LoopIterator
{
public:
    typedef T															iterator_type;
    typedef typename std::input_iterator_tag					        iterator_category;
    typedef typename T													value_type;
    typedef typename ptrdiff_t											difference_type;

    LoopIterator(T* container, size_t loopID) :
        _container(container),
        _loopID(loopID)
    {}

    // LegacyIterator concept
    LoopIterator() = default;
    LoopIterator(LoopIterator const& other) = default;
    LoopIterator(LoopIterator&& other) = default;

    LoopIterator& operator=(LoopIterator const& other) = default;
    LoopIterator& operator=(LoopIterator&& other) = default;

    typename T::reference operator*() const 
    { 
        KU_ASSERT(_container);
        return _container->at(_id); 
    }

    LoopIterator& operator++()
    { 
        if (++_id == _loopID)
            _id = 0;
        return *this;
    };

    LoopIterator operator+(int val) const
    {
        LoopIterator newIt = *this;
        int res = (static_cast<int>(_id) + val) % static_cast<int>(_loopID);
        newIt._id = res >= 0 ? res : res + _loopID;
        return newIt;
    };

    LoopIterator& operator+=(int val)
    {
        _id = (_id + val) % _loopID;
        return *this;
    }

    LoopIterator operator-(int val) const { return *this + -val; }
    LoopIterator& operator-=(int val) { return *this += -val; }

    // EqualityComparable concept
    bool operator==(LoopIterator const& other) const { return _id == other._id; }
    bool operator!=(LoopIterator const& other) const { return !(*this == other); }

private:
    size_t  _id = 0;
    size_t  _loopID = 1;
    T*      _container = nullptr;
};

template<class T>
struct std::iterator_traits<LoopIterator<T>>
{
    typedef typename LoopIterator<T>::iterator_type					iterator_type;
    typedef typename LoopIterator<T>::iterator_category    			iterator_category;
    typedef typename LoopIterator<T>::value_type    				value_type;
    typedef typename LoopIterator<T>::difference_type    			difference_type;
};