#pragma once

#include <vector>
#include <algorithm>
#include <functional>

template <class Ret, class... Args>
class Delegate;

template <class Ret, class... Args>
class Delegate<Ret(Args...)>
{
public:
	using TFunction = std::function<Ret(Args...)>;

	void AddListener(TFunction function)
	{
		_functionList.push_back(function);
	}

	void RemoveListener(TFunction function)
	{
		auto res = std::find_if(_functionList.begin(), _functionList.end(), [&function](TFunction const& val) -> bool
        {
            return val.target<void>() == function.target<void>();
        });
		if (res != _functionList.end())
			_functionList.erase(res);
	}

	void Invoke(Args... args)
	{
		for (auto function : _functionList)
			function(args...);
	}

private:
	std::vector<TFunction> _functionList;
};

