#pragma once

#include "glm/glm.hpp"
#include "glm/gtc/quaternion.hpp"


struct Transform
{
public:
	glm::vec3 const& GetPosition() const { return _position; }
	glm::vec3 const& GetScale() const { return _scale; }
	glm::quat const& GetRotation() const { return _rotation; }

	void SetPosition(glm::vec3 const& position) { _position = position; }
	void SetScale(glm::vec3 const& scale) { _scale = scale; }
	void SetRotation(glm::quat const& rotation) { _rotation = rotation; }

	glm::mat4 GetTransformMatrix() const;

	void Move(glm::vec3 const& move);
	void RotateAbsolute(glm::vec3 const& rotationAxis, float angle);
	void RotateRelative(glm::vec3 const& rotationAxis, float angle);
	void Rotate(glm::quat const& rotationDelta);
	void Scale(glm::vec3 const& deltaScale);

	void RotationRelativeMove(glm::vec3 const& relativeMove);

	void LookAt(glm::vec3 const& eye, glm::vec3 const& target, glm::vec3 const& up);

	glm::vec3 TransformPoint(glm::vec3 const& position);
	glm::vec3 TransformVector(glm::vec3 const& vector);

private:
	glm::vec3 _position = {};
	glm::vec3 _scale = glm::vec3(1);
	glm::quat _rotation = {};
};