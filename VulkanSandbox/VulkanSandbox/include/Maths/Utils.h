#pragma once

#define UNROLL_VEC2(vec) vec.x, vec.y
#define UNROLL_VEC3(vec) UNROLL_VEC2(vec), vec.z
#define UNROLL_VEC4(vec) UNROLL_VEC3(vec), vec.w

namespace Maths
{
	template <class T>
	constexpr T Sign(T value) 
	{
		if (value > T(0))
			return T(1);
		else if (value < T(0))
			return T(-1);
		else
			return T(0);
	}
}