#pragma once

#include <vector>

struct MemorySlot
{
public:
	std::vector<char> const& GetData() const { return data; };
	std::vector<char>& GetData() { return data; };

	void Internal_Allocate(size_t allocSize);
	template<class T>
	T* Internal_GetDataPointerAs(size_t byteOffset = 0) { return reinterpret_cast<T*>(data.data() + byteOffset); }

private:
	std::vector<char> data;
};
