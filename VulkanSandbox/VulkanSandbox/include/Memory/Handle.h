#pragma once

#include "Debugging/Assert.h"

class IHandle
{
public:
	virtual bool operator==(IHandle* other) = 0;

	virtual void* GetRawPtr() = 0;
};

template <class T>
class Handle : public IHandle
{
public:
	Handle() = default;
    ~Handle() = default;

	Handle(T* ptr) { _ptr = ptr; }

	Handle(Handle const& handle)
	{
		_ptr = handle._ptr;
	}
	Handle(Handle&& handle) = default;

	Handle& operator=(Handle const& handle)
	{
		_ptr = handle._ptr;
		return *this;
	}
	Handle& operator=(Handle&& handle) = default;

	virtual bool operator==(IHandle* other) override
	{
		return GetRawPtr() == other->GetRawPtr();
	}

    bool operator==(Handle const& other) const
	{
		return GetRawPtr() == other.GetRawPtr();
	}

    template<class T>
    operator Handle<T>()
    {
        // Can we avoid the construction?
        return { static_cast<T*>(_ptr) };
    }

	virtual void* GetRawPtr() override
	{
		return reinterpret_cast<void*>(_ptr);
	}
    
	void const* GetRawPtr() const
	{
		return reinterpret_cast<void*>(_ptr);
	}

	operator bool() const
	{
		return !!_ptr;
	}

	T* operator->() const
	{
		KU_ASSERT(_ptr);
		return _ptr;
	}

	T& operator*() const
	{
		return *_ptr;
	}

	T* Get() { return _ptr; }

	void Reset() { _ptr = nullptr; }

private:
	T* _ptr = nullptr;
};

