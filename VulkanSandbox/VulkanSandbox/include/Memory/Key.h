#pragma once

#include "Debugging/Assert.h"

template<class Container, class KeyType>
class Key
{
// Internal
public:
    KeyType Internal_GetKey() const
    {
        return _key;
    }

#ifdef _DEBUG
    Container const*  Internal_GetContainer() const
    {
        return _container;
    }
#endif

public:
	Key() = default;

    template<typename CT>
    Key(KeyType const& key, CT const& container)
		: _key(key)
#ifdef _DEBUG
		, _container(&container)
#endif
	{}

    template<typename Other>
    Key(Other const& other)
        : _key(other.Internal_GetKey())
#ifdef _DEBUG
        , _container(reinterpret_cast<Container const*>(other.Internal_GetContainer()))
#endif
    {}

    // TODO forbid wrong implicit conversion
    /*
    
    Key(Key const& other)
        : _key(other._key)
#ifdef _DEBUG
        , _container(reinterpret_cast<Container const*>(other._container))
#endif
    {}

    Key(Key<void, KeyType> const& other)
        : _key(other._key)
#ifdef _DEBUG
        , _container(reinterpret_cast<Container const*>(other._container))
#endif
    {}*/

#ifdef _DEBUG
    template<typename CT>
	KeyType Get(CT const& container) const
	{ 
		KU_ASSERT(_container == &container);
		return _key;
	}
#else
    template<typename CT>
    KeyType Get(CT const& container) const { return _key; }
#endif

    template<typename CT>
	auto const& Unlock(CT const& container) const
	{
		return container.at(Get(container));
	}

    template<typename CT>
    auto& Unlock(CT& container)
    {
        return container.at(Get(container));
    }

    bool operator==(Key const& other) const
    {
#ifdef _DEBUG
        KU_ASSERT(_container == other._container || _container == nullptr || other._container == nullptr);
#endif
        return _key == other._key;
    }
	
private:
	KeyType _key = KeyType();

#ifdef _DEBUG
	Container const* _container = nullptr;
#endif
};

template<class Container>
using IKey = Key<Container, size_t>;
using VoidKey = Key<void, size_t>;