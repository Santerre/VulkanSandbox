#pragma once

#include "Memory/Handle.h"

#define AllocRes(resourceType) Allocator::GetResourceAllocator()->Allocate<resourceType>()
#define ReleaseRes(resourceHandle) Allocator::GetResourceAllocator()->Destroy(resourceHandle)

class Allocator
{
public:
	static Allocator* GetResourceAllocator();

	template<class T>
	Handle<T> Allocate()
	{
		return Handle(new T);
	}

	template<class T>
	void Destroy(Handle<T> object)
	{
		delete object.Get();
	}
};