#pragma once

#include <map>
#include <vector>

#include "Core/Path.h"

#include "Debugging/Log.h"
#include "Debugging/Assert.h"

#include "Memory/MemorySlot.h"
#include "Resource/Generator/IGenerator.h"

DEF_LOG_CATEGORY(HierarchicalMemory);

#define HM_RES_MESH_PATH "Resources/Meshes"

class HierarchicalMemory
{
public:
	bool Init();
	void Shutdown();

	MemorySlot& GetSlot(Path const& slotPath);

private:
	template<class T>
	void		BindGenerator(Path const& path)
	{
		std::string const& relativePath = path.GetString();
		KU_ASSERT(generators.find(relativePath) == generators.end());
		generators[relativePath] = std::make_unique<T>();
	}

	bool GenerateSlot(Path const& path, MemorySlot& outSlot);

	std::map<std::string, MemorySlot> memory;

	std::map<std::string, std::unique_ptr<IGenerator>> generators;
};