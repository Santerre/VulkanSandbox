#pragma once

#include <istream>
#include <fstream>
#include <memory>

#include "Core/Path.h"

class ILoader
{
public :
	virtual std::istream*	OpenRead() = 0;
	// Closes the read stream. Must do nothing if the stream is not oppened.
	virtual void			CloseRead() = 0;

	virtual std::ostream*	OpenWrite() = 0;
	// Closes the write stream. Must do nothing if the stream is not oppened.
	virtual void			CloseWrite() = 0;

	virtual void			Shutdown();
};

class DiskFileLoader : public ILoader
{
public:
	DiskFileLoader(Path path, bool bResolve = true);

	virtual std::istream*	OpenRead() override;
	virtual void			CloseRead() override;

	virtual std::ostream*	OpenWrite() override;
	virtual void			CloseWrite() override;

private:
	Path _path;
    bool _bResolve = true;

	std::unique_ptr<std::ifstream> _inFile;
	std::unique_ptr<std::ofstream> _outFile;
};

class DummyLoader : public ILoader
{
public:
	virtual std::istream*	OpenRead() override;
	virtual void			CloseRead() override;

	virtual std::ostream*	OpenWrite() override;
	virtual void			CloseWrite() override;
};