#pragma once

/// To add a resource to the reflexion system :
/// 
/// Add your new resource type in the ResourceType enum
/// Call RESOURCE_REGISTER_TYPE(yourtype) in your header file
///
enum struct EResourceType
{
    Unknown = 0,

    SPVShader,
    GLSLShader,
    DynamicShader,
    Texture,
    Mesh,
    Image,
    Cubemap,
    CubemapImage,
    ProfileData,

    End
};

namespace ResourceUtils
{
    template<class T>
    EResourceType GetResourceType() { return EResourceType::Unknown; }

    template<EResourceType type>
    struct ResourceTypeTraits
    {
        using Type = void;
    };

}

#define RESOURCE_REGISTER_TYPE(TypeName) \
class TypeName;\
namespace ResourceUtils\
{\
    template<>\
    inline EResourceType GetResourceType<TypeName>() { return EResourceType::TypeName; }\
    \
    template<>\
    struct ResourceTypeTraits<EResourceType::TypeName>\
    {\
        using Type = TypeName;\
    };\
}\

