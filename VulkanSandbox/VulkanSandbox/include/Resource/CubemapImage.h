#pragma once

#include <vector>
#include <array>

#include "stb_image.h"

#include "glm/glm.hpp"

#include "Resource/Resource.h"
#include "Resource/Parsers/IParser.h"
#include "Resource/Parsers/Parser.h"

#include "Core/MeshData.h"

#include "Resource/Parsers/ISerializable.h"
#include "Resource/Parsers/JsonParser.h"

#include "Resource/Image.h"

struct CubemapImageDesc : public ISerializable
{
	CubemapImageDesc() {};
	~CubemapImageDesc() {};

	union 
	{
		std::array<std::string, 6> imageNames = {};

		struct {
			std::string X;
			std::string nX;
			std::string Y;
			std::string nY;
			std::string Z;
			std::string nZ;
		};
	};

	virtual void	Serialize(Document& outDocument) const;
	virtual bool	Unserialize(Document& document);
};

RESOURCE_REGISTER_TYPE(CubemapImage);

class CubemapImage : public Resource
{
	friend Parser<CubemapImage>;

public:
	CubemapImage() : Resource(EResourceType::CubemapImage){};
	~CubemapImage() {};

	virtual void Init(ResourceInitContext const& Context) override;
	virtual void Shutdown() override;

	Handle<Image>	GetImage(size_t imageId) { return _images[imageId]; };
	glm::ivec2		GetExtent() { return _extents; };

private:
	union
	{
		Handle<Image> _images[6] = {};

		struct {
			Handle<Image> _X;
			Handle<Image> _nX;
			Handle<Image> _Y;
			Handle<Image> _nY;
			Handle<Image> _Z;
			Handle<Image> _nZ;
		};
	};

	glm::ivec2 _extents = {};
};

template<>
class Parser<CubemapImage> : JsonParser
{
public:
	static bool	Unserialize(Handle<CubemapImage> resource, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Reload(Handle<CubemapImage> handle, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Serialize(Handle<CubemapImage> handle, ResourceStreamInfo const& info, std::ostream& stream);
};