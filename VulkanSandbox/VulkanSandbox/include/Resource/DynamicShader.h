#pragma once

#include "Resource/Shader.h"
#include "RHI/ShaderCompiler.h"

RESOURCE_REGISTER_TYPE(DynamicShader);

class ShaderCompilationContext;

class DynamicShader : public Resource
{
	friend Parser<DynamicShader>;
public:

    DynamicShader() : Resource(EResourceType::DynamicShader) {}

    void FromGLSLShader( std::shared_ptr<ResourceWrapper<GLSLShader>> wrapper, ResourceInitContext context);

	virtual void	    Init(ResourceInitContext const& Context) override;

	virtual void	    Shutdown() override;

	Handle<SPVShader>   RequestSPVShader(ShaderCompilationContext const& context, ShaderCompiler& compiler);

    std::shared_ptr<ResourceWrapper<GLSLShader>>  GetTargetShader() const { return _targetShader; }

    std::map<std::string, std::string> const& GetShaderAttributes() const { return _shaderAttributes; }
	void SetShaderAttributes(std::map<std::string, std::string>&& newAttributes) { _shaderAttributes = std::move(newAttributes); }
	// TODO allow this in a cleaner way
	int32_t GetPrependPatchMeshDepth() const { return _prependPatchMeshDepth; }
	void SetPrependPatchMeshDepth(int32_t depth) { _prependPatchMeshDepth = depth; }

   static std::string   GetShaderDefaultExtension( ShaderType type );
   static std::string   GetDefaultNameFromGLSL(std::shared_ptr<ResourceWrapper<GLSLShader>> shader);

private:
	ShaderType		                                _shaderType;
	std::shared_ptr<ResourceWrapper<GLSLShader>>    _targetShader;
	// TODO allow this in a cleaner way
	int32_t											_prependPatchMeshDepth = -1;
	std::map<std::string, std::string>              _shaderAttributes;

// Transient
private:
    ResourceMgr*                        _resourceMgr = nullptr;

    int                                 _generatedShaderCount = 0;
    IApplication*                       _application;
};

template<>
class Parser<DynamicShader> : JsonParser
{
public:
	static bool	Unserialize(Handle<DynamicShader> resource, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Reload(Handle<DynamicShader> handle, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Serialize(Handle<DynamicShader> handle, ResourceStreamInfo const& info, std::ostream& stream);
};
