#pragma once

#include <string>

#include "Core/IApplication.h"

#include "Debugging/Log.h"
#include "Resource/ResourceType.h"

DEF_LOG_CATEGORY(Resource);

struct ResourceInitContext
{
	IApplication* application;
};

class Resource
{
public:
    Resource(EResourceType type) : _type(type){}

	virtual void Init(ResourceInitContext const& Context) {}
	virtual void Shutdown() {}

	std::string const& GetName() const { return _name; }

	bool IsComplete() { return _isComplete; }

    template<class T>
	bool IsOfType() { return ResourceUtils::GetResourceType<T>() == _type; }
	EResourceType GetType() { return _type; }

protected:
	bool		    _isComplete = false;
	std::string     _name;

    EResourceType   _type = EResourceType::Unknown;
};