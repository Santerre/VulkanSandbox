#pragma once

#include <istream>

#include "Resource/ResourceMgr.h"

#include "Resource/Resource.h"
#include "Resource/Parsers/IParser.h"
#include "Resource/Parsers/Parser.h"
#include "Resource/Parsers/JSonParser.h"

#include "Resource/Parsers/ISerializable.h"

#include "Memory/Handle.h"

// Todo - Remove : Vulkan specific
#include "RHI/Vulkan/VKRHI_Shader.h"

RESOURCE_REGISTER_TYPE(GLSLShader);
RESOURCE_REGISTER_TYPE(SPVShader);

class GLSLShader : public Resource
{
	friend Parser<GLSLShader>;
public:
    GLSLShader() : Resource(EResourceType::GLSLShader) {}

	virtual void	Init(ResourceInitContext const& Context) override;

	virtual void	Shutdown() override;

    std::string const& GetShaderData() const { return _source; }
    void SetShaderData(std::string const& newData) { _source = newData; }

    static std::string     GetShaderDefaultExtension(ShaderType type);

    IApplication*   GetApplication() { return _application; }

private:
    std::string     _source;
    IApplication*   _application;
};

template<>
class Parser<GLSLShader> : IParser
{
public:
	static bool	Unserialize(Handle<GLSLShader> resource, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Reload(Handle<GLSLShader> handle, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Serialize(Handle<GLSLShader> handle, ResourceStreamInfo const& info, std::ostream& stream);
};

class SPVShader : public Resource
{
	friend Parser<SPVShader>;
public:
    SPVShader() : Resource(EResourceType::SPVShader) {}
    
    void FromData(std::string const& name, ShaderType type, std::vector<uint32_t>&& str);

	virtual void	Init(ResourceInitContext const& Context) override;

	virtual void	Shutdown() override;

	// Todo - Remove : Vulkan specific
	VKRHI_Shader*	GetRHIShader() const { return _rhiShader.get(); }

    ShaderType      GetShaderType() const { return _shaderType; }

   static std::string     GetShaderDefaultExtension(ShaderType type);
private:
	std::vector<uint32_t>	      _shaderData;

	ShaderType		              _shaderType;

	// Todo - Remove : Vulkan specific
	std::unique_ptr<VKRHI_Shader> _rhiShader = std::make_unique<VKRHI_Shader>();
};

template<>
class Parser<SPVShader> : IParser
{
public:
	static bool	Unserialize(Handle<SPVShader> resource, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Reload(Handle<SPVShader> handle, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Serialize(Handle<SPVShader> handle, ResourceStreamInfo const& info, std::ostream& stream);
};

struct DynamicShaderDesc : public ISerializable
{
	DynamicShaderDesc() {};
	~DynamicShaderDesc() {};

    std::string			                shaderPath;
	std::map<std::string, std::string>  shaderAttributes;

	virtual void	Serialize(Document& outDocument) const;
	virtual bool	Unserialize(Document& document);
};

struct ShaderParserUtils
{
	static ShaderType PathToShaderType(Path const& path);
};