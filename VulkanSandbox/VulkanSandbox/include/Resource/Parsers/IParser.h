#pragma once

#include <vector>
#include <istream>

#include "Debugging/Assert.h"

class IParser
{
public:
	template<typename T>
	static void WriteAllStreamData(std::ostream& outStream, std::vector<T> const& data)
	{
        outStream.write(reinterpret_cast<const char*>(data.data()), data.size() * sizeof(T));
	}

	template<typename T>
	static void LoadAllStreamData(std::istream& inStream, std::vector<T>& outDestination)
	{
		inStream.seekg(0, inStream.end);
		auto size = inStream.tellg();
		KU_ASSERT(size % sizeof(T) == 0);
		outDestination.resize(static_cast<unsigned int>(size) / sizeof(T));
		inStream.seekg(0);
		inStream.read(reinterpret_cast<char*>(outDestination.data()), size);
	}

	template<typename T>
	static void LoadStreamArrayAs(std::istream& inStream, T outDestination, size_t arraySize = 1)
	{
		inStream.read(reinterpret_cast<char*>(outDestination), arraySize * sizeof(std::remove_pointer<T>::type));
	}
    
	static std::istream::pos_type GetStreamSize(std::istream& inStream)
	{
		auto pos = inStream.tellg();
		inStream.seekg(0, inStream.end);
		auto size = inStream.tellg();
		inStream.seekg(pos);
        return size;
	}
};
