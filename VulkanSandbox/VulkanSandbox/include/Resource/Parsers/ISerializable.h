#pragma once

#include "rapidjson/document.h"

#include "Debugging/Assert.h"

#include <istream>
#include <string>
#include <map>

using Document = rapidjson::Document;

namespace _detail
{

template<class T, class ObjectType>
struct RapidjsonValueWrapper
{
	static T RetrieveValue(ObjectType& object)
	{
		KU_ASSERT(object.Is<T>());
		return object.Get<T>();
	}

	static T& ConvertToRapidjson(T& value)
	{
		return value;
	}
	
	static void SetValue(ObjectType const& object, T const& value)
	{
		object.Set<T>(ConvertToRapidjson(value));
	}
    
    static void AddValue(rapidjson::Document& doc, ObjectType& object, std::string const& name, T const& value)
    {
		object.AddMember(rapidjson::Value(name.c_str(), doc.GetAllocator()).Move(), _detail::RapidjsonValueWrapper<T, ObjectType>::ConvertToRapidjson(value), doc.GetAllocator());
    }
};

template<class ObjectType>
struct RapidjsonValueWrapper<std::string, ObjectType>
{
	static std::string RetrieveValue(ObjectType& object)
	{
		return std::string(object.GetString(), object.GetStringLength());
	}

	static auto ConvertToRapidjson(std::string const& value)
	{
		auto rjValue = rapidjson::StringRef(value.data(), value.size());
		return rjValue;
	}

	static void SetValue(ObjectType& object, std::string const& value)
	{
		object.SetString(ConvertToRapidjson(value));
	}
        
    static void AddValue(rapidjson::Document& doc, ObjectType& object, std::string const& name, std::string const& value)
    {
		object.AddMember(rapidjson::Value(name.c_str(), doc.GetAllocator()).Move(), ConvertToRapidjson(value), doc.GetAllocator());
    }
};

template<class ObjectType, class MappedType>
struct RapidjsonValueWrapper<std::map<std::string, MappedType>, ObjectType>
{
    using WrappedType = std::map<std::string, MappedType>;

	static WrappedType RetrieveValue(ObjectType& object)
	{
        WrappedType map;
        KU_ASSERT(object.IsObject());
        for ( auto&& itr = object.MemberBegin(); itr != object.MemberEnd(); itr++)
		    map[itr->name.GetString()] = RapidjsonValueWrapper<MappedType, ObjectType>::RetrieveValue(itr->value);

		return map;
	}

    /*
	static auto ConvertToRapidjson(WrappedType const& value)
	{
		auto rjValue = rapidjson::StringRef(value.data(), value.size());
		return rjValue;
	}

	static void SetValue(ObjectType& object, WrappedType const& value)
	{
		object.SetString(ConvertToRapidjson(value));
	}*/
        
    static void AddValue(rapidjson::Document& doc, ObjectType& object, std::string const& name, WrappedType const& value)
    {
        rapidjson::Value map(rapidjson::Type::kObjectType);

        for ( auto&& pair : value )
            RapidjsonValueWrapper<MappedType, rapidjson::Value>::AddValue(doc, map, pair.first, pair.second);

		object.AddMember(rapidjson::Value(name.c_str(), doc.GetAllocator()).Move(), map, doc.GetAllocator());
    }
};


}

class ISerializable
{
public:
	virtual void Serialize(Document& outDocument) const = 0;
	virtual bool Unserialize(Document& document) = 0;

	template<class T, class ObjectType>
	static T RetrieveValue(ObjectType& object)
	{
		return _detail::RapidjsonValueWrapper<T, ObjectType>::RetrieveValue(object);
	}

	template<class T, class ObjectType>
	static void SetValue(ObjectType& object, T const& value)
	{
		_detail::RapidjsonValueWrapper<T, ObjectType>::SetValue(object, value);
	}

	template<class T, class ObjectType>
	static void AddValue(rapidjson::Document& doc, ObjectType& object, std::string const& name, T const& value)
	{
        _detail::RapidjsonValueWrapper<T, ObjectType>::AddValue(doc, object, name, value);
	}
};