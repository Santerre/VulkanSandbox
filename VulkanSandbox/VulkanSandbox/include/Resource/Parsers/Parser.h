#pragma once

#include <istream>

#include "Memory/Handle.h"
#include "Resource/Parsers/IParser.h"

template<class T>
class Parser : public IParser
{
	static bool	Unserialize(Handle<T> resource, struct ResourceStreamInfo const& info, std::istream& stream) = 0;
	static bool	Reload(Handle<T> handle, struct ResourceStreamInfo const& info, std::istream& stream) = 0;
	static bool	Serialize(Handle<T> handle, ResourceStreamInfo const& info, std::ostream& stream) = 0;
};
