#pragma once

#include<istream>
#include<ostream>

#include "Resource/Parsers/IParser.h"

class TextParser : public IParser
{
public:
	static bool	UnserializeText(std::istream& stream, std::string& object);
	static bool	SerializeText(std::ostream& stream, std::string const& object);
};