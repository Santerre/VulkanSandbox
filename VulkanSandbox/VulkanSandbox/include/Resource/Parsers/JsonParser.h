#pragma once

#include<istream>
#include<ostream>

#include "Resource/Parsers/IParser.h"

class JsonParser : public IParser
{
public:
	static bool	UnserializeObject(std::istream& stream, class ISerializable* object);
	static bool	SerializeObject(std::ostream& stream, class ISerializable const* object);
};