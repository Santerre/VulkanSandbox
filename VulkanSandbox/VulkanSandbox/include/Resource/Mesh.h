#pragma once

#include <vector>

#include "Resource/Resource.h"
#include "Resource/Parsers/IParser.h"
#include "Resource/Parsers/Parser.h"

#include "Core/MeshData.h"

RESOURCE_REGISTER_TYPE(Mesh);

class Mesh : public Resource
{
	friend Parser<Mesh>;

public:
    Mesh() : Resource(EResourceType::Mesh) {}

	virtual void Init(ResourceInitContext const& Context) override;
	virtual void Shutdown() override;

	MeshData const& GetMeshData() const { return _data; }

private:
	MeshData _data;
};

template<>
class Parser<Mesh> : IParser
{
public:
	static bool	Unserialize(Handle<Mesh> resource, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Reload(Handle<Mesh> handle, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Serialize(Handle<Mesh> handle, ResourceStreamInfo const& info, std::ostream& stream);
};