#pragma once

enum struct ShaderType
{
    None,

    Vertex,
    TessellationControl,
    TessellationEvaluation,
    Geometry,
    Fragment,

    Compute,

    Task,
    Mesh
};
