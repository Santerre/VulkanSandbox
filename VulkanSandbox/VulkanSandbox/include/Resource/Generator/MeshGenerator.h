#pragma once

#include "glm/glm.hpp"

#include "Resource/Generator/IGenerator.h"
#include "Core/MeshData.h"

class MeshBuilder
{
public:
	virtual size_t GetVerticesCount() const = 0;
	virtual size_t GetIndicesCount() const = 0;
	virtual bool Build(Vertex* outVertices, MeshIndex* outIndices) = 0;
};

class TriangleBuilder : public MeshBuilder
{
public:
	virtual size_t GetVerticesCount() const override { return 3u; }
	virtual size_t GetIndicesCount() const override { return 3u; }
	virtual bool Build(Vertex* outVertices, MeshIndex* outIndices) override;
};

class RectangleBuilder : public MeshBuilder
{
public:
	virtual size_t GetVerticesCount() const override { return 4u; }
	virtual size_t GetIndicesCount() const override { return 6u; }
	virtual bool Build(Vertex* outVertices, MeshIndex* outIndices) override;
};

class CubeBuilder : public MeshBuilder
{
public:
	virtual size_t GetVerticesCount() const override { return 8u; }
	virtual size_t GetIndicesCount() const override { return 36u; }
	virtual bool Build(Vertex* outVertices, MeshIndex* outIndices) override;
};

class GridBuilder : public MeshBuilder
{
public:
	struct Desc
	{
		glm::vec2	extents;
		bool		isForTesselation = false;
	};

public:
	void FromDesc(Desc const& desc);

	virtual size_t GetVerticesCount() const override;
	virtual size_t GetIndicesCount() const override;
	virtual bool Build(Vertex* outVertices, MeshIndex* outIndices) override;

private:
	glm::uvec2	_extents;
	bool		_isForTesselation;
};

class PatchBuilder : public MeshBuilder
{
public:
	struct InitDesc
	{
		size_t tessellationDepth;
	};

	void FromDesc(InitDesc const& desc);

	virtual size_t GetVerticesCount() const override;
	virtual size_t GetIndicesCount() const override;
	virtual bool Build(Vertex* outVertices, MeshIndex* outIndices) override;

private:
	size_t GetGridSideCount() const;

private:
	size_t _tessellationDepth;
};

// Can generate :
//    - "Triangle.raw"
//    - "Rectangle.raw"
//    - "Cube.raw"
//    - "16x16Grid.raw"
//    - "16x16TesGrid.raw"
class MeshGenerator : public IGenerator
{
public:
	virtual bool GenerateSlot(Path const& path, MemorySlot& slot) override;
};