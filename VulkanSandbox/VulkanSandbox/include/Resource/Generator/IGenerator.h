#pragma once

#include "Memory/MemorySlot.h"
#include "Core/Path.h"

class IGenerator
{
	friend MemorySlot;

public:
	virtual bool GenerateSlot(Path const& path, MemorySlot& slot) = 0;
};
