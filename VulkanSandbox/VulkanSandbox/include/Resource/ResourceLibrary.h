#pragma once

#include <map>
#include <memory>

#include "Memory/Handle.h"
#include "Core/Path.h"

class Resource;

struct IResourceWrapper
{
public:
	virtual void Release() = 0;
	virtual bool IsResourceLoaded() = 0;

	virtual Handle<Resource> GetResource() = 0;

	virtual bool ContainsResource(IHandle* handle) = 0;

public:
	virtual Path GetResourcePath() = 0;

};

class ResourceLibrary
{
public:
    void Init(){}
    void Shutdown(){}

    void AddResource(std::string const& path, std::shared_ptr<IResourceWrapper> res);

    std::shared_ptr<IResourceWrapper> GetResource(std::string const& path) const;
    template<class T>
    std::shared_ptr<T> GetResourceCast(std::string const& path) const { return std::static_pointer_cast<T>(GetResource(path)); }

    bool HasResource(std::string const& path) const;

    void UnregisterResource(std::string const& path);
    void Clear();

private:
	std::map<std::string, std::shared_ptr<IResourceWrapper>> _resources;

	using ResourceMap = decltype(_resources);
};
