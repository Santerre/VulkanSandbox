#pragma once

#include <streambuf>

#include "Resource/Loader.h"

class MemoryBuffer : public std::basic_streambuf<char>
{
public:
	MemoryBuffer() = default;
	MemoryBuffer(char_type* start, std::streamsize size);

	void Open(char_type* start, std::streamsize size);
	void Close();

protected:
	virtual pos_type seekoff(
		off_type off, 
		std::ios_base::seekdir way,
		std::ios_base::openmode which = std::ios_base::in | std::ios_base::out) override;

	virtual pos_type seekpos(
		pos_type off, 
		std::ios_base::openmode which = std::ios_base::in | std::ios_base::out) override;

	virtual std::streamsize showmanyc() override;

	virtual std::streamsize xsgetn(char_type* s, std::streamsize n) override;
	virtual int_type underflow() override;
};

class MemoryLoader : public ILoader
{
public:
	MemoryLoader(class HierarchicalMemory& memory, Path const& path);

	virtual std::istream*	OpenRead() override;
	virtual void			CloseRead() override;

	virtual std::ostream*	OpenWrite() override;
	virtual void			CloseWrite() override;

private:
	Path _path;

	class HierarchicalMemory& _memory;

	std::unique_ptr<MemoryBuffer> _memoryBuffer;
	std::unique_ptr<std::istream> _inFile;
};