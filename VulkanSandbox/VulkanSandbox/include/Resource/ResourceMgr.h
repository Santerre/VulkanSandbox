#pragma once

#include <string>
#include <memory>

#include "Core/IApplication.h"

#include "Memory/Handle.h"

#include "Resource/Loader.h"
#include "Resource/Parsers/Parser.h"
#include "Resource/ResourceStreamInfo.h"
#include "Resource/ResourceLibrary.h"
#include "Resource/Loaders/MemoryLoader.h"

#include "Resource/Resource.h"

#include "Debugging/Log.h"

#include "Memory/Allocator.h"

#include "Utils/Delegate.h"

DEF_LOG_CATEGORY(ResourceMgr);

template<class T>
struct ResourceWrapper : public IResourceWrapper
{
public:
	virtual void Release() override
	{
        if (loader)
        {
	        loader->Shutdown();
	        loader = nullptr;
        }

		resource->Shutdown();
		ReleaseRes(resource);
	}

	virtual Handle<Resource> GetResource() override { return resource; }
	Handle<T> GetTrueResource() { return resource; }

	virtual bool IsResourceLoaded() override { return !!resource; }

	virtual bool ContainsResource(IHandle* handle) override
	{
		return resource == handle;
	}

public:
	virtual Path GetResourcePath() override { return path; }

public:
	Path		path;
	Handle<T>	resource;
	Parser<T>	parser;
	std::shared_ptr<ILoader>  loader;
};

class ResourceMgr
{
public:
	void Init(IApplication* application, ResourceLibrary* library);

	void ReleaseAllResources();
	void Shutdown();

    template<class T>
	Handle<T> CreateResource();

	template<class T>
	Handle<T> Get(std::string const& pathStr);

    template<class T>
	std::shared_ptr<ResourceWrapper<T>> GetWrapper(std::string const& pathStr);
    
    template<class T>
	std::shared_ptr<ResourceWrapper<T>> CreateResourceWrapper();

	template<class T>
	bool SaveResource(Handle<T> resource, std::string const& pathStr = "");

    template<class T>
	bool RegisterResource(std::string const& pathStr, std::shared_ptr<ResourceWrapper<T>> resource);

	std::vector<std::shared_ptr<IResourceWrapper>> GetAllResources() const;

private:
	template<class T>
	Handle<T> LoadResource(std::string const& pathStr, ResourceWrapper<T>& inoutWrapper);

	template<class T>
	std::shared_ptr<ResourceWrapper<T>> AddNewResource(std::string const& pathStr);

	template<class T>
	std::shared_ptr<ResourceWrapper<T>> GetResourceWrapper(Handle<T> resource) const;

	template<class T>
	bool GetResourcePath(Handle<T> resource, std::string& outRes) const;

	std::shared_ptr<ILoader> GetLoaderAtPath(std::string const& pathStr) const;

public:
    Delegate<void(std::shared_ptr<IResourceWrapper>)> OnResourceRegistered;

private:
	IApplication*                                  _application;
    ResourceLibrary*                               _library;
    std::vector<std::shared_ptr<IResourceWrapper>> _resources;
};

template<class T>
Handle<T> ResourceMgr::Get(std::string const& pathStr)
{
    auto&& wrapper = GetWrapper<T>(pathStr);
    if (wrapper)
	    return wrapper->GetResource();
    else
        return nullptr;
}

template<class T>
std::shared_ptr<ResourceWrapper<T>> ResourceMgr::GetWrapper( std::string const& pathStr )
{
    std::shared_ptr<ResourceWrapper<T>> result = std::static_pointer_cast<ResourceWrapper<T>>(_library->GetResource(pathStr));
	if (!result)
		return AddNewResource<T>(pathStr);

	ResourceWrapper<T>& wrapper = *result;
	if (!wrapper.IsResourceLoaded())
		LoadResource<T>(pathStr, wrapper);

	return result;
}

template<class T>
Handle<T> ResourceMgr::CreateResource()
{
    return CreateResourceWrapper<T>()->resource;
}

template<class T>
std::shared_ptr<ResourceWrapper<T>> ResourceMgr::CreateResourceWrapper()
{
	std::shared_ptr<ResourceWrapper<T>> wrapperPtr(std::make_shared<ResourceWrapper<T>>());
	wrapperPtr->resource = AllocRes(T);

    _resources.push_back(wrapperPtr);

    return wrapperPtr;
}

template<class T>
bool ResourceMgr::RegisterResource(std::string const& pathStr, std::shared_ptr<ResourceWrapper<T>> resource)
{
	if (_library->HasResource(pathStr))
	{
		WLOG(LogResource, "Tried to register a resource at path that already exists : %s", pathStr);
        return false;
	}

    resource->path.FromPathStr(pathStr);
    resource->loader = GetLoaderAtPath(pathStr);
    KU_ASSERT(resource->loader);

    _library->AddResource(pathStr, resource);

    OnResourceRegistered.Invoke(resource);

    return true;
}

template<class T>
Handle<T> ResourceMgr::LoadResource(std::string const& pathStr, ResourceWrapper<T>& inoutWrapper)
{
    KU_ASSERT(inoutWrapper.loader);
	std::istream* loadingStream = inoutWrapper.loader->OpenRead();
	if (!loadingStream)
	{
		WLOG(LogResourceMgr, "The following asset couldn't load data at path : %s", pathStr.c_str());
		return {};
	}

	Path path = Path(pathStr);

	ResourceStreamInfo info;
	info.path = &path;
	info.resourceMgr = this;

	if (!inoutWrapper.parser.Unserialize(inoutWrapper.resource, info, *loadingStream))
	{
		WLOG(LogResourceMgr, "The following asset couldn't be unserialized correctly : %s", pathStr.c_str());
		inoutWrapper.loader->CloseRead();
		return {};
	}
	inoutWrapper.loader->CloseRead();

	ResourceInitContext context;
	context.application = _application;
	inoutWrapper.resource->Init(context);
	if (!inoutWrapper.resource->IsComplete())
	{
		WLOG(LogResourceMgr, "The following asset couldn't be loaded correctly : %s", pathStr.c_str());
		return {};
	}

	return inoutWrapper.resource;
}

template<class T>
std::shared_ptr<ResourceWrapper<T>> ResourceMgr::AddNewResource(std::string const& pathStr)
{
    //TODO avoid test loader creation
    std::shared_ptr<ILoader> loader = GetLoaderAtPath(pathStr);
    if (!loader->OpenRead())
        return nullptr;
    loader->CloseRead();

	std::shared_ptr<ResourceWrapper<T>> wrapper = CreateResourceWrapper<T>();
    KU_ASSERT(wrapper);

    if (!RegisterResource( pathStr, wrapper ))
    {
		WLOG(LogResource, "Couldn't register resource : %s", pathStr);
        return nullptr;
    }

    if (!LoadResource<T>(pathStr, *wrapper.get()))
        return nullptr;
	return wrapper;
}

template<class T>
bool ResourceMgr::SaveResource(Handle<T> resource, std::string const& inPathStr)
{
	std::string pathStr = inPathStr;
    
	if (pathStr == "")
	{
		KU_VERIFY(GetResourcePath(resource, pathStr));
	}

	std::shared_ptr<ResourceWrapper<T>> wrapper = _library->GetResourceCast<ResourceWrapper<T>>(pathStr); 

	if (!wrapper)
	{
		wrapper = GetResourceWrapper(resource);
		if (!wrapper)
		{
			WLOG(LogResourceMgr, "Couldn't emplace the resource : %s", pathStr.c_str());
			return false;
		}

		if (!RegisterResource(inPathStr, wrapper))
		{
			WLOG(LogResourceMgr, "Couldn't register the anonymous resource for saving to : %s", pathStr.c_str());
			return false;
		}
	}

	std::ostream* writeStream = wrapper->loader->OpenWrite();
	if (!writeStream)
	{
		WLOG(LogResourceMgr, "Couldn't prepare stream for writing for resource : %s", pathStr.c_str());
		return false;
	}

	Path path = Path(pathStr);
	ResourceStreamInfo info;
	info.path = &path;
	info.resourceMgr = this;
	if (!wrapper->parser.Serialize(resource, info, *writeStream))
	{
		WLOG(LogResourceMgr, "Couldn't parse the folowing asset : %s", pathStr.c_str());
		return false;
	}

	wrapper->loader->CloseWrite();
	return true;
}

template<class T>
std::shared_ptr<ResourceWrapper<T>> ResourceMgr::GetResourceWrapper(Handle<T> resource) const
{
	for (auto&& resWrapper : _resources)
		if (resWrapper->ContainsResource(&resource))
		{
			return std::static_pointer_cast<ResourceWrapper<T>>(resWrapper);
		}
	return {};
}

template<class T>
bool ResourceMgr::GetResourcePath(Handle<T> resource, std::string& outRes) const
{
	auto&& resWrapper = GetResourceWrapper(resource);
	if (!resWrapper)
		return false;

	outRes = resWrapper->GetResourcePath().GetString();
	return true;
}