#pragma once

#include <vector>

#include "stb_image.h"

#include "glm/glm.hpp"

#include "Resource/Resource.h"
#include "Resource/Parsers/IParser.h"
#include "Resource/Parsers/Parser.h"

#include "Core/MeshData.h"

RESOURCE_REGISTER_TYPE(Image);

class Image : public Resource
{
	friend Parser<Image>;

public:
    Image() : Resource(EResourceType::Image) {}

	virtual void Init(ResourceInitContext const& Context) override;
	virtual void Shutdown() override;

	size_t					GetByteSize() const;
	glm::ivec2 const&		GetExtent() const { return _extent; }
	int						GetTexChannels() const { return _texChannels; }
	unsigned char const*	GetBuffer() const { return _buffer; }

private:
	glm::ivec2				_extent;
	int						_texChannels;
	unsigned char const*	_buffer = nullptr;
};

template<>
class Parser<Image> : IParser
{
public:
	static bool	Unserialize(Handle<Image> resource, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Reload(Handle<Image> handle, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Serialize(Handle<Image> handle, ResourceStreamInfo const& info, std::ostream& stream);
};