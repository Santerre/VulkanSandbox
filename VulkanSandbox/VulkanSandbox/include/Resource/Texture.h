#pragma once

#include "Resource/Resource.h"

#include "Resource/Image.h"
#include "Resource/Parsers/ISerializable.h"
#include "Resource/Parsers/JsonParser.h"

RESOURCE_REGISTER_TYPE(Texture);

struct TextureDesc : public ISerializable
{
	std::string albedo;
	std::string heightMap;
	std::string normalMap;

	virtual void	Serialize(Document& outDocument) const;
	virtual bool	Unserialize(Document& document);
};

class Texture : public Resource
{
	friend class Parser<Texture>;

public:
    Texture() : Resource(EResourceType::Texture) {}

	virtual void Init(ResourceInitContext const& Context) override;
	virtual void Shutdown() override;

	Handle<Image> GetAlbedo() const { return _albedo; }
	Handle<Image> GetHeightMap() const { return _heightMap; }
	Handle<Image> GetNormalMap() const { return _normalMap; }

	void SetAlbedo(Handle<Image> albedo) { _albedo = albedo; }
	void SetHeightMap(Handle<Image> heightMap) { _heightMap = heightMap; }
	void SetNormalMap(Handle<Image> normalMap) { _normalMap = normalMap; }

private:
	Handle<Image> _albedo;
	Handle<Image> _heightMap;
	Handle<Image> _normalMap;
};

template<>
class Parser<Texture> : JsonParser
{
public:
	static bool	Unserialize(Handle<Texture> resource, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Reload(Handle<Texture> handle, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Serialize(Handle<Texture> handle, ResourceStreamInfo const& info, std::ostream& stream);

private:
	static Handle<Image> LoadImage(ResourceStreamInfo const& info, std::string const& name);
};