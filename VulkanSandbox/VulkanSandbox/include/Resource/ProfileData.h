#pragma once

#include <vector>
#include <unordered_map>

#include "Resource/Resource.h"
#include "Resource/Parsers/IParser.h"
#include "Resource/Parsers/Parser.h"

#include "Debugging/Profiler.h"

RESOURCE_REGISTER_TYPE(ProfileData);

class ProfileData : public Resource
{
	friend Parser<ProfileData>;

public:

    ProfileData() : Resource(EResourceType::ProfileData) {}

	virtual void Init(ResourceInitContext const& Context) override;
	virtual void Shutdown() override;

public:
    void AddDump(Profiler::ProfilerFrameDump const& dump);
	std::vector<Profiler::ProfilerFrameDump> const& GetData() { return _dumps; }

private:
    std::vector<Profiler::ProfilerFrameDump> _dumps;
};

template<>
class Parser<ProfileData> : IParser
{
public:
	static bool	Unserialize(Handle<ProfileData> resource, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Reload(Handle<ProfileData> handle, struct ResourceStreamInfo const& info, std::istream& stream);
	static bool	Serialize(Handle<ProfileData> handle, ResourceStreamInfo const& info, std::ostream& stream);
};