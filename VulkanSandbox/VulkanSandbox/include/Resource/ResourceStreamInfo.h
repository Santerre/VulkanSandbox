#pragma once

#include "Core/Path.h"

struct ResourceStreamInfo
{
	class ResourceMgr* resourceMgr = nullptr;
	Path const* path = nullptr;
};