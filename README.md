#  Vulkan Sandbox

## Introduction
A Vulkan sandbox made to test the new rendering technologies.

Author : Benjamin Santerre

## Compilation
Open and compile the sln VulkanSandbox/VulkanSandbox.sln with VisualStudio 2019

_A CMake version is planned._

## Controls
- wasd : Camera movement
- mouse wheel : Change camera speed
- qe : Camera up-down
- F1 : Go back to original position
- F2 : Open editor
- F5 : Reload rendering

## Used libraries
General :
- std
	
Maths:
- glm

Gui :
- imgui

Rendering :
- vulkan
- glfw3
- LunarG's VulkanSDK
- libshaderc
	
Parsing :
- stbimage
- tinyobj
- rapidjson


_Many thanks to the authors of these great libs._